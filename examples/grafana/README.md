This directory contains some dashboard, that can be used
with grafana (https://grafana.com). The data can be collected
from the CA process using prometheus (https://prometheus.io).

How to use these dashboards:
- Create a job in prometheus to collect data from CA/OCSP processes
- Import Dashboard JSON file into grafana
