#!/usr/bin/env node

import https from 'node:https';
import http from 'node:http';
import tls from 'node:tls';
import os from 'node:os';
import url from 'node:url';
import path from 'node:path';
import { inspect } from 'node:util';
import process from 'node:process';
import fs from 'node:fs';
import EventEmitter from 'node:events';

import express from 'express';
import nodemailer from 'nodemailer';
//import { MongoClient } from 'mongodb';
import Agenda from 'agenda';
import jsrsasign from 'jsrsasign';
import winston from 'winston';
import morgan from 'morgan';
import * as rfs from 'rotating-file-stream';
import CronJob from 'cron';

import packageJson from './package.json' assert { type: 'json' };
import config from './etc/config.js';
global.config = config;
global.logger = config.logger;
import acme from '../acme-client/acme.mjs';
import AcmeRouter from './routers/acme.js';
import Scep from './routers/scep.js';
import DnsProxy from './lib/dns.mjs';
import HttpsContext from './lib/https.mjs';

if (process.env.NODE_ENV !== 'production' && config.logger) {
    const level = 'info';
    config.logger.add(new winston.transports.Console({
        format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple(),
        ),
        level
    }));
    logger.log(level, `Console logging enabled`);
}
logger.warn(`starting ${packageJson.name} (${packageJson.version}) with node (${process.version}) on ${process.platform}-${process.arch} as user ${process.geteuid()}:${process.getegid()}`);

const emitter = new EventEmitter();

if (config.database) {
    let dbClass = await import('./lib/sqlite.mjs');
    logger.debug(`using database ${dbClass.default.constructor.name} ...`);
    var database = new dbClass.default(emitter, config.database);
}

if (config.ldap) {
    let ldap = await import('./lib/ldap.mjs');
    logger.debug(`starting ldap ...`);
    let ldapClient = new ldap.Client(emitter, config.ldap);
}

if (config.mqtt) {
    let mqtt = await import('./lib/mqtt.cjs');
    logger.debug(`starting mqtt ...`);
    let mqttClient = new mqtt.Client(emitter, config.mqtt);
}

const transporter = nodemailer.createTransport(config.mail.transport);
function sendMail(to, message) {
    logger.info(`sendMail to ${to}, message: ${message}`);
    transporter.sendMail({
        from: config.mail.from,
        to: to,
        subject: 'Certificate expiration warning',
        text: message
    })
    .catch((error) => {
        logger.error(`sendMail failed (${error})`);
    });
}

function sendCertificateExpirationRemainders() {
    logger.info(`Sending certificate expiration remainders`);
    let sql = `SELECT
                   certificates.serialNumber,
                   certificates.validTo,
                   contacts.email
               FROM
                   accounts
                   INNER JOIN certificates ON (certificates.account = accounts.id)
                   INNER JOIN contacts ON (contacts.account = accounts.id)
               WHERE
                   certificates.validTo < datetime('now', '-2 weeks')
               AND
                   certificates.validTo < datetime('now', '-15 days')
               ;`;
    database.each(sql, (error, row) => {
       if (error) throw error;
       logger.info(`found expiring certificate with serial number ${row.serialNumber}`);
       sendMail(row.email, `Certificate with SN ${row.serialNumber} is going to expire on ${row.validTo}`);
    });
}

if (config.notifications.enabled) {
    if (/* MongoClient */ false) {
        let agenda = new Agenda();
        agenda.define("send certificate expiration", async (job) => {
            logger.info(`Runnning send certificate expiration job: ${job}`);
            sendCertificateExpirationRemainders();
        });
        (async function () {
            logger.debug('starting agenda ...');
            await agenda.start();
            logger.info('agenda started');
            logger.debug('scheduling jobs ...');
            await agenda.every("24 hours", "send certificate expiration");
            logger.info('job scheduled');
        })();

        async function agendaStop() {
            logger.debug(`stopping agenda ...`);
            await agenda.stop();
            logger.info(`stopped agenda`);
        }
        process.on('SIGTERM', agendaStop);
        process.on('SIGINT', agendaStop);
    }
    else {
        logger.debug('starting cron ...');
        var job = new CronJob('0 8 * * * *', sendCertificateExpirationRemainders);
        job.start();
        logger.info('cron started');
    }
}

async function certify(csrPem) {
    logger.silly(`csrPem: ${csrPem}`);
    let csr = jsrsasign.asn1.csr.CSRUtil.getParam(csrPem);
    logger.verbose(`csr: ${inspect(csr)}`);
    let identifiers = [];
    let subjectAltName = csr.extreq.find(item => item.extname == 'subjectAltName');
    for (let san of subjectAltName.array) {
        let ancestorDomain = undefined;
        for (let identifier of config['pre-authorizations']) {
            if (identifier.subdomainAuthAllowed === true) {
                if (san.dns) {
                    if (san.dns.endsWith(`.${identifier.value}`)) {
                        ancestorDomain = identifier.value;
                        break;
                    }
                }
                else if (san.rfc822) {
                    let email = new url.URL(`mailto:${san.rfc822}`);
                    if (email.domain == identifier.value) {
                        ancestorDomain = identifier.value;
                        break;
                    }
                }
            }
        }
        if (san.dns)
            identifiers.push({type: 'dns', value: san.dns, ancestorDomain});
        else if (san.rfc822)
            identifiers.push({type: 'email', value: san.rfc822, ancestorDomain});
    }
    return await new Promise((resolve, reject) => {
        logger.debug(`identifiers: ${inspect(identifiers)}`);
        account.getCertificate(identifiers, undefined, undefined, csrPem, undefined, undefined, async (error, chain) => {
            if (error) {
                logger.error(`failed to receive certificate (${inspect(error)})`);
                reject(error);
            }
            else {
                chain.map((x509) => emitter.emit('certificate', undefined, x509));
                resolve([chain[0].toString(), chain[1].toString()]);
            }
        });
    });
}

function acquireCertificate(context, identifiers) {
    logger.info(`acquiring certificate for ${identifiers.map(x => x.value)} ...`);

    let csr = context.getCertificationRequest(identifiers);
    logger.silly(`csr: ${csr}`);

    account.getCertificate(identifiers, undefined, undefined, csr, undefined, undefined, async (error, chain) => {
        if (error) {
            logger.error(`failed to get certificate (${error})`);
        }
        else {
            logger.info(`got certificate for ${chain[0].subject}`);
            logger.silly(`cert: ${chain[0].toString()}`);

            context.storeCertificates(chain);
            if (config.interfaces?.scep) {
                logger.debug(`loading SCEP interface ...`);
                let keyPem = jsrsasign.KEYUTIL.getPEM(key.prvKeyObj, 'PKCS1PRV');
                let scep = new Scep.Scep(keyPem, [chain[0].toString(), chain[1].toString()], certify);
                app.use(config.interfaces.scep, scep.router);
                logger.info(`bound protocol SCEP interface beneath ${config.interfaces.scep}`);
            }

            if (config.https?.enabled) {
                let server = https.createServer(context, app);
                server.on(`secureConnection`, (tlsSocket) => {
                    logger.silly(`new secure connection ${inspect(tlsSocket)}`);
                });
                server.on('tlsClientError', (exception, tlsSocket) => {
                    logger.error(`exception: ${inspect(exception)}`);
                });
                server.listen(config.https.port, () => {
                    logger.info(`listening on https://${os.hostname()}:${config.https.port}`);
                });
            }
        }
    });
}

async function setupAcmeClient() {
    let responders = [];
    if (config.dns.enabled) {
        const proxy = new DnsProxy(config.dns);
        //proxy.add({name: '_acme-challenge2.rz-bsd.my.corp', type: 16, class: 1, ttl: 120, data: ['xxx']});
        responders.push(new acme.DnsProxyChallengeResponder(proxy));
    }
    responders.push(
        new acme.WebrootChallengeResponder(config.webroot),
        /* Solving challenges manually should be last */
        new acme.ManualChallengeResponder()
    );
    return acme.Account.create(`${config.target}/acme/Sub/directory`, responders, undefined, { logger });
}

let httpsContext = new HttpsContext(config.https.context ?? {}, config.https.key);

/* @todo: ACME should use PKCS#11 if available, too */
global.account = await setupAcmeClient();
process.on('SIGINT', (code) => {
    if (global.account) {
        let account = global.account;
        if (httpsContext?.cert) {
            logger.debug('revoking https certificate ...');
            let cert = httpsContext.cert;
            httpsContext.cert = undefined;
            account.revokeCertificate(cert);
            logger.info('https certificate revoked');
        }

        logger.debug('deactivating ACME account ...');
        global.account = undefined;
        account.deactivate(() => {
            logger.info('ACME account deactivated');
            process.exit(0);
        });
    }
});

let proxyIdentifier = {type: 'dns', value: os.hostname()};
if (config['pre-authorizations'] && config['pre-authorizations'].length > 0) {
    logger.debug('acquiring pre-authorizations ...');
    for (let identifier of config['pre-authorizations']) {
        account.preAuthorize(identifier, (error, challenges) => {
            if (error) {
                logger.error(`got error during pre-authorization of ${identifier.type}:${identifier.value}: ${inspect(error)}`);
                throw error;
            }
            else {
                logger.info(`acquired pre-authorization for ${identifier.type}:${identifier.value}`);
                if (proxyIdentifier.value.endsWith(`.${identifier.value}`)) {
                    proxyIdentifier.ancestorDomain = identifier.value;
                    acquireCertificate(httpsContext, [proxyIdentifier]);
                }
            }
        });
    }
}
else {
    acquireCertificate(httpsContext, [proxyIdentifier]);
}

let app = express();
app.disable('x-powered-by');
let accessLogStream = rfs.createStream(path.basename(config.access_log), {
    interval: '1d', // rotate daily
    path: path.dirname(config.access_log)
});
app.use(morgan('combined', { stream: accessLogStream }));
app.use(express.json({type: ['application/json', 'application/problem+json', 'application/jose+json']}));
app.use(express.raw({type: ['*/*']}));

if (config.interfaces?.acme) {
    logger.debug(`loading ACMEv2 interface ...`);
    app.use(config.interfaces.acme, new AcmeRouter.AcmeRouter(emitter, config).router);
    logger.info(`bound protocol ACMEv2 interface beneath ${config.interfaces.acme}`);
}

app.get('/', (request, response) => {
    response.redirect('/index.html');
});
app.all('/index.html', (request, response, next) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Methods', 'GET,POST,HEAD,OPTIONS');
    next();
});

if (config.htdocs) {
    fs.accessSync(config.htdocs, fs.constants.F_OK);
    app.use(express.static(config.htdocs));
}

if (config.http?.enabled) {
    http.createServer(app).listen(config.http.port, () => {
        logger.info(`listening on http://localhost:${config.http.port}`);
    });
}

