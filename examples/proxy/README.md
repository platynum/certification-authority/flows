# ACMEv2 proxy

The ACMEv2 protocol can be proxied using this software. The proxy can take
over responsibility for a number of tasks, i.e.:

- translate incoming protocols like SCEP or EST into ACMEv2, or
- evaluate the external account binding (EAB) when creating new accounts, or
- publish certificates into an LDAP directory or external database, or
- notify the contacts provided by the user via email upon certificate
  generation/close expiry, or
- tigthen security with additional checks, or
- ...

This project serves as an example how this can be accomplished, YMMV. It
has been tested with https://gitlab.com/platynum/certification-authority/

## Pre-requisites

- A working ACME certification authority with matching `HOSTNAME` or
  `ACME_URL_PROTECTION=lax` and `ACME_PRE_AUTHORIZATION_ENABLED=true`
- Install pkcs11 engine for OpenSSL
- Setup configuration (see `etc/config.js` and `etc/engine.conf`)

## Installation

Install the required packages for `acme-proxy`:

```
$ npm install
```

Install the required packages for `acme-client`:

```
$ (cd ../acme-client && npm install)
```

### FreeBSD

Install packages

```
# pkg install libp11
```

### OpenBSD

OpenBSD installs OpenSSL along with node:

```
# pkg_add node
# ln -s /usr/bin/c++ /usr/bin/g++
```

Build PKCS#11 engine for OpenSSL:

```
$ doas pkg_add git automake autoconf libtool
$ git clone https://github.com/OpenSC/libp11.git
$ cd libp11
$ AUTOCONF_VERSION=2.72 AUTOMAKE_VERSION=1.16 ./bootstrap
$ CFLAGS=-I/usr/local/include/eopenssl32 \
  LDFLAGS=-L/usr/local/lib/eopenssl32 \
  ./configure --with-enginesdir=/usr/local/lib/eopenssl32/engines-22.0
$ make
```

Install PKCS#11 engine for OpenSSL:

```
$ doas cp ./src/.libs/pkcs11.so /usr/local/lib/eopenssl31/engine-22.0
```

### SoftHSM2

Install packages for SoftHSM2:

#### FreeBSD

```
pkg install softhsm2
```

#### OpenBSD

```
pkg_add softhsm2
```

./home/user/.config/softhsm2/softhsm2.conf
[source]
====
directories.tokendir = /home/user/.softhsm2
====

#### Token initialization

Initialize SoftHSM with the following interactive command:

```
$ softhsm2-util --init-token --slot 0 --label "SoftHSM Slot Label"
```

## Startup

Start-up `acme-proxy`:

```
$ npm start
```

## External account binding (EAB)

The proxy can check external account bindings, to create a new external account
binding, use the following command:

```
$ sqlite3 data/proxy.db "INSERT INTO accounts (kid, key) VALUES ('kid-new', '$(openssl rand -base64 32)') RETURNING *;"
```

