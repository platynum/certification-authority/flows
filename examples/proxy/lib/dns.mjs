import dgram from 'node:dgram';
import { inspect } from 'node:util';
import { Buffer } from 'node:buffer';

import packet from 'native-dns-packet';

class DnsProxy {

    constructor(config) {
        this.entries = [];

        const server = dgram.createSocket(config.listener.options);
        server.on('error', (error) => {
            logger.error(`DNS server error (${error})`);
        });
        server.on('message', (message, cinfo) => {

            const parsed = packet.parse(message);
            logger.verbose(`query from: ${inspect(cinfo)}`);
            logger.silly(`query: ${inspect(parsed)}`);

            let records = this.entries.filter(item => item.name === parsed.question[0].name
                                                   && item.type === parsed.question[0].type);
            if (records.length > 0) {
                parsed.answer = records;
                parsed.header.qr = 1;
                parsed.header.aa = 1;
                parsed.header.ra = 1;
                let answer = Buffer.alloc(4096);
                let length = packet.write(answer, parsed);
                server.send(answer.slice(0, length), cinfo.port, cinfo.address, (error) => {
                    if (error)
                        logger.error(`sending reply failed (${error})`);
                    else
                        logger.debug(`reply about ${parsed.question[0].name} sent to ${cinfo.family}://${cinfo.address}:${cinfo.port}`);
                });
            }
            else {
                let sprotocol = config?.server?.protocol ?? 'udp4';
                let stimeout = config?.server?.timeout ?? 5000;
                let client = dgram.createSocket(sprotocol);

                client.on('message', (message /*, sinfo */) => {
                    const parsed = packet.parse(message);
                    logger.silly(`response: ${inspect(parsed)}`);
                    server.send(message, cinfo.port, cinfo.address, (error) => {
                        if (error)
                            logger.error(`replying to client failed (${error})`);
                        else {
                            logger.debug(`reply about ${parsed.question[0].name} sent to ${cinfo.family}://${cinfo.address}:${cinfo.port}`);
                            
                            setTimeout(() => { client.close(); }, stimeout);
                        }
                    });
                });
                let sport = config?.server?.port ?? 53;
                let saddress = config?.server?.address ?? '127.0.0.1';
                client.send(message, sport, saddress, (error) => {
                    if (error)
                        logger.error(`sending query failed (${error})`);
                    else
                        logger.debug(`message forwarded to ${sprotocol}://${saddress}:${sport}`);
                });
            }
        });
        server.on('listening', () => {
            const address = server.address();
            let desc = `${config?.server?.protocol ?? 'udp4'}://${address.address}:${address.port}`;
            logger.info(`DNS proxy listening on ${desc}`);
        });
        server.bind(config.listener.port);
    }
    push(record) {
        this.entries.push(record);
    }
    remove(record) {
        this.entries = this.entries.filter(item => item.name !== record.name
                                                && item.type !== record.type)
    }
}

export default DnsProxy;
