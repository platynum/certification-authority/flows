import url from 'node:url';
import { inspect } from 'node:util';
import { Buffer } from 'node:buffer';
import fs from 'node:fs';
import process from 'node:process';

import jsrsasign from 'jsrsasign';
import pkcs11js from 'pkcs11js';
import readline from 'readline-sync';

export default class HttpsContext {
    constructor(config, keySpec) {
        logger.info(`Setting up https context ...`);
        Object.assign(this, config);

        this.keySpec = keySpec;
        // Node.JS url needs a `hostname`, fake one for pkcs11
        if (keySpec?.reference?.startsWith('pkcs11:'))
            this.keyRef = url.parse(keySpec.reference.replace(/^pkcs11:/, 'pkcs11://workaround;'), true, true);
        else
            this.keyRef = url.parse(keySpec.reference, true, true);
        this.updateKey();
    }
    getCertificationRequest(identifiers) {
        let parameters = {
            /* @todo: Remove subject? */
            subject: { str: `/CN=${identifiers[0].value}` },
            extreq: [{ extname: 'subjectAltName', array: [] }]
        };
        for (let identifier of identifiers) {
            parameters.extreq[0].array.push({dns: identifier.value});
        }
        logger.verbose(`identifiers: ${inspect(parameters.extreq[0].array)}`);
        let csr;
        if (this.pkcs11) {
            if (this.keySpec.algorithm === 'EC') {
                let pub = this.pubKeyObj.toString('hex').slice(4);
                let ecdsa = new jsrsasign.KJUR.crypto.ECDSA({});
                ecdsa.setNamedCurve(this.keySpec.curve);
                ecdsa.pubKeyHex = pub;
                parameters.sbjpubkey = ecdsa;
                //parameters.sigalg = 'SHA256withECDSA';
                parameters.sigalg = 'SHA1withECDSA';
                let csri = new jsrsasign.asn1.csr.CertificationRequestInfo(parameters);
                let hex = csri.getEncodedHex();

                let hash = jsrsasign.KJUR.crypto.Util.hashHex(hex, 'sha1');

                this.pkcs11.C_SignInit(this.session, { mechanism: pkcs11js.CKM_ECDSA }, this.prvKeyObj);
                let signature = this.pkcs11.C_Sign(this.session, Buffer.from(hash, 'hex'), Buffer.alloc(256));

                //this.pkcs11.C_SignInit(this.session, { mechanism: pkcs11js.CKM_ECDSA_SHA256 }, this.prvKeyObj);
                //let signature = this.pkcs11.C_Sign(this.session, Buffer.from(hex, 'hex'), Buffer.alloc(256));

                let signatureHex = signature.toString('hex');
                let sigstr = jsrsasign.KJUR.asn1.ASN1Util.newObject({
                    'seq': [
                        { 'int': { 'hex': signatureHex.slice(0, signatureHex.length / 2) } },
                        { 'int': { 'hex': signatureHex.slice(signatureHex.length / 2) } }
                    ]
                });
                parameters.sighex = sigstr.getEncodedHex();
            }
            else {
                parameters.sbjpubkey = this.pubKeyObj;
                parameters.sigalg = 'SHA256withRSA';
                let csri = new jsrsasign.asn1.csr.CertificationRequestInfo(parameters);
                let hex = csri.getEncodedHex();
                this.pkcs11.C_SignInit(this.session, { mechanism: pkcs11js.CKM_SHA256_RSA_PKCS }, this.prvKeyObj);
                let signature = this.pkcs11.C_Sign(this.session, Buffer.from(hex, 'hex'), Buffer.alloc(512));
                logger.silly(`signature: ${signature.toString('hex')}`);
                parameters.sighex = signature.toString('hex');
            }
            logger.verbose(`parameters: ${inspect(parameters)}`);
            let obj = new jsrsasign.asn1.csr.CertificationRequest(parameters);
            csr = obj.getPEM();
        }
        else {
            let key = jsrsasign.KEYUTIL.getKey(this.key);
            parameters.sbjpubkey = key;
            parameters.sigalg = key.type === 'EC' ? 'SHA256withECDSA' : 'SHA256withRSA';
            parameters.sbjprvkey = key;
            csr = jsrsasign.asn1.csr.CSRUtil.newCSRPEM(parameters);
        }
        logger.verbose(`csr: ${csr}`);
        return csr;
    }
    storeCertificates(chain) {
        logger.debug(`got certificates ${chain}`);
        this.cert = chain[0].toString();
        if (chain.length > 1)
            this.ca = [ chain[1].toString() ];
        if (this.pkcs11) {
            /* @todo: store certificate(s) in PKCS#11 token */
            logger.warn('cannot store certificate in PKCS#11 token');
        }
        /* else */ {
            /* @todo: rewrite keyfile name key(.pem) -> key.cert.pem, key.ca.pem) */
            logger.info('storing TLS certificate ./etc/ssl/cert.pem ...');
            fs.writeFile('./etc/ssl/cert.pem', chain[0].toString(), { encoding: 'utf8' }, (error) => {
                if (error)
                    logger.error(`writing certificate failed (${error})`);
            });
            if (chain.length > 1) {
                logger.info('storing CA certificate ./etc/ssl/ca.pem ...');
                fs.writeFile('./etc/ssl/ca.pem', chain[1].toString(), { encoding: 'utf8' }, (error) => {
                    if (error)
                        logger.error(`writing CA certificate failed (${error})`);
                });
            }
        }
    }
    pkcs11Initialize(keyRef) {
        logger.info(`loading pkcs11 module ${this.keyRef.query['module-path']}`);

        this.pkcs11 = new pkcs11js.PKCS11();
        this.pkcs11.load(keyRef.query['module-path']);
        this.pkcs11.C_Initialize();
        process.on('SIGINT', (/* code */) => {
            if (this.pkcs11) {
                logger.debug(`finalizing pkcs11 module`);
                let _pkcs11 = this.pkcs11;
                this.pkcs11 = undefined;
                _pkcs11.C_Finalize();
            }
        });
    }
    updateKey() {
        logger.verbose(`Using private key from ${this.keyRef.href}`);
        if (this.keyRef.protocol === 'file:') {
            const keyfile = this.keyRef.path;
            fs.access(keyfile, fs.constants.R_OK, (error) => {
                if (error) {
                    let keySpec = config.https?.key ?? { algorithm: 'RSA', size: 2048 };
                    let algorithm = keySpec?.algorithm ?? 'RSA';
                    let curveOrLength = algorithm === 'EC' ? keySpec.curve : keySpec.size;
                    logger.info(`generating new TLS ${algorithm} ${curveOrLength} key ...`);
                    let key = jsrsasign.KEYUTIL.generateKeypair(algorithm, curveOrLength);
                    this.key = jsrsasign.KEYUTIL.getPEM(key.prvKeyObj, 'PKCS1PRV');
                    logger.info(`writing TLS key to '${keyfile}' ...`);
                    fs.writeFile(keyfile, this.key, { mode: 0o640, encoding: 'utf8' }, (error) => {
                        if (error)
                            logger.error(`writing key failed (${error})`);
                    });
                }
                else {
                    logger.info(`loading existing TLS key from '${keyfile}' ...`);
                    this.key = fs.readFileSync(keyfile, 'utf8');
                }
            });
        }
        else if (this.keyRef.protocol === 'pkcs11:') {

            this.pkcs11Initialize(this.keyRef);

            logger.silly(`pathname: ${inspect(this.keyRef.pathname)}`);
            let components = this.keyRef.pathname.split(';').map(component => {
                let kv = component.split('=');
                let returnValue = {};
                returnValue[kv[0]] = kv[1];
                return returnValue;
            });
            components = Object.assign({}, ...components);
            logger.silly(`components: ${inspect(components)}`);

            // Getting info about PKCS11 Module
            var module_info = this.pkcs11.C_GetInfo();
            logger.verbose(`module_info: ${inspect(module_info)}`);

            // Getting list of slots
            var slots = this.pkcs11.C_GetSlotList(true);

            // Select slot
            let slotId = 0;
            if (components['slot-id'] === undefined)
                logger.warn(`missing "slot-id" component (using slot ${slotId})`);
            else
                slotId = Number.parseInt(components['slot-id']);
            var slot = slots[slotId];
            logger.debug(`slot: ${inspect(slot)}`);

            this.session = this.pkcs11.C_OpenSession(slot, pkcs11js.CKF_RW_SESSION | pkcs11js.CKF_SERIAL_SESSION);
            logger.silly(`this.session: ${inspect(this.session)}`);

            // Getting info about Session
            //var info = this.pkcs11.C_GetSessionInfo(this.session);
            //logger.info(`this.session info: ${inspect(info)}`);

            // Get PIN value
            let pin;
            if (this.keyRef.query['pin-value'] !== undefined)
                pin = this.keyRef.query['pin-value'];
            else if (components['pin-source'] === 'file:/dev/stdin')
                pin = readline.question('Please enter PIN:', {hideEchoBack: true});
            else if (components['pin-source']?.startsWith('file:'))
                pin = fs.readFileSync(components['pin-source'].slice(5));
            else
                throw new Error(`Missing PKCS#11 PIN`);
            this.pkcs11.C_Login(this.session, pkcs11js.CKU_USER, pin);

            // Getting info about Session
            var info = this.pkcs11.C_GetSessionInfo(this.session);
            logger.debug(`this.session info: ${inspect(info)}`);

            let filter = [];
            // Select with type
            let type = {type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PRIVATE_KEY};
            if (components['type'] === undefined) {
                logger.info(`missing 'type' component (using ${inspect(type)})`);
            }
            else {
                switch (components['type']) {
                    case 'cert':       { type.value = pkcs11js.CKO_CERTIFICATE; break; }
                    case 'data':       { type.value = pkcs11js.CKO_DATA;        break; }
                    case 'private':    { type.value = pkcs11js.CKO_PRIVATE_KEY; break; }
                    case 'public':     { type.value = pkcs11js.CKO_PUBLIC_KEY;  break; }
                    case 'secret-key': { type.value = pkcs11js.CKO_SECRET_KEY;  break; }
                    default: {
                        throw new Error(`Unknown type '${components['type']}'`); }
                }
            }
            filter.push(type);

            // Select with label
            if (components['object'] !== undefined) {
                let label = {type: pkcs11js.CKA_LABEL, value: components['object']};
                filter.push(label);
            }
            // Select with ID
            if (components['id'] !== undefined) {
                let idString = decodeURIComponent(components.id);
                let id = {type: pkcs11js.CKA_ID, value: Buffer.from(idString, 'binary')};
                filter.push(id);
            }
            logger.debug(`searching with filter: ${inspect(filter)}`);
            this.pkcs11.C_FindObjectsInit(this.session, filter);
            let hObject = this.pkcs11.C_FindObjects(this.session);
            const ecAttributes = [
                /* { type: 0x129 }, // pkcs11js.CKA_PUBLIC_KEY_INFO */
                { type: pkcs11js.CKA_EC_POINT },
                { type: pkcs11js.CKA_EC_PARAMS },
                { type: pkcs11js.CKA_CLASS },
                { type: pkcs11js.CKA_ID },
                { type: pkcs11js.CKA_LABEL }
            ];
            const rsaAttributes = [
                /* { type: 0x129 }, // pkcs11js.CKA_PUBLIC_KEY_INFO */
                { type: pkcs11js.CKA_MODULUS },
                { type: pkcs11js.CKA_PUBLIC_EXPONENT },
                { type: pkcs11js.CKA_CLASS },
                { type: pkcs11js.CKA_ID },
                { type: pkcs11js.CKA_LABEL }
            ];
            let pubkeyAttributes = ecAttributes;
            while (hObject) {
                logger.debug(`found private key ${inspect(hObject)}`);
                let attributes = this.pkcs11.C_GetAttributeValue(this.session, hObject, [
                    { type: pkcs11js.CKA_CLASS },
                    { type: pkcs11js.CKA_KEY_TYPE },
                    { type: pkcs11js.CKA_TOKEN },
                    { type: pkcs11js.CKA_ID },
                    { type: pkcs11js.CKA_LABEL }
                ]);
                attributes.map(attribute => {
                    logger.verbose(`type: ${attribute.type}, value: ${attribute.value.toString('hex')}`);
                });
                switch (attributes[1].value.toString('hex')) {
                    case '0000000000000000': { pubkeyAttributes = rsaAttributes; break; }
                    case '0300000000000000': { pubkeyAttributes = ecAttributes;  break; }
                    default: {
                        throw new Error(`unkown key type (${attributes[1].value.toString('hex')})`);
                    }
                }
                this.prvKeyObj = hObject;
                // Looking for more objects? => this.pkcs11.C_FindObjects(this.session);
                hObject = this.pkcs11.C_FindObjects(this.session);
            }
            this.pkcs11.C_FindObjectsFinal(this.session);

            // Generate key
            if (this.prvKeyObj === undefined) {
                // this.pkcs11.C_Logout(this.session);
                // this.pkcs11.C_Login(this.session, pkcs11js.CKU_USER, pin);

                let id;
                if (components['id'] === undefined) {
                    id = Buffer.from('0000000001', 'binary');
                }
                else {
                    let idString = decodeURIComponent(components.id);
                    id = Buffer.from(idString, 'binary');
                }
                let publicKeyTemplate;
                let privateKeyTemplate;
                let mechanism;
                switch (this.keySpec.algorithm) {
                    case 'EC': {
                        let ecParameters;
                        switch (this.keySpec.curve) {
                            case 'secp256r1': { ecParameters = '06082A8648CE3D030107'; break; }
                            case 'secp384r1': { ecParameters = '06052B81040022';       break; }
                            case 'secp521r1': { ecParameters = '06052B81040023';       break; }
                            default: {
                                throw new Error('unkown ECDSA algorithm');
                            }
                        }
                        pubkeyAttributes = ecAttributes;
                        publicKeyTemplate = [
                            // { type: pkcs11js.CKA_EXTRACTABLE, value: true},
                            { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PUBLIC_KEY },
                            { type: pkcs11js.CKA_ID, value: id},
                            { type: pkcs11js.CKA_EC_PARAMS, value: Buffer.from(ecParameters, "hex") }
                        ];
                        privateKeyTemplate = [
                            { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PRIVATE_KEY },
                            // { type: pkcs11js.CKA_DERIVE, value: true },
                            { type: pkcs11js.CKA_ID, value: id},
                            // { type: pkcs11js.CKA_SENSITIVE, value: true}
                        ];
                        mechanism = pkcs11js.CKM_EC_KEY_PAIR_GEN;
                        break;
                    }
                    case 'RSA': {
                        pubkeyAttributes = rsaAttributes;
                        let bitlength = this.keySpec.size ?? 3072;
                        let exponent = (this.keySpec.exponent ?? 65537).toString(16);
                        if (exponent.length % 2 === 1)
                            exponent = exponent.replace('0x', '0');
                        else
                            exponent = exponent.replace('0x', '');
                        publicKeyTemplate = [
                            // { type: pkcs11js.CKA_EXTRACTABLE, value: true},
                            { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PUBLIC_KEY },
                            { type: pkcs11js.CKA_ID, value: id},
                            { type: pkcs11js.CKA_MODULUS_BITS, value: bitlength },
                            { type: pkcs11js.CKA_PUBLIC_EXPONENT, value: Buffer.from(exponent, 'hex') }
                        ];
                        privateKeyTemplate = [
                            { type: pkcs11js.CKA_CLASS, value: pkcs11js.CKO_PRIVATE_KEY },
                            // { type: pkcs11js.CKA_DERIVE, value: true },
                            { type: pkcs11js.CKA_ID, value: id},
                            { type: pkcs11js.CKA_PRIVATE, value: true},
                            // { type: pkcs11js.CKA_SENSITIVE, value: true}
                        ];
                        mechanism = pkcs11js.CKM_RSA_PKCS_KEY_PAIR_GEN;
                        break;
                    }
                    default: {
                        throw new Error(`unknown algorithm (${this.keySpec.algorithm})`);
                    }
                }
                //const label = { type: pkcs11js.CKA_LABEL, value: components['object'] };
                //publicKeyTemplate.push(label);
                //privateKeyTemplate.push(label);
                const token = { type: pkcs11js.CKA_TOKEN, value: true };
                publicKeyTemplate.push(token);
                privateKeyTemplate.push(token);

                logger.info(`generating new https key`);
                let keys = this.pkcs11.C_GenerateKeyPair(
                    this.session,
                    { "mechanism": mechanism },
                    publicKeyTemplate,
                    privateKeyTemplate
                );
                logger.verbose(`keys.publicKey: ${inspect(keys.publicKey)}`);
                logger.verbose(`keys.privateKey: ${inspect(keys.privateKey)}`);
                this.prvKeyObj = keys.privateKey;
            }

            type.value = pkcs11js.CKO_PUBLIC_KEY;
            logger.debug(`searching with filter: ${inspect(filter)}`);
            this.pkcs11.C_FindObjectsInit(this.session, filter);
            hObject = this.pkcs11.C_FindObjects(this.session);
            while (hObject) {
                logger.debug(`found public key ${inspect(hObject)}`);
                logger.debug(`pubkeyAttributes: ${JSON.stringify(pubkeyAttributes)}`);
                let attributes = this.pkcs11.C_GetAttributeValue(this.session, hObject, pubkeyAttributes);
                attributes.map(attribute => {
                    logger.verbose(`type: ${attribute.type}, value: ${attribute.value?.toString('hex')}`);
                });

                if (this.keySpec.algorithm === 'EC') {
                    this.pubKeyObj = attributes[0].value;
                }
                else if (this.keySpec.algorithm === 'RSA') {
                    let bitstr = jsrsasign.KJUR.asn1.ASN1Util.newObject({
                        'seq': [
                            { 'int': { 'hex': attributes[0].value.toString('hex') } },
                            { 'int': { 'hex': attributes[1].value.toString('hex') } }
                        ]
                    });
                    let pubKeyHex = jsrsasign.KJUR.asn1.ASN1Util.newObject({
                        'seq': [
                            { 'seq': [
                                {'oid': '1.2.840.113549.1.1.1'},
                                {'null': 'null'}
                            ] },
                            { 'bitstr': { 'hex': '00' + bitstr.getEncodedHex() } }
                        ]
                    }).getEncodedHex();

                    this.pubKeyObj = '-----BEGIN PUBLIC KEY-----\n'
                                   + Buffer.from(pubKeyHex, 'hex').toString('base64') + '\n'
                                   + '-----END PUBLIC KEY-----';
                    logger.debug(`pubKeyObj: ${this.pubKeyObj}`);
                    logger.silly(`key: ${inspect(jsrsasign.KEYUTIL.getKey(this.pubKeyObj))}`);
                }
                hObject = this.pkcs11.C_FindObjects(this.session);
            }
            this.pkcs11.C_FindObjectsFinal(this.session);

            /* Setup reference for OpenSSL PKCS#11 engine */
            if (components['id'])
                this.privateKeyIdentifier = `pkcs11:id=${components['id']};type=private?pin-value=${pin}`;
            else
                this.privateKeyIdentifier = `pkcs11:type=private?pin-value=${pin}`;
            logger.silly(`OpenSSL privateKeyIdentifier: ${this.privateKeyIdentifier}`);
        }
        else {
            throw new Error(`Cannot resolve https key reference (${this.keyRef})`);
        }
    }
}
