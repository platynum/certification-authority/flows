var jsrsasign = require('jsrsasign');

jsrsasign.ASN1HEX.dump = function(hexOrObj, flags, idx, indent) {
    var _ASN1HEX = jsrsasign.ASN1HEX;
    var _getV = _ASN1HEX.getV;
    var _dump = _ASN1HEX.dump;
    var _getChildIdx = _ASN1HEX.getChildIdx;

    let hex = hexOrObj;
    if (hexOrObj instanceof jsrsasign.KJUR.asn1.ASN1Object)
        hex = hexOrObj.getEncodedHex();

    if (flags === undefined) flags = { "ommit_long_octet": 32 };
    if (idx === undefined) idx = 0;
    if (indent === undefined) indent = "";

    let tag = hex.slice(idx, 2);

    if (tag == "01") {
        return { bool: (_getV(hex, idx) != "00") };
    }
    if (tag == "02") {
        return { int: { hex: _getV(hex, idx) } };
    }
    if (tag == "03") {
        let v = _getV(hex, idx);
        if (_ASN1HEX.isASN1HEX(v.slice(2))) {
            return { bitstr: { hex: v, expanded: _dump(v.slice(2), flags, 0, indent + "  ") } };
        } else {
            return { bitstr: { hex: v } };
        }
    }
    if (tag == "04") {
        let v = _getV(hex, idx);
        if (_ASN1HEX.isASN1HEX(v)) {
            return { octstr: { hex: v, expanded: _dump(v, flags, 0, indent + "  ") } };
        } else {
            return { octstr: { hex: v } };
        }
    }
    if (tag == "05") {
        return { null: {} };
    }
    if (tag == "06") {
        var hV = _getV(hex, idx);
        var oidDot = jsrsasign.KJUR.asn1.ASN1Util.oidHexToInt(hV);
        var oidName = jsrsasign.KJUR.asn1.x509.OID.oid2name(oidDot);
        if (oidName == '') {
              return { oid: { oid: oidDot } };
        } else {
              return { oid: { name: oidName, oid: oidDot} };
        }
    }
    if (tag == "0a") {
        return { enum: Number.parseInt(_getV(hex, idx)) };
    }
    if (tag == "0c") {
        return { utf8str: jsrsasign.hextoutf8(_getV(hex, idx)) };
    }
    if (tag == "13") {
        return { prnstr: jsrsasign.hextoutf8(_getV(hex, idx)) };
    }
    if (tag == "14") {
        return { telstr: jsrsasign.hextoutf8(_getV(hex, idx)) };
    }
    if (tag == "16") {
        return { ia5str: jsrsasign.hextoutf8(_getV(hex, idx)) };
    }
    if (tag == "17") {
        return { utctime: jsrsasign.hextoutf8(_getV(hex, idx)) };
    }
    if (tag == "18") {
        return { gentime: jsrsasign.hextoutf8(_getV(hex, idx)) };
    }
    if (tag == "1a") {
        return { visstr: jsrsasign.hextoutf8(_getV(hex, idx)) };
    }
    if (tag == "1e") {
        return { bmpstr: jsrsasign.hextoutf8(_getV(hex, idx)) };
    }
    if (tag == "30") {
        if (hex.slice(idx, 4) == "3000") {
            return { seq: [] };
        }

        let s = [];
        let aIdx = _getChildIdx(hex, idx);

        var flagsTemp = flags;

        if ((aIdx.length == 2 || aIdx.length == 3) &&
            hex.slice(aIdx[0], 2) == "06" &&
            hex.slice(aIdx[aIdx.length - 1], 2) == "04") { // supposed X.509v3 extension
            let oidName = _ASN1HEX.oidname(_getV(hex, aIdx[0]));
            var flagsClone = JSON.parse(JSON.stringify(flags));
            flagsClone.x509ExtName = oidName;
            flagsTemp = flagsClone;
        }

        for (let i = 0; i < aIdx.length; i++) {
            s.push(_dump(hex, flagsTemp, aIdx[i], indent + "  "));
        }
        return { seq: s };
    }
    if (tag == "31") {
        let s = [];
        let aIdx = _getChildIdx(hex, idx);
        for (let i = 0; i < aIdx.length; i++) {
            s.push(_dump(hex, flags, aIdx[i], indent + "  "));
        }
        return { set: s };
    }
    tag = Number.parseInt(tag, 16);
    if ((tag & 128) != 0) { // context specific
        var tagNumber = tag & 31;
        if ((tag & 32) == 0) { // primitive tag
            var v = _getV(hex, idx);
            if (jsrsasign.ASN1HEX.isASN1HEX(v)) {
                return { tag: 8*16 + tagNumber, obj: _dump(v, flags, 0, indent + "  ") };
            } else if (v.slice(0, 8) == "68747470") { // http
                v = jsrsasign.hextoutf8(v);
            } else if (flags.x509ExtName === "subjectAltName" &&
                       tagNumber == 2) {
                v = jsrsasign.hextoutf8(v);
            }
            // else if (ASN1HEX.isASN1HEX(v))

            return {tag: {tag: 10*16 + tagNumber, explicit: true, obj: v}};
        } else { // structured tag
            let s = [];
            let aIdx = _getChildIdx(hex, idx);
            for (let i = 0; i < aIdx.length; i++) {
                s.push(_dump(hex, flags, aIdx[i], indent + "  "));
            }
            return {tag: {tag: 8*16 + tagNumber, explicit: false, seq: s}};
        }
    }
    return { unknown: { tag: tag, value: _getV(hex, idx) } };
}

