import fs from 'node:fs';
import winston from 'winston';

let config = {
    source: 'https://proxy.dc-bsd.my.corp:3001',
    target: 'https://nodejs.dc-bsd.my.corp',
    webroot: '/usr/local/www/apache24/data',
    htdocs: '../browser-ra/htdocs',
    acme: {
        delegations: './data/delegations'
    },
    dns: {
        enabled: true,
        port: 5300,
        server: {
            protocol: 'udp4',
            address: '10.198.203.1',
            port: 53
        },
        listener: {
            port: 5300,
            options: {
                type: 'udp4',
                reuseAddr: true
            }
        }
    },
    http: {
        enabled: true,
        port: 3000,
    },
    https: {
        enabled: true,
        port: 3001,
        key: {
            //reference: 'file:./etc/ssl/key.pem',
            //reference: 'pkcs11:slot-id=0;object=tls-key;type=private;id=%01?module-path=/usr/local/lib/softhsm/libsofthsm2.so&pin-value=123456',
            //reference: 'pkcs11:slot-id=0;object=tls-key;type=private;pin-source=file:/dev/stdin;id=%30?module-path=/usr/local/lib/pkcs11-spy.so',
            reference: 'pkcs11:slot-id=0;type=private;label=?module-path=/usr/local/lib/softhsm/libsofthsm2.so&pin-value=123456',
	    // export PKCS11SPY=/usr/local/lib/softhsm/libsofthsm2.so
            // reference: 'pkcs11:slot-id=0;type=private;id=%30?module-path=/usr/local/lib/pkcs11-spy.so&pin-value=123456',
            //reference: 'pkcs11:slot-id=0;type=private;id=%01;pin-source=file:/dev/stdin?module-path=/usr/local/lib/libykcs11.so',
            //reference: 'pkcs11:slot-id=0;type=private;id=%01?module-path=/usr/local/lib/libykcs11.so&pin-value=123456',
            // algorithm: 'RSA',
            algorithm: 'EC',
            size: 2048,
            exponent: '0x10001',
            curve: 'secp256r1'
        },
        context: {
            enableTrace: true,
	    // align with etc/engine.conf#engine_section
            privateKeyEngine: 'pkcs11',
        }
    },
    mail: {
       transport: {
           host: 'mail',
           port: 25
       },
       from: 'pki@mail'
    },
    xmqtt: {
        broker: 'mqtt://localhost',
        username: 'acme-proxy',
        password: 'password',
        //clientId: 'acme-proxy',
        queueQoSZero: false
    },
    xldap: {
        server: 'ldaps://ldap/',
        username: 'cn=admin,dc=my,dc=corp',
        password: 'password',
    },
    database: {
        file: './data/proxy.db'
    },
    notifications: {
        enabled: false
    },
    eab: {
        required: false
    },
    // When enabled, ensure, that $(hostname -f) falls within one domain here
    "pre-authorizations": [
        {"type": "dns", "value": "acme-proxy.dc-bsd.my.corp", "subdomainAuthAllowed": true}
    ],
    "interfaces": {
	// Currently does not work with PKCS#11 key
        "xscep": "/cgi-bin",
        "acme": "/acme"
    },
    "logger": winston.createLogger({
        level: 'verbose',
        format: winston.format.combine(
            // winston.format.timestamp(),
            winston.format.json(),
        ),
        // defaultMeta: { business: { service: 'pki' }, service: 'acme-proxy' },
        transports: [
            new winston.transports.File({ filename: 'logs/proxy.log', level: 'info' })
        ],
    }),
    'access_log': 'logs/access.log'
};

export default config;
