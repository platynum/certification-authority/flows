import unicorn from "eslint-plugin-unicorn";
import security from "eslint-plugin-security";
import node from "eslint-plugin-node";
import mocha from "eslint-plugin-mocha";

export default [
    {
        "languageOptions": {
            "ecmaVersion": "latest",
            "sourceType": "module",
            "globals": {
                "config": false,
                "logger": false
            }
        },
        "plugins": {
            unicorn,
            mocha,
            security,
            node
        },
        "rules": {
        },
    }
];

