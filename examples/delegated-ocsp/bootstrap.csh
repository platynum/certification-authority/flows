#!/bin/csh
set BASE = `dirname $0`
sudo pkg install -y python rust
echo "Creating virtual environment in ${BASE}/ansible"
python -m venv "${BASE}/ansible"
source ${BASE}/ansible/bin/activate.csh
pip install --upgrade pip
pip install ansible
# DevTools
#pip install ansible-lint yamllint molecule testinfra
