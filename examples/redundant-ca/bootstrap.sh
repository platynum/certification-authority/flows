#!/bin/bash
sudo apt-get update
sudo apt-get install -y python3-venv
python3 -m venv ~/ansible
# shellcheck disable=SC1090
source ~/ansible/bin/activate
pip install ansible
ansible-galaxy collection install gluster.gluster
# DevTools
#pip install ansible-lint yamllint molecule testinfra
