#!/usr/bin/env node
import url from 'node:url';
import fs from 'node:fs';
import console from 'node:console';

import acme from '../acme-client/acme.mjs';

import nodemailer from 'nodemailer';
import Imap from 'node-imap';

if (import.meta.url === `file://${process.argv[1]}`) {
    /* Enable the following two lines, if you do not trust the ACME service
    console.log('Disable certificate validation ...');
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0; */

    const kas = 'ecdsa-p256-9c';
    //const kas = 'rsa-2048-9a';

    console.log('Reading certificate request ...');
    const csr = fs.readFileSync(`${kas}.csr.pem`, 'utf8');

    let attestation;
    try {
        fs.accessSync('rsa-2048-f9.crt.pem');
        fs.accessSync(`${kas}.attestation.crt.pem`);
        console.log('Reading attestation certificates ...');
        attestation = [
            fs.readFileSync('rsa-2048-f9.crt.pem', 'utf8'),
            fs.readFileSync(`${kas}.attestation.crt.pem`, 'utf8')
        ];
    }
    catch {}

    console.log('Setup incoming email client ...');
    const imap = new Imap({ user: process.env.IMAP_USER,
                            password: process.env.IMAP_PASSWORD,
                            host: process.env.IMAP_SERVER,
                            port: process.env.IMAP_PORT ?? 993,
                            tls: true,
                            connTimeout: 500000,
                            authTimeout: 500000
                          });

    console.log('Setup outgoing email client ...');
    const smtp = nodemailer.createTransport({
        host: process.env.SMTP_SERVER,
        port: process.env.SMTP_PORT ?? 587,
        secure: process.env.SMTP_SECURE,
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASSWORD
        }
    });
    let responder = new acme.EmailChallengeResponder(imap, smtp);
    let uri = new url.URL(`${process.env.ACME_SERVER}`);

    console.log('Setup ACME account');
    let account = await acme.Account.create(uri, [responder]);

    console.log('Requesting certificate ...');
    let identifier1 = {type: 'email', value: process.env.ACME_EMAIL};
    account.getCertificate([identifier1], null, null, csr, null, attestation, (err, chain) => {
        if (err) throw err;
        console.log(chain[0].toString());
        fs.writeFileSync(`${kas}.crt.pem`, chain[0].toString(), 'utf8');
        imap.end();
    });
}
