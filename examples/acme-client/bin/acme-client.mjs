#!/usr/bin/env node

import fs from 'node:fs';
import os from 'node:os';
import process from 'node:process';
import crypto from 'node:crypto';
const { subtle } = crypto.webcrypto;
// import { inspect } from 'node:util';

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import jsrsasign from 'jsrsasign';
import nodemailer from 'nodemailer';
import Imap from 'node-imap';

import json from '../package.json' with { type: "json" };
import acme from '../acme.mjs';

console.log(`Running ${json.name}@${json.version} with node ${process.versions.node} on ${os.platform()}/${os.release()}/${os.arch()} as user ${process.getuid()}/${process.geteuid()}`);

function acme_client_create_csr(identifiers, key, options) {
    let parameters = {
        /* @todo: Remove subject? */
        subject: { str: `/CN=${identifiers[0].value}` },
        extreq: [{ extname: 'subjectAltName', array: [] }]
    };
    for (let identifier of identifiers) {
        switch (identifier.type) {
            case 'dns':
                parameters.extreq[0].array.push({dns: identifier.value});
                break;
            case 'ip':
                parameters.extreq[0].array.push({ip: identifier.value});
                break;
            case 'email':
                parameters.extreq[0].array.push({rfc822: identifier.value});
                break;
            default:
                throw new Error(`Unknown identifier type (${identifier.type})`);
        }
    }
    if (options?.basicConstraints !== undefined) {
        let bc = {extname: 'basicConstraints', 'critical': true, 'cA': true};
        if (options.basicConstraints)
            bc.pathLen = 0;
        else
            bc.cA = false;
        parameters.extreq.push(bc);
    }
    if (options?.keyUsage) {
        let keyUsage = {extname: 'keyUsage', 'critical': true, names: []};
        for (let oid of options.keyUsage)
            keyUsage.names.push(oid);
        parameters.extreq.push(keyUsage);
    }
    if (options?.extKeyUsage) {
        let extKeyUsage = {extname: 'extKeyUsage', 'critical': true, array: []};
        for (let oid of options.extKeyUsage)
            extKeyUsage.array.push({'name': oid});
        parameters.extreq.push(extKeyUsage);
    }
    if (options?.ocspNoCheck) {
        parameters.extreq.push({'extname': 'ocspNoCheck', 'extn': '0500'});
    }
    if (options?.tlsStatusRequest) {
        parameters.extreq.push({'extname': '1.3.6.1.5.5.7.1.24', 'extn': '3003020105'});
    }

    parameters.sbjpubkey = key;
    parameters.sigalg = key.type === 'EC' ? 'SHA256withECDSA' : 'SHA256withRSA';
    parameters.sbjprvkey = key;
    // console.log(`parameters: ${inspect(parameters, { "depth": 4 })}`);

    return jsrsasign.asn1.csr.CSRUtil.newCSRPEM(parameters);
}

function acme_client_create_responders(argv) {
    console.log(`Setup responders ...`);

    let responders = [];
    if (argv['tls-alpn-01']) {
        if (argv.standalone && os.userInfo().uid === 0) {
            console.log(' - TlsAlpnChallengeResponder');
            responders.push(new acme.TlsAlpnChallengeResponder());
        }
    }
    if (argv['http-01']) {
        if (argv.standalone) {
            console.log(' - HttpServerChallengeResponder');
            responders.push(new acme.HttpServerChallengeResponder());
        }
        else {
            console.log(' - WebrootChallengeResponder');
            responders.push(new acme.WebrootChallengeResponder(argv.webroot));
        }
    }
    if (argv['email-reply-00']) {
        console.log(`Setup incoming email client (imap${argv.imap.tls ? 's' : ''}://${argv.imap.host}:${argv.imap.port}) ...`);

        if (argv.imap?.tlsOptions?.ca) {
            console.log(`Reading additional CA certificate (${argv.imap.tlsOptions.ca}) ...`);
            argv.imap.tlsOptions.ca = fs.readFileSync(argv.imap.tlsOptions.ca, 'utf8');
        }
        const imap = new Imap(argv.imap);

        console.log(`Setup outgoing email client (smtp://${argv.smtp.host}:${argv.smtp.port}) ...`);
        const smtp = nodemailer.createTransport(argv.smtp);

        let options = {
            verification: argv['email-verification']
        };
        responders.push(new acme.EmailChallengeResponder(imap, smtp, options));
    }
    if (argv['manual']) {
        console.log(' - ManualChallengeResponder');
        responders.push(new acme.ManualChallengeResponder());
    }
    if (argv['shell']) {
        console.log(' - ShellChallengeResponder');
        responders.push(new acme.ShellChallengeResponder(argv['shell']));
    }

    if (responders.length === 0)
        throw new Error('Cannot create usable responders');

    return responders;
}

async function acme_client_register(argv) {
    let responders = acme_client_create_responders(argv);

    console.log(`Setup ACME account at ${argv.server}`);
    let account = await acme.Account.create(argv.server, responders, undefined, argv.account);
    console.log(`account KID is ${account.kid}`);

    let buf;
    switch (argv.account.key.format) {
        case 'pem':
            buf = account.key.privateKey.export({type: 'pkcs8', format: 'pem'})
            break;
        case 'jwk':
            buf = JSON.stringify(account.key.jwk, null, 4);
            break;
        default:
            throw new Error(`Unknown account.key.format (${argv.account.key.format})`);
    }
    // console.log(buf);

    console.log(`Storing account.key.file (${argv.account.key.file})`);
    fs.writeFileSync(argv.account.key.file, buf, { encoding: 'utf8', mode: 0o600 });

    return account;
}

async function acme_client_request_certificate(argv) {
    let account = await acme_client_register(argv);
    let identifiers = [];
    if (argv.domain)
        for (let domain of argv.domain)
            identifiers.push({type: 'dns', value: domain});
    if (argv.email)
        for (let email of argv.email)
            identifiers.push({type: 'email', value: email});
    if (argv.ip)
        for (let ip of argv.ip)
            identifiers.push({type: 'ip', value: ip});
    if (identifiers.length === 0)
        throw new Error(`Cannot find any identifiers to be certified`);

    let validity;
    if (argv.order?.notBefore || argv.order?.notAfter) {
        validity = { "notBefore": argv.order?.notBefore, "notAfter": argv.order?.notAfter };
        console.log(`Requesting validity: ${JSON.stringify(validity)}`);
    }
    console.log(`Requesting certificate for:\n${JSON.stringify(identifiers, null, 4)}`);

    let csr;
    if (argv.certificate?.csr?.file && fs.existsSync(argv.certificate.csr.file)) {
        console.log(`Reading certificate.csr.file (${argv.certificate.csr.file})`);
        csr = fs.readFileSync(argv.certificate.csr.file, 'utf8');
    }
    else {
        let key;
        if (fs.existsSync(argv.certificate.key.file)) {
            let pem = fs.readFileSync(argv.certificate.key.file, 'utf8');
            key = jsrsasign.KEYUTIL.getKey(pem);
        }
        else {
            /* @TODO: Replace with nodejs crypto */
            console.log(`Generating a new ${argv.certificate.key.algorithm} key`);
            let pair;
            if (argv.certificate.key.algorithm === 'ec')
                pair = jsrsasign.KEYUTIL.generateKeypair('EC', argv.certificate.key.namedCurve);
            else
                pair = jsrsasign.KEYUTIL.generateKeypair('RSA', argv.certificate.key.modulusLength);
            let pem = jsrsasign.KEYUTIL.getPEM(pair.prvKeyObj, 'PKCS1PRV');
            console.log(`Storing certificate.key.file (${argv.certificate.key.file})`);
            fs.writeFileSync(argv.certificate.key.file, pem, { encoding: 'utf8', mode: 0o600 });
            key = pair.prvKeyObj;
        }
        csr = acme_client_create_csr(identifiers, key, argv.certificate?.csr);
        if (argv.certificate?.csr?.file) {
            console.log(`Storing certificate.csr.file (${argv.certificate.csr.file})`);
            fs.writeFileSync(argv.certificate.csr.file, csr, { encoding: 'utf8', mode: 0o640 });
        }
    }

    let attestation = [];
    if (argv.attestation?.certificate) {
        console.log(`Reading attestation certificate (${argv.attestation.certificate})`);
        let pem = fs.readFileSync(argv.attestation.certificate, 'utf8');
        attestation.push(pem);
        if (argv.attestation?.authority) {
            console.log(`Reading attestation authority (${argv.attestation.authority})`);
            pem = fs.readFileSync(argv.attestation.authority, 'utf8');
            attestation.push(pem);
        }
    }

    let renewal;
    if (argv?.certificate['auto-renewal']) {
        console.log('Requesting a STAR certificate');
        renewal = argv.certificate['auto-renewal'];
    }

    account.getCertificate(identifiers, validity, renewal, csr, argv.chain, attestation, (err, chain) => {
        if (err) {
            console.error('Cannot get certificate');
            if (err.response?.text)
                console.error(err.response.text);
            else
                console.error(err);
        }
        if (chain) {
            console.log(`Successfully downloaded certificate${ argv.chain ? ' for chain ' + argv.chain : ''}`);
            if (argv.certificate.console) {
                for (let cert of chain)
                    console.log(cert.toString());
            }
            if (argv.certificate.file)
                fs.writeFileSync(argv.certificate.file, chain[0].toString(), 'utf8');
            if (argv.certificate.chain) {
                let chainedCertificates = "";
                for (let cert of chain)
                    chainedCertificates += cert.toString();
                if (argv.certificate.chain)
                    fs.writeFileSync(argv.certificate.chain, chainedCertificates, 'utf8');
            }
            if (argv.certificate.issuer && chain.length > 1)
                fs.writeFileSync(argv.certificate.issuer, chain[1].toString(), 'utf8');
        }
    });
}

async function acme_client_pre_authorize(argv) {
    let account = await acme_client_register(argv);
    let identifiers = [];
    if (argv.domain) {
        for (let value of argv.domain) {
            account.preAuthorize({type: 'dns', value}, (err) => {
                if (err == null)
                    console.log(`Successfully pre-authorized ${value}`);
                else
                    console.error(`Cannot pre-authorize ${value} (${err})`);
            });
        }
    }
    else {
        throw new Error('Missing domain(s) for pre-authorization');
    }
}

function acme_client_get_subtle_options(name) {
    switch (name) {
        case 'prime256v1': return {name: 'ECDSA', namedCurve: 'P-256'};
        case 'secp384r1':  return {name: 'ECDSA', namedCurve: 'P-384'};
        case 'secp521r1':  return {name: 'ECDSA', namedCurve: 'P-521'};
        default:
            throw new Error(`Cannot map curve (${name})`);
    }
}

async function acme_client_read_private_key(filename) {
    let certKey = {};

    console.log(`Reading key file (${filename})`);
    let pem = fs.readFileSync(filename, 'utf8');
    let key = jsrsasign.KEYUTIL.getKey(pem);
    let pkcs8pem = jsrsasign.KEYUTIL.getPEM(key, "PKCS8PRV");
    let pkcs8b64 = pkcs8pem.replaceAll(/-{5}(BEGIN|END) PRIVATE KEY-{5}[\n\r]?/g, '');
    const options = acme_client_get_subtle_options(key.curveName);
    return subtle.importKey('pkcs8', Buffer.from(pkcs8b64, 'base64'), options, true, ['sign'])
        .then((privateKey) => {
             certKey.privateKey = privateKey;
             certKey.alg = 'ES256';
             return subtle.exportKey('jwk', privateKey);
        })
        .then((jwk) => {
            jwk.d = jwk.key_ops = jwk.ext = undefined;
            certKey.jwk = jwk;
            return certKey;
        });
}

async function acme_client_recover_account(argv) {
    console.log('Recovering account ...');

    let client = new acme.Client(argv.server);

    console.log(`Using account key for recovery at ${argv.server}`);
    return acme_client_read_private_key(argv.account.key.file)
        .then((key) => {
            return client.recoverAccount(key);
        })
        .then((account) => {
            console.log(`Successfully recovered KID ${account.kid}`);
            return account;
        })
        .catch((error) => {
            console.error(`Recovery (${error})`);
            if (error?.response?.text)
                console.error(error.response.text);
        });
}

async function acme_client_deactivate_account(argv) {

    let account = await acme_client_recover_account(argv);

    console.log('Deactivating account ...');
    return account.deactivate()
        .then((account) => {
            console.log(`Successfully deactivated account`);
            return account;
        })
        .catch((error) => {
            console.error(`Deactivation (${error})`);
            if (error?.response?.text)
                console.error(error.response.text);
        });
}

async function acme_client_revoke_certificate(argv) {
    console.log(`Revoking certificate with reason ${argv.revocation.reason} ...`);

    console.log(`Reading certificate.file (${argv.certificate.file})`);
    let cert = fs.readFileSync(argv.certificate.file, 'utf8');

    let client = new acme.Client(argv.server);
    let revocation;
    if (fs.existsSync(argv.certificate.key.file)) {
        console.log(`Using certificate key for revocation at ${argv.server}`);
        revocation = acme_client_read_private_key(argv.certificate.key.file)
            .then((key) => {
                return client.revokeCert(key, cert, argv.revocation.reason);
            });
    }
    else {
        console.log(`Using account key for revocation at ${argv.server}`);
        revocation = acme_client_read_private_key(argv.account.key.file)
            .then((key) => {
                return client.recoverAccount(key);
            })
            .then((account) => {
                return account.revokeCertificate(cert, argv.revocation.reason);
            });
    }
    revocation.then(() => {
        console.log('Successfully revoked certificate');
        return true;
    })
    .catch((error) => {
        console.error(`Revocation failed (${error})`);
        if (error?.response?.text)
            console.error(error.response.text);
        return false;
    });
}

async function acme_client_list_orders(argv) {
    console.log('Listing orders ...');

    let client = new acme.Client(argv.server);
    console.log(`Using account key to list orders at ${argv.server}`);
    let account;
    acme_client_read_private_key(argv.account.key.file)
        .then((key) => {
            return client.recoverAccount(key);
        })
        .then((_account) => {
            account = _account;
            return account.listOrders();
        })
        .then((orders) => {
            console.log(`orders: ${JSON.stringify(orders, null, 4)}`);
            let requests = [];
            for (let uri of orders)
                requests.push(account.getOrder(uri));
            return Promise.all(requests);
        })
        .then((orders) => {
            for (let i=0; i<orders.length; i++) {
                let order = orders[`${i}`];
                console.log(`order[${i}]: ${JSON.stringify(order, null, 4)}`);
            }
            return orders;
        })
        .catch((error) => {
            console.error(`Listing orders failed (${error})`);
            if (error?.response?.text)
                console.error(error.response.text);
        });
}

let addresses = [];
let nics = os.networkInterfaces();
for (let nic of Object.keys(nics)) {
    for (let addr of nics[`${nic}`]) {
        if (!addr.internal && addr.family === 'IPv6')
            addresses.push(addr.address);
    }
}

yargs(hideBin(process.argv))
     .scriptName(`${json.name}`)
     .usage("$0 <cmd> [args]")

     .command('config', 'Show config passed to client',
         function (argv) { console.log(`${JSON.stringify(argv.argv, null, 4)}`) })
     .example('$0 config', 'Show currently used configuration')

     .command('register', 'Register an ACME account (using account-key)', (yargs) => { return yargs; }, acme_client_register)
     .example('$0 register --server https://acme.my.corp/acme/Sub/directory', 'Register an ACME account at <server>')
     .example('ACME_CLIENT_ACCOUNT__EAB__HMAC="Y0...OK=" $0 register --server https://acme.zerossl.com/v2/DV90 --account.key.file acme.key --account.eab.kid proxy.my.corp', 'Register an ACME account at <server> using an external account binding (EAB)')

     .command('request', 'Request certificate', (yargs) => { return yargs; }, acme_client_request_certificate)
     .example('$0 request -D host.example.com', 'Request certificate for "host.example.com"')
     .example('$0 request --certificate.auto-renewal.lifetime 86400 -I ::ffff:172.23.45.67', 'Request STAR certificate for IP "::ffff:172.323.45.67"')
     .example('$0 request --order.notBefore "2022-09-01T00:00:00.000Z" --order.notAfter "2022-10-31T17:29:59.999Z" -D host.example.com', 'Request a certificate with limited lifetime')
     .example('ACME_CLIENT_IMAP__PASSWORD=password $0 request -E acme-ca-01@dc-bsd.my.corp --imap.host mail.dc-bsd.my.corp --imap.user=acme-client-01 --smtp.host mail.dc-bsd.my.corp --smtp.port 25 -e --http-01 false', 'Request an email S/MIME certificate')

     .command('revoke', 'Revoke certificate', (yargs) => { return yargs; }, acme_client_revoke_certificate)
     .example('$0 revoke --certificate.file certificate --certificate.key.file certificate.key', 'Revoke certificate using the certificate key')
     .example('$0 revoke --certificate.file certificate --account.key.file account.key', 'Revoke certificate using the account key')

     .command('recover', 'Recover account key ID', (yargs) => { return yargs; }, acme_client_recover_account)
     .example('$0 recover --account.key.file priv.pem', 'Recover an ACME account KID using keyfile')
     .command('pre-authorize', 'Pre-authorize a domain', (yargs) => { return yargs; }, acme_client_pre_authorize)
     .example('$0 pre-authorize -D toplevel.example.com -h false -m false --shell /usr/local/bin/setup-dns-challenge.sh')

     .command('deactivate', 'Deactivate account', (yargs) => { return yargs; }, acme_client_deactivate_account)
     .example('$0 deactivate --account.key.file priv.pem', 'Deactivate an ACME account using keyfile')

     .command('orders', 'Show current orders', (yargs) => { return yargs; }, acme_client_list_orders)
     .example('$0 orders --account.key.file priv.pem', 'Show all orders of an ACME account')

     .example('$0 --help --show-hidden', 'Show all command line switches')

     .option('server', {
         nargs: 1, alias: 's', type: 'string', group: "Required options",
         default: "https://acme-staging-v02.api.letsencrypt.org/directory",
         describe: 'URI of the ACME server'
     })
     .option('chain', {
         nargs: 1, alias: 'C', type: 'string',
         describe: 'Select the CA certificate chain'
     })

     .option('domain', {
         alias: 'D', type: 'array', group: 'Subjects',
         // default: [`${os.hostname()}`],
         describe: 'Domain(s) to request a certificate for'
     })
     .option('email', {
         alias: 'E', type: 'array', group: 'Subjects',
         // default: [`${os.userInfo().username}@${os.hostname()}`],
         describe: 'Email address(es) to request a certificate for'
     })
     .option('ip', {
         alias: 'I', type: 'array', group: 'Subjects',
         // default: addresses,
         describe: 'IP (v4/v6) address(es) to request a certificate for'
     })

     .option('tls-alpn-01', {
         alias: 't', type: 'boolean', group: 'Challenge responders',
         default: false,
         describe: 'Enable tls-alpn challenge responder'
     })
     .option('http-01', {
         alias: 'h', type: 'boolean', group: 'Challenge responders',
         default: true,
         describe: 'Enable http-01 challenge responder'
     })
     .option('dns-01', {
         alias: 'd', type: 'boolean', group: 'Challenge responders',
         default: false,
         describe: 'Enable dns-01 challenge responder'
     })
     .option('email-reply-00', {
         alias: 'e', type: 'boolean', group: 'Challenge responders',
         default: false,
         describe: 'Enable email-reply-00 challenge responder'
     })
     .option('shell', {
         type: 'string', group: 'Challenge responders',
         default: null,
         describe: 'Execute shell command to setup challenge response'
     })
     .option('manual', {
         alias: 'm', type: 'boolean', group: 'Challenge responders',
         default: true,
         describe: 'Enable manual challenge responder'
     })
     .option('webroot', {
         nargs: 1, alias: 'w', group: 'http-01',
         default: '/usr/local/www/apache24/data/',
         describe: 'Webroot where challenge is placed'
     })
     .option('standalone', {
         alias: 'S', type: 'boolean',
         default: true,
         describe: 'Spin off a local service to serve http-01/tls-alpn-01 challenges'
     })
     .option('imap.host', {
         alias: 'i', type: 'string', group: 'email-reply-00',
         default: 'mail.dc-bsd.my.corp',
         describe: 'IMAP server to be used reading incoming email'
     })
     .option('imap.port', {
         group: 'email-reply-00',
         default: 993,
         describe: 'IMAP server port for incoming email'
     })
     .option('imap.tls', {
         type: 'boolean', group: 'email-reply-00',
         default: true,
         describe: 'Use TLS to connect to IMAP server'
     })
     .option('imap.tlsOptions.ca', {
         type: 'string', group: 'email-reply-00', hidden: true,
         describe: 'Use TLS CA certificate (PEM-file)'
     })
     .option('imap.user', {
         type: 'string', group: 'email-reply-00',
         default: `${os.userInfo().username}`,
         describe: 'Username to authenticate against incoming email server'
     })
     .option('imap.password', {
         type: 'string', group: 'email-reply-00',
         describe: 'Password to authenticate against incoming email server'
     })
     .option('smtp.host', {
         alias: 'o', type: 'string', group: 'email-reply-00',
         default: 'mail.dc.bsd.my.corp',
         describe: 'SMTP server to used for sending email'
     })
     .option('smtp.port', {
         type: 'integer', group: 'email-reply-00',
         default: 587,
         describe: 'SMTP server port for outgoing email'
     })
     .option('smtp.secure', {
         type: 'boolean', group: 'email-reply-00',
         describe: 'Use TLS to protect outgoing email credentials'
     })
     .option('smtp.auth.user', {
         type: 'string', group: 'email-reply-00',
         default: `${os.userInfo().username}`,
         describe: 'Username to authenticate against outgoing email server'
     })
     .option('smtp.auth.pass', {
         type: 'string', group: 'email-reply-00',
         describe: 'Password to authenticate against outgoing email server'
     })
     .option('email-verification', {
         type: 'boolean', group: 'email-reply-00',
         default: true,
         describe: 'Verify incoming email challenge'
     })
     .option('attestation.certificate', {
         nargs: 1, type: 'string', hidden: true,
         describe: 'Certificate file to be sent with CSR'
     })
     .option('attestation.authority', {
         nargs: 1, type: 'string', hidden: true,
         describe: 'Authority certificate file to be sent with CSR'
     })

     .option('order.notBefore', {
         nargs: 1, type: 'date', group: 'Validity',
         describe: "Specify order's validity notBefore date"
     })
     .option('order.notAfter', {
         nargs: 1, type: 'date', group: 'Validity',
         describe: "Specify order's validity notAfter date"
     })

     .option('certificate.auto-renewal.start-date', {
         nargs: 1, type: 'string', group: 'STAR certificate',
         describe: "Certificate's auto-renewal start date"
     })
     .option('certificate.auto-renewal.end-date', {
         nargs: 1, type: 'string', group: 'STAR certificate',
         describe: "Certificate's auto-renewal end date"
     })
     .option('certificate.auto-renewal.lifetime', {
         nargs: 1, type: 'int', group: 'STAR certificate',
         describe: "Certificate's auto-renewal lifetime (in seconds)"
     })
     .option('certificate.auto-renewal.lifetime-adjust', {
         nargs: 1, type: 'int', group: 'STAR certificate',
         describe: "Certificate's auto-renewal lifetime-adjust (in seconds)"
     })
     .option('certificate.auto-renewal.allow-certificate-get', {
         type: 'boolean', group: 'STAR certificate',
         describe: "Allow certifcate download via HTTP GET requests"
     })

     .option('certificate.key.file', {
         nargs: 1, alias: 'c', type: 'string', hidden: true,
         default: 'certificate.key',
         describe: 'Certificate key file, will be generated if it does not exist'
     })
     .option('certificate.key.algorithm', {
         nargs: 1, type: 'string', hidden: true,
         default: 'ec',
         // choices: ['rsa', 'rsa-pss', 'dsa', 'ec', 'ed25519', 'ed448', 'x25519', 'x448', 'dh']
         choices: ['ec'],
         describe: 'Certificate key algorithm when generating an account key'
     })
     .option('certificate.key.publicExponent', {
         nargs: 1, type: 'string', hidden: true,
         describe: "Certificate key public exponent, if algorithm is 'rsa'"
     })
     .option('certificate.key.namedCurve', {
         nargs: 1, type: 'string', hidden: true,
         default: 'secp384r1',
         choices: ['prime256v1', 'secp384r1', 'secp521r1'],
         describe: "Certificate key curve name used, if algorithm is 'ec'"
     })
     .option('certificate.key.modulusLength', {
         nargs: 1, alias: 'L', type: 'int', hidden: true,
         describe: "Certificate key modulus length, if algorithm is 'rsa'"
     })
     .option('certificate.csr.basicConstraints', {
         type: 'boolean', hidden: true,
         describe: "Set basicConstraints of CSR"
     })
     .option('certificate.csr.extKeyUsage', {
         type: 'array', hidden: true,
         // default: ['ocspSigning'],
         describe: "Set extKeyUsage flags of CSR"
     })
     .option('certificate.csr.keyUsage', {
         type: 'array', hidden: true,
         describe: "Set keyUsage flags of CSR"
     })
     .option('certificate.csr.ocspNoCheck', {
         type: 'boolean', hidden: true,
         default: false,
         describe: "Set ocspNoCheck flag of CSR"
     })
     .option('certificate.csr.tlsStatusRequest', {
         type: 'boolean', hidden: true,
         default: false,
         describe: "Set TLS status request (aka OCSP check)"
     })
     .option('certificate.csr.file', {
         nargs: 1, type: 'string', hidden: true,
         describe: 'Store CSR used to request certificate'
     })
     .option('certificate.file', {
         nargs: 1, type: 'string',
         default: 'certificate',
         describe: 'File where certificate is stored'
     })
     .option('certificate.console', {
         type: 'boolean', hidden: true,
         default: false,
         describe: 'Log certificates to stdout'
     })
     .option('certificate.chain', {
         nargs: 1, type: 'string',
         default: 'certificate.chain',
         describe: 'File where certificate and CA certificate(s) are stored'
     })
     .option('certificate.issuer', {
         nargs: 1, type: 'string',
         default: 'certificate.issuer',
         describe: 'File where issuing CA\'s certificate is stored'
     })
     .option('account.eab.kid', {
         nargs: 1, type: 'string',
         describe: 'External account binding (EAB) key identifier (KID)',
         implies: ['account.eab.hmac']
     })
     .option('account.eab.hmac', {
         nargs: 1, type: 'string',
         describe: 'External account binding (EAB) HMAC key (base64 encoded)',
         implies: ['account.eab.kid']
     })
     .option('account.agree-tos', {
         type: 'boolean',
         default: false,
         describe: 'Agree to Terms-of-Service (TOS)'
     })
     .option('account.contact', {
         type: 'array', hidden: true,
         default: [`mailto:${os.userInfo().username}@${os.hostname}`],
         describe: '(Email) contacts to be registered with ACME account'
     })
     .option('account.key.file', {
         nargs: 1, alias: 'a', type: 'string', hidden: true,
         default: 'account.key',
         describe: 'Account key file, will be generated if it does not exist'
     })
     .option('account.key.format', {
         nargs: 1, type: 'string', hidden: true,
         default: 'pem',
         choices: ['pem', 'jwk'],
         describe: 'Store account key using this format'
     })
     .option('account.key.algorithm', {
         nargs: 1, alias: 'A', type: 'string', hidden: true,
         default: 'ec',
         // choices: ['rsa', 'rsa-pss', 'dsa', 'ec', 'ed25519', 'ed448', 'x25519', 'x448', 'dh']
         choices: ['ec'],
         describe: 'Account key algorithm when generating an account key'
     })
     .option('account.key.publicExponent', {
         nargs: 1, type: 'string', hidden: true,
         describe: "Account key public exponent, if algorithm is 'rsa'"
     })
     .option('account.key.namedCurve', {
         nargs: 1, type: 'string', hidden: true,
         default: 'secp521r1',
         choices: ['prime256v1', 'secp384r1', 'secp521r1'],
         describe: "Account key curve name used, if algorithm is 'ec'"
     })
     .option('account.key.modulusLength', {
         nargs: 1, type: 'int', hidden: true,
         describe: "Account key modulus length, if algorithm is 'rsa'"
     })
     .option('revocation.reason', {
         nargs: 1, type: 'int', hidden: true,
         default: 4,
         choices: [0, 1, 2, 3, 4, 5, 6, 8, 9, 10],
         describe: 'Revocation reason'
     })

     .config('settings', function (configPath) {
         console.log(`Reading settings from configuration file ${configPath}`);
         return JSON.parse(fs.readFileSync(configPath));
     })
     .alias('settings', 'f')
     .describe('settings', 'Configuration file including options')
     .count('verbose').alias('v', 'verbose')
     .env('ACME_CLIENT')
     .demandCommand()
     .help()
     .wrap(null)
     .argv;

