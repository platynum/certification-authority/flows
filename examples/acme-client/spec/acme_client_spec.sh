Describe 'acme-client'
    Path account.key.file="account.key"
    Path certificate.file="certificate"
    Path certificate.key.file="certificate.key"
    Path certificate.issuer="certificate.issuer"
    Path certificate.chain="certificate.chain"
    It 'emits help'
        When run command npm start -- --help
        The status should be success
        The first word of fifth line of output should equal Running
    End
    It 'emits config'
        When run command npm start -- config
        The status should be success
        The first word of line 5 of stdout should equal Running
    End
    It 'can register an account'
        When run command npm start -- register --server "https://$HOSTNAME/acme/Sub/"
        The status should be success
        The word 1 of line 5 of stdout should equal Running
        The path account.key.file should be exist
    End
    Describe 'TLS certificates'
        Describe 'http-01 challenge'
            setup() { rm -f certificate*; };
            BeforeAll 'setup'

            It 'can request a TLS certificate'
                When run command npm start -- request --server "https://$HOSTNAME/acme/Sub/" -D "$HOSTNAME"
                The status should be success
                The word 1 of line 5 of stdout should equal Running
                The path certificate.file should be exist
                The path certificate.key.file should be exist
                The path certificate.issuer should be exist
                The path certificate.chain should be exist
            End
            It 'can list orders'
                When run command npm start -- orders --server "https://$HOSTNAME/acme/Sub/" --account.key.file account.key
                The status should be success
                The word 1 of line 5 of stdout should equal Running
            End
            It 'can revoke a TLS certificate'
                When run command npm start -- revoke --server "https://$HOSTNAME/acme/Sub/" --certificate.file certificate --certificate.key.file certificate.key
                The status should be success
                The word 1 of line 5 of stdout should equal Running
            End
            It 'can specify notAfter via order'
                When run command npm start -- request --server "https://$HOSTNAME/acme/Sub/" -D "$HOSTNAME" --order.notAfter "$(date --date="14 days" "+%Y-%m-%dT00:00:00.000Z")"
                The status should be success
                The word 1 of line 5 of stdout should equal Running
            End
            It 'retrieved certificate has requested notAfter'
                When run command openssl x509 -noout -in certificate -enddate
                The status should be success
                The first line of stdout should equal "$(date --date='14 days' '+notAfter=%b %e 00:00:00 %Y GMT')"
            End
            It 'can specify notBefore via order'
                When run command npm start -- request --server "https://$HOSTNAME/acme/Sub/" -D "$HOSTNAME" --order.notBefore "$(date --date="tomorrow" "+%Y-%m-%dT00:00:00.000Z")"
                The status should be success
                The word 1 of line 5 of stdout should equal Running
            End
            It 'retrieved certificate has requested notBefore'
                When run command openssl x509 -noout -in certificate -startdate
                The status should be success
                The first line of stdout should equal "$(date --date='tomorrow' '+notBefore=%b %e 00:00:00 %Y GMT')"
            End
        End
        Describe 'tls-alpn-01 challenge'
            setup() { rm -f certificate*; };
            port_in_use() { netstat -ntl | grep -q ":443"; };

            BeforeAll 'setup'

            Skip if "port 443 already used" port_in_use
            It 'can request a TLS certificate with tls-alpn-01 challenge'
                When run command npm start -- request --server "https://$HOSTNAME/acme/Sub/" -D "$HOSTNAME" --http-01=false --tls-alpn-01
                The status should be success
                The word 1 of line 5 of stdout should equal Running
                The path certificate.file should be exist
                The path certificate.key.file should be exist
                The path certificate.issuer should be exist
                The path certificate.chain should be exist
            End
        End
    End
    Describe 'S/MIME certificates'
        setup() { rm -f certificate*; }
        BeforeAll 'setup'

        It 'can request an S/MIME certificate'
            When run command npm start -- request --server "https://$HOSTNAME/acme/Sub/" -E user@mail --email-reply-00 --imap.user user@mail --imap.password user@mail --imap.host mail --imap.port 3143 --imap.tls false --smtp.auth.user user@mail --smtp.auth.pass user@mail --smtp.host mail --smtp.port 3025 --email-verification false
            The status should be success
            The word 1 of line 5 of stdout should equal Running
            The path certificate.file should be exist
            The path certificate.key.file should be exist
            The path certificate.issuer should be exist
            The path certificate.chain should be exist
        End
        It 'can revoke an S/MIME certificate'
            When run command npm start -- revoke --server "https://$HOSTNAME/acme/Sub/" --certificate.file certificate --account.key.file account.key
            The status should be success
            The word 1 of line 5 of stdout should equal Running
        End
    End
    It 'can deactivate an account'
        When run command npm start -- deactivate --server "https://$HOSTNAME/acme/Sub/"
        The status should be success
        The word 1 of line 5 of stdout should equal Running
    End
End
