import { Buffer } from 'node:buffer';
import path from 'node:path';
import url from 'node:url';
import tls from 'node:tls';
import net from 'node:net';
import crypto from 'node:crypto';
import { webcrypto } from 'node:crypto';
const subtle = webcrypto.subtle;
import { inspect, TextEncoder }  from 'node:util';
import fs from 'node:fs';
import console from 'node:console';
import { setTimeout } from 'node:timers';
import http from 'node:http';
import { execFileSync } from 'node:child_process';

import readline from 'readline-sync';
import Imap from 'node-imap';
import request from 'superagent';
import superagentProxy from 'superagent-proxy';
superagentProxy(request);
import prefix from 'superagent-prefix';
import ip6addr from 'ip6addr';
import jsrsasign from 'jsrsasign';
import { dkimVerify } from 'mailauth/lib/dkim/verify.js';
//import { simpleParser } from 'mailparser';
import express from 'express';

export async function createJwkThumbprint(jwk) {
    let pubkeyString;
    if (jwk.kty === "EC") {
        pubkeyString = "{" +
                    "\"crv\":\"" + jwk.crv + "\"," +
                    "\"kty\":\"" + jwk.kty + "\"," +
                    "\"x\":\"" + jwk.x + "\"," +
                    "\"y\":\"" + jwk.y + "\"" +
                  "}";
    }
    else if (jwk.kty == 'OKP') {
        pubkeyString = "{" +
                    "\"crv\":\"" + jwk.crv + "\"," +
                    "\"kty\":\"" + jwk.kty + "\"," +
                    "\"x\":\"" + jwk.x + "\"" +
                  "}";
    }
    else { /* RSA */
        pubkeyString = "{" +
                    "\"e\":\"" + jwk.e + "\"," +
                    "\"kty\":\"" + jwk.kty + "\"," +
                    "\"n\":\"" + jwk.n + "\"" +
                  "}";
    }
    let hash = await subtle.digest('SHA-256', new TextEncoder().encode(pubkeyString));
    let hashString = String.fromCharCode.apply(undefined, new Uint8Array(hash));
    return Buffer.from(hashString, 'binary').toString('base64url');
}

class ConsoleLogger {
    log(msg) {
        console.log(`${new Date().toISOString()} ${msg}`);
    }
    trace(...args) {
        this.log(`TRACE: ${args.join(' ')}`);
    }
    debug(...args) {
        this.log(`DEBUG: ${args.join(' ')}`);
    }
    info(...args) {
        this.log(`INFO: ${args.join(' ')}`);
    }
    warn(...args) {
        this.log(`WARN: ${args.join(' ')}`);
    }
    error(...args) {
        this.log(`ERROR: ${args.join(' ')}`);
    }
    fatal(...args) {
        this.log(`FATAL: ${args.join(' ')}`);
    }
}

export class ShellChallengeResponder {
    constructor(command) {
        this.type = [ 'dns-01', 'http-01', 'tls-alpn-01', 'email-reply-00' ];
        this.problems = [];
        this.command = command;
    }

    async solve(key, authorization, challenge) {
        let thumbprint = await createJwkThumbprint(key.jwk);
        let str = `${challenge.token}.${thumbprint}`;
        let env = {
          'TYPE': challenge.type,
          'IDENTIFIER': authorization.identifier.value,
          'THUMBPRINT': thumbprint,
          'VALIDATION': str
        };
        switch (challenge.type) {
            case 'http-01':
                env['TOKEN'] = challenge.token;
                break;
            case 'tls-alpn-01':
            case 'email-reply-00':
                break;
            case 'dns-01':
                {
                    let hash = await subtle.digest('SHA-256', new TextEncoder().encode(str));
                    let hashString = String.fromCharCode.apply(undefined, new Uint8Array(hash));
                    let b64u = Buffer.from(hashString, 'binary').toString('base64url');
                    env['VALIDATION'] = b64u;
                }
                break;
            default:
                throw new Error(`Unsupported challenge type (${challenge.type})`);
        }
        execFileSync(this.command, ['setup'], {
            env,
            shell: true,
            detached: false
        });
    }
}

export class ManualChallengeResponder {
    constructor() {
        this.type = [ 'dns-01', 'http-01' ];
        this.problems = [];
    }

    async solve(key, authorization, challenge) {
        let thumbprint = await createJwkThumbprint(key.jwk);
        let str = `${challenge.token}.${thumbprint}`;
        switch (challenge.type) {
            case 'http-01':
                console.log(`Setup http://${authorization.identifier.value}/.well-known/acme-challenge/${challenge.token}`);
                console.log(`with content:'${str}'`);
                break;

            case 'dns-01':
                {
                    let hash = await subtle.digest('SHA-256', new TextEncoder().encode(str));
                    let hashString = String.fromCharCode.apply(undefined, new Uint8Array(hash));
                    let b64u = Buffer.from(hashString, 'binary').toString('base64url');

                    console.log('Setup DNS TXT record with the following content:');
                    console.log(`_acme-challenge.${authorization.identifier.value} TXT ${b64u}`);
                }
                break;
            default:
                throw new Error(`Unsupported challenge type (${challenge.type})`);
        }
        readline.question('<enter>');
        console.log('challenge response was setup');
    }
}

export class DnsProxyChallengeResponder {
    constructor(database) {
        this.database = database;
        this.type = 'dns-01';
        this.problems = [];
    }
    async solve(key, authorization, challenge) {
        let thumbprint = await createJwkThumbprint(key.jwk);
        let response = `${challenge.token}.${thumbprint}`;
        if (challenge.type === 'dns-01') {
            let hash = await subtle.digest('SHA-256', new TextEncoder().encode(response));
            let hashString = String.fromCharCode.apply(undefined, new Uint8Array(hash));
            let b64u = Buffer.from(hashString, 'binary').toString('base64url');

            this.database.push({
                type: 16, class: 1, ttl: 120,
                name: `_acme-challenge.${authorization.identifier.value}`,
                data: [ `${b64u}` ]
            });
        }
        else {
            throw new Error(`Unsupported challenge type (${challenge.type})`);
        }
    }
}

export class WebrootChallengeResponder {
    constructor(webroot, options) {
        this.webroot = webroot
        this.type = 'http-01';
        this.problems = [];
        this.logger = options?.logger ?? new ConsoleLogger();
    }

    async solve(key, authorization, challenge) {
        this.logger.info(`challenge: ${inspect(challenge)}`);
        let thumbprint = await createJwkThumbprint(key.jwk);
        let str = `${challenge.token}.${thumbprint}`;
        if (challenge.type != this.type)
            throw new Error(`Cannot solve ${challenge.type} challenge`);

        let response = path.join(this.webroot, '.well-known/acme-challenge', challenge.token);
        this.logger.info(`Creating ${response}`);
        fs.writeFileSync(response, str, { encoding: 'utf8', mode: 0o644 });
    }
}

export class HttpServerChallengeResponder {
    constructor(options) {
        this.challenges = [];
        this.type = 'http-01';
        this.logger = options?.logger ?? new ConsoleLogger();
        this.app = express();
        this.app.get('/.well-known/acme-challenge/:token', (request, response) => {
            let content = this.challenges[`${request.params.token}`];
            if (content)
                response.send(content);
            else
                response.status(404).end();
        });
        this.options = {
            port: 80,
            address: '0.0.0.0'
        };
        if (options)
            Object.assign(this.options, options);
    }

    async solve(key, authorization, challenge) {
        this.logger.info(`challenge: ${inspect(challenge)}`);
        if (challenge.type != this.type)
            throw new Error(`Cannot solve ${challenge.type} challenge`);

        let thumbprint = await createJwkThumbprint(key.jwk);
        this.challenges[challenge.token] = `${challenge.token}.${thumbprint}`;

        let server = http.createServer(this.app);
        server.unref();
        server.listen(this.options.port, this.options.address, () => {
            this.logger.info(`http server listening on http://${this.options.address}:${this.options.port}`);
        });
    }
}

export class TlsAlpnChallengeResponder {
    constructor(options) {
        this.type = 'tls-alpn-01';
        this.problems = [];
        this.logger = options?.logger ?? new ConsoleLogger();
        this.options = {
            port: 443,
            minVersion: 'TLSv1.2',
            maxVersion: 'TLSv1.3',
            ALPNProtocols: ['acme-tls/1']
        };
        if (options)
            Object.assign(this.options, options);
    }

    async solve(key, authorization, challenge) {
        let domain = authorization.identifier.value;
        let keyPem = key.privateKey.export({type: 'pkcs8', format: 'pem'});

        let thumbprint = await createJwkThumbprint(key.jwk);
        let str = `${challenge.token}.${thumbprint}`;
        let hash = await subtle.digest('SHA-256', new TextEncoder().encode(str));
        let hashString = String.fromCharCode.apply(undefined, new Uint8Array(hash));
        let hex = Buffer.from(hashString, 'binary').toString('hex');

        let params = {
            serial: 1,
            sigalg: {name: 'SHA256withECDSA'},
            issuer: {str: `/CN=${domain}`},
            notbefore: jsrsasign.datetozulu(new Date(Date.now())),
            notafter: jsrsasign.datetozulu(new Date(Date.now() + 1000 * 60 * 15)),
            subject: {str: `/CN=${domain}`},
            sbjpubkey: keyPem,
            ext: [
                { extname: '1.3.6.1.5.5.7.1.31', critical: true, extn: `0420${hex}` ?? '0500' }
            ],
            cakey: keyPem
        };

        if (net.isIPv6(domain)) {
            let ip = ip6addr.parse(domain);
            let ipStr = ip.toString({format: 'v6'});
            params.ext.push({extname: 'subjectAltName', array: [{'ip': `${ipStr}`}] });
        }
        else if (net.isIPv4(domain)) {
            params.ext.push({extname: 'subjectAltName', array: [{'ip': `${domain}`}] });
        }
        else {
            params.ext.push({extname: 'subjectAltName', array: [{'dns': `${domain}`}] });
        }

        let cert = new jsrsasign.asn1.x509.Certificate(params);
        this.options.cert = cert.getPEM();
        // console.log(`pem: ${this.options.cert}`);

        if (net.isIPv4(domain)) {
            this.options.servername = domain.split('.').reverse().join('.') + '.in-addr.arpa';
        }
        else if (net.isIPv6(domain)) {
            let ip = ip6addr.parse(domain).toString({format: 'v6', zeroElide: false});
            let digits = [];
            ip.split(/[.:]/).map((word) => {
                let n = Number.parseInt(word, 16);
                digits.push((n - (n%256)) / 256, n%256);
            });
            this.options.servername = digits.reverse().join('.') + '.ip6.arpa';
        }
        else {
            this.options.servername = domain;
        }
        //console.log(`servername: ${this.options.servername}`);

        this.options.host = domain;
        this.options.key = keyPem;

        this.server = tls.createServer(this.options, () => {
            this.logger.info(`${this.options.minVersion} server created for ${this.options.ALPNProtocols[0]} with domain ${domain} and servername ${this.options.servername}`);
        });
        this.server.unref();
        this.server.listen(this.options.port, () => {
            this.logger.debug(`https server listening on https://localhost:${this.options.port}`);
        });
    }
}

export class EmailChallengeResponder {
    setupImap(imap, options, messageHandler, subjectHandler) {
        this.imap = imap;
        if (imap) {
            imap.on('error', (error) => {
                this.logger.error(`IMAP error: ${error}`);
                if (error) throw error;
            });
            imap.on('ready', () => {
                this.logger.trace('Connected, opening INBOX ...');
                imap.openBox('INBOX', false, (error, box) => {
                    if (error) throw error;
                    this.logger.info(`searching INBOX with ${box.messages.new} unseen of ${box.messages.total} messages for ACME challenges ...`);
                    imap.search(['UNSEEN', ['SUBJECT', 'ACME: ']], (error, results) => {
                        this.logger.debug(`Found ${results.length} challenge(s)`);
                        let f = imap.fetch(results, {
                            bodies: [''],
                            markSeen: true
                        });
                        f.logger = this.logger;
                        f.on('message', function(message, seqno) {
                            let header = '';
                            let body = '';
                            message.logger = this.logger;
                            message.on('body', function(stream, info) {
                                if (info.which === 'TEXT')
                                    this.logger.debug(`fetching message ${seqno} with ${info.size} bytes ...`);
                                else
                                    this.logger.trace(`fetching headers of message ${seqno} (${info.size} bytes) ...`);
                                stream.on('data', function(chunk) {
                                    if (info.which === 'TEXT')
                                        body += chunk.toString('utf8');
                                    else
                                        header += chunk.toString('utf8');
                                });
                            });
                            message.once('end', async function(/*stream, info*/) {
                                let headers = Imap.parseHeader(header);
                                // console.log(`headers: ${inspect(headers)}`);
                                if (headers.subject && headers.subject.length > 0) {
                                    if (headers.subject[0].startsWith('ACME: ')) {
                                        if (headers['authentication-results'] && headers['authentication-results'].length > 0) {
                                            if (/dkim=pass/.test(headers['authentication-results'][0]))
                                                this.logger.debug(`found dkim=pass in "${headers['authentication-results'][0]}`);
                                            else
                                                this.logger.warn(`missing dkim=pass in "${headers['authentication-results'][0]}`);
                                            /* Check d= and throw error on permfail */
                                        }
                                    }
                                }
                                /* Extract challenge */
                                let challenge = {
                                    verified: false,
                                    from: headers.from,
                                    to: headers.to,
                                    token: headers.subject[0].substring(6)
                                };

                                /* Try to verify DKIM signature */
                                const result = await dkimVerify(header + body);
                                challenge.from = result.headerFrom[0];
                                for (let entry of result.results) {
                                    if (result.headerFrom[0].endsWith(`@${entry.signingDomain}`) && entry.status.result === 'pass') {
                                        let receivedHeaders = Object.keys(headers);
                                        const requireSignature = new Set([
                                            'from', 'to', 'subject', 'date', 'sender', 'reply-to', 'cc',
                                            'in-reply-to', 'references', 'message-id', 'auto-submitted',
                                            'content-type', 'content-transfer-encoding'
                                        ]);
                                        let requiredHeaders = receivedHeaders.filter((key) => requireSignature.has(key));
                                        let signedHeaders = entry.signingHeaders.keys.split(': ').map((key) => key.toLowerCase());
                                        if (requiredHeaders.filter(field => !signedHeaders.includes(field)).length === 0)
                                            challenge.verified = true;
                                        else
                                            this.logger.warn('Found unsigned headers in incoming email');
                                    }
                                }
                                if (!challenge.verified)
                                    this.logger.warn(`DKIM verification failed: ${inspect(result, { depth: 4 })}`);

                                /* TODO: Verify S/MIME signature here
                                simpleParser(header + body, { skipTextToHtml: true })
                                    .then((parsed) => {
                                        console.log(`parsed: ${inspect(parsed)}`);
                                    })
                                    .catch((error) => {
                                        console.log(`Cannot parse incoming email (${error})`);
                                    })
                                    .finally(() => {
                                        subjectHandler(challenge.verified ? undefined : "Can't verify challenge", challenge);
                                    }); */
                                if (!challenge.verified && options?.verification === false) {
                                    this.logger.warn('Overriding missing challenge verification');
                                    challenge.verified = true;
                                }
                                if (messageHandler != undefined)
                                    messageHandler(header + body);

                                subjectHandler(challenge.verified ? undefined : "Can't verify challenge", challenge);
                            });
                        });
                        f.once('end', function() {
                            imap.end();
                        });
                    });
                });
            });
        }
    }
    async sendReply(error, challenge) {
        this.logger.info(`Challenge (via email): ${inspect(challenge)}`);
        if (error)
            throw new Error(`Challenge received (${error})`);
        let part1 = Buffer.from(challenge.token, 'base64');
        let index = this.problems.findIndex(problem => problem.authorization.identifier.value == challenge.to);
        if (index == -1)
            throw new Error('Cannot find problem for challenge received');
        let problem = this.problems[`${index}`];
        this.problems.splice(index, 1);

        let from;
        if (typeof challenge.from === "string")
            from = challenge.from;
        else if (Array.isArray(challenge.from)) {
            if (challenge.from.length != 1)
                 throw new Error(`Got more than one sender address (${JSON.stringify(challenge.from)})`);
            from = challenge.from[0];
        }
        if (from !== problem.challenge.from) {
            this.logger.debug(`from: "${from}" is not problem.challenge.from: "${problem.challenge.from}"`);
            throw new Error('The from header of the email does not match the from sent by challenge');
        }
        let part2 = Buffer.from(problem.challenge.token, 'base64url');

        let token = Buffer.concat([part1, part2]);
        let thumbprint = await createJwkThumbprint(this.key.jwk);
        let keyAuthorization = `${token.toString('base64url')}.${thumbprint}`;

        let hash = crypto.createHash('sha256');
        hash.update(keyAuthorization);
        let response = '-----BEGIN ACME RESPONSE-----\r\n'
                     + hash.digest('base64url') + '\r\n'
                     + '-----END ACME RESPONSE-----\r\n'

        this.logger.trace(`sending to ${challenge.to} from ${problem.challenge.from} with\n${response}`);
        this.smtp.sendMail({
            from: challenge.to, // problem.authorization.identifier.value?
            to: problem.challenge.from,
            subject: `Re: ACME: ${part1.toString('base64url')}`,
            text: response,
        });
        problem.solved = true;
        return true;
    }

    constructor(imap, smtp, options) {
        this.type = 'email-reply-00';
        this.problems = [];
        this.logger = options?.logger ?? new ConsoleLogger();
        this.options = options;
        this.smtp = smtp;
        this.imap = imap;
        this.setupImap(imap, options, (eml) => { this.messageHandler(eml); }, (error, challenge) => { this.sendReply(error, challenge); });
    }

    messageHandler(eml) {}

    solve(key, authorization, challenge, timeout=12 * 1000) {
        this.key = key;
        this.problems.push({authorization: authorization, challenge: challenge});
        this.timer = setTimeout(() => {
            this.timer = undefined;
            this.logger.debug('Connecting to IMAP server ...');
            this.imap.connect();
        }, timeout);
    }

    stop() {
        try {
            if (this.timer) {
                clearTimeout(this.timer);
                this.timer = undefined;
            }
            this.imap.end();
        }
        catch {}
    }
}

function signMessage(key, json) {
    let protectedB64u = Buffer.from(JSON.stringify(json.protected), 'utf8').toString('base64url');
    let payloadStr;
    if (typeof json.payload === 'string')
        payloadStr = json.payload;
    else
        payloadStr = JSON.stringify(json.payload);
    let payloadB64u;
    if (payloadStr === "")
        payloadB64u = "";
    else
        payloadB64u = Buffer.from(payloadStr, 'utf8').toString('base64url');
    let tbs = protectedB64u + '.' + payloadB64u;

    let hash;
    switch (key.alg) {
        case 'RS256': case 'ES256': case 'PS256': hash = 'sha256'; break;
        case 'RS384': case 'ES384': case 'PS384': hash = 'sha384'; break;
        case 'RS512': case 'ES512': case 'PS512': hash = 'sha512'; break;
        default:
            throw new Error(`Unkown key algorithm (${key.alg})`);
    }
    let signer = crypto.createSign(hash);
    signer.update(tbs);
    let sigBuf = signer.sign(key.privateKey);

    let sigHex = sigBuf.toString('hex');

    let offset = sigHex[2] === '8' ? 8 : 6;

    let rLengthHex = sigHex.substring(offset, offset + 2);
    let rLength = Number.parseInt(rLengthHex, 16);

    //let sLengthHex = sigHex.substring(10 + (rLength * 2), 12 + (rLength * 2));
    //let sLength = Number.parseInt(sLengthHex, 16);
    let r = sigHex.substring(offset + 2, offset + 2 + (rLength * 2));
    while (r.startsWith('00'))
        r = r.substring(2);
    let s = sigHex.substring(offset + 6 + (rLength * 2));
    while (s.startsWith('00'))
        s = s.substring(2);

    while (r.length < s.length)
        r = '0' + r;
    while (s.length < r.length)
        s = '0' + s;

    let signature = Buffer.from(r + s, 'hex').toString('base64url');
    let content = {
        'protected': protectedB64u,
        payload: payloadB64u,
        signature: signature
    };
    return content;
}

async function signWithHMAC(uri, jwk, eab) {

    let base = new url.URL(uri);
    let protectedStr = JSON.stringify({
        alg: "HS256",
        kid: eab.kid,
        url: `${base.protocol}//${base.hostname}${base.port && base.port !== 443 ? ':' + base.port : ''}${uri.pathname}`
    });
    let protectedB64u = Buffer.from(protectedStr, 'utf8').toString('base64url');
    let payload = JSON.stringify(jwk);
    let payloadB64u = Buffer.from(payload, 'utf8').toString('base64url');

    let eabAlgorithm = {name: 'HMAC', hash: {name: 'SHA-256'}};
    let eabKeyBuf = Buffer.from(eab.hmac, 'base64url');
    return await subtle.importKey('raw', eabKeyBuf, eabAlgorithm, true, ['sign', 'verify'])
        .then((key) => {
            let tbs = `${protectedB64u}.${payloadB64u}`;
            return subtle.sign(eabAlgorithm, key, new TextEncoder().encode(tbs));
        })
        .then((signature) => {
            let sigBuf = Buffer.from(new Uint8Array(signature), 'binary');
            let externalAccountBinding = {
                "protected": protectedB64u,
                "payload": payloadB64u,
                "signature": sigBuf.toString('base64url')
            };
            //console.log(`externalAccountBinding: ${JSON.stringify(externalAccountBinding)}`);
            return externalAccountBinding;
        })
        .catch((error) => {
            console.log(`error signing with eab.key (${error})`);
            throw error;
        });
}

export class Client {
    constructor(uri) {
        this.base = new url.URL(uri);
        this.prefix = prefix(`${this.base.protocol}//${this.base.hostname}:${this.base.port}`);
        this.nonces = [];
    }
    getDirectory(responseHandler) {
        this.nonces = [];
        let ret = request.get(this.base.pathname)
                         .use(this.prefix);
        if (typeof responseHandler === 'function') {
            ret.end((error, response) => {
                if (response?.headers['replay-nonce'])
                   this.nonces.push(response.headers['replay-nonce']);
                if (!error)
                   this.directory = response.body;
                responseHandler(error, this.directory);
            });
        }
        else {
            return ret.then((response) => {
                if (response.headers['replay-nonce'])
                   this.nonces.push(response.headers['replay-nonce']);
                this.directory = response.body;
                return response.body;
            });
        }
    }
    newNonce(responseHandler) {
        let uri = new url.URL(this.directory.newNonce);
        let req = request.head(uri.pathname)
                         .use(this.prefix);
        if (typeof responseHandler === 'function') {
            req.end((error, response) => {
                if (response?.headers['replay-nonce'])
                    this.nonces.push(response.headers['replay-nonce']);
                responseHandler(error, response);
            });
        }
        else {
            return req.then((response) => {
                let nonce = response.headers['replay-nonce'];
                if (nonce)
                    this.nonces.push(nonce);
                return nonce;
            });
        }
    }
    async sendMessage(uri, message, responseHandler) {
        let dest = new url.URL(uri);
        let req = request.post(dest.pathname + (dest.search ?? ''))
                         .use(this.prefix)
                         .set('Content-Type', 'application/jose+json')
                         .send(JSON.stringify(message))
                         .buffer();
        if (typeof responseHandler === 'function') {
            req.end((error, response) => {
                if (response?.headers['replay-nonce'])
                    this.nonces.push(response.headers['replay-nonce']);
                responseHandler(error, response);
            });
        }
        else {
            return new Promise((resolve, reject) => {
                req.end((error, response) => {
                    if (response?.headers['replay-nonce'])
                        this.nonces.push(response.headers['replay-nonce']);
                    if (error)
                        reject(error);
                    else
                        resolve(response);
                });
            });
        }
    }
    request(uri, key, payload, responseHandler) {
        let url = `${this.base.protocol}//${this.base.hostname}${this.base.port && this.base.port !== 443 ? ':' + this.base.port : ''}${uri.pathname}`;
        let message = {
            'protected': {
                alg: key.alg,
                jwk: key.jwk,
                nonce: this.nonces.pop(),
                url
            },
            'payload': payload
        };
        let signed = signMessage(key, message);
        return this.sendMessage(message.protected.url, signed, responseHandler);
    }
    async createKey(key, options) {
        if (key === undefined) {
            let defaults = {
                type: 'ec',
                modulusLength: 3072,
                publicExponent: 0x8020001,
                hashAlgorithm: 'sha256',
                mgf1HashAlgorithm: 'sha256',
                namedCurve: 'prime256v1'
            }
            if (options)
                Object.assign(defaults, options);
            key = await crypto.generateKeyPairSync(defaults.type, defaults);
            key.jwk = key.publicKey.export({type: 'pkcs1', format: 'jwk'});

            /* Set JWS "alg" Header Parameter Value for JWS */
            switch (defaults.type) {
                case 'ec':
                    switch (defaults.namedCurve) {
                        case 'prime256v1': key.alg = 'ES256'; break;
                        case 'secp384r1': key.alg = 'ES384'; break;
                        case 'secp521r1': key.alg = 'ES512'; break;
                        default:
                            throw new Error(`Missing or unknown named curve (${defaults.namedCurve})`);
                    }
                    break;
                case 'rsa':
                    key.alg = defaults.modulusLength >= 4096 ? 'RS512' : 'RS256';
                    break;
                case 'rsa-pss':
                    switch (defaults.hashAlgorithm) {
                        case 'sha256': key.alg = 'PS256'; break;
                        case 'sha384': key.alg = 'PS384'; break;
                        case 'sha512': key.alg = 'PS512'; break;
                        default:
                            throw new Error(`Missing or unknown hashAlgorithm (${defaults.hashAlgorithm})`);
                    }
                    break;
                case 'ed25519':
                case 'ed448':
                    key.alg = 'EdDSA';
                    break;
                default:
                    throw new Error(`Missing or unknown algorhm type (${defaults.type})`);
            }
        }
        else {
            //key.pkcs8 = key.privateKey.export({type: 'pkcs8', format: 'pem'});
            key.jwk = key.publicKey.export({type: 'pkcs1', format: 'jwk'});
        }
        return key;
    }
    async newAccount(key, eab, payload, responseHandler) {
        if (this.directory === undefined) {
            this.directory = await this.getDirectory();
        }
        if (this.nonces.length === 0)
            await this.newNonce();

        let uri = new url.URL(this.directory.newAccount);
        let _payload = {
            termsOfServiceAgreed: true,
            contact: []
        };
        if (!payload)
            payload = _payload;
        if (eab)
            payload.externalAccountBinding = await signWithHMAC(uri, key.jwk, eab);
        if (typeof responseHandler === 'function') {
            this.request(uri, key, payload, (error, response) => {
                if (error) {
                    if (error.response?.res?.text.includes('"urn:'))
                        error = JSON.parse(error.response.res.text);
                    responseHandler(error);
                }
                else {
                    let account = new Account(this, response.headers['location'], key);
                    account.orders = response.body?.orders;
                    responseHandler(error, account);
                }
            });
        }
        else {
            return new Promise((resolve, reject) => {
                this.request(uri, key, payload, (error, response) => {
                    if (error) {
                        if (error.response?.res?.text.includes('"urn:'))
                            error = JSON.parse(error.response.res.text);
                        reject(error);
                    }
                    else {
                        let account = new Account(this, response.headers['location'], key);
                        account.orders = response.body?.orders;
                        resolve(account);
                    }
                });
            });
        }
    }
    async recoverAccount(key, responseHandler) {
        return this.newAccount(key, undefined, {'onlyReturnExisting': true}, responseHandler);
    }
    async revokeCert(key, cert, reason, responseHandler) {
        if (this.directory === undefined) {
            this.directory = await this.getDirectory();
        }
        let uri = new url.URL(this.directory.revokeCert);
        let pem = cert.toString();
        let b64 = pem.replaceAll(/-{5}(BEGIN|END) CERTIFICATE-{5}[\n\r]?/g, '');
        let b64u = Buffer.from(b64, 'base64').toString('base64url');
        let payload = {'certificate': b64u, 'reason': reason};
        if (typeof responseHandler === 'function') {
            this.request(uri, key, payload, responseHandler);
        }
        else {
            return new Promise((resolve, reject) => {
                this.request(uri, key, payload, (error, response) => {
                    if (error)
                        reject(error);
                    else
                        resolve(response.body);
                });
            });
        }
    }
}

export class Account {
    constructor(client, kid, key, responders, options) {
        this.client = client;
        this.kid = new url.URL(kid);
        this.key = key;
        this.responders = responders;
        this.logger = options?.logger ?? new ConsoleLogger();
    }
    static async create(uri, responders, key, options) {
        let client = new Client(new url.URL(uri));
        let acmeKey = await client.createKey(key, options?.key);
        let account = await client.newAccount(acmeKey, options?.eab);
        account.responders = responders;
        if (options?.logger) account.logger = options.logger;
        return account;
    }
    request(uri, payload, responseHandler) {
        let url = `${this.client.base.protocol}//${this.client.base.hostname}${this.client.base.port && this.client.base.port !== 443 ? ':' + this.client.base.port : ''}${uri.pathname}`;
        if (uri.search)
            url += uri.search;
        let message = {
            'protected': {
                alg: this.key.alg,
                kid: this.kid,
                nonce: this.client.nonces.pop(),
                url
            },
            'payload': payload
        };
        let signed = signMessage(this.key, message);
        return this.client.sendMessage(message.protected.url, signed, responseHandler);
    }
    sendPostAsGet(uri, responseHandler) {
        uri = new url.URL(uri);
        if (typeof responseHandler === 'function') {
            return this.request(uri, '', (error, response) => {
                return responseHandler(error, response?.body);
            });
        }
        else {
            return this.request(uri, '')
                       .then((response) => {
                           return response.body;
                       });
        }
    }
    deactivate(responseHandler) {
        let message = {status: "deactivated"};
        if (typeof responseHandler === 'function') {
            this.request(this.kid, message, (error, response) => {
                this.status = response.body.status;
                this.orders = response.body.orders;

                responseHandler(error, this);
            });
        }
        else {
            return this.request(this.kid, message)
                       .then((response) => {
                           this.status = response.body.status;
                           this.orders = response.body.orders;
                           return this;
                       });
        }
    }
    newAuthz(identifier, responseHandler) {
        let uri = new url.URL(this.client.directory.newAuthz);
        let message = { notBefore: new Date().toISOString(),
                        notAfter: new Date(Date.now() + 365 * 24 * 60 * 60 * 1000).toISOString(),
                        identifier };
        if (typeof responseHandler === 'function') {
            this.request(uri, message, (error, response) => {
                             let authorization = response.body;
                             authorization.url = response.headers['location'];
                             responseHandler(error, authorization);
                         });
        }
        else {
            return this.request(uri, message)
                       .then((response) => {
                           let authorization = response.body;
                           authorization.url = response.headers['location'];
                           return authorization;
                       });
        }
    }
    newOrder(identifiers, validity, responseHandler) {
        let message = {
            'notBefore': validity?.notBefore,
            'notAfter': validity?.notAfter,
            identifiers
        };
        return this._newOrder(message, responseHandler);
    }
    newStarOrder(identifiers, validity, renewal, responseHandler) {
        const defaults = {
            'start-date': new Date().toISOString(),
            'end-date': new Date(Date.now() + 30 * 24 * 60 * 60 * 1000).toISOString(),
            lifetime: 86400
        };
        if (renewal)
            Object.assign(defaults, renewal);
        let message = {
            identifiers,
            'notBefore': validity?.notBefore,
            'notAfter': validity?.notAfter,
            'auto-renewal': defaults
        };
        return this._newOrder(message, responseHandler);
    }
    _newOrder(message, responseHandler) {
        let uri = new url.URL(this.client.directory.newOrder);
        if (typeof responseHandler === 'function') {
            this.request(uri, message, (error, response) => {
                             let order = response.body;
                             order.url = response.headers['location'];
                             responseHandler(error, order);
                         });
        }
        else {
            return this.request(uri, message)
                       .then((response) => {
                           let order = response.body;
                           order.url = response.headers['location'];
                           return order;
                       });
        }
    }
    cancelOrder(order, responseHandler) {
        let uri = new url.URL(order.url);
        const message = {status: 'canceled'};
        if (typeof responseHandler === 'function') {
            this.request(uri, message, (error, response) => {
                let order = response.body;
                order.url = response.headers['location'];
                responseHandler(error, order);
            });
        }
        else {
            return this.request(uri, message)
                       .then((response) => {
                           let order = response.body;
                           order.url = response.headers['location'];
                           return order;
                       });
        }
    }
    finalizeOrder(order, csr, attestation, responseHandler) {
        let b64 = csr.replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
        let chain = attestation ? attestation.map(cert =>
            Buffer.from(
                cert.replaceAll(/(-{5}(BEGIN|END) CERTIFICATE-{5}|[\n\r])/g, ''),
                'base64'
            ).toString('base64url')
        ) : undefined;
        let payload = {
           attestation: chain,
           csr: Buffer.from(b64, 'base64').toString('base64url')
        }
        let uri = new url.URL(order.finalize);
        function getAlternates(response) {
            let alternates = {};
            let header = response.headers['link'];
            if (header) {
                let links = header.split(', ');
                for (let link of links) {
                    let rel = link.split(';rel=');
                    if (rel[1] === '"alternate"') {
                        let href = rel[0].replaceAll(/[<>]/g, '');
                        let uri = new url.URL(href);
                        let chain = uri.searchParams.get('chain');
                        alternates[`${chain}`] = uri.toString();
                    }
                }
            }
            return alternates;
        }
        if (typeof responseHandler === 'function') {
            this.request(uri, payload, (error, response) => {
                if (response?.body)
                    response.body.alternates = getAlternates(response);
                responseHandler(error, response?.body);
            });
        }
        else {
            return this.request(uri, payload)
                       .then((response) => {
                           if (response?.body)
                               response.body.alternates = getAlternates(response);
                           return response?.body;
                       });
        }
    }
    retrieveCertificate(order, chain, responseHandler) {
        let certificateUrl;
        if (chain) {
            if (order?.alternates[`${chain}`])
                certificateUrl = order.alternates[`${chain}`];
            else
                throw new Error(`Cannot find certificate chain (${chain})`);
        }
        else {
            certificateUrl = order.certificate ?? order['star-certificate'];
        }
        let uri = new url.URL(certificateUrl);

        this.request(uri, {}, (error, response) => {
            let certs = [];
            if (error) {
                if (error.response?.text?.contains('urn:'))
                    responseHandler(JSON.parse(error.res.text));
                else
                    responseHandler(error);
            }
            else {
                let candidates = response.text.toString('binary').split(
                    /(?<=-{5}END CERTIFICATE-{5})[\n\r]+/gm);
                for (let candidate of candidates) {
                    try {
                        let cert = new crypto.X509Certificate(candidate);
                        certs.push(cert);
                    }
                    catch { /* nothing */ }
                }
                responseHandler(null, certs);
            }
        });
    }
    revokeCertificate(certificate, reason = 4, responseHandler) {
        let b64 = certificate.replace(/^-{5}.*$/m, '');
        let payload = {
            reason: reason,
            certificate: Buffer.from(b64, 'base64').toString('base64url')
        }
        let uri = new url.URL(this.client.directory.revokeCert);
        return this.request(uri, payload, responseHandler);
    }
    listOrders(responseHandler) {
        let uri = new url.URL(this.orders);
        if (typeof responseHandler === 'function') {
            this.request(uri, {}, (error, response) => {
                responseHandler(error, response?.body?.orders);
            });
        }
        else {
            return this.request(uri, {})
                       .then((response) => {
                           return response?.body?.orders;
                       });
        }
    }
    getOrder(order, responseHandler) {
        let uri = new url.URL(order);
        if (typeof responseHandler === 'function') {
            this.request(uri, {}, (error, response) => {
                responseHandler(error, response?.body);
            });
        }
        else {
            return this.request(uri, {})
                       .then((response) => {
                           return response?.body;
                       });
        }
    }
    async solve(authorization, challenge, resultHandler) {
        for (let responder of this.responders) {
            if ((typeof responder.type === 'string' && responder.type == challenge.type)
              || (responder.type.includes(challenge.type))) {
                try {
                    await responder.solve(this.key, authorization, challenge);
                }
                catch (error) {
                    this.logger.error(`responder failed: ${error}`);
                }
                break;
            }
        }
        try {
            await this.pollChallenge(challenge, resultHandler);
        }
        catch (error) {
            this.logger.error(`querying challenge failed: ${error}`);
            resultHandler(error, null);
        }
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    async poll(ms, count, check) {
        let iterations;
        for (iterations=0; iterations<count && ! await check(iterations); iterations++) {
            await this.sleep(ms);
        }
        if (iterations>=count)
            throw new Error(`Timed out after ${iterations} tries`);
    }

    async pollChallenge(challenge, responseHandler) {
        await this.poll(14 * 1000, 24, async (counter) => {
            let response = await this.sendPostAsGet(challenge.url);
            switch (response?.status) {
                case 'valid':
                    responseHandler(null, response);
                    return true;
                case 'invalid':
                    responseHandler(new Error(`Challenge is invalid (after ${counter} tries)`));
                    return true;
                case 'pending':
                default:
                    break;
            }
            return false;
        });
    }
    activateChallenge(challenge, responseHandler) {
        let uri = new url.URL(challenge.url);
        if (typeof responseHandler === 'function') {
            this.request(uri, {}, (error, response) => {
                return responseHandler(error, response?.body);
            });
        }
        else {
            return this.request(uri, {})
                       .then((response) => {
                           return response.body;
                       });
        }
    }
    getChallenge(challenge, responseHandler) {
        return this.sendPostAsGet(challenge.url, responseHandler);
    }
    async solveOne(authorization) {
        let solvers = [];
        for (let responder of this.responders) {
            for (let challenge of authorization.challenges) {
                if ((typeof responder.type === 'string' && responder.type === challenge.type)
                  || (responder.type.includes(challenge.type))) {
                    solvers.push(new Promise((resolve, reject) => {
                        try {
                            responder.solve(this.key, authorization, challenge);
                            this.logger.debug(`Setup challenge responder ${responder.constructor.name}`);
                            this.activateChallenge(challenge, (error, challenge) => {
                                if (error) {
                                    this.logger.warn('challenge failed');
                                    reject(error);
                                }
                                else {
                                    resolve(challenge);
                                }
                            });
                        }
                        catch (error) {
                            this.logger.error(`Error: ${error}`);
                            reject(new Error(`challenge couldn't be solved (${error})`));
                        }
                    }));
                    authorization.handled = true;
                    break;
                }
            }
            if (authorization.handled) {
                this.logger.info(`authorization for identifier {type: "${authorization.identifier.type}", value: "${authorization.identifier.value}"} handled`);
                break;
            }
        }
        return Promise.all(solvers);
    }

    preAuthorize(identifier, responseHandler) {
        this.client.newNonce((error) => {
            if (error) throw error;
            this.newAuthz(identifier, (error, preAuthorization) => {
                if (error) {
                    return responseHandler(error);
                }
                else {
                    this.solveOne(preAuthorization)
                        .then((/* challenge */) => {
                            return this.sendPostAsGet(preAuthorization.url);
                        })
                        .then((authorization) => {
                            return responseHandler(null, authorization);
                        })
                        .catch((error) => {
                            this.logger.error(`got error solving challenges (${error})`);
                            return responseHandler(error);
                        });
                }
            });
        });
    }
    getCertificate(identifiers, validity, renewal, csr, chain, attestation, responseHandler) {
        let orderUrl;
        let account = this;
        this.client.newNonce()
            .then(() => {
                if (renewal)
                    return this.newStarOrder(identifiers, validity, renewal);
                else
                    return this.newOrder(identifiers, validity);
            })
            .then((order) => {
                orderUrl = order.url;
                return this.authorizeOrder(order);
            })
            .then((/* authorization */) => {
                return this.sendPostAsGet(orderUrl);
            })
            .then((/* order */) => {
                return new Promise(function (resolve, reject) {
                    (async function pollOrder() {
                        let order = await account.sendPostAsGet(orderUrl);
                        if (order.status === 'ready')
                            return resolve(order);
                        else if (order.status === 'invalid')
                            return reject(new Error(`Order failed, order status is ${order.status}`));
                        setTimeout(pollOrder, 1000);
                    })();
                });
            })
            .then((order) => {
                return this.finalizeOrder(order, csr, attestation);
            })
            .then((order) => {
                if (order['star-certificate'] && order['auto-renewal']['allow-certificate-get'])
                    this.logger.info(`STAR certificate URL for GET is ${order['star-certificate']}`);
                return this.retrieveCertificate(order, chain, responseHandler);
            })
            .catch((error) => {
                if (error.response?.res?.text.includes('"urn:'))
                    error = JSON.parse(error.response.res.text);
                this.logger.error(`cannot get certificate (${inspect(error)})`);
                if (responseHandler)
                    responseHandler(error);
                else
                    throw error;
            });
    }
    async authorizeOrder(order, responseHandler) {
        let authorizations = [];
        for (let authorizationUrl of order.authorizations) {
            authorizations.push(new Promise((resolve, reject) => {
                let solvers = [];
                solvers.push(new Promise((resolve, reject) => {
                    this.sendPostAsGet(authorizationUrl)
                        .then((authorization) => {
                            authorization.url = authorizationUrl;
                            return this.solveOne(authorization);
                        })
                        .then((challenge) => {
                            this.logger.info(`prepared challenge: ${inspect(challenge, null, 4)}`);
                            return this.sendPostAsGet(authorizationUrl);
                        })
                        .then((authorization) => {
                            return resolve(authorization);
                        })
                        .catch((error) => {
                            this.logger.error(`got error solving challenges: ${error}`);
                            return reject(error);
                        });
                }));
                Promise.all(solvers)
                       .then((authorizations) => {
                           return resolve(authorizations);
                       })
                       .catch((error) => {
                           return reject(error);
                       });
            }));
        }
        if (typeof responseHandler === 'function') {
            Promise.all(authorizations)
                .then((authorizations) => {
                    return responseHandler(null, authorizations);
                })
                .catch((error) => {
                    return responseHandler(error);
                });
        }
        else {
            return Promise.all(authorizations);
        }
    }
}

export default {
    createJwkThumbprint,
    Account,
    Client,
    DnsProxyChallengeResponder,
    EmailChallengeResponder,
    HttpServerChallengeResponder,
    ManualChallengeResponder,
    ShellChallengeResponder,
    TlsAlpnChallengeResponder,
    WebrootChallengeResponder
};
