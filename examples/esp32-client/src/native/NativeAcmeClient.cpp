#include <cstdlib>
#include <exception>
#include <iostream>

#include <unistd.h>

#include <curl/curl.h>
#include <mbedtls/version.h>

#include <Acme.hpp>

namespace {
    // const constexpr char ACME_SERVER[] = "https://acme.local/acme/Sub/directory";
    // const constexpr char ACME_SERVER[] = "https://acme-v02.api.letsencrypt.org/directory";
    const constexpr char ACME_SERVER[] = "https://acme-staging-v02.api.letsencrypt.org/directory";
    // const constexpr char ACME_SERVER[] = "https://acme.zerossl.com/v2/DV90";
    // const constexpr char ACME_SERVER[] = "https://api.buypass.com/acme/directory";
    // const constexpr char ACME_SERVER[] = "https://acme.ssl.com/sslcom-dv-ecc";

    const constexpr char ACME_CLIENT_ADDRESS[] = "no-reply@platynum.ch";

    const constexpr char ACME_ACCOUNT_KEYFILE[] = "account.key.pem";
    const constexpr mbedtls_pk_type_t ACME_ACCOUNT_KEY_ALG = MBEDTLS_PK_ECKEY;

    const constexpr char ACME_TLS_KEYFILE[] = "tls.key.pem";
    const constexpr mbedtls_pk_type_t ACME_TLS_KEY_ALG = MBEDTLS_PK_ECKEY;
    const constexpr char ACME_TLS_CERTIFICATE[] = "tls.crt.pem";

    const constexpr char ACME_CHALLENGE_PATH[] = "/var/www";
}

class FileWebServer : public Acme::WebServer {
    protected:
        String path, challenge {};
    public:
        explicit FileWebServer(String path_) :
            path{std::move(path_)} {
            std::cout << "Storing challenges beneath " << path << std::endl;
        }
        ~FileWebServer() override = default;
        auto enable(const String &uri, const String &content) -> bool override {
            if (challenge != "")
                disable();
            challenge = path + uri;
            std::cout << "Enable challenge " << challenge << std::endl;
            return Acme::File::write(
                challenge.c_str(),
                reinterpret_cast<const unsigned char *>(content.c_str()),
                content.length());
        };
        auto disable() -> bool override {
            std::cout << "Deleting challenge " << challenge << std::endl;
            return unlink(challenge.c_str()) == 0;
        }
};

class NativeHttpsClient : public Acme::HttpsClient {
        CURL *curl;
        static size_t content_callback(char *ptr, size_t size, size_t nmemb, String *str) noexcept
        {
            size_t length = size*nmemb;
            std::cout << static_cast<void *>(ptr) << "[" << length << "]: " << str << std::endl;
            try {
                str->append(ptr, length);
                // std::cout << str->c_str();
            }
            catch (std::bad_alloc &e) {
                return 0;
            }
            return length;
        }
    public:
        NativeHttpsClient([[maybe_unused]] const char *root_certificates)
        {
            if ((curl = curl_easy_init()) == nullptr)
                throw std::bad_alloc();
            char *verbose = getenv("CURL_VERBOSE");
            if (verbose != nullptr && strcmp(verbose, "1") == 0)
                curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
            curl_easy_setopt(curl, CURLOPT_USERAGENT, Acme::version);
        }
        ~NativeHttpsClient() override
        {
            if (curl != nullptr)
                curl_easy_cleanup(curl);
        }
        auto request_nonce(const String &url) -> String override
        {
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "HEAD");
            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);

            if (CURLcode res = curl_easy_perform(curl); res != 0) {
                std::cout << "res: " << res << ", " << curl_easy_strerror(res);
                return {};
            }

            long code;
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &code);
            std::cout << "code: " << code << std::endl;
            if (code > 400)
                return {};

            struct curl_header *prev = nullptr, *header;
            while ((header = curl_easy_nextheader(curl, CURLH_HEADER, -1, prev)) != nullptr) {
                if (strcasecmp(header->name, "replay-nonce") == 0) {
                    nonce = header->value;
                    break;
                }
                prev = header;
            }
            std::cout << "nonce: " << nonce << std::endl;
            return std::move(nonce);
        }
        auto perform_query(const String &query, const String *topost, const char *accept_message) -> std::optional<String> override
        {
            std::cout << "query: " << query << ", topost: " << static_cast<const void *>(topost);
            if (accept_message)
                std::cout << ", accept_encoding: " << accept_message;
            std::cout << std::endl;

            location = "";
            retry_after = 0;

            curl_easy_setopt(curl, CURLOPT_URL, query.c_str());

            struct curl_slist *list = nullptr;
            if (accept_message != nullptr)
                list = curl_slist_append(list, (String("Accept: ") + accept_message).c_str());

            if (topost != nullptr) {
                curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
                list = curl_slist_append(list, "Content-Type: application/jose+json");
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, topost->c_str());
                curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, topost->length());
            }
            else {
                curl_easy_setopt(curl, CURLOPT_HTTPGET, NULL);
            }
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, content_callback);
            String content = "";
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &content);

            std::cout << "Starting HTTPS POST request " << query << " ..." << std::endl;
            CURLcode res = curl_easy_perform(curl);
            curl_slist_free_all(list);
            if (res != 0) {
                std::cout << "res: " << res << ", " << curl_easy_strerror(res) << std::endl;
                return std::nullopt;
            }
            std::cout << "content: " << content << std::endl;

            long code;
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &code);
            std::cout << "code: " << code << std::endl;
            if (code > 400)
                return {};

            struct curl_header *prev = nullptr, *header;
            while ((header = curl_easy_nextheader(curl, CURLH_HEADER, -1, prev)) != nullptr) {
                if (strcasecmp(header->name, "replay-nonce") == 0)
                    nonce = header->value;
                else if (strcasecmp(header->name, "location") == 0)
                    location = header->value;
                else if (strcasecmp(header->name, "retry-after") == 0)
                    retry_after = parse_retry_after(header->value);
                prev = header;
            }
            return make_optional(std::move(content));
        }
};

int main(int argc, char **argv)
{
    std::cout << "Running native ACME client "
              << curl_version()
              << " "
              << MBEDTLS_VERSION_STRING_FULL
              << std::endl;
#ifdef DEBUG
    std::cout << "DEBUG is enabled" << std::endl;
#endif
    if (argc < 2) [[unlikely]] {
        std::cout << "\n"
            "Usage: " << argv[0] << " domain [domain ...]" "\n"
            "\n"
            "Environment variables used:\n"
            "\n"
            " - CURL_VERBOSE" "\n"
            " - ACME_SERVER (default " << ACME_SERVER << ")" "\n"
            " - ACME_EAB_KID" "\n"
            " - ACME_EAB_KEY (base64 URL encoded)" "\n"
            " - ACME_CHALLENGE_PATH (default " << ACME_CHALLENGE_PATH << ")" "\n"
            " - ACME_CLIENT_ADDRESS (default " << ACME_CLIENT_ADDRESS << ")" "\n"
            << std::endl;
        exit(EXIT_FAILURE);
    }

    const char *acme_server = getenv("ACME_SERVER");
    if (acme_server == nullptr)
        acme_server = ACME_SERVER;

    std::cout << "Acquiring certificate from " << acme_server << " using " << Acme::version << std::endl;
    Acme::Client<NativeHttpsClient> acme(nullptr);

    const char *challenge_path = getenv("ACME_CHALLENGE_PATH");
    if (challenge_path == nullptr)
        challenge_path = ACME_CHALLENGE_PATH;
    acme.setWebServer(new FileWebServer(challenge_path));

    acme.setDirectoryUrl(strdup(acme_server));

    const char *eab_kid = getenv("ACME_EAB_KID");
    const char *eab_key = getenv("ACME_EAB_KEY");
    if (eab_kid != nullptr && eab_key != nullptr) {
        std::cout << "Using external account binding for " << eab_kid << std::endl;
        acme.setExternalAccountBinding(eab_kid, eab_key);
    }
    const char *client_address = getenv("ACME_CLIENT_ADDRESS");
    if (client_address == nullptr)
        client_address = ACME_CLIENT_ADDRESS;
    acme.setAccountEmail(client_address);

    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_ctr_drbg_init(&ctr_drbg);

    mbedtls_entropy_context entropy;
    mbedtls_entropy_init(&entropy);

    if (int err = mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy, nullptr, 0); err != 0)
        std::cout << "mbedtls_ctr_drbg_seed failed " << err << std::endl;
    Acme::PrivateKey *key = Acme::PrivateKey::load(ACME_ACCOUNT_KEYFILE);
    if (key == nullptr) {
        key = Acme::PrivateKey::generate(ACME_ACCOUNT_KEY_ALG, mbedtls_ctr_drbg_random, &ctr_drbg);
        // acme.account_key->store(ACME_ACCOUNT_KEYFILE);
    }
    acme.account_key = std::make_shared<Acme::PrivateKey>(*key);

    acme.certificate_key = acme.setup_key(ACME_TLS_KEY_ALG, ACME_TLS_KEYFILE);
    acme.setCertificateFilename(ACME_TLS_CERTIFICATE);

    acme.setDnsIdentifier(argv[1]);
    for (int i=2; i<argc; i++)
        acme.addDnsIdentifier(argv[i]);

    try {
        while (!acme.loop()) {
            (void) fflush(stdout);
            sleep(3);
        }
    }
    catch (const std::exception &err) {
        std::cerr << err.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);
}
