#include <Arduino.h>

#include <DNSServer.h>
#include <LittleFS.h>
#include <NTPClient.h>
#include <Update.h>
#include <WiFi.h>
#include <WiFiManager.h>
#include <mdns.h>

#include <ctime>

#include "esp_http_server.h"
#include "esp_https_server.h"
#include "esp_log.h"
#include "esp_tls.h"
#include "nvs_flash.h"

#include <mbedtls/version.h>

#include "Acme.hpp"
#include "every.hpp"

#define str(x) #x
#define xstr(x) str(x)

// Defaults for WiFiManager setup
static const String WIFI_MANAGER_SSID = "WLAN-ESP32C3";
static const String WIFI_MANAGER_PASS = "ESP32C3-PASSWORD";

// Default values only, used values can be set via WiFiManager
static const String WIFI_HOSTNAME = xstr(PROJECT_NAME);
// static const constexpr String WIFI_DOMAINNAME = "duckdns.org";
static const String WIFI_DOMAINNAME = "fritz.box";

// Configuration where Acme stores keys and TLS certificate
static const String ACME_FS_PREFIX = "/littlefs";
static const String ACME_ACCOUNT_KEYFILE = ACME_FS_PREFIX + "/account.key.pem";
static const constexpr mbedtls_pk_type_t ACME_ACCOUNT_KEY_ALG = MBEDTLS_PK_ECKEY;

static const String ACME_TLS_KEYFILE = ACME_FS_PREFIX + "/tls.key.pem";
static const constexpr mbedtls_pk_type_t ACME_TLS_KEY_ALG = MBEDTLS_PK_ECKEY;
static const String ACME_TLS_CERTIFICATE = ACME_FS_PREFIX + "/tls.crt.pem";

// Root certificates used to verify TLS connection to ACME server
extern const char cafile_pem_start[] asm("_binary_src_cafile_pem_start");
extern const char cafile_pem_end[] asm("_binary_src_cafile_pem_end");

// Single internal LED to signal system is running/working (ESP32-C3!)
static const constexpr int LED_INTERNAL = 8; // PIN#
static const constexpr int LED_INTERVAL = 200; // ms

// Instance to run ACME protocol to acquire a TLS certificate
static Acme::Client<Acme::Esp32HttpsClient> acme(cafile_pem_start);

// WiFiManager to configure WiFi and ACME settings
static WiFiManager wmgr;

// Sample configuration filled with WiFiManager and stored in NVS ("acme")
static struct acmeConfig {
    char directory[128], hostname[256], contact[128], eab_kid[128], eab_key[128];
} config = {
    "https://acme-staging-v02.api.letsencrypt.org/directory",
    "acme-client.dc-iot.my.corp",
    "no-reply@dc-iot.my.corp",
    "",
    ""
};

static auto serve_index_https_handler(httpd_req_t *req) -> esp_err_t
{
    httpd_resp_sendstr(req,
        "<!DOCTYPE html>"
        "<html lang='en'>"
            "<head>"
                "<meta charset='UTF-8' />"
                "<meta http-equiv=\"refresh\" content=\"5; url=/\" />"
                "<title>" xstr(PROJECT_NAME) "</title>"
            "</head>"
            "<body>"
                "<h1>It works!!!</h1>"
            "</body>"
        "</html>");
    return ESP_OK;
}

static const httpd_uri_t serve_index_https = {
    .uri = "/",
    .method = HTTP_GET,
    .handler = serve_index_https_handler,
    .user_ctx = nullptr
};
static httpd_handle_t webserver_https;
static httpd_ssl_config_t webserver_https_conf = HTTPD_SSL_CONFIG_DEFAULT();

static auto webserver_https_setup() -> httpd_handle_t
{
    delete webserver_https_conf.cacert_pem;
    delete webserver_https_conf.prvtkey_pem;

    webserver_https_conf = HTTPD_SSL_CONFIG_DEFAULT();
    webserver_https_conf.httpd.ctrl_port++;

    size_t len;
    webserver_https_conf.cacert_pem = Acme::File::read(ACME_TLS_CERTIFICATE.c_str(), &len);
    webserver_https_conf.cacert_len = len + 1;

    webserver_https_conf.prvtkey_pem = Acme::File::read(ACME_TLS_KEYFILE.c_str(), &len);
    webserver_https_conf.prvtkey_len = len + 1;

    httpd_handle_t server = nullptr;
    if (httpd_ssl_start(&server, &webserver_https_conf) == ESP_OK)
        httpd_register_uri_handler(server, &serve_index_https);

    // If httpd_ssl_start failed, handle will be nullptr
    return server;
}

static auto serve_index_http_handler(httpd_req_t *req) -> esp_err_t
{
    // When https was setup (ACME got a certificate), redirect to the real thing
    if (webserver_https != nullptr) {
        httpd_resp_set_hdr(req, "Location", ("https://" + WIFI_HOSTNAME + "." + WIFI_DOMAINNAME + "/").c_str());
        httpd_resp_set_status(req, "302 Found");
    }
    httpd_resp_sendstr(req,
        "<!DOCTYPE html>"
        "<html lang='en'>"
            "<head>"
                "<meta charset='UTF-8' />"
                "<meta http-equiv=\"refresh\" content=\"5; url=/\" />"
                "<title>" xstr(PROJECT_NAME) "</title>"
            "</head>"
            "<body>"
                "<h1>Working on it ...</h1>"
            "</body>"
        "</html>");
    return ESP_OK;
}

static const httpd_uri_t serve_index_http = {
    .uri = "/",
    .method = HTTP_GET,
    .handler = serve_index_http_handler,
    .user_ctx = nullptr
};
static httpd_handle_t webserver_http;

static auto webserver_http_setup() -> httpd_handle_t
{
    const httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    httpd_handle_t server = nullptr;
    if (httpd_start(&server, &config) == ESP_OK)
        httpd_register_uri_handler(server, &serve_index_http); // != ESP_OK, so what?

    // If server failed to start, handle will be nullptr
    return server;
}

static void service_restart() {
    printf(PSTR("(Re-)starting HTTPS webserver at https://%s/ ..."), config.hostname);
    (void) fflush(stdout);

    mdns_service_remove("_https", "_tcp");
    httpd_ssl_stop(webserver_https);

    if ((webserver_https = webserver_https_setup()) != nullptr) {
        mdns_service_add(nullptr, "_https", "_tcp", 443, nullptr, 0);
        printf(PSTR("ok\r\n"));
    }
    else {
        printf(PSTR("failed\r\n"));
    }
}

static auto rtc_setup() -> String
{
    WiFiUDP ntpUDP;
    NTPClient timeClient(ntpUDP, "pool.ntp.org");

    timeClient.begin();
    timeClient.update();
    for(int j=0; !timeClient.isTimeSet(); j++) {
        timeClient.update();
        delay(200);
    }
    String ret = timeClient.getFormattedTime() + " GMT";

    const struct timeval tv = {
        .tv_sec = static_cast<time_t>(timeClient.getEpochTime()),
        .tv_usec = 0
    };
    settimeofday(&tv, nullptr);

    timeClient.end();

    // TODO 🕐🌏: Set your local timezone here
    setenv("TZ", "KST-9", 1);
    tzset();

    return ret;
}

static void config_save()
{
    nvs_handle_t handle = 0;

    esp_err_t err = nvs_open("acme", NVS_READWRITE, &handle);
    if (err != 0) {
        printf(PSTR("nvs_open failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
        return;
    }
    if ((err = nvs_set_str(handle, "directory", config.directory)) != ESP_OK) {
        printf(PSTR("nvs_set_str failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    if ((err = nvs_set_str(handle, "contact", config.contact)) != ESP_OK) {
        printf(PSTR("nvs_set_str failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    if ((err = nvs_set_str(handle, "hostname", config.hostname)) != ESP_OK) {
        printf(PSTR("nvs_set_str failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    if ((err = nvs_set_str(handle, "eab_kid", config.eab_kid)) != ESP_OK) {
        printf(PSTR("nvs_set_str failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    if ((err = nvs_set_str(handle, "eab_key", config.eab_key)) != ESP_OK) {
        printf(PSTR("nvs_set_str failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    if ((err = nvs_commit(handle)) != ESP_OK) {
        printf(PSTR("nvs_commit failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    nvs_close(handle);
}

static auto config_load() -> bool
{
    nvs_handle_t handle = 0;

    esp_err_t err = nvs_open("acme", NVS_READONLY, &handle);
    if (err != 0) {
        printf(PSTR("nvs_open failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
        return false;
    }
    size_t length = sizeof(config.directory);
    if ((err = nvs_get_str(handle, "directory", config.directory, &length)) != ESP_OK) {
        printf(PSTR("nvs_get_str failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    length = sizeof(config.contact);
    if ((err = nvs_get_str(handle, "contact", config.contact, &length)) != ESP_OK) {
        printf(PSTR("nvs_get_str failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    length = sizeof(config.hostname);
    if ((err = nvs_get_str(handle, "hostname", config.hostname, &length)) != ESP_OK) {
        printf(PSTR("nvs_get_str failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    length = sizeof(config.eab_kid);
    if ((err = nvs_get_str(handle, "eab_key", config.eab_key, &length)) != ESP_OK) {
        printf(PSTR("nvs_get_str failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    length = sizeof(config.eab_key);
    if ((err = nvs_get_str(handle, "eab_kid", config.eab_kid, &length)) != ESP_OK) {
        printf(PSTR("nvs_get_str failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    nvs_close(handle);
    return true;
}

static auto acme_setup() -> bool
{
    printf(PSTR("Acquiring certificate from %s using %s\r\n"), config.directory, Acme::version);
    // Configure local http server to serve http-01 challenges
    acme.setWebServer(new Acme::Esp32WebServer(webserver_http));
    // Configure ACME server
    acme.setDirectoryUrl(config.directory);

    // Configure ACME account (with EAB if required)
    acme.setAccountEmail(config.contact);
    acme.account_key = acme.setup_key(ACME_ACCOUNT_KEY_ALG, ACME_ACCOUNT_KEYFILE);
    if (config.eab_kid[0] != '\0' && config.eab_key[0] != '\0') {
        printf(PSTR("Using external account binding for %s\r\n"), config.eab_kid);
        acme.setExternalAccountBinding(config.eab_kid, config.eab_key);
    }

    // Configure domains to request a certificate for
    acme.setDnsIdentifier(config.hostname);
    // acme.addDnsIdentifier("other" "." WIFI_DOMAINNAME);

    // Force a key/certificate renewal
    unlink(ACME_TLS_CERTIFICATE.c_str());
    unlink(ACME_TLS_KEYFILE.c_str());

    // Configure certificate and associated key
    acme.certificate_key = acme.setup_key(ACME_TLS_KEY_ALG, ACME_TLS_KEYFILE.c_str());
    // If there is a certificate, everything was done previously
    return acme.setCertificateFilename(ACME_TLS_CERTIFICATE.c_str()) != nullptr;

    // TLS certfificate and TLS key filename are setup, when Acme::Client::loop() returns true,
    // read them out to create an TLS server resource (or use it to authenticate a TLS client)
}

static inline void debug_print_memory()
{
    const UBaseType_t hwm = uxTaskGetStackHighWaterMark(nullptr);
    printf(PSTR("Stack high water mark: %li words\r\n"), hwm);
    heap_caps_print_heap_info(MALLOC_CAP_DEFAULT);
}

static bool wmgrSaveParametersFlag = false;
static void wmgrSaveParameters() {
    wmgrSaveParametersFlag = true;
}

void setup()
{
    Serial.begin(115200);
    while (!Serial) {}
#ifdef DEBUG
    delay(2000);
#endif

    pinMode(LED_INTERNAL, OUTPUT);
    digitalWrite(LED_INTERNAL, LOW);

    printf(PSTR("Starting " xstr(PROJECT_NAME) " (revision " xstr(GIT_COMMIT) " compiled " __DATE__ " " __TIME__ " using SDK %s)\r\n"),
        ESP.getSdkVersion());
    printf(PSTR("Running on %s/rev%i with %i MHz\r\n"),
        ESP.getChipModel(),
        ESP.getChipRevision(),
        ESP.getCpuFreqMHz());
    printf(PSTR("Using Arduino " xstr(ESP_ARDUINO_VERSION_MAJOR) "." xstr(ESP_ARDUINO_VERSION_MINOR) "." xstr(ESP_ARDUINO_VERSION_PATCH) "\r\n"));
    printf(PSTR("Using " MBEDTLS_VERSION_STRING_FULL "\r\n"));

#if 0
    printf(PSTR("Erasing NVS flash ..."));
    (void) fflush(stdout);
    if (nvs_flash_erase() == ESP_OK)
        printf(PSTR("ok\r\n"));
    else
        printf(PSTR("failed\r\n"));
#endif

    printf(PSTR("Setup LittleFS beneath %s ... "), ACME_FS_PREFIX.c_str());
    (void) fflush(stdout);
    if (LittleFS.begin(true, ACME_FS_PREFIX.c_str()))
        printf(PSTR("ok\r\n"));
    else
        printf(PSTR("failed\r\n"));

    printf(PSTR("Loading configuration from NVS ... "));
    (void) fflush(stdout);
    if (config_load())
        printf(PSTR("ok\r\n"));
    else
        printf(PSTR("failed\r\n"));

    // Delete WiFiManger configuration
    //wmgr.resetSettings();

    // Initialize "WiFi" here, so that wm.getWiFiIsSaved() returns something useful
    const wifi_init_config_t wic = WIFI_INIT_CONFIG_DEFAULT();
    esp_wifi_init(&wic);

    WiFiManagerParameter wmgrAcmeDirectory("directory", "ACME directory", config.directory, sizeof(config.directory));
    wmgr.addParameter(&wmgrAcmeDirectory);
    WiFiManagerParameter wmgrAcmeContact("contact", "ACME contact email", config.contact, sizeof(config.contact));
    wmgr.addParameter(&wmgrAcmeContact);
    WiFiManagerParameter wmgrAcmeHostname("hostname", "Hostname to acquire certificate for", config.hostname, sizeof(config.hostname));
    wmgr.addParameter(&wmgrAcmeHostname);
    WiFiManagerParameter wmgrAcmeEabKID("eab_kid", "ACME EAB KID", config.eab_kid, sizeof(config.eab_kid));
    wmgr.addParameter(&wmgrAcmeEabKID);
    WiFiManagerParameter wmgrAcmeEabKey("eab_key", "ACME EAB Key", config.eab_key, sizeof(config.eab_key));
    wmgr.addParameter(&wmgrAcmeEabKey);

    std::vector<const char *> menu{"wifi", "info", "param", "sep", "restart", "exit"};
    wmgr.setMenu(menu);
    //wmgr.setAPCallback(wifi_ap_started);
    wmgr.setHostname(config.hostname);
    //wmgr.setDebugOutput(true, WM_DEBUG_VERBOSE);
    wmgr.setSaveConfigCallback(wmgrSaveParameters);

    if (wmgr.getWiFiIsSaved()) {
        printf(PSTR("Starting WiFi ... "));
        wmgr.setEnableConfigPortal(false);
    }
    else {
        printf(PSTR("Starting WiFi manager ... "));
    }
    (void) fflush(stdout);

    for (;;) {
        // default credentials for setup, only
        if (wmgr.autoConnect(WIFI_MANAGER_SSID.c_str(), WIFI_MANAGER_PASS.c_str())) {
            printf(PSTR("connected to %s (at %s, RSSI %i) with %s/%i\r\n"),
                WiFi.SSID(), WiFi.BSSIDstr().c_str(), WiFi.RSSI(),
                WiFi.localIP().toString().c_str(), WiFi.subnetCIDR());
            break;
        }
        printf(PSTR("not connected\r\n"));
        delay(1000);
        printf(PSTR("Re-starting WiFiManager ... "));
        (void) fflush(stdout);
    }
    if (wmgrSaveParametersFlag) {
        strncpy(config.directory, wmgrAcmeDirectory.getValue(), sizeof(config.directory));
        strncpy(config.contact,   wmgrAcmeContact.getValue(),   sizeof(config.contact));
        strncpy(config.hostname,  wmgrAcmeHostname.getValue(),  sizeof(config.hostname));
        strncpy(config.eab_kid,   wmgrAcmeEabKID.getValue(),    sizeof(config.eab_kid));
        strncpy(config.eab_key,   wmgrAcmeEabKey.getValue(),    sizeof(config.eab_key));
        config_save();
    }

    if (esp_err_t err = mdns_init(); err == ESP_OK) {
        if ((err = mdns_hostname_set(config.hostname)) != ESP_OK)
            printf(PSTR("mdns_hostname_set failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
        if ((err = mdns_instance_name_set(config.hostname)) != ESP_OK)
            printf(PSTR("mdns_instance_name_set failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    else {
        printf(PSTR("mdns_init failed 0x%04x, %s"), err, esp_err_to_name(err));
    }

    printf(PSTR("Starting HTTP webserver at http://%s/ ... "), config.hostname);
    (void) fflush(stdout);
    if ((webserver_http = webserver_http_setup()) != nullptr) {
        printf(PSTR("ok\r\n"));
        mdns_service_add(nullptr, "_http", "_tcp", 80, nullptr, 0);
    }
    else {
        printf(PSTR("failed\r\n"));
    }

    printf(PSTR("Gathering current time ... "));
    (void) fflush(stdout);
    const String time = rtc_setup();
    printf(PSTR("%s\r\n"), time.c_str());

    // Boilerplate done ... now to the real example code
    if (acme_setup()) // => Could load previous certificate
        service_restart();

#ifdef DEBUG
    debug_print_memory();
#endif
}

void loop()
{
    // Refresh certificate, when it is going to expire
    every(3, seconds, [](){
        if (acme.loop()) { // => got a new certificate
            // debug_print_memory();
            service_restart();
        }
    });

    // Let internal LED blink to keep people happy ...
    every(LED_INTERVAL, millis, [](){
        digitalWrite(LED_INTERNAL, !digitalRead(LED_INTERNAL));
    });

    delay(10);
}
