#include <stdio.h>

#include <nvs_flash.h>
#include <pgmspace.h>

void esp32_nvs_dump(const char *ns)
{
    nvs_handle_t handle;

    nvs_stats_t stats;
    esp_err_t err = nvs_get_stats(NVS_DEFAULT_PART_NAME, &stats);
    if (err != ESP_OK) {
        printf(PSTR("nvs_get_stats failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    printf(PSTR("free: %zu, NS count: %zu, total: %zu, used: %zu\r\n"),
        stats.free_entries, stats.namespace_count, stats.total_entries, stats.used_entries);

    if ((err = nvs_open(ns, NVS_READONLY, &handle)) != 0) {
        printf(PSTR("nvs_open failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
        return;
    }

    size_t used_entries = 0;
    if ((err = nvs_get_used_entry_count(handle, &used_entries)) != ESP_OK) {
        printf(PSTR("nvs_get_used_entry_count failed 0x%04x, %s\r\n"), err, esp_err_to_name(err));
    }
    printf(PSTR("used entries: %zu\r\n"), used_entries);

    nvs_iterator_t it = nvs_entry_find(NVS_DEFAULT_PART_NAME, ns, NVS_TYPE_ANY);
    while (it != NULL) {
        nvs_entry_info_t info;
        nvs_entry_info(it, &info); // Can omit error check if parameters are guaranteed to be non-NULL
        printf(PSTR("key '%s', type %d"), info.key, info.type);
        switch (info.type) {
            case NVS_TYPE_U8: {
                uint8_t u8;
                if ((err = nvs_get_u8(handle, info.key, &u8)) == ESP_OK)
                    printf(PSTR(", value %d\r\n"), u8);
                break;
            }
            case NVS_TYPE_I8: {
                int8_t i8;
                if ((err = nvs_get_i8(handle, info.key, &i8)) == ESP_OK)
                    printf(PSTR(", value %d\r\n"), i8);
                break;
            }
            case NVS_TYPE_U16: {
                uint16_t u16;
                if ((err = nvs_get_u16(handle, info.key, &u16)) == ESP_OK)
                    printf(PSTR(", value %u\r\n"), u16);
                break;
            }
            case NVS_TYPE_I16: {
                int16_t i16;
                if ((err = nvs_get_i16(handle, info.key, &i16)) == ESP_OK)
                    printf(PSTR(", value %d\r\n"), i16);
                break;
            }
            case NVS_TYPE_U32: {
                uint32_t u32;
                if ((err = nvs_get_u32(handle, info.key, &u32)) == ESP_OK)
                    printf(PSTR(", value %u\r\n"), u32);
                break;
            }
            case NVS_TYPE_I32: {
                int32_t i32;
                if ((err = nvs_get_i32(handle, info.key, &i32)) == ESP_OK)
                    printf(PSTR(", value %i\r\n"), i32);
                break;
            }
            case NVS_TYPE_U64: {
                uint64_t u64;
                if ((err = nvs_get_u64(handle, info.key, &u64)) == ESP_OK)
                    printf(PSTR(", value %llu\r\n"), u64);
                break;
            }
            case NVS_TYPE_I64: {
                int64_t i64;
                if ((err = nvs_get_i64(handle, info.key, &i64)) == ESP_OK)
                    printf(PSTR(", value %lli\r\n"), i64);
                break;
            }
            case NVS_TYPE_STR: {
                size_t str_len = 0;
                if ((err = nvs_get_str(handle, info.key, NULL, &str_len)) == ESP_OK) {
                    char *str = (char *)malloc(str_len);
                    if ((err = nvs_get_str(handle, info.key, str, &str_len)) == ESP_OK)
                        printf(PSTR(", value '%s'\r\n"), str);
                    free(str);
                }
                break;
            }
            case NVS_TYPE_BLOB: {
                size_t str_len = 0;
                if ((err = nvs_get_blob(handle, info.key, NULL, &str_len)) == ESP_OK) {
                    uint8_t *str = (uint8_t *)malloc(str_len);
                    if ((err = nvs_get_blob(handle, info.key, str, &str_len)) == ESP_OK) {
                        printf(PSTR(", len %zu, value "), str_len);
                        for (size_t i=0; i<str_len; i++)
                            printf(PSTR("%02x"), str[i]);
                        printf(PSTR("\r\n"));
                    }
                    free(str);
                }
                break;
            }
            default:
                break;
        }
        if (err != ESP_OK)
            printf(PSTR(", failed to get value 0x%04x, %s\r\n"), err, esp_err_to_name(err));
        it = nvs_entry_next(it);
    }
    nvs_release_iterator(it);
    nvs_close(handle);
}