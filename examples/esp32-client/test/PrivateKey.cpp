#define DOCTEST_CONFIG_IMPLEMENT
#include <doctest.h>

#include <Acme.hpp>

mbedtls_ctr_drbg_context ctr_drbg;
mbedtls_entropy_context entropy;

TEST_SUITE("PrivateKey") {
	TEST_CASE("generate") {
		SUBCASE("MBEDTLS_PK_RSA") {
			CHECK(Acme::PrivateKey::generate(MBEDTLS_PK_RSA, mbedtls_ctr_drbg_random, &ctr_drbg) != nullptr);
		}
		SUBCASE("MBEDTLS_PK_ECKEY") {
			CHECK(Acme::PrivateKey::generate(MBEDTLS_PK_ECKEY, mbedtls_ctr_drbg_random, &ctr_drbg) != nullptr);
		}
		SUBCASE("fail without random function") {
			CHECK(Acme::PrivateKey::generate(MBEDTLS_PK_RSA, nullptr, &ctr_drbg) == nullptr);
		}
		SUBCASE("fail without random context") {
			CHECK(Acme::PrivateKey::generate(MBEDTLS_PK_RSA, mbedtls_ctr_drbg_random, nullptr) == nullptr);
		}
	}
	TEST_CASE("store") {
		SUBCASE("MBEDTLS_PK_RSA") {
			SUBCASE("rsa.pem") {
				auto *pk = Acme::PrivateKey::generate(MBEDTLS_PK_RSA, mbedtls_ctr_drbg_random, &ctr_drbg);
				CHECK(pk != nullptr);
				CHECK(pk->store("rsa.pem") == true);
			}	
			SUBCASE("rsa.der") {
				auto *pk = Acme::PrivateKey::generate(MBEDTLS_PK_RSA, mbedtls_ctr_drbg_random, &ctr_drbg);
				CHECK(pk != nullptr);
				CHECK(pk->store("rsa.der") == true);
			}
		}
		SUBCASE("MBEDTLS_PK_ECKEY") {
			SUBCASE("eckey.pem") {
				auto *pk = Acme::PrivateKey::generate(MBEDTLS_PK_ECKEY, mbedtls_ctr_drbg_random, &ctr_drbg);
				CHECK(pk != nullptr);
				CHECK(pk->store("eckey.pem") == true);
			}
			SUBCASE("eckey.der") {
				auto *pk = Acme::PrivateKey::generate(MBEDTLS_PK_ECKEY, mbedtls_ctr_drbg_random, &ctr_drbg);
				CHECK(pk != nullptr);
				CHECK(pk->store("eckey.der") == true);
			}
		}
	}
	TEST_CASE("load") {
		SUBCASE("does not exist") {
			CHECK(Acme::PrivateKey::load("does.not.exist") == nullptr);
		}
		SUBCASE("PEM") {
			SUBCASE("ECKEY exist") {
				CHECK(Acme::PrivateKey::load("eckey.pem") != nullptr);
			}
			SUBCASE("RSA exist") {
				CHECK(Acme::PrivateKey::load("rsa.pem") != nullptr);
			}
		}
		SUBCASE("DER") {
			SUBCASE("ECKEY exist") {
				CHECK(Acme::PrivateKey::load("eckey.der") != nullptr);
			}
			SUBCASE("RSA exist") {
				CHECK(Acme::PrivateKey::load("rsa.der") != nullptr);
			}
		}
	}
}

int main(int argc, char **argv) {
    mbedtls_ctr_drbg_init(&ctr_drbg);
    mbedtls_entropy_init(&entropy);    
	
	mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy, nullptr, 0);

	doctest::Context context;

	context.setOption("success", true);
	context.setOption("no-exitcode", true);
	
	context.applyCommandLine(argc, argv);
	return context.run();
}