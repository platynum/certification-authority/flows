/*
 * This module implements the ACME (Automated Certicifate Management Environment) protocol.
 *
 * Copyright (c) 2019, 2020, 2021, 2023, 2024 Danny Backx
 *
 * License (MIT license):
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#pragma once

#include <map>
#include <memory>
#include <optional>
#include <vector>

#include <ArduinoJson.h>
#if ARDUINOJSON_VERSION_MAJOR < 7
#error ArduinoJson v7 required
#endif

#if defined(ESP32) or defined(ESP8266)
#include <esp_http_client.h>
#include <esp_http_server.h>
#else
using String = std::string;
#endif

#include <mbedtls/ctr_drbg.h>
#include <mbedtls/entropy.h>
#include <mbedtls/error.h>
#include <mbedtls/sha256.h>
#include <mbedtls/ssl.h>
#include <mbedtls/x509.h>
#include <mbedtls/x509_csr.h>

namespace Acme {

  static constexpr const char *version = "esp32-acme-client/0.1.2";

  struct Directory {
    Directory(String newNonce_, String newAccount_, String newOrder_) :
      newNonce(std::move(newNonce_)),
      newAccount(std::move(newAccount_)),
      newOrder(std::move(newOrder_)) {}
    String newAccount, newNonce, newOrder;
  };

  static const constexpr char *acme_status_str[] = {
    "pending",          "cancelled",       "processing",
    "ready",            "valid",           "invalid",
    "revoked",          "expired",         "deactivated"
  };

  // keep in sync with acme_status_str[] offsets
  enum class Status : int {
    PENDING = 0x0, CANCELLED,    PROCESSING,
    READY,         VALID,        INVALID,
    REVOKED,       EXPIRED,      DEACTIVATED
  };

  static /* constexpr const */ std::map<String, Acme::Status> acme_status_map = {
    {"pending",     Acme::Status::PENDING},
    {"cancelled",   Acme::Status::CANCELLED},
    {"processing",  Acme::Status::PROCESSING},
    {"ready",       Acme::Status::READY},
    {"valid",       Acme::Status::VALID},
    {"invalid",     Acme::Status::INVALID},
    {"revoked",     Acme::Status::REVOKED},
    {"expired",     Acme::Status::EXPIRED},
    {"deactivated", Acme::Status::DEACTIVATED}
  };

  static constexpr const char *identifier_type_map[] = {
    "dns", "ip", "email"
  };

  struct Identifier {
    enum class Type : int { DNS = 0x0, IP, EMAIL } type;
    String value;
  };

  static constexpr const char *challenge_type_map[] = {
    "dns-01", "http-01", "tls-alpn-01", "email-reply-00"
  };

  class Resource {
    protected:
      explicit Resource(String url_) :
        url(std::move(url_)) {};
    public:
      String url;
      Status status = Acme::Status::PENDING;
      auto update_status(JsonDocument &json) -> std::optional<Acme::Status>;
  };

  struct Authorization : public Resource {
    public:
      explicit Authorization(const String &url) : Resource(url) {};
      std::vector<std::unique_ptr<struct Identifier>> identifiers;
      time_t expires = 0; // Until when this authorization can be used (0 == not specified)
      // Although an authorization might include multiple challenges, we keep
      // only the last http-01 challenge, Acme::Client doesn't understand any other
      struct Challenge {
        enum class Type : int { DNS_01 = 0x0, HTTP_01, TLS_ALPN_01, EMAIL_REPLY_00 } type;
        Status status;
        String url; // URL to trigger challenge
        String token;
        bool active; // internal flag if challenge was triggered
      } challenge = { Challenge::Type::DNS_01, Acme::Status::PENDING, "", "", false };
      auto read(JsonDocument &json) -> std::optional<Acme::Status>;
  };

  class Order : public Resource {
    public:
      explicit Order(const String &url_) :
        Resource(url_) {};
      virtual ~Order() {
        identifiers.clear();
        authorizations.clear();
      }
      time_t expires = 0; // Until when this order can be used (0 == not specified)
      // identifiers that have been ordered
      std::vector<std::unique_ptr<struct Identifier>> identifiers;
      // authorizations, that have to be granted by the CA
      std::vector<std::unique_ptr<Authorization>> authorizations;
      String finalize {}; // URL to upload a CSR when order is ready
      String certificate {}; // URL to download the certificate when order is valid
      auto read(JsonDocument &json) -> std::optional<Acme::Status>;
  };

  struct Certificate : public ::mbedtls_x509_crt {
    Certificate() { mbedtls_x509_crt_init(this); }
    Certificate(Certificate &) = delete;
    ~Certificate() { mbedtls_x509_crt_free(this); }
  };

  // Interface to request HTTPS resources from ACME service
  class HttpsClient {
    protected:
      HttpsClient() = default;
      String nonce; // Value of "Replay-Nonce" HTTP header of last request (if present)
      static auto parse_retry_after(const char *value) -> time_t;
      static void display_error(const char *json);
    public:
      virtual ~HttpsClient() = default;
      String location; // Value of "Location" HTTP header of last request (if present)
      time_t retry_after = 0; // Timestamp calculated from 'Retry-After' HTTP header of last request
      auto nonce_use() -> String { return std::move(nonce); };
      virtual auto request_nonce(const String &uri) -> String = 0;
      auto perform_json_query(const String &query, const String *topost) -> std::optional<JsonDocument>;
      virtual auto perform_query(const String &query, const String *topost, const char *accept_msg) -> std::optional<String> = 0;
      static const constexpr char POST_AS_GET[] = "";
  };

  // Interface to serve http-01 challenges
  class WebServer {
    public:
      virtual ~WebServer() = default;
      virtual auto enable(const String &uri, const String &content) -> bool = 0;
      virtual auto disable() -> bool = 0;
  };

#if defined(ESP32) or defined(ESP8266)
  class Esp32HttpsClient : public HttpsClient {
      const char *root_certificates;
      String reply_buffer;
      // These is a static member function used as C callback
      static auto HttpEvent(esp_http_client_event_t *event) -> esp_err_t;
      // HTTP client used for all queries
      esp_http_client_handle_t client;
      // Functions to invigorate client handle (esp. when connection is broken)
      auto rebuild() -> esp_http_client_handle_t;
      auto create() -> esp_http_client_handle_t;
      void destroy();
    public:
      explicit Esp32HttpsClient(const char root_certificates_[]);
      ~Esp32HttpsClient() override;
      auto request_nonce(const String &uri) -> String override;
      auto perform_query(const String &query, const String *topost, const char *accept_msg) -> std::optional<String> override;
  };

  class Esp32WebServer : public WebServer {
      httpd_handle_t server;
      httpd_uri_t *webserver_handler = nullptr;
      static auto acme_http_get_handler(httpd_req_t *) -> esp_err_t;
    public:
      explicit Esp32WebServer(httpd_handle_t server_) :
        server{server_} {};
      ~Esp32WebServer() override {
        delete webserver_handler;
      }
      auto enable(const String &uri, const String &content) -> bool override;
      auto disable() -> bool override;
  };
#endif

  // Wrapper for mbedtls_pk_context with file storage/retrieval
  class PrivateKey : public ::mbedtls_pk_context {
      static constexpr const unsigned int rsa_keysize = 2048;
      static constexpr const unsigned long rsa_exponent = 0x10001;
      // Generate an RSA key using rsa_keysize, and rsa_exponent
      static auto generate_RSA_key(int (*f_rng)(void *, unsigned char *, size_t), mbedtls_ctr_drbg_context *p_rng) -> PrivateKey *;
      // Generate an ECDSA key (secp384r1)
      static auto generate_EC_key(int (*f_rng)(void *, unsigned char *, size_t), mbedtls_ctr_drbg_context *p_rng) -> PrivateKey *;
    public:
      PrivateKey() { mbedtls_pk_init(this); };
      ~PrivateKey() { mbedtls_pk_free(this); };
      auto store(const String &filename) -> bool;
      static auto load(const String &filename) -> PrivateKey *;
      static auto generate(mbedtls_pk_type_t type, int (*f_rng)(void *, unsigned char *, size_t), mbedtls_ctr_drbg_context *p_rng) -> PrivateKey *;
  };

  class ClientBase {
    public:
      // Configure a custom root certificate(s) (PEM format), used when contacting ACME server
      explicit ClientBase(const char root_certificates[] = nullptr);
      virtual ~ClientBase();

      // Configure the location of the ACME directory descriptor (required)
      void setDirectoryUrl(const char *url);

      // Configure account email to use for registration
      void setAccountEmail(const char *address);
      // Configure external account binding (kid and hmac key), this is optional
      void setExternalAccountBinding(const char *kid, const char *keyBase64Url);
      // Private key used to register an account (required, @see setup_key())
      std::shared_ptr<PrivateKey> account_key;
      // Setup a private key (account_key or certificate_key)
      auto setup_key(mbedtls_pk_type_t type, const String &filename) -> std::shared_ptr<PrivateKey>;

      // Configure a DNS domain to request a certificate for (required)
      void setDnsIdentifier(const char *domain);
      // Configure additional DNS domains for storage alternative names (optional)
      void addDnsIdentifier(const char *domain);

      // Configure a webserver to serve http-01 challenges, has to listen on TCP port 80 (required)
      void setWebServer(WebServer *server);

      // Private key used to create a Certificate Signing Request (CSR) (required, @see setup_key())
      std::shared_ptr<PrivateKey> certificate_key;
      // Configure filename where the certificate generated will be stored,
      // return the certificate stored if present
      auto setCertificateFilename(const char *filename) -> std::shared_ptr<Certificate>;

      /* Return true, if a new certificate has to be requested, this is whenever
        - there is no current certificate, or
        - current certificate's lifetime is less than 20%. */
      auto needs_renewal(time_t now) -> bool;
      auto needs_renewal() -> bool;

      /* This is supposed to get called periodically to continue work.
       * The parameter should be the current timestamp.
       *
       * Two types of action occur :
       *  - trigger the ACME request engine (finite state machine) to advance order status
       *  - check if the current certificate should be renewed, and cause that (which stumbles into the above)
       *
       * Returns true if there was a change to the certificate.
       */
      auto loop(time_t now) -> bool;
      auto loop() -> bool;

    protected:
      static const constexpr time_t SHORT_HOLD_DOWN = 30L; // in seconds
      static const constexpr time_t LONG_HOLD_DOWN = 60L * 60L; // 1 hour in seconds
      // Try to renew certificate after 80% of validity period
      static const constexpr float CERTIFICATE_LIFESPAN_FACTOR = 0.2;
      // Character array of PEM-encoded, trusted root certificates passed to HTTPS_CLIENT
      const char *root_certificates = nullptr;

      String directory_location; // ACME server URL
      auto request_directory() -> struct Directory *; // Download ACME directory
      struct Directory *directory = nullptr; // Directory downloaded with request_directory()

      String nonce; // Last Replay-Nonce seen
      auto nonce_use() -> String; // Use (and discard) nonce
  
      String eab_kid, // External Account Binding Key ID
             eab_key; // External Account Binding Key (Base64url)

      const char *email_address = nullptr; // Email address in the account (starts with mailto:)
      // Request the newAccount resource to register a new account or recover an existing
      auto request_newAccount(const String &contact, bool onlyReturnExisting) -> bool;
      String account_location; // Account KID as reported by ACME server

      // identifiers to request a certificate for (at least one is needed)
      std::vector<std::unique_ptr<struct Identifier>> identifiers;

      // Current order this client is currently working on (nullptr otherwise)
      Order *order = nullptr;
      // Create a new order on the server, sotring the results in order
      auto request_newOrder(const String &newOrderUrl) -> std::optional<Acme::Status>;
      // Request a resource (order/authorization) from server and return current status of it
      auto update(Acme::Resource &resource) -> std::optional<Acme::Status>;
  
      // Create HS256 signature over jwk for external account binding (EAB)
      static auto generate_EAB(const String &url, const String &account, const String &key, const String &jwk) -> std::optional<String>;

      auto make_message_JWK(const String &url, const String &payload, const String &jwk) -> std::optional<String>;

      auto make_message_KID(const String &url, const String &payload) -> std::optional<String>;
      auto make_protected_KID(const String &query) -> String;

      // Enable local webserver for serving a challenge and trigger this challenge on the server
      auto trigger(const struct Authorization::Challenge &challenge) -> std::optional<Acme::Status>;

      // Download a single authorization resource and place it into order->authorizations
      auto download_authorization(Authorization &authorization) -> std::optional<Acme::Status>;
      // Download all authorization resource into current order
      auto download_authorizations() -> bool;

      auto send_message(const String &url, const String &content) -> std::optional<JsonDocument>;
      // Perform an HTTPS JSON request using client
      auto perform_json_query(const String &query, const String *topost) -> std::optional<JsonDocument>;
      // Create a new HttpsClient instance (see Acme::Client::client_new())
      virtual auto client_new(const char root_certificates[]) -> HttpsClient * = 0;
      // HttpsClient created with client_new()
      HttpsClient *client = nullptr;

      // Webserver resource to serve http-01 challenges on port 80
      WebServer *webserver = nullptr;
      // Enable webserver resource to serve a specific challenge/token
      auto webserver_enable(const String &token) -> bool;
      // Disable currently configure resource
      void webserver_disable();

      // Insert Storage Alternative Name (SAN) entries for all identifiers into req
      auto create_san_entries(mbedtls_x509write_csr req) -> int;
      // Generate a CSR for identifiers using certificate_key return result as Base64url
      auto generate_CSR() -> String;
      // Upload CSR into server
      auto finalize_order() -> bool;

      const char *cert_fn = nullptr; // Certificate filename
      // Load certificate chain from local file (cert_fn)
      auto read_certificate() -> std::shared_ptr<Certificate>;
      std::shared_ptr<Certificate> certificate; // Pointer to last acquired certificate (chain)
      // Download certificate (with chain) from server
      auto download_certificate() -> std::shared_ptr<Certificate>;

      // Supress operation until this time has passed (aka clock is set)
      static const constexpr time_t SYSTEM_STARTUP_TIME = 1000;
      // Suppress further processing until this timestamp
      time_t hold_down = SYSTEM_STARTUP_TIME;

      // Helpers to get mbedTLS going ...
      mbedtls_ctr_drbg_context ctr_drbg;
      mbedtls_entropy_context entropy;
  };

  template <class HTTPS_CLIENT>
  class Client : public ClientBase {
    public:
      explicit Client(const char *root_certificates) : ClientBase(root_certificates) {};
      ~Client() override = default;
      auto client_new(const char root_certificates[]) -> HttpsClient * override {
        return new HTTPS_CLIENT(root_certificates);
      };
  };

  namespace File
  {
    // Utility function to read filename, caller must delete [] buffer returned
    extern auto read(const char *filename, size_t *plen) -> unsigned char *;
    // Utility function to write buflen chars of buf to filename (previous content is deleted)
    extern auto write(const char *filename, const unsigned char *buf, size_t buflen) -> bool;
  } // namespace File
} // namespace Acme
