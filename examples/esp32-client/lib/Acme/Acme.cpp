/* This module implements the ACME (Automated Certicifate Management Environment) protocol.
 * The client has been tested with:
 *  - platynum (https://acme.platynum.ch),
 *
 * ACME relies on the presence of a web server for validation.
 * The IoT device can implement its own web server, we'll register for servicing just the
 * required file, only in the time that the ACME server needs it.
 *
 * Copyright (c) 2019, 2020, 2021, 2022, 2023, 2024 Danny Backx
 *
 * License (MIT license):
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#include "Acme.hpp"

#include <algorithm>
#include <array>
#include <cstdio>
#include <cstring>

#if defined(ESP32) or defined(ESP8266)
#include <esp_crt_bundle.h>
#include <esp_http_client.h>
#include <esp_log.h>

// TODO 😱: Use (,)__VA_OPTS__ here
static constexpr const char acme_tag[] = "acme";
#ifdef DEBUG
#define ACME_LOGV(fmt, ...) ESP_LOGV(acme_tag, "V %s: " fmt, __PRETTY_FUNCTION__, ##__VA_ARGS__)
#define ACME_LOGD(fmt, ...) ESP_LOGD(acme_tag, "D %s(): " fmt, __func__, ##__VA_ARGS__)
#else // DEBUG
#define ACME_LOGV(fmt, ...) do {} while(0)
#define ACME_LOGD(fmt, ...) do {} while(0)
#endif // DEBUG
#define ACME_LOGI(fmt, ...) printf("[%6u] " fmt "\r\n", (unsigned long)(esp_timer_get_time() / 1000ULL), ##__VA_ARGS__)
#define ACME_LOGE(fmt, ...) ESP_LOGE(acme_tag, fmt, ##__VA_ARGS__)
#endif // defined(ESP32) || defined(ESP8266)

static constexpr const int LINE_LENGTH = 80;
#define ACME_MBEDTLS_LOGE(fmt, err, ...) do { \
    std::array<char, LINE_LENGTH> buf; \
    mbedtls_strerror(err, buf.data(), buf.size()); \
    ACME_LOGE(fmt " error 0x%04x, %s", ##__VA_ARGS__, err, buf.data()); \
  } while (0)

#include <sys/time.h>
#if !defined(ESP32) and !defined(ESP8266)
#ifndef DEBUG
#define ACME_LOGV(fmt, ...) do {} while(0)
//#define ACME_LOGV(fmt, ...) printf("[V] %s: " fmt "\r\n", __PRETTY_FUNCTION__, ##__VA_ARGS__)
#define ACME_LOGD(fmt, ...) printf("[D] %s: " fmt "\r\n", __PRETTY_FUNCTION__, ##__VA_ARGS__)
#define ACME_LOGI(fmt, ...) printf("[I] %s(): " fmt "\r\n", __func__, ##__VA_ARGS__)
#define ACME_LOGE(fmt, ...) printf("[E] %s(): " fmt "\r\n", __func__, ##__VA_ARGS__)
#else // !DEBUG
#include <iostream>
#include <iomanip>
#include <source_location>
namespace {
  template<typename... ARGS> constexpr void acme_log(const std::source_location& loc, char verbosity, const char *fmt, ARGS... args)
  {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    std::cout << tv.tv_sec << '.' << std::setfill('0') << std::setw(6) << tv.tv_usec << std::setfill('\0')
              << " [" << verbosity << "] "
              << loc.file_name() << ':' << loc.line() << " "
              << loc.function_name() << ": "
              << fmt;
    ((std::cout << ", " << std::forward<ARGS>(args)), ...);
    std::cout << '\n';
  }
  template<typename... ARGS> struct ACME_LOGV {
    ACME_LOGV(const char *fmt, ARGS... args, const std::source_location& loc = std::source_location::current())
    {
      acme_log(loc, 'V', fmt, args...);
    }
  };
  template<typename... ARGS> ACME_LOGV(const char *fmt, ARGS&&...) -> ACME_LOGV<ARGS...>;
  template<typename... ARGS> struct ACME_LOGD {
    ACME_LOGD(const char *fmt, ARGS... args, const std::source_location& loc = std::source_location::current())
    {
      acme_log(loc, 'D', fmt, args...);
    }
  };
  template<typename... ARGS> ACME_LOGD(const char *fmt, ARGS&&...) -> ACME_LOGD<ARGS...>;
  template<typename... ARGS> struct ACME_LOGI {
    ACME_LOGI(const char *fmt, ARGS... args, const std::source_location& loc = std::source_location::current())
    {
      acme_log(loc, 'I', fmt, args...);
    }
  };
  template<typename... ARGS> ACME_LOGI(const char *fmt, ARGS&&...) -> ACME_LOGI<ARGS...>;
  template<typename... ARGS> struct ACME_LOGE {
    ACME_LOGE(const char *fmt, ARGS... args, const std::source_location& loc = std::source_location::current())
    {
      acme_log(loc, 'E', fmt, args...);
    }
  };
  template<typename... ARGS> ACME_LOGE(const char *fmt, ARGS&&...) -> ACME_LOGE<ARGS...>;
#undef ACME_MBEDTLS_LOGE
  template<typename... ARGS> struct ACME_MBEDTLS_LOGE {
    ACME_MBEDTLS_LOGE(const char *fmt, int err, ARGS... args, const std::source_location& loc = std::source_location::current())
    {
      std::array<char, LINE_LENGTH> buf;
      mbedtls_strerror(err, buf.data(), buf.size());
      acme_log(loc, 'E', (String(fmt) + " %d, %s").c_str(),  args..., err, buf.data());
    }
  };
  template<typename... ARGS> ACME_MBEDTLS_LOGE(const char *fmt, int err, ARGS&&...) -> ACME_MBEDTLS_LOGE<ARGS...>;
}
#endif // DEBUG
#endif // !ESP32 && !ESP8266

#include <mbedtls/asn1write.h>
#include <mbedtls/base64.h>
#include <mbedtls/net_sockets.h>
#include <mbedtls/oid.h>
#include <mbedtls/psa_util.h>
#include <mbedtls/x509_csr.h>

// TODO 🗑: Remove me, with next MbedTLS upgrade ...
extern "C" {
  extern int mbedtls_ecdsa_der_to_raw(size_t bits, const unsigned char *der, size_t der_len, unsigned char *raw, size_t raw_size, size_t *raw_len);
}

namespace {
  // Decode MbedTLS timestamp into seconds since 1970, convert from mbedtls_x509_time to time_t
  auto TimeMbedToTimestamp(mbedtls_x509_time t) -> time_t
  {
    struct tm tms = {
      .tm_sec = t.sec,     // 00-60 (C99) / 00-61 (C90)
      .tm_min = t.min,     // 00-59
      .tm_hour = t.hour,   // 00-23
      .tm_mday = t.day,    // 01-31
      .tm_mon = t.mon - 1, // 00-11
      .tm_year = t.year - 1900,
      .tm_isdst = 0        // false;
    };
    return mktime(&tms);
  }
} // anonymous namespace

namespace RFC3339 {
  // Convert timestamp from ACME (e.g. 2019-11-25T16:56:52Z) into time_t.
  auto parse_timestamp(const String &ts) -> time_t
  {
    struct tm tms = {};
    const char *r = strptime(ts.c_str(), "%FT%T", &tms);
    if (r == nullptr || (*r != 'Z' && *r != '.' && *r != ','))
      return 0; // Failed to scan

    return mktime(&tms);
  }
} // namespace RFC3339

namespace RFC4648
{
  auto Base64Url(const uint8_t *buf, size_t buflen) -> std::unique_ptr<char>
  {
    if (buf == nullptr) {
      ACME_LOGD("null");
      return nullptr;
    }

    size_t olen;
    (void) mbedtls_base64_encode(nullptr, 0, &olen, buf, buflen);
    ACME_LOGV("(_,%d) olen %d", buflen, olen);

    std::unique_ptr<char>r = std::unique_ptr<char>(new char [olen + 1]);
    (void) mbedtls_base64_encode(reinterpret_cast<unsigned char *>(r.get()), olen+1, &olen, buf, buflen);

    // Replace some characters by acceptable ones, without making the string longer. Also in the RFCs.
    char *raw = r.get();
    for (int i=0; i<=olen; i++)
      if (raw[i] == '+')
        raw[i] = '-';
      else if (raw[i] == '/')
        raw[i] = '_';
      else if (raw[i] == '=')
        raw[i] = 0;

    return std::move(r);
  }

  // Caller needs to free the result.
  auto Base64Url(const char *str) -> std::unique_ptr<char>
  {
    if (str == nullptr) {
      ACME_LOGD("null");
      return nullptr;
    }

    const size_t sl = strlen(str);
    if (sl == 0) {
      ACME_LOGV("empty string");
      std::unique_ptr<char>r = std::unique_ptr<char>(new char[1]);
      r.get()[0] = '\0';
      return r;
    }
    return Base64Url(reinterpret_cast<const unsigned char *>(str), sl);
  }

  auto Base64UrlStr(const uint8_t *buf, size_t buflen) -> String
  {
    ACME_LOGV("(%p, %zu)", static_cast<const void *>(buf), buflen);
    String ret;
    if (buflen != 0) {
      std::unique_ptr<char> b64 = Base64Url(buf, buflen);
      ACME_LOGV("%s", b64.get());
      if (b64 != nullptr)
        ret = b64.get();
    }
    return ret;
  }

  auto Base64UrlStr(const String *str) -> String
  {
    return Base64UrlStr(reinterpret_cast<const unsigned char *>(str->c_str()), str->length());
  }

  auto Base64UrlStr(const String &str) -> String
  {
    return Base64UrlStr(reinterpret_cast<const unsigned char *>(str.c_str()), str.length());
  }

  // And the opposite
  auto Unbase64Url(const char *buf, size_t buflen) -> std::unique_ptr<char>
  {
    size_t append_equal = buflen % 4;
    if (append_equal == 3) // Invalid input length
      return nullptr;

    const size_t r_len = buflen + append_equal;
    // For a trailing 0 and up to 2 trailing '='
    std::vector<char> r(r_len + 1, 0);
    for (int i=0; i<=buflen; i++)
      if (buf[i] == '-')
        r[i] = '+';
      else if (buf[i] == '_')
        r[i] = '/';
      else if (buf[i] == 0) {
        while (append_equal--)
          r[i++] = '=';
        r[i] = 0;
      } else
        r[i] = buf[i];
    ACME_LOGV("r[%d]: %s", r.size(), r.data());

    size_t olen = 0;
    (void) mbedtls_base64_decode(nullptr, 0, &olen, reinterpret_cast<unsigned char *>(r.data()), r.size()-1);

    ACME_LOGV("buflen: %d, padded/r_len: %d, olen: %d", buflen, r.size(), olen);
    std::unique_ptr<char>obuf = std::unique_ptr<char>(new char[olen+1]);
    
    const int err = mbedtls_base64_decode(reinterpret_cast<unsigned char *>(obuf.get()), olen, &olen, reinterpret_cast<unsigned char *>(r.data()), r.size()-1);
    ACME_LOGV("olen: %d", olen);
    if (err != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_base64_decode", err);
      obuf = nullptr;
    }
    else {
      obuf.get()[olen] = '\0';
      // xxd("obuf", (unsigned char *)obuf.get(), olen+1, stdout);
    }
    return std::move(obuf);
  }

  auto Unbase64Url(const char *s) -> std::unique_ptr<char>
  {
    return Unbase64Url(s, strlen(s));
  }

  auto Unbase64UrlStr(const String &str) -> String {
    ACME_LOGE("str[%li]=%s", str.length(), str.c_str());
    auto raw = Unbase64Url(str.c_str(), str.length());
    if (raw == nullptr)
      return {};
    String ret = raw.get();
    return ret;
  }
} // namespace RFC4648

namespace RFC7515 {
  struct HmacKey {
    String value;
  };

  auto sign(
    const HmacKey &key,
    const String &prB64url,
    const String &plB64url)
  ->
    String
  {
    mbedtls_md_context_t ctx;
    mbedtls_md_init(&ctx);

    const mbedtls_md_info_t *info = mbedtls_md_info_from_type(MBEDTLS_MD_SHA256);
    int err = mbedtls_md_setup(&ctx, info, 1);
    if (err != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_md_setup", err);
      return {};
    }

    if ((err = mbedtls_md_hmac_starts(&ctx, reinterpret_cast<const unsigned char *>(key.value.c_str()), key.value.length())) != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_md_hmac_starts", err);
      return {};
    }
    if ((err = mbedtls_md_hmac_update(&ctx, reinterpret_cast<const unsigned char *>(prB64url.c_str()), prB64url.length())) != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_md_hmac_update", err);
      return {};
    }
    if ((err = mbedtls_md_hmac_update(&ctx, reinterpret_cast<const unsigned char *>("."), 1)) != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_md_hmac_update", err);
      return {};
    }
    if ((err = mbedtls_md_hmac_update(&ctx, reinterpret_cast<const unsigned char *>(plB64url.c_str()), plB64url.length())) != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_md_hmac_update", err);
      return {};
    }
    ACME_LOGV("tbs: %s.%s", prB64url.c_str(), plB64url.c_str());

    const unsigned char hmac_size = mbedtls_md_get_size(info);
    std::vector<unsigned char> hmac(hmac_size, 0);

    if ((err = mbedtls_md_hmac_finish(&ctx, hmac.data())) != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_md_hmac_finish", err);
      return {};
    }

    return RFC4648::Base64UrlStr(hmac.data(), hmac.size());
  }

  auto sign(
    mbedtls_pk_context *key,
    const String &prB64url,
    const String &plB64url,
    int (*f_rng)(void *, unsigned char *, size_t),
    void *p_rng)
  ->
    String
  {
    ACME_LOGV("prB64url[%d] %s", prB64url.length(), prB64url.c_str());
    ACME_LOGV("plB64url[%d] %s", plB64url.length(), plB64url.c_str());

    const String bb = prB64url + "." + plB64url;
    ACME_LOGV("signing input (length %d) {%s}", bb.length(), bb.c_str());

    const mbedtls_pk_type_t type = mbedtls_pk_get_type(key);
    mbedtls_md_type_t md;
    switch (type) {
      case MBEDTLS_PK_RSA:   md = MBEDTLS_MD_SHA256; break;
      case MBEDTLS_PK_ECKEY: md = MBEDTLS_MD_SHA384; break;
      default:
        ACME_LOGE("unknown key type %d", type);
        return {};
    }
    const mbedtls_md_info_t *mdi = mbedtls_md_info_from_type(md);
    if (mdi == nullptr) {
      ACME_LOGE("mbedtls_md_info_from_type: md_info not found");
      return {};
    }

    const unsigned char hash_size = mbedtls_md_get_size(mdi);
    std::vector<unsigned char> hash(hash_size, 0);

    int ret = mbedtls_md(mdi, reinterpret_cast<const unsigned char *>(bb.c_str()), bb.length(), hash.data());
    if (ret != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_md", ret);
      return {};
    }

    const size_t signature_size = MBEDTLS_PK_SIGNATURE_MAX_SIZE;
    auto *signature = static_cast<unsigned char *>(alloca(signature_size+1));
    if (signature == nullptr) {
      ACME_LOGE("alloca failed, mbedtls_pk_get_len %zu", mbedtls_pk_get_len(key));
      return {};
    }
    ACME_LOGV("signature_size %d", signature_size);

    size_t siglen = signature_size;
    if ((ret = mbedtls_pk_sign(key, md, hash.data(), hash.size(), signature, &siglen, f_rng, p_rng)) != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_pk_sign", ret);
      return {};
    }
    if (type == MBEDTLS_PK_ECKEY) {
      const size_t bits = mbedtls_pk_get_bitlen(key);
      if (bits == 0) {
        ACME_LOGE("mbedtls_pk_get_bitlen failed");
        return {};
      }
      if ((ret = mbedtls_ecdsa_der_to_raw(bits, signature, siglen, signature, signature_size, &siglen)) != 0) {
        ACME_MBEDTLS_LOGE("mbedtls_ecdsa_der_to_raw", ret);
        return {};
      }
    }

    ACME_LOGV("signature size %d, siglen %d", signature_size, siglen);
    return RFC4648::Base64UrlStr(signature, siglen);
  }

  auto message(
    const String &protected64,
    const String &payload64,
    const String &signature64)
  ->
    std::optional<String>
  {
    String json;
    json.reserve(2048);
    json = "{"
      "\"protected\":\"" + protected64 + "\","
      "\"payload\":\"" + payload64 + "\","
      "\"signature\":\"" + signature64 + "\""
    "}";
    return std::make_optional<String>(json);
  }

  // Create an RFC7515 message signed with key, returns
  // Base64Url(protected) || "." || Base64Url(payload) || "." || Base64Url(signature)
  auto message(
    mbedtls_pk_context *key,
    const String &protected_,
    const String &payload,
    int (*f_rng)(void *, unsigned char *, size_t),
    void *p_rng)
  ->
    std::optional<String>
  {
    const String protected64 = RFC4648::Base64UrlStr(protected_);
    if (!protected64.isEmpty()) {
      String payload64 = "";
      if (!payload.isEmpty())
        payload64 = RFC4648::Base64UrlStr(payload);
      if (payload.isEmpty() || !payload64.isEmpty()) {
        const String signature64 = sign(key, protected64, payload64, f_rng, p_rng);
        if (!signature64.isEmpty())
          return message(protected64, payload64, signature64);
      }
    }
    return std::nullopt;
  }

  auto message(
    const HmacKey &key,
    const String &protected_,
    const String &payload)
  ->
    std::optional<String>
  {
    const String protected64 = RFC4648::Base64UrlStr(protected_);
    if (!protected64.isEmpty()) {
      const String payload64 = RFC4648::Base64UrlStr(payload);
      if (!payload.isEmpty()) {
        const String signature64 = sign(key, protected64, payload64);
        if (!signature64.isEmpty())
          return message(protected64, payload64, signature64);
      }
    }
    return std::nullopt;
  }
} // namespace RFC7515

namespace RFC7517 {
  /* This basically prints out the N (public key modulus) field from the
   * key in the RSA context pointer. We're extracting the N and E mpi's.
   * Can start with 0 if not allocated properly, and not null-terminated.
   * Hence the two-parameter call to Base64(). */
  auto JWK(mbedtls_rsa_context *rsa) -> String
  {
    ACME_LOGV("()");
    if (rsa == nullptr) {
      ACME_LOGE("fail, rsa null");
      return {};
    }

    unsigned char E[4]; // E will be at the rear end of this array
    int ne = sizeof(E);

    const size_t nl = mbedtls_rsa_get_len(rsa);
    std::vector<unsigned char> N(nl, 0);

    const int err = mbedtls_rsa_export_raw(rsa, N.data(), N.size(), /* P */ nullptr, 0, /* Q */ nullptr, 0, /* D */ nullptr, 0, E, ne);
    if (err != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_rsa_export_raw", err);
      return {};
    }

    // E is at the rear end of this array, point q to it
    unsigned char *q = E;
    for (; *q==0; q++,ne--); // Skip initial zeroes

    ACME_LOGV("call RFC4648::Base64UrlStr(_,%d)", nl);
    const String bN = RFC4648::Base64UrlStr(N.data(), N.size());
    if (bN.isEmpty()) {
      ACME_LOGE("failed bN = 0");
      return {};
    }
    ACME_LOGV("call RFC4648::Base64UrlStr(_,%d)", ne);
    const String bE = RFC4648::Base64UrlStr(q, ne);
    if (bE.isEmpty()) {
      ACME_LOGE("failed bE = 0");
      return {};
    }

    ACME_LOGV("N %s, E %s", bN, bE);

    String ret = "{"
      "\"e\":\"" + bE + "\","
      "\"kty\":\"RSA\","
      "\"n\":\"" + bN + "\""
    "}";
    ACME_LOGV(" -> %s", ret.c_str());

    return ret;
  }
  
  // Create public JSON web key from EC key
  auto JWK(mbedtls_ecdsa_context *ecdsa) -> String
  {
    ACME_LOGV("(%p)", static_cast<void *>(ecdsa));

    if (ecdsa == nullptr) {
      ACME_LOGE("fail, ecdsa null");
      return {};
    }

    const size_t y_len = mbedtls_mpi_size(&ecdsa->Q.Y);
    unsigned char y[y_len];
    int err = mbedtls_mpi_write_binary(&ecdsa->Q.Y, y, y_len);
    if (err < 0) {
      ACME_MBEDTLS_LOGE("mbedtls_mpi_write_binary", abs(err));
      return {};
    }

    const String bY = RFC4648::Base64UrlStr(y, y_len);
    if (bY.isEmpty())
      return {};
    ACME_LOGV("%s", bY.c_str());

    const size_t x_len = mbedtls_mpi_size(&ecdsa->Q.X);
    unsigned char x[x_len];
    if ((err = mbedtls_mpi_write_binary(&ecdsa->Q.X, x, x_len)) < 0) {
      ACME_MBEDTLS_LOGE("mbedtls_mpi_write_binary", abs(err));
      return {};
    }

    const String bX = RFC4648::Base64UrlStr(x, x_len);
    if (bX.isEmpty())
      return {};
    ACME_LOGV("%s", bX.c_str());

    return "{"
      "\"crv\":\"P-384\","
      "\"kty\":\"EC\","
      "\"x\":\"" + bX + "\","
      "\"y\":\"" + bY + "\""
    "}";
  }

  // Create public JSON web key from RSA key
  auto JWK(mbedtls_pk_context *key) -> String
  {
    ACME_LOGV("()");

    if (key == nullptr) {
      ACME_LOGE("Missing key");
      return {};
    }

    switch (const mbedtls_pk_type_t type = mbedtls_pk_get_type(key); type) {
      case MBEDTLS_PK_RSA:   return JWK(mbedtls_pk_rsa(*key));
      case MBEDTLS_PK_ECKEY: return JWK(mbedtls_pk_ec(*key));
      default:
        ACME_LOGE("unknown key type %d", type);
        return {};
    }
  }
} // namespace RFC7517

namespace RFC7638
{
  auto JWSThumbprint(mbedtls_pk_context *key) -> String
  {
    const String json = RFC7517::JWK(key);

    const mbedtls_md_info_t *mdi = mbedtls_md_info_from_type(MBEDTLS_MD_SHA256);
    if (mdi == nullptr) {
      ACME_LOGE("mbedtls_md_info_from_type: md_info not found");
      return {};
    }

    const unsigned char hash_size = mbedtls_md_get_size(mdi);
    std::vector<unsigned char> hash(hash_size, 0);

    const int ret = mbedtls_md(mdi, reinterpret_cast<const unsigned char *>(json.c_str()), json.length(), hash.data());
    if (ret != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_md", ret);
      return {};
    }

    return RFC4648::Base64UrlStr(hash.data(), hash.size());
  }
} // namespace RFC7638

Acme::ClientBase::ClientBase(const char root_certificates_[]) :
  root_certificates{root_certificates_}
{
  mbedtls_entropy_init(&entropy);
  mbedtls_ctr_drbg_init(&ctr_drbg);

  if (const int err = mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy, nullptr, 0); err != 0) {
    ACME_MBEDTLS_LOGE("mbedtls_ctr_drbg_seed", err);
    throw std::bad_alloc();
  }
}

auto Acme::ClientBase::perform_json_query(const String &query, const String *topost) -> std::optional<JsonDocument>
{
  auto ret = client->perform_json_query(query, topost);
  if (ret)
    nonce = client->nonce_use();
  return ret;
}

auto Acme::HttpsClient::perform_json_query(const String &query, const String *topost) -> std::optional<JsonDocument>
{
  auto response = perform_query(query, topost, nullptr);
  if (!response) {
    ACME_LOGD("perform_query failed");
    return std::nullopt;
  }
  JsonDocument root;
  if (const DeserializationError jsonError = deserializeJson(root, response->c_str()); jsonError) {
    ACME_LOGE("Failed to parse response: %s", jsonError.c_str());
    return std::nullopt;
  }

  return std::make_optional(std::move(root));
}

auto Acme::HttpsClient::parse_retry_after(const char *value) -> time_t
{
  time_t ret;

  // TODO 🛡️👮: Use stroul(3) instead of atol(3)
  if (isdigit(value[0]) != 0)
    ret = time(nullptr) + atol(value);
  else
    ret = RFC3339::parse_timestamp(value);

  {
    std::array<char,32>str;
    ACME_LOGI("suppressing next requests until %.24s GMT", ctime_r(&ret, str.data()));
    const time_t now = time(nullptr);
    ACME_LOGI("%li seconds from now %.24s GMT", ret - now, ctime_r(&now, str.data()));
  }

  return ret;
}

void Acme::HttpsClient::display_error(const char *json)
{
  JsonDocument root;
  if (const DeserializationError jsonError = deserializeJson(root, json); jsonError) {
    ACME_LOGE("could not parse error JSON (%s)", jsonError.c_str());
  }
  else {
    const char *type = root["type"];
    const char *detail = root["detail"];
    ACME_LOGI("%s: %s", type != nullptr ? type : "(null)", detail != nullptr ? detail : "(null)");
  }
}

#if defined(ESP32) or defined(ESP8266)
Acme::Esp32HttpsClient::Esp32HttpsClient(const char root_certificates_[]) :
  root_certificates{root_certificates_}
{
  client = create();
}

Acme::Esp32HttpsClient::~Esp32HttpsClient()
{
  destroy();
}

auto Acme::Esp32HttpsClient::create() -> esp_http_client_handle_t
{
  esp_http_client_config_t httpc;

  memset(&httpc, 0, sizeof(httpc));
  httpc.event_handler = HttpEvent;
  httpc.url = "https://youporn.com/";
  httpc.user_data = this;
  httpc.user_agent = version;
  if (root_certificates)
    httpc.cert_pem = root_certificates;
#ifdef CONFIG_MBEDTLS_CERTIFICATE_BUNDLE
  else
    httpc.crt_bundle_attach = arduino_esp_crt_bundle_attach;
#endif
  return esp_http_client_init(&httpc);
}

auto Acme::Esp32HttpsClient::rebuild() -> esp_http_client_handle_t
{
  destroy();
  client = create();
  return client;
}

void Acme::Esp32HttpsClient::destroy()
{
  if (client != nullptr) {
    esp_err_t err = esp_http_client_close(client);
    if (err != ESP_OK)
      ACME_LOGE("esp_http_client_close failed 0x%04x, %s", err, esp_err_to_name(err));
    if ((err = esp_http_client_cleanup(client)) != ESP_OK)
      ACME_LOGE("esp_http_client_cleanup failed 0x%04x, %s", err, esp_err_to_name(err));
    client = nullptr;
  }
}

auto Acme::Esp32HttpsClient::perform_query(const String &query, const String *topost, const char *accept_message) -> std::optional<String>
{
  ACME_LOGV("%s %s", topost == nullptr ? "GET" : "POST", query.c_str());
  if (topost != nullptr)
    ACME_LOGV("%s", topost->c_str());

  location = "";
  reply_buffer = "";
  retry_after = 0;

  esp_err_t err = esp_http_client_set_url(client, query.c_str());
  if (err != ESP_OK) {
    ACME_LOGE("esp_http_client_set_url error 0x%04x, %s", err, esp_err_to_name(err));
    return std::nullopt;
  }

  if (topost != nullptr) {
    if ((err = esp_http_client_set_post_field(client, topost->c_str(), topost->length())) != ESP_OK) {
      ACME_LOGE("set_post_field error 0x%04x, %s", err, esp_err_to_name(err));
      return std::nullopt;
    }
    ACME_LOGV("set_post_field %s length %d", topost->c_str(), topost->length());

    if ((err = esp_http_client_set_header(client, "Content-Type", "application/jose+json")) != ESP_OK)
      ACME_LOGE("client_set_header(\"Content-Type\"=\"application/jose+json\") error 0x%04x, %s", err, esp_err_to_name(err));
    // Don't fail on this.

    // Do a POST query if we're posting data.
    if ((err = esp_http_client_set_method(client, HTTP_METHOD_POST)) != ESP_OK) {
      ACME_LOGE("client_set_method error %d, %s", err, esp_err_to_name(err));
      return std::nullopt;
    }
  }

  // When this parameter is supplied, the "Accept:" is implied
  if (accept_message) {
    if ((err = esp_http_client_set_header(client, "Accept", accept_message)) != ESP_OK)
      ACME_LOGE("esp_client_set_header(\"Accept\"=%s) error %d, %s", accept_message, err, esp_err_to_name(err));
      // Don't fail on this.
  }

  if ((err = esp_http_client_perform(client)) != ESP_OK) {
    ACME_LOGE("esp_http_client_perform() failed 0x%04x, %s", err, esp_err_to_name(err));
    if (err == -MBEDTLS_ERR_NET_RECV_FAILED || err == ESP_ERR_HTTP_FETCH_HEADER)
      rebuild();
    return std::nullopt;
  }

  // Ok, now the data has been captured in Acme::Esp32HttpsClient::HttpEvent, just pass it on and finish up.
  ACME_LOGV(" -> %s", reply_buffer.c_str());

  if (const int code = esp_http_client_get_status_code(client); code > 400) {
    ACME_LOGE("server returned HTTP status code %d", code);
    display_error(reply_buffer.c_str());
    return std::nullopt;
  }
  return std::make_optional(std::move(reply_buffer));
}

/* This function catches HTTP headers (two of which we trap), and data sent to us as replies.
 * We gatter the latter in the reply_buffer field, whose alloc/free is rather sensitive. */
esp_err_t Acme::Esp32HttpsClient::HttpEvent(esp_http_client_event_t *event)
{
  auto *client = static_cast<Esp32HttpsClient *>(event->user_data);
  switch (event->event_id) {
    case HTTP_EVENT_ON_HEADER:
      ACME_LOGV("%s: %s", event->header_key, event->header_value);
      if (strcasecmp(event->header_key, "replay-nonce") == 0)
        client->nonce = event->header_value;
      else if (strcasecmp(event->header_key, "location") == 0)
        client->location = event->header_value;
      else if (strcasecmp(event->header_key, "retry-after") == 0) {
        client->retry_after = client->parse_retry_after(event->header_value);
      }
      break;
    case HTTP_EVENT_ON_DATA:
      ACME_LOGV("HTTP_EVENT_ON_DATA (len %d)", event->data_len);
      client->reply_buffer.concat(static_cast<uint8_t *>(event->data), event->data_len);
    case HTTP_EVENT_ON_FINISH:
      ACME_LOGV("received %s", client->reply_buffer.c_str());
      break;
    default:
      break;
  }
  return ESP_OK;
}

auto Acme::Esp32HttpsClient::request_nonce(const String &uri) -> String
{
  ACME_LOGV("(%s)", uri.c_str());

  esp_err_t err = esp_http_client_set_url(client, uri.c_str());
  if (err != ESP_OK) {
    ACME_LOGE("esp_http_client_set_url error 0x%04x, %s", err, esp_err_to_name(err));
    return {};
  }

  if ((err = esp_http_client_set_method(client, HTTP_METHOD_HEAD)) != ESP_OK) {
    ACME_LOGE("client_set_method error 0x%04x, %s", err, esp_err_to_name(err));
    return {};
  }

  if ((err = esp_http_client_perform(client)) != ESP_OK) {
    ACME_LOGE("client_perform error 0x%04x, %s", err, esp_err_to_name(err));
    if (err == -MBEDTLS_ERR_NET_RECV_FAILED || err == ESP_ERR_HTTP_FETCH_HEADER)
      rebuild();
    return {};
  }

  // It should already be there, so report back
  return std::move(nonce);
}
#endif // ESP32 || ESP8266

Acme::ClientBase::~ClientBase()
{
  webserver_disable();
  certificate.reset();
  delete order;
  identifiers.clear();
  delete directory;
  delete client;
}

void Acme::ClientBase::setExternalAccountBinding(const char *kid, const char *keyBase64Url)
{
  eab_key = RFC4648::Unbase64UrlStr(keyBase64Url);
  eab_kid = kid;
  ACME_LOGI("new EAB credentials for %s (key length %zu)", eab_kid.c_str(), eab_key.length());
}

void Acme::ClientBase::setAccountEmail(const char *address)
{
  ACME_LOGD("(%s)", address);
  email_address = address;
}

auto Acme::ClientBase::loop() -> bool
{
  struct timeval tv;
  (void) gettimeofday(&tv, nullptr);
  return loop(tv.tv_sec);
}

auto Acme::ClientBase::loop(time_t now) -> bool
{
  if (hold_down > now) {
    ACME_LOGV("Supressing further requests until %.24s", ctime(&hold_down));
    return false;
  }

  // Avoid calling ACME server if we are way before renewal
  if (!needs_renewal(now)) {
    ACME_LOGV("Still in grace period");
    return false;
  }

  // TODO 🔎: query ARI here? add suppression timer (eg. every(12, hours, [](){}))

  if (client == nullptr) {
    ACME_LOGD("enable https client");
    client = client_new(root_certificates); // throws!
  }
  if (client->retry_after > now) {
    ACME_LOGV("Waiting for retry_after to expire, %li seconds", client->retry_after - now);
    return false;
  }
  if (directory == nullptr) {
    if ((directory = request_directory()) == nullptr) {
      ACME_LOGE("cannot get directory descriptor");
      return false;
    }
  }

  if (nonce.isEmpty()) {
    nonce = client->request_nonce(directory->newNonce);
    if (nonce.isEmpty()) {
      ACME_LOGE("failed to get new nonce");
      return false;
    }
    ACME_LOGV("got new nonce: %s", nonce.c_str());
  }

  if (account_location.isEmpty()) {
    ACME_LOGI("Querying existing account");
    const String contact = email_address ? email_address : "";
    if (request_newAccount(contact, true)) {
      ACME_LOGI("Reusing account %s", account_location.c_str());
    }
    else if (request_newAccount(contact, false)) {
      ACME_LOGI("Created new account %s", account_location.c_str());
    }
    else {
      ACME_LOGE("couldn't create new account");
      return false;
    }
    if (account_location.isEmpty()) {
      ACME_LOGE("fail, no account available");
      return false;
    }
  }

  std::optional<Status> response;
  if (order == nullptr) {
    ACME_LOGD("no current order, requesting a new one");
    if (!(response = request_newOrder(directory->newOrder))) {
      ACME_LOGE("failed to order a new certificate");
      return false;
    }
  }
  else {
    if (!(response = update(*order))) {
      ACME_LOGD("updating state of current order failed");
      return false;
    }
  }
  const Status order_status = *response;
  {
    std::array<char,32>str;
    const int offset = static_cast<int>(order_status);
    ACME_LOGI("Order is %s, and expires %.24s GMT", acme_status_str[offset], ctime_r(&order->expires, str.data()));
  }
  auto not_downloaded = [](const std::unique_ptr<Authorization> &item) {
    return item->identifiers.empty();
  };
  auto const &it = std::find_if(order->authorizations.begin(), order->authorizations.end(), not_downloaded);
  if (it != order->authorizations.end()) {
    if (!download_authorizations()) {
      ACME_LOGD("Downloading authorizations failed");
      return false;
    }
  }

  switch (order_status) {
    case Status::PENDING:
      // poll authorization of currently triggered challenge
      // if we drop out here (eg. ZeroSSL, poll order instead)
      for (auto &authorization : order->authorizations) {
        if (authorization->challenge.status == Status::PENDING && authorization->challenge.active) {
          auto response = update(*authorization);
          if (!response)
            return false;
          else if (*response == Status::VALID)
            break; // Trigger next challenge
          return false;
        }
      }
      // nothing active is pending, trigger next challenge
      for (auto &authorization : order->authorizations) {
        if (authorization->challenge.status == Status::PENDING && !authorization->challenge.active) {
          if (trigger(authorization->challenge))
            authorization->challenge.active = true;
          return false;
        }
      }
      break;
    case Status::PROCESSING:
      ACME_LOGI("waiting %d seconds for server to process current order", SHORT_HOLD_DOWN);
      hold_down = time(nullptr) + SHORT_HOLD_DOWN; // Suppress requests for some seconds
      ACME_LOGD("disable http client"); // to avoid stale connection problems
      delete client, client = nullptr;
      break;
    case Status::READY:
      webserver_disable();
      finalize_order();
      break;
    case Status::VALID:
      if (!order->certificate.isEmpty() && download_certificate() != nullptr) {
        delete order, order = nullptr;

        ACME_LOGD("disable http client");
        delete client, client = nullptr;

        delete directory, directory = nullptr;

        return true;
      }
      break;
    default:
    case Status::INVALID:
    case Status::EXPIRED:
      hold_down = time(nullptr) + LONG_HOLD_DOWN;
      [[ fallthrough ]];
    case Status::CANCELLED:
    case Status::DEACTIVATED:
    {
      const int offset = static_cast<int>(order_status);
      ACME_LOGI("order is %s, deleting current order", acme_status_str[offset]);
      delete order, order = nullptr; // restart ACME certification process

      ACME_LOGD("disable http client");
      delete client, client = nullptr;
      break;
    }
  }
  return false;
}

auto Acme::ClientBase::generate_EAB(const String &url, const String &account, const String &key, const String &jwk) -> std::optional<String>
{
  const String eab = "{"
    "\"alg\":\"HS256\","
    "\"kid\":\"" + account + "\","
    "\"url\":\"" + url + "\""
  "}";
  return RFC7515::message(RFC7515::HmacKey{key}, eab, jwk);
}

// Make an ACME message, this version makes the ones that include a "jwk" field.
auto Acme::ClientBase::make_message_JWK(
  const String &url,
  const String &payload,
  const String &jwk
) ->
  std::optional<String>
{
  ACME_LOGV("(%s, %s, %s)", url.c_str(), payload.c_str(), jwk.c_str());

  const String nonce = nonce_use();
  if (nonce.isEmpty())
    return {};

  // TODO 🄕: Make this block a function, for next algorithm :-)
  String alg;
  switch (const mbedtls_pk_type_t type = mbedtls_pk_get_type(account_key.get()); type) {
    case MBEDTLS_PK_RSA:   alg = "RS256"; break;
    case MBEDTLS_PK_ECKEY: alg = "ES384"; break;
    default:
      ACME_LOGE("unknown key type %d", type);
      return {};
  }

  const String p_rotected = "{"
    "\"alg\":\"" + alg + "\","
    "\"jwk\":" + jwk + ","
    "\"nonce\":\"" + nonce + "\","
    "\"url\":\"" + url + "\""
  "}";
  
  return RFC7515::message(account_key.get(), p_rotected, payload, mbedtls_ctr_drbg_random, &ctr_drbg);
}

/* Fetch the "directory" of the ACME server.
 * This gives us a set of URLs for our queries. */
auto Acme::ClientBase::request_directory() -> Acme::Directory *
{
  if (directory_location.isEmpty()) {
    ACME_LOGE("no ACME server configured");
    return nullptr;
  }

  ACME_LOGI("Querying directory at %s", directory_location.c_str());

  auto response = perform_json_query(directory_location, nullptr);
  if (!response) {
    ACME_LOGE("failed perform_json_query");
    return nullptr;
  }

  JsonDocument json = *response;
  return new Directory(json["newNonce"], json["newAccount"], json["newOrder"]);
}

auto Acme::ClientBase::nonce_use() -> String
{
  ACME_LOGV("use nonce %s", nonce.c_str());
  return std::move(nonce);
}

auto Acme::PrivateKey::generate_RSA_key(
  int (*f_rng)(void *, unsigned char *, size_t),
  mbedtls_ctr_drbg_context *p_rng
)
  -> Acme::PrivateKey *
{
  ACME_LOGI("Generating RSA-%u private key ...", rsa_keysize);

  auto *pk = new PrivateKey();

  int ret = mbedtls_pk_setup(pk, mbedtls_pk_info_from_type(MBEDTLS_PK_RSA));
  if (ret != 0) {
    ACME_MBEDTLS_LOGE("mbedtls_pk_setup", ret);
    delete pk;
    return nullptr;
  }

  if ((ret = mbedtls_rsa_gen_key(mbedtls_pk_rsa(*pk), f_rng, p_rng, rsa_keysize, rsa_exponent)) != 0) {
    ACME_MBEDTLS_LOGE("mbedtls_rsa_gen_key", ret);
    delete pk;
    return nullptr;
  }

  return pk;
}

auto Acme::PrivateKey::generate_EC_key(
  int (*f_rng)(void *, unsigned char *, size_t),
  mbedtls_ctr_drbg_context *p_rng
)
  -> Acme::PrivateKey *
{
  ACME_LOGI("Generating EC secp384r1 private key ...");

  auto *pk = new PrivateKey();

  int ret = mbedtls_pk_setup(pk, mbedtls_pk_info_from_type(MBEDTLS_PK_ECKEY));
  if (ret != 0) {
    ACME_MBEDTLS_LOGE("mbedtls_pk_setup", ret);
    delete pk;
    return nullptr;
  }

  if ((ret = mbedtls_ecp_gen_key(MBEDTLS_ECP_DP_SECP384R1, mbedtls_pk_ec(*pk), f_rng, p_rng)) != 0) {
    ACME_MBEDTLS_LOGE("mbedtls_ecp_gen_key", ret);
    delete pk;
    return nullptr;
  }

  return pk;
}

auto Acme::PrivateKey::generate(
  mbedtls_pk_type_t type,
  int (*f_rng)(void *, unsigned char *, size_t),
  mbedtls_ctr_drbg_context *p_rng
) ->
  Acme::PrivateKey *
{
  if (f_rng == nullptr || p_rng == nullptr) {
    ACME_LOGE("missing random generator");
    return nullptr;
  }

  switch (type) {
    case MBEDTLS_PK_RSA:   return generate_RSA_key(f_rng, p_rng);
    case MBEDTLS_PK_ECKEY: return generate_EC_key(f_rng, p_rng);
    default:
      ACME_LOGE("unknown key type %d", type);
      return nullptr;
  }
}

// Read a private key from a file, caller can specify file name.
auto Acme::PrivateKey::load(const String &filename) -> Acme::PrivateKey *
{
  ACME_LOGV("(%s)", filename.c_str());

  auto *pk = new PrivateKey();

  if (const int ret = mbedtls_pk_parse_keyfile(pk, filename.c_str(), nullptr); ret != 0) {
    ACME_MBEDTLS_LOGE("mbedtls_pk_parse_keyfile(%s)", ret, filename.c_str());
    delete pk;
    return nullptr;
  }

  ACME_LOGI("read key file %s ok", filename.c_str());
  return pk;
}

/* Write a private key to a file, caller can specify file name.
 * Prepends our path prefix prior to use. */
auto Acme::PrivateKey::store(const String &filename) -> bool
{
  ACME_LOGV("(%s)", filename.c_str());

  unsigned char key_str[2048] = {'\0'}, *key_ptr;
  size_t key_len;
  // PEM or DER?
  if (filename.endsWith(".pem")) {
    // TODO 🪤💣: This causes severe heap degradation on ESP32
    // heap_caps_print_heap_info(MALLOC_CAP_DEFAULT);
    if (const int ret = mbedtls_pk_write_key_pem(this, key_str, sizeof(key_str)); ret != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_write_key_pem", ret);
      return false;
    }
    // heap_caps_print_heap_info(MALLOC_CAP_DEFAULT);
    key_len = strlen(reinterpret_cast<const char *>(key_str));
    key_ptr = key_str;
    ACME_LOGV("key_ptr: %s", key_ptr);
  }
  else {
    const int ret = mbedtls_pk_write_key_der(this, key_str, sizeof(key_str));
    if (ret < 0) {
      ACME_MBEDTLS_LOGE("mbedtls_write_key_der", -ret);
      return false;
    }
    key_len = ret;
    key_ptr = key_str + sizeof(key_str) - key_len;
  }
  ACME_LOGD("key_len: %li", key_len);

  if (!File::write(filename.c_str(), key_ptr, key_len)) {
    ACME_LOGE("Failed to store private key in %s", filename.c_str());
    return false;
  }

  ACME_LOGI("Written private key to %s", filename.c_str());
  return true;
}

auto Acme::ClientBase::setup_key(mbedtls_pk_type_t type, const String &filename) -> std::shared_ptr<PrivateKey>
{
  Acme::PrivateKey *ret = PrivateKey::load(filename);
  if (ret == nullptr) {
    ret = PrivateKey::generate(type, mbedtls_ctr_drbg_random, &ctr_drbg);
    ret->store(filename);
  }
  return std::make_shared<Acme::PrivateKey>(*ret);
}

auto Acme::ClientBase::request_newAccount(const String &contact, bool onlyReturnExisting) -> bool
{
  ACME_LOGV("(%s%s)", contact.c_str(), onlyReturnExisting ? ", onlyReturnExisting" : "");

  const String jwk = RFC7517::JWK(account_key.get());
  if (jwk.isEmpty()) {
    ACME_LOGD("JWK() failed");
    return false;
  }

  String payload = "{";
  if (onlyReturnExisting) {
    payload += "\"onlyReturnExisting\":true";
  }
  else {
    payload += "\"contacts\":[";
    if (!contact.isEmpty()) {
      if (contact.startsWith("mailto:"))
        payload += "\"" + contact + "\"";
      else
        payload += "\"mailto:" + contact + "\"";
    }
    payload += "],";
    payload += "\"termsOfServiceAgreed\":true";
    if (!eab_kid.isEmpty() && !eab_key.isEmpty()) {
      payload += ",\"externalAccountBinding\":";
      auto eab = generate_EAB(directory->newAccount, eab_kid, eab_key, jwk);
      if (!eab)
        return false;

      payload += *eab;
    }
  }
  payload += "}";

  auto msg = make_message_JWK(directory->newAccount, payload, jwk);
  if (!msg)
    return false;

  const String content = *msg;
  ACME_LOGV("content: %s", content.c_str());

  auto response = perform_json_query(directory->newAccount, &content);
  if (!response) {
    ACME_LOGE("perform_json_query -> null");
    return false;
  }

  JsonDocument account = *response;
  const char *account_status = account["status"];
  if (account_status == nullptr || strcmp(account_status, "valid") != 0) {
    ACME_LOGE("account isn't valid (%s)", account_status != nullptr ? account_status : "(null)");
    return false;
  }
  if (client->location.isEmpty()) {
    ACME_LOGE("server didn't publish account location");
    return false;
  }
  account_location = std::move(client->location);
  return true;
}

auto Acme::ClientBase::request_newOrder(const String &newOrderUrl) -> std::optional<Acme::Status>
{
  ACME_LOGV("(%s)", newOrderUrl.c_str());

  String req = "{\"identifiers\":[";
  for (auto it=identifiers.begin(); it !=identifiers.end(); it++) {
    int type = static_cast<int>((*it)->type);
    req += String("{"
      "\"type\":\"") + identifier_type_map[type] + "\","
      "\"value\":\"" + (*it)->value + "\""
    "}";
    if (std::next(it) != identifiers.end())
      req += ",";
  }
  req += "]}";
  ACME_LOGI("Requesting new order for %s", req.c_str());

  auto response = send_message(newOrderUrl, req);
  if (!response)
    return std::nullopt;

  JsonDocument json = *response;
  if (client->location.isEmpty()) {
    ACME_LOGE("server didn't publish order location");
    return std::nullopt;
  }
  ACME_LOGV("client->location = %s", client->location.c_str());
  const String location = std::move(client->location);
  order = new Order(location);
  if (order->read(json)) {
    // TODO ≢: Order may be ready (if pre-validation was done!)
    if (order->status != Status::PENDING) {
      ACME_LOGE("new order isn't pending");
      delete order, order = nullptr;
      return std::nullopt;
    }
    return std::make_optional(order->status);
  }
  
  delete order, order = nullptr;
  return std::nullopt;
}

auto Acme::Order::read(JsonDocument &json) -> std::optional<Acme::Status>
{
  const char *status_str = json["status"];
  if (status_str == nullptr)
    return std::nullopt;
  status = acme_status_map[status_str];

  finalize = (const char *)json["finalize"];

  const char *expires_str = json["expires"];
  if (expires_str != nullptr)
    expires = RFC3339::parse_timestamp(expires_str);

  const JsonArray jia = json["identifiers"];
  ACME_LOGD("%zu identifiers", jia.size());
  for (auto item : jia) {
    std::unique_ptr<struct Identifier> identifier(new Identifier);
    if (item["type"] != "dns")
      ACME_LOGE("invalid type %s", (const char *)item["type"]);
    identifier->type = Identifier::Type::DNS;
    identifier->value = (const char *)item["value"];
    ACME_LOGV("Identifiers %i %s", static_cast<int>(identifier->type), identifier->value.c_str());
    identifiers.emplace_back(std::move(identifier));
  }

  const JsonArray jaa = json["authorizations"];
  ACME_LOGD("%zu authorizations", jaa.size());
  for (auto item : jaa) {
    const char *url = item;
    std::unique_ptr<Authorization> authorization(new Authorization(url));
    ACME_LOGV("Auth %s", authorization->url.c_str());
    authorizations.emplace_back(std::move(authorization));
  }
  return std::make_optional(status);
}

/* Enable the challenge response on local webserver, and
 * Send a request to the ACME server to read our token.
 * We're only implementing the http-01 protocol here... */
auto Acme::ClientBase::trigger(const struct Authorization::Challenge &challenge) -> std::optional<Acme::Status>
{
  if (webserver != nullptr)
    webserver_enable(challenge.token);
  else
    ACME_LOGE("external web server not set");

  ACME_LOGI("Trigger challenge at %s", challenge.url.c_str());

  auto response = send_message(challenge.url, "{}");
  if (!response)
    return std::nullopt;

  JsonDocument root = *response;
  const char *status_str = root["status"];
  if (status_str == nullptr)
    return std::nullopt;

  return std::make_optional(acme_status_map[status_str]);
}

/* The default format of the certificate is application/pem-certificate-chain
 * The ACME client MAY request other formats by [..] use the media type
 * "application/pkix-cert" [RFC2585] or "application/pkcs7-mime" [RFC5751] to
 * request the end-entity certificate in DER format.
 * Server support for alternate formats is OPTIONAL. */
auto Acme::ClientBase::download_certificate() -> std::shared_ptr<Certificate>
{
  ACME_LOGI("Downloading certificate from %s", order->certificate.c_str());

  auto msg = make_message_KID(order->certificate, HttpsClient::POST_AS_GET);
  if (!msg)
    return nullptr;

  const String content = *msg;
  auto response = client->perform_query(order->certificate, &content, "application/pem-certificate-chain");
  if (!response) {
    ACME_LOGD("perform_query didn't return data");
  }
  else {
    const String chain = *response;
    if (cert_fn == nullptr)
      ACME_LOGE("missing certificate filename");
    else if (File::write(cert_fn, reinterpret_cast<const unsigned char *>(chain.c_str()), chain.length()))
      return read_certificate();
  }
  return nullptr;
}

auto Acme::ClientBase::download_authorization(Acme::Authorization &authorization) -> std::optional<Acme::Status>
{
  auto response = send_message(authorization.url, Acme::HttpsClient::POST_AS_GET);
  if (!response)
    return std::nullopt;

  JsonDocument json = *response;
  return authorization.read(json);
}

auto Acme::ClientBase::download_authorizations() -> bool
{
  ACME_LOGV("");
  for (auto &&authorization : order->authorizations) {
    if (authorization->identifiers.empty()) {
      auto response = download_authorization(*authorization);
      if (!response) {
        ACME_LOGE("Failed to download authorization %s", authorization->url.c_str());
        return false;
      }
    }
  }
  return true;
}

auto Acme::Authorization::read(JsonDocument &json) -> std::optional<Acme::Status>
{
  const char *status_str = json["status"];
  if (status_str == nullptr)
    return std::nullopt;

  status = acme_status_map[status_str];

  const char *expires_str = json["expires"];
  if (expires_str != nullptr)
    expires = RFC3339::parse_timestamp(expires_str);

  // we're not reading the identifier, as we're not using it
  const JsonArray jca = json["challenges"];
  ACME_LOGD("%zu challenges", jca.size());

  for (auto item : jca) {
    const char *ct = item["type"];
    ACME_LOGV("type=%s", ct != nullptr ? ct : "(null)");
    if (ct != nullptr && strcmp(ct, "http-01") == 0) {
      challenge.type = Challenge::Type::HTTP_01;
      const char *c_status_str = item["status"];
      if (c_status_str != nullptr)
        challenge.status = acme_status_map[c_status_str];
      challenge.url = (const char *)item["url"];
      challenge.token = (const char *)item["token"];
      challenge.active = false; // new, internal
      ACME_LOGV("challenge: type: %d, url: %s, token: %s, status: %d, active: %d",
        static_cast<int>(challenge.type),
        challenge.url.c_str(),
        challenge.token.c_str(),
        static_cast<int>(challenge.status),
        challenge.active);
      return std::make_optional(status);
    }
  }
  return std::nullopt;
}

auto Acme::ClientBase::make_message_KID(
  const String &url,
  const String &payload
) ->
  std::optional<String>
{
  ACME_LOGV("(%s, %s)", url.c_str(), payload.c_str());

  const String protected_ = make_protected_KID(url);
  if (protected_.isEmpty()) {
    ACME_LOGD("MakeProtectedKID -> null");
    return {};
  }
  ACME_LOGV("protected_: %s", protected_.c_str());

  return RFC7515::message(account_key.get(), protected_, payload, mbedtls_ctr_drbg_random, &ctr_drbg);
}

auto Acme::ClientBase::make_protected_KID(const String &query) -> String
{
  const String nonce = nonce_use();
  if (nonce.isEmpty())
    return {};

  // TODO 🄕: de-dup this code block
  String alg;
  const mbedtls_pk_type_t type = mbedtls_pk_get_type(account_key.get());
  switch (type) {
    case MBEDTLS_PK_RSA:   alg = "RS256"; break;
    case MBEDTLS_PK_ECKEY: alg = "ES384"; break;
    default:
      ACME_LOGE("unknown key type %d", type);
      return {};
  }

  return "{"
    "\"alg\":\"" + alg + "\","
    "\"nonce\":\"" + nonce + "\","
    "\"url\":\"" + query + "\","
    "\"kid\":\"" + account_location + "\""
  "}";
}

/* Create an ASN1 representation of the list of alternative URLs.
 *
 * See https://github.com/ARMmbed/mbedtls/issues/1878 */
auto Acme::ClientBase::create_san_entries(mbedtls_x509write_csr req) -> int
{
  size_t l = 20;
  for (auto &&identifier : identifiers)
    l += identifier->value.length() + 20;
  auto *buf = static_cast<unsigned char *>(alloca(l)), *p = buf + l;

  int len = 0, ret = 0;

  for (auto &&identifier : identifiers) {
    MBEDTLS_ASN1_CHK_ADD(len, mbedtls_asn1_write_raw_buffer(&p, buf, (unsigned char *)identifier->value.c_str(), identifier->value.length()));
    MBEDTLS_ASN1_CHK_ADD(len, mbedtls_asn1_write_len(&p, buf, identifier->value.length()));
    MBEDTLS_ASN1_CHK_ADD(len, mbedtls_asn1_write_tag(&p, buf, MBEDTLS_ASN1_CONTEXT_SPECIFIC | 2));
  }

  MBEDTLS_ASN1_CHK_ADD(len, mbedtls_asn1_write_len(&p, buf, len));
  MBEDTLS_ASN1_CHK_ADD(len, mbedtls_asn1_write_tag(&p, buf, MBEDTLS_ASN1_CONSTRUCTED | MBEDTLS_ASN1_SEQUENCE));

  return mbedtls_x509write_csr_set_extension(&req, MBEDTLS_OID_SUBJECT_ALT_NAME, MBEDTLS_OID_SIZE(MBEDTLS_OID_SUBJECT_ALT_NAME), p, len);
}

/* A Certificate Signing Request (CSR) is a required parameter to the Finalize query.
 * It can be used to add administrative data to the process, and is validated thoroughly.
 * One such additional parameter is the domain private key. */
auto Acme::ClientBase::generate_CSR() -> String
{
  ACME_LOGV("()");

  mbedtls_x509write_csr req;
  // memset(&req, 0, sizeof(req));
  mbedtls_x509write_csr_init(&req);

  mbedtls_x509write_csr_set_md_alg(&req, MBEDTLS_MD_SHA256);
  // Not set by default
  // mbedtls_x509write_csr_set_key_usage(&req, MBEDTLS_X509_NS_CERT_TYPE_SSL_CLIENT);
  mbedtls_x509write_csr_set_key(&req, certificate_key.get());

  if (!identifiers.empty()) {
    // Specify our first identifier as the common name
    const String cn = "CN=" + identifiers[0]->value;
    if (const int ret = mbedtls_x509write_csr_set_subject_name(&req, cn.c_str());ret != 0) {
      ACME_MBEDTLS_LOGE("mbedtls_x509write_csr_set_subject_name", ret);
      mbedtls_x509write_csr_free(&req);
      return {};
    }
  }
  if (identifiers.size() > 1) {
    if (const int ret = create_san_entries(req); ret != 0) {
      ACME_MBEDTLS_LOGE("create_san_entries", ret);
      mbedtls_x509write_csr_free(&req);
      return {};
    }
  }

  const int buflen = 4096; // This is used in mbedtls_x509 functions internally
  std::vector<unsigned char>buffer(buflen, 0);
  
  // RFC 8555 §7.4 says write in (base64url-encoded) DER format
  const int len = mbedtls_x509write_csr_der(&req, buffer.data(), buffer.size(), mbedtls_ctr_drbg_random, &ctr_drbg);
  mbedtls_x509write_csr_free(&req);
  if (len < 0) {
    ACME_MBEDTLS_LOGE("mbedtls_x509write_csr_der", len);
    return {};
  }

  // output is written at the end of the buffer, so point to it
  const unsigned char *p = buffer.data() + buflen - len;
  return RFC4648::Base64UrlStr(p, len);
}

auto Acme::ClientBase::send_message(const String &url, const String &content) -> std::optional<JsonDocument>
{
  auto msg = make_message_KID(url, content);
  if (!msg)
    return std::nullopt;
  ACME_LOGV("msg: %s", msg->c_str());

  auto response = perform_json_query(url, &*msg);
  if (!response)
    ACME_LOGE("perform_json_query didn't return data");
  return response;
}

auto Acme::ClientBase::finalize_order() -> bool
{
  ACME_LOGI("Uploading CSR to %s", order->finalize.c_str());

  const String csr = generate_CSR();
  if (csr.isEmpty()) {
    ACME_LOGE("can't generate CSR");
    return false;
  }
  const String csr_param = "{"
    "\"csr\":\"" + csr + "\""
  "}";

  auto response = send_message(order->finalize, csr_param);
  if (!response)
    return false;

  JsonDocument json = *response;
  const char *certificate = json["certificate"];
  if (certificate == nullptr) {
    ACME_LOGE("Couldn't find certificate URL");
    return false;
  }

  order->certificate = certificate;
  return true;
}

auto Acme::Resource::update_status(JsonDocument &json) -> std::optional<Acme::Status>
{
  const char *status_str = json["status"];
  if (status_str == nullptr)
    return std::nullopt;

  status = acme_status_map[status_str];
  return std::make_optional(status);
}

auto Acme::ClientBase::update(Acme::Resource &resource) -> std::optional<Acme::Status>
{
  ACME_LOGV("(%s)", resource.url.c_str());

  auto response = send_message(resource.url, HttpsClient::POST_AS_GET);
  if (!response)
    return std::nullopt;

  JsonDocument json = *response;
  return resource.update_status(json);
}

// Read the certificate from local storage
auto Acme::ClientBase::read_certificate() -> std::shared_ptr<Certificate>
{
  ACME_LOGV("()");
  if (cert_fn == nullptr) {
    ACME_LOGE("fail, no certificate file name");
    return nullptr;
  }

  // TODO 📄💥: This binds the client to FS, move this part
  certificate = std::shared_ptr<Certificate>(new Certificate);

  if (const int ret = mbedtls_x509_crt_parse_file(certificate.get(), cert_fn); ret == 0) {
    ACME_LOGD("we have a certificate in %s", cert_fn);
    ACME_LOGI("Certificate is valid from %04d-%02d-%02d %02d:%02d:%02d GMT to %04d-%02d-%02d %02d:%02d:%02d GMT",
      certificate->valid_from.year, certificate->valid_from.mon, certificate->valid_from.day,
      certificate->valid_from.hour, certificate->valid_from.min, certificate->valid_from.sec,
      certificate->valid_to.year, certificate->valid_to.mon, certificate->valid_to.day,
      certificate->valid_to.hour, certificate->valid_to.min, certificate->valid_to.sec);
    return certificate;
  }
  else if (ret != MBEDTLS_ERR_PK_FILE_IO_ERROR) {
    /* Only print unexpected errors, a non-existing certificate file isn't. */
    ACME_MBEDTLS_LOGE("could not read certificate from %s, mbedtls_x509_crt_parse_file", ret, cert_fn);
  }
  certificate.reset();
  return certificate;
}

auto Acme::ClientBase::needs_renewal() -> bool
{
  struct timeval now = {};
  gettimeofday(&now, nullptr);

  return needs_renewal(now.tv_sec);
}

auto Acme::ClientBase::needs_renewal(time_t now) -> bool
{
  if (certificate == nullptr)
    return true;

  // Check date ranges
  const time_t vfrom = TimeMbedToTimestamp(certificate->valid_from);
  if (vfrom > now) {
    ACME_LOGE("Certificate is not valid yet");
    return false;
  }

  const time_t vto = TimeMbedToTimestamp(certificate->valid_to);
  const long lifespan = (vto - vfrom) * CERTIFICATE_LIFESPAN_FACTOR;
  if (vto < now + lifespan) {
    ACME_LOGV("now: %li, vto: %li, vfrom: %li, validity: %lis, lifespan: %li", now, vto, vfrom, vto-vfrom, lifespan);
    ACME_LOGD("Certificate has expired or is going to expire soon (%.24s)", ctime(&vto));
    return true;
  }

  return false;
}

void Acme::ClientBase::addDnsIdentifier(const char *domain)
{
  std::unique_ptr<struct Identifier> identifier(new struct Identifier);
  identifier->type = Identifier::Type::DNS;
  identifier->value = domain;
  identifiers.emplace_back(std::move(identifier));
}

void Acme::ClientBase::setDnsIdentifier(const char *domain)
{
  addDnsIdentifier(domain);
}

void Acme::ClientBase::setDirectoryUrl(const char *url)
{
  ACME_LOGD("(%s)", url != nullptr ? url : "(null)");
  directory_location = url;
}

auto Acme::ClientBase::setCertificateFilename(const char *filename) -> std::shared_ptr<Certificate>
{
  ACME_LOGV("(%s)", filename != nullptr ? filename : "(null)");
  cert_fn = filename;
  return read_certificate();
}

void Acme::ClientBase::setWebServer(Acme::WebServer *server)
{
  webserver = server;
}

auto Acme::ClientBase::webserver_enable(const String &token) -> bool
{
  ACME_LOGV("%s", token.c_str());

  if (webserver == nullptr) {
    ACME_LOGE("configuration error, no webserver set");
    return false;
  }

  webserver_disable();

  // The file name that should be queried is a short form of the above remotefn
  static const char well_known[] = "/.well-known/acme-challenge/";

  return webserver->enable(well_known + token, token + "." + RFC7638::JWSThumbprint(account_key.get()));
}

void Acme::ClientBase::webserver_disable()
{
  if (webserver != nullptr && webserver->disable())
    ACME_LOGI("Disabled local web server resource");
}

#if defined(ESP32) or defined(ESP8266)
struct ChallengeContext
{
  String file, content;
};

auto Acme::Esp32WebServer::acme_http_get_handler(httpd_req_t *req) -> esp_err_t
{
  auto *ctx = static_cast<ChallengeContext *>(req->user_ctx);
  if (strcmp(req->uri, ctx->file.c_str()) == 0) {
    ACME_LOGV("URI %s", req->uri);
    httpd_resp_set_type(req, "text/plain");
    httpd_resp_send(req, ctx->content.c_str(), ctx->content.length());
  }
  else {
    ACME_LOGD("URI %s -> 404", req->uri);
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "File not found");
  }
  return ESP_OK;
}

auto Acme::Esp32WebServer::enable(const String &uri, const String &content) -> bool
{
  auto *ctx = new ChallengeContext{
    .file = uri,
    .content = content
  };
  webserver_handler = new httpd_uri_t{
    .uri = ctx->file.c_str(),
    .method = HTTP_GET,
    .handler = acme_http_get_handler,
    .user_ctx = ctx
  };

  if (const esp_err_t err = httpd_register_uri_handler(server, webserver_handler); err != ESP_OK) {
    ACME_LOGE("failed to register URI handler for %s (%d %s)", webserver_handler->uri, err, esp_err_to_name(err));
    delete static_cast<ChallengeContext *>(webserver_handler->user_ctx);
    delete webserver_handler;
    webserver_handler = nullptr;
    return false;
  }

  ACME_LOGI("Enabled local webserver resource %s", webserver_handler->uri);
  return true;
}

auto Acme::Esp32WebServer::disable() -> bool
{
  if (webserver_handler) {
    if (httpd_unregister_uri_handler(server, webserver_handler->uri, HTTP_GET) != ESP_OK)
      ACME_LOGE("failed to unregister httpd URI handler");

    delete static_cast<ChallengeContext *>(webserver_handler->user_ctx);
    delete webserver_handler;
    webserver_handler = nullptr;
    return true;
  }
  return false;
}
#endif // !ESP32 or !ESP8266

namespace Acme::File {
/* Utility function to read a file from loval storage
 *
 * Caller must free allocated memory
 *
 * The path is used as is (no prefix added), and length read is returned in the 2nd param. */
  auto read(const char *filename, size_t *plen) -> unsigned char *
  {
    FILE *f = fopen(filename, "re");
    if (f == nullptr) {
      ACME_LOGE("Could not open file %s, error %d", filename, errno);
      if (plen != nullptr) *plen = 0;
      return nullptr;
    }
    ACME_LOGV("(%s)", filename);
    if (fseek(f, 0L, SEEK_END) != 0) {
      ACME_LOGE("Cannot request file end position of %s", filename);
      (void) fclose(f);
      return nullptr;
    }
    const long len = ftell(f);
    (void) fseek(f, 0L, SEEK_SET);
    auto *buffer = static_cast<unsigned char *>(new unsigned char[len + 1]);
    const size_t total = fread(buffer, 1, len, f);
    buffer[total] = 0;
    (void) fclose(f);
    ACME_LOGD("read from %s, len %zu", filename, total);
    if (plen != nullptr) *plen = total;
    return buffer;
  }

  auto write(const char *filename, const unsigned char *buf, size_t buflen) -> bool
  {
    ACME_LOGI("Writing %s", filename);

    FILE *f = fopen(filename, "we");
    if (f == nullptr) {
      ACME_LOGE("Could not open %s for writing, error %d", filename, errno);
      return false;
    }

    const size_t written = fwrite(buf, 1, buflen, f);
    (void) fclose(f);
    if (written != buflen) {
      ACME_LOGE("Failed to write to %s, %zu of %zu written", filename, written, buflen);
      return false;
    }

    ACME_LOGV("Written %d bytes to %s", written, filename);
    return true;
  }
} // namespace Acme::File
