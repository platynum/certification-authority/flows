Testing ACME with client library for ESP32 (https://sourceforge.net/projects/esp32-acme-client/)

The ESP32 device will connect to a WiFi network, contact an NTP
timesource (pool.ntp.org), and requests a TLS certificate from
an ACME CA.

The TLS certificate and key will be stored in flash memory of the
device. It will be reused in subsequent startups, until 80% of the
certificate lifetime has been used. 

## Pre-requisites (building it)

This looks like a lot of stuff to do, but it should work in a normal
home network without major efforts though.

What is needed to use this example:

 - A working ACME CA of course 😜!!! You'll need it's domainname and
   it's TLS Root certificate.
 - ESP32-C3 Super Mini (called device):
   - Connect the device to your computer using the USB-C port, inspect
     the new CDC ACM device node in your kernel log / device manager.
   - the device can contact pool.ntp.org to gather the current time, for
     TLS certificate checks (using UDP/123),
   - the device's domainname is resolveable by ACME server (`/etc/hosts`
     entry is sufficient),
   - the device is reachable by ACME server over TCP port 80 (http),
   - the ACME server's domainname is resolveable by the device, and
   - the device can reach the ACME server over TCP port 443 (https).
 - Platform.io (https://platformio.org/install) to build and upload
   this example:
   - install `Espressif 32` and `Native` platforms (`PIO Home` ->
     `Platforms`),
   - import this folder into your workspace (`File` ->
     `Add Folder to Workspace ...`),
   - open project file `platformio.ini`, and
   - configure `upload_port` / `monitor_port` in section `[esp32]`, the
     default might be fine
 - Copy your ACME CA root certificate(s) into `src/cafile.pem`.
 - Adjust some defines in `src/esp32/Esp32AcmeClient.cpp` to your needs:
   - `WIFI_MANAGER_SSID`: Initial Wifi SSID used for setup,
   - `WIFI_MANAGER_PASS`: Initial WiFi password for setup.
 - Build `env:esp32c3-release` and upload firmware (`⭢`).

## Device setup

After uploading the firmware onto the device, some diagnostic messages
should be displayed in serial monitor after startup. Connect to
`WIFI_MANAGER_SSID` using `WIFI_MANAGER_PASS` and setup:
  - your ACME client configuration under `Parameters`, and
  - your local WiFi network credentials.

After connecting to your WiFi network, the ACME server is contacted and
issues the TLS certificate.

You can connect to your device using a web browser on port 80 (http) or
443 (https).
