#!/bin/sh

set -ex

PREFIX=usr/local/ca
mkdir -p "$(dirname "${PREFIX}")"
if [ ! -d "${PREFIX}" ]; then
	mkdir -m 2770 "${PREFIX}"
fi

# Populate directory
cp -rv ../settings.js \
       ../package.json \
       ../package-lock.json \
       ../flows.json \
       ../scripts \
       "${PREFIX}"
(cd "${PREFIX}"; npm install --omit=dev --omit=optional --no-audit --no-fund)
cp -v ../bin/node-red-ca.js \
      "${PREFIX}/node_modules/node-red/red.js"

# Download certificate transparency stuff
CTLOGS=var/lib/ca/ctlogs
mkdir -p "${CTLOGS}"
curl --remote-time -o "${CTLOGS}/apple_log_list.json" https://valid.apple.com/ct/log_list/current_log_list.json
curl --remote-time -o "${CTLOGS}/chrome_log_list.json" https://www.gstatic.com/ct/log_list/v3/all_logs_list.json
curl --remote-time -o "${CTLOGS}/chrome_log_list.sig" https://www.gstatic.com/ct/log_list/v3/all_logs_list.sig
curl --remote-time -o "${CTLOGS}/chrome_log_list_pubkey.pem" https://www.gstatic.com/ct/log_list/v3/log_list_pubkey.pem
openssl dgst -sha256 -verify "${CTLOGS}/chrome_log_list_pubkey.pem" -signature "${CTLOGS}/chrome_log_list.sig" "${CTLOGS}/chrome_log_list.json"

curl --remote-time -o var/lib/ca/public_suffix_list.dat https://publicsuffix.org/list/public_suffix_list.dat

find usr/local/ca var/lib/ca -type f -o -type l > files
PKG_CREATE_VERBOSE=true
export PKG_CREATE_VERBOSE
pkg create -r "${PWD}" -m "${PWD}" -p "${PWD}/plist" -o ..

