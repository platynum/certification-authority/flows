#!/usr/bin/env node

const fs = require('node:fs');
const fsPromises = fs.promises;
const { ESLint } = require('eslint');
const junitFormatter = require('eslint-junit');

function writeFile(file, content) {
    console.log(`creating ${file}`);
    return fsPromises.writeFile(file, content);
}

async function dumpFlows(directory, filename) {
    const json = fs.readFileSync(filename, 'utf8');
    let obj = JSON.parse(json);

    let files = [];
    for (let node of obj) {
        let libs = '';
        if (node.libs) {
            for (let lib of node.libs) {
                libs += `import ${lib.var} from 'node:${lib.module}';\n`;
            }
            libs += '\n';
        }
        if (node.func) {
            let fname = 'node_' + node.id.replaceAll(/\./g, '_');
            let content = `${libs}` +
                          `/* name: ${node.name} */\n`;
            if (node.initialize)
                content += `${node.initialize}\n\n`;
            content += `async function ${fname}() {\n\n${node.func.replaceAll(/^/gm, '    ')}\n}\n\n` +
                      `await ${fname}();`;
            if (node.finalize)
                content += `${node.finalize}\n\n`;
            files.push(writeFile(`${directory}/${node.id}.mjs`, content));
        }
        else {
            if (node.initialize)
                files.push(writeFile(`${directory}/${node.id}.initialize.mjs`, `${libs}${node.initialize}`));
            if (node.finalize)
                files.push(writeFile(`${directory}/${node.id}.finalize.mjs`, `${libs}${node.finalize}`));
        }
    }
    return Promise.all(files);
}

(async function main() {
    const tmpdir = './eslint.tmp/';
    if (!fs.existsSync(tmpdir)) {
        /* eslint-disable-next-line security/detect-non-literal-fs-filename */
        fs.mkdirSync(tmpdir);
    }
    const directory = fs.mkdtempSync(tmpdir);
    console.log(`created ${directory}`);

    console.log("dumping flows ...");
    await dumpFlows(directory, process.argv[2]);

    console.log("executing eslint ...");
    const eslint = new ESLint();
    const results = await eslint.lintFiles([`${directory}/*.mjs`]);

    junitFormatter(results)

    const formatter = await eslint.loadFormatter('stylish');
    const resultText = formatter.format(results);
    console.log(resultText);
})().catch((error) => {
    process.exitCode = 1;
    console.error(error);
});
