#!/usr/bin/env node

import fs from 'node:fs';
import jsrsasign from 'jsrsasign';

if (process.argv.length < 3) {
	console.error(`usage: node ${process.argv[1]} <file> [<file> ...]`);
	process.exit(1);
}

let depth = Number.parseInt(process.env.CA_BLACKLIST_DEPTH || 0);

console.time('overall');
for (let i=2; i<process.argv.length; i++) {
	console.time('stat');
	try {
		let file = process.argv[`${i}`];
		let pem = fs.readFileSync(file, 'utf8');
		let key;
		if (/-----BEGIN ((RSA|EC) )?PRIVATE KEY----/.test(pem)) {
			key = jsrsasign.KEYUTIL.getKey(pem, 'PKCS1PRV');
		}
		else if (/-----BEGIN PUBLIC KEY----/.test(pem)) {
			key = jsrsasign.KEYUTIL.getKey(pem);
		}
		else if (/-----BEGIN CERTIFICATE REQUEST-----/.test(pem)) {
			let csr = jsrsasign.KJUR.asn1.csr.CSRUtil.getParam(pem);
			key = jsrsasign.KEYUTIL.getKey(csr.sbjpubkey);
		}
		else if (/-----BEGIN CERTIFICATE-----/.test(pem)) {
			let x509 = new jsrsasign.X509(pem);
			let param = x509.getParam();
			key = jsrsasign.KEYUTIL.getKey(param.sbjpubkey);
		}

		let hex = key.type === 'EC' ? key.pubKeyHex : key.n.toString(16);
		let hash = jsrsasign.crypto.Util.hashHex(hex, 'sha256');

		let entry;
		switch (depth) {
			case 0:
				entry = `blacklist/${hash}`;
				break;
			case 1:
				entry = `blacklist/${hash.replace(/^(..)(.*)/, '$1/$2')}`;
				break;
			case 2:
				entry = `blacklist/${hash.replace(/^(..)(..)(.*)/, '$1/$2/$3')}`;
				break;
			default:
				throw new Error('Please specify CA_BLACKLIST_DEPTH to be 0, 1, or 2');
		}
		let stat = fs.statSync(entry, {throwIfNoEntry: false});
		if (stat) {
			console.log(`key already blacklisted ${hash} (${stat.birthtime})`);
		}
		else {
			console.log(`blacklisting key sha256:${hash} in ${entry}`);
			fs.writeFileSync(entry, `${pem}`, 'utf8');
		}
		fs.unlink(file, (error) => {
			if (error) console.error(`cannot unlink file (${error})`);
		});
	}
	catch (error) {
		console.log(`blacklisting ${process.argv[i]} failed (${error})`);
	}
	console.timeEnd('stat');
}
console.timeEnd('overall');
