#!/usr/bin/env node

import fs from 'node:fs';
import jsyaml from 'js-yaml';

try
{
    let yaml = fs.readFileSync(process.argv[2], 'utf8');
    let obj = jsyaml.safeLoad(yaml);
    let json = JSON.stringify(obj, null, 4);
    console.log(json);
}
catch (error) {
    console.error(error);
}
