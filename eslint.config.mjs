import node from "eslint-plugin-n";
import mocha from "eslint-plugin-mocha";
import promise from "eslint-plugin-promise";
import security from "eslint-plugin-security";
import unicorn from "eslint-plugin-unicorn";
import * as eslintrc from "@eslint/eslintrc";
import sonarjs from "eslint-plugin-sonarjs";

export default [
  mocha.configs.flat.recommended,
  node.configs["flat/recommended"],
  promise.configs["flat/recommended"],
  security.configs.recommended,
  sonarjs.configs.recommended,
  unicorn.configs["flat/recommended"],
  {
    files: ["**/*js"],
    plugins: {
      mocha, node, promise, security
    },
    rules: {
      "mocha/consistent-spacing-between-blocks": "off",
      "security/detect-non-literal-fs-filename": "off",
      "sonarjs/cognitive-complexity": "off",
      "sonarjs/max-switch-cases": "off",
      "sonarjs/new-cap": "off",
      "sonarjs/no-duplicate-string": "off",
      "sonarjs/no-fallthrough": "off",
      "sonarjs/no-empty-function": "off",
      "sonarjs/no-hardcoded-ip": "off",
      "sonarjs/no-nested-functions": "off",
      "sonarjs/no-throw-literal": "off",
      "sonarjs/prefer-spread": "off",
      "sonarjs/pseudo-random": "off",
      "sonarjs/sonar-no-fallthrough": "off",
      "sonarjs/todo-tag": "off",
      "unicorn/consistent-function-scoping": "off",
      "unicorn/prefer-dom-node-append": "off",
      "unicorn/prefer-string-slice": "off",
      "unicorn/prefer-ternary": "off",
      "unicorn/prevent-abbreviations": "off",
      "unicorn/no-lonely-if": "off",
      "unicorn/no-null": "off",
      "unicorn/no-useless-switch-case": "off",
      "unicorn/numeric-separators-style": "off",
      "unicorn/switch-case-braces": "off",
      "unicorn/import-style": "off",
      "unicorn/prefer-global-this": "off"
    },
    languageOptions: {
      ecmaVersion: "latest",
      sourceType: "module",
      // Additional globals from Node-RED
      globals: {
        "msg": false,
        "node": false,
        "context": false,
        "flow": false,
        "global": false,
        "RED": false,
        "env": false,
        "util": false
      }
    },
  },
  {
    "files": [ "test/*js", "test/fixture/*js" ],
    "rules": {
      "n/no-unsupported-features/node-builtins": "off"
    }
  },
  {
    "files": [ "test/*js" ],
    "rules": {
      "mocha/max-top-level-suites": "off",
      "mocha/no-sibling-hooks": "off",
      "n/no-unpublished-import": "off",
      "n/no-unpublished-require": "off",
      "unicorn/prefer-module": "off"
    }
  },
  {
    "files": [ "test/fixture/*js" ],
    "rules": {
      "n/no-unpublished-import": "off"
    }
  },
  {
    "files": [ "test/fixture/node-red-settings.js" ],
    "rules": {
      "unicorn/prefer-module": "off"
    }
  }
]
