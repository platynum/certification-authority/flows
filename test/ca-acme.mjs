import fs from 'node:fs';
import os from 'node:os';
import url from 'node:url';
import { inspect } from 'node:util';
import crypto from 'node:crypto';
const { subtle } = crypto.webcrypto;

import { Machine, assign } from 'xstate';
import { createModel } from '@xstate/test';

import jose from 'node-jose';
import jsrsasign from 'jsrsasign';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import encode from './lib/encode.mjs';
import helpers from './lib/helpers.mjs';

import * as dnsserver from './fixture/dnsserver.mjs';
import * as NodeRED from './fixture/node-red.mjs';
import * as httpServer from './fixture/http-server.mjs';
import * as tlsAlpnServer from './fixture/tls-alpn-server.mjs';
import * as ctlogServer from './fixture/ctlog-server.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('Automatic Certificate Management Environment (ACME)', function() {

    let directory;
    let nonces = [];
    let account;
    let order = {};
    let preAuthz = {};

    before(function() {
        process.env.ACME_PRE_AUTHORIZATION_ENABLED = 'true';
    });

    describe('directory descriptor', function() {
        it('redirects from /.well-known/acme', function(done) {
            chai.request.execute(server).get('/.well-known/acme')
                .redirects(0)
                .end(function(err, res) {
                    expect(res).to.have.status(302);
                    expect(res).to.have.header('location');
                    done();
                });
        });
        it('is provided beneath .well-known/acme', function(done) {
            chai.request.execute(server).get('/.well-known/acme')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);

                    expect(res).to.be.json;
                    expect(res).to.have.header('content-type', 'application/json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    done();
                });
        });
        it('is provided', function(done) {
            chai.request.execute(server).get('/acme/Sub/directory')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    expect(res).to.have.header('content-type', 'application/json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);
                    directory = JSON.parse(res.body.toString());
                    //console.log(directory);
                    done();
                });
        });
        it('provides a Replay-Nonce header', function(done) {
            chai.request.execute(server).get('/acme/Sub/directory')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    expect(res).to.have.header('content-type', 'application/json; charset=utf-8');
                    expect(res).to.have.header('replay-nonce');
                    done();
                });
        });
        it('provides a newNonce resource', function() {
            let newNonce = new URL(directory.newNonce);
            expect(newNonce).not.be.null;
        });
        it('provides meta fields', function() {
            expect(directory.meta).not.to.be.undefined;
        });
        it('provides meta.termsOfService', function() {
            if (directory.meta.termsOfService === undefined)
              this.skip();
            expect(directory.meta.termsOfService).not.to.be.undefined;
            let termsOfService = new URL(directory.meta.termsOfService);
            expect(termsOfService).not.to.be.null;
        });
        describe('terms of service', function() {
            it('answers GET request with a redirection', function(done) {
                let termsOfService = new URL(directory.meta.termsOfService);
                chai.request.execute(server).get(termsOfService.pathname)
                    .redirects(0)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.redirect;
                        done();
                    });
            });
        });
        it('provides meta.subdomainAuthAllowed', function() {
            expect(directory.meta.subdomainAuthAllowed).to.be.true;
        });
        it('provides meta.caaIdentities', function() {
            expect(directory.meta.caaIdentities).not.to.be.null;
            expect(directory.meta.caaIdentities).to.be.instanceOf(Array);
            if (directory.meta.caaIdentities.length === 0)
                this.skip();
        });
        it('provides meta.externalAccountRequired', function() {
            expect(directory.meta.externalAccountRequired).not.to.be.null;
            expect(directory.meta.externalAccountRequired).to.be.false;
        });
        it('provides STAR certificates (auto-renewal)', function() {
            if (directory.meta['auto-renewal'] === undefined)
                this.skip();
            expect(directory.meta['auto-renewal']).to.be.instanceOf(Object);
        });
        describe('STAR certificates', function() {
            beforeEach(function () {
                if (directory.meta['auto-renewal'] === undefined)
                    this.skip();
            });
            it('disallow certificate HTTP GET', function() {
                if (process.env.STAR_CERTIFICATE_GET === 'true')
                    this.skip();
                expect(directory.meta['auto-renewal']['allow-certificate-get']).to.be.false;
            });
            it('allow certificate HTTP GET when enabled', function() {
                if (process.env.STAR_CERTIFICATE_GET !== 'true')
                    this.skip();
                expect(directory.meta['auto-renewal']['allow-certificate-get']).to.be.true;
            });
            it('specify meta.auto-renewal.min-lifetime', function() {
                expect(directory.meta['auto-renewal']['min-lifetime']).to.be.an.integer();
            });
            it('specify meta.auto-renewal.max-duration', function() {
                expect(directory.meta['auto-renewal']['max-duration']).to.be.an.integer();
            });
            it('have max-duration above min-lifetime', function() {
                expect(directory.meta['auto-renewal']['max-duration']).to.be.above(
                    directory.meta['auto-renewal']['min-lifetime']);
            });
        });
    });

    async function getDirectory() {
        return chai.request.execute(server).get('/acme/Sub/directory')
            .buffer()
            .parse(helpers.binaryParser)
            .then(function(res) {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res).to.have.header('content-type', 'application/json; charset=utf-8');
                expect(res.body).to.be.an.instanceof(Buffer);
                directory = JSON.parse(res.body.toString());
                //console.log(directory);
                return directory;
            });
    }

    describe('newNonce resource', function() {
        before(getDirectory);

        it('is functional', function(done) {
            chai.request.execute(server)
                .head(new URL(directory.newNonce).pathname)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res).to.have.header('replay-nonce');
                    done();
                });
        });
        it('answers GET requests', function(done) {
            chai.request.execute(server)
                .get(new URL(directory.newNonce).pathname)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(204);
                    expect(res).to.have.header('replay-nonce');
                    done();
                });
        });
        it('includes cache control', function(done) {
            chai.request.execute(server)
                .get(new URL(directory.newNonce).pathname)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(204);
                    expect(res).to.have.header('replay-nonce');

                    let cacheControl = res.header['cache-control'];
                    expect(cacheControl).not.to.be.undefined;
                    expect(cacheControl).to.match(/no-store/i);
                    done();
                });
        });
        it('has no content', function(done) {
            chai.request.execute(server)
                .get(new URL(directory.newNonce).pathname)
                .buffer()
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(204);
                    expect(res).to.have.header('replay-nonce');
                    done();
                });
        });
    });

    function signMessage(account, json) {
        let protectedContent = encode.strtob64u(JSON.stringify(json.protected));
        let payload = encode.strtob64u(JSON.stringify(json.payload));
        const tbs = protectedContent + '.' + payload;

        let signer = crypto.createSign('sha256');
        signer.update(tbs);
        let sigBuf = signer.sign(account.key);
        let sigHex = sigBuf.toString('hex');
        //console.log(`sigHex: ${sigHex}`);

        let rLengthHex = sigHex.substring(6, 8);
        let rLength = Number.parseInt(rLengthHex, 16);
        let r = sigHex.substring(8, 8 + (rLength * 2));
        while (r.length < 64)
            r = '0' + r;
        if (r.length > 64)
            r = r.substring(2);

        //let sLengthHex = sigHex.substring(10 + (rLength * 2), 12 + (rLength * 2));
        //let sLength = Number.parseInt(sLengthHex, 16);
        let s = sigHex.substring(12 + (rLength * 2));
        while (s.length < 64)
            s = '0' + s;
        if (s.length > 64)
            s = s.substring(2);

        let signature = encode.strtob64u(Buffer.from(r + s, 'hex').toString('binary'));
        let content = {
            protected: protectedContent,
            payload: payload,
            signature: signature
        };
        //console.log(`sending: ${JSON.stringify(content)}`);
        return content;
    }

    async function signWithHMAC(uri, jwk, eab) {
        let base = new url.URL(uri);
        let protectedStr = JSON.stringify({
            alg: "HS256",
            kid: eab.kid,
            url: `https://${base.hostname}${base.port && base.port !== 443 ? ':' + base.port : ''}${base.pathname}`
        });
        let protectedB64u = Buffer.from(protectedStr, 'utf8').toString('base64url');
        let payload = JSON.stringify(jwk);
        let payloadB64u = Buffer.from(payload, 'utf8').toString('base64url');

        let eabAlgorithm = {name: 'HMAC', hash: {name: 'SHA-256'}};
        let eabKeyBuf = Buffer.from(eab.key, 'base64url');
        return await subtle.importKey('raw', eabKeyBuf, eabAlgorithm, true, ['sign', 'verify'])
            .then((key) => {
                let tbs = `${protectedB64u}.${payloadB64u}`;
                return subtle.sign(eabAlgorithm, key, new TextEncoder().encode(tbs));
            })
            .then((signature) => {
                let sigBuf = Buffer.from(new Uint8Array(signature), 'binary');
                let externalAccountBinding = {
                    "protected": protectedB64u,
                    "payload": payloadB64u,
                    "signature": sigBuf.toString('base64url')
                };
                //console.log(`externalAccountBinding: ${JSON.stringify(externalAccountBinding)}`);
                return externalAccountBinding;
            })
            .catch((error) => {
                console.log(`error signing with eab.key (${error})`);
                throw error;
            });
    }

    async function _sendMessage(account, uri, json, responseHandler) {
        let content = signMessage(account, json);
        //console.log(`send request: ${JSON.stringify(json, null, 4)}`);
        const res = await chai.request.execute(server)
            .post(uri.pathname)
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(content))
            .buffer()
            .parse(helpers.binaryParser);
        //console.log(`got response: ${JSON.stringify(res.body.toString())}`);
        //console.log(`nonce: ${res.header['replay-nonce']}`);
        nonces.push(res.header['replay-nonce']);
        if (responseHandler)
            responseHandler(res);
    }

    async function sendMessage(account, json, responseHandler) {
        return _sendMessage(account, new URL(json.protected.url), json, responseHandler);
    }

    async function sendMessageSync(account, json) {
        let content = signMessage(account, json);
        //console.log(`send request: ${JSON.stringify(json, null, 4)}`);
        const res = await chai.request.execute(server)
            .post(new URL(json.protected.url).pathname)
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(content))
            .buffer()
            .parse(helpers.binaryParser);
        //console.log(`got response: ${JSON.stringify(res.body.toString())}`);
        //console.log(`nonce: ${res.header['replay-nonce']}`);
        nonces.push(res.header['replay-nonce']);
        return res;
    }

    let accountKeys = [
        {type: 'ec',  params: {namedCurve: 'prime256v1'},             alg: 'ES256', supported: true},
        //{type: 'ec',  params: {namedCurve: 'secp384r1'},              alg: 'ES384', supported: true},
        //{type: 'ec',  params: {namedCurve: 'secp512r1'},              alg: 'ES512', supported: true},
        //{type: 'rsa', params: {modulusLength: 1024, hash: 'SHA-256'}, alg: 'RS256', supported: false},
        //{type: 'rsa', params: {modulusLength: 2048, hash: 'SHA-256'}, alg: 'RS256', supported: true},
        //{type: 'rsa', params: {modulusLength: 3072, hash: 'SHA-256'}, alg: 'RS256', supported: true},
        //{type: 'rsa', params: {modulusLength: 4096, hash: 'SHA-256'}, alg: 'RS256', supported: true},
        //{type: 'rsa', params: {modulusLength: 2048, hash: 'SHA-384'}, alg: 'RS384', supported: true},
        //{type: 'rsa', params: {modulusLength: 2048, hash: 'SHA-512'}, alg: 'RS512', supported: true},
        {type: 'rsa', params: {modulusLength: 2048, hash: 'SHA-256'}, alg: 'PS256', supported: true},
        {type: 'rsa', params: {modulusLength: 2048, hash: 'SHA-384'}, alg: 'PS256', supported: true},
        {type: 'rsa', params: {modulusLength: 2048, hash: 'SHA-512'}, alg: 'PS256', supported: true}
    ];

    function _createAccountKey(key) {
        // console.log(`crypto: ${JSON.stringify(crypto.getCurves())}`);
        account = crypto.generateKeyPairSync(key.type, key.params);
        account.alg = key.alg;
        account.key = account.privateKey.export({type: 'pkcs8', format: 'pem'});

        return account;
    }
    function createAccountKey() {
        return _createAccountKey(accountKeys[0]);
    }

    async function getNonce() {
        let newNonce = new URL(directory.newNonce);
        let res = await chai.request.execute('http://' + newNonce.host).head(newNonce.pathname);
        //console.log(`nonce: ${res.header['replay-nonce']}`);
        nonces.push(res.header['replay-nonce']);

        return res;
    }

    async function createAccountJwk() {
        return jose.JWK.asKey(account.key, 'pem')
            .then(function(result) {
                account.jwk = result;
                return account.jwk;
            });
    }

    async function newAccount() {
        let json = {
            protected: {
                alg: account.alg,
                jwk: account.jwk.toJSON(),
                nonce: nonces.pop(),
                url: directory.newAccount
            },
            payload: {
                termsOfServiceAgreed: true,
                contact: []
            }
        };
        await sendMessage(account, json, function(res) {
            expect(res).to.have.status(201);
            expect(res).to.have.header('location');
            expect(res.body).to.be.an.instanceof(Buffer);
            expect(res).to.be.json;

            account.data = JSON.parse(res.body.toString());
            account.kid = res.header['location'];
        });
        return account;
    }

    async function deactivateAccount() {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop(),
                url: account.kid
            },
            payload: {
                status: 'deactivated'
            }
        };
        await sendMessage(account, json, function(res) {
            expect(res).to.have.status(200);
            expect(res).to.be.json;

            expect(res.body).to.be.an.instanceof(Buffer);
            account.data = JSON.parse(res.body.toString());
            account.kid = res.header['location'];
        });
    }

    async function getAccount() {
        let json = {
            protected: {
                alg: account.alg,
                jwk: account.jwk.toJSON(),
                nonce: nonces.pop(),
                url: directory.newAccount
            },
            payload: {
                onlyReturnExisting: true
            }
        };
        await sendMessage(account, json, function(res) {
            expect(res).to.have.status(200);
            expect(res).to.be.json;

            expect(res.body).to.be.an.instanceof(Buffer);
            account.data = JSON.parse(res.body.toString());
            if (res.header['location'])
                account.kid = res.header['location'];
        });
        return account;
    }

    async function newOrder(account, payload, callback) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop(),
                url: directory.newOrder
            }
        };
        json.payload = payload;
        //console.log(`newOrder request: ${JSON.stringify(json, null, 4)}`);
        await sendMessage(account, json, callback ?? function(res) {
            expect(res).to.have.status(201);
            //expect(res).to.have.header('location');
            expect(res.body).to.be.an.instanceof(Buffer);
            //expect(res.body).to.be.json;
            //console.log(`newOrder response: ${res.body.toString()}`);
            order.data = JSON.parse(res.body.toString());
            order.url = res.header['location'];
        });
        return order;
    }

    async function newPreAuthorization(account, identifier) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop(),
                url: directory.newAuthz
            }
        };
        json.payload = { identifier: identifier };
        // console.log(`newAuthz request: ${JSON.stringify(json, null, 4)}`);
        let res = await sendMessageSync(account, json);
        expect(res).to.have.status(201);
        expect(res).to.have.header('location');
        expect(res.body).to.be.an.instanceof(Buffer);
        //expect(res.body).to.be.json;
        // console.log(`newAuthz response: ${res.body.toString()}`);
        preAuthz.data = JSON.parse(res.body.toString());
        // console.log(`newAuthz location: ${res.header['location']}`);
        preAuthz.url = res.header['location'];
        return preAuthz;
    }

    async function deactivateAuthorization(account, authorization, callback) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop(),
                url: authorization.url
            },
            payload: {
               "status": "deactivated"
            }
        }
        let res = await sendMessageSync(account, json);
        if (callback) {
            return callback(res);
        }
        else {
            expect(res).to.have.status(200);
            expect(res.body).to.be.an.instanceOf(Buffer);
            let response = JSON.parse(res.body.toString());
            expect(response).to.have.property('status');
            expect(response.status).to.be.equal('deactivated');
            response.url = authorization.url;
            return response;
        }
    }

    async function newDnsOrder(account, callback) {
        let payload = {
            identifiers: [
                {'type': 'dns', 'value': 'localhost'}
            ],
            notAfter: new Date(Date.now() + 364 * 24 * 60 * 60 * 1000).toISOString()
        };
        return await newOrder(account, payload, callback);
    }
    async function newStarOrder(account, callback) {
        let payload = {
            identifiers: [
                {'type': 'dns', 'value': 'localhost'}
            ],
            'auto-renewal': {
                'start-date': new Date().toISOString(),
                'end-date': new Date(Date.now() + 364 * 24 * 60 * 60 * 1000).toISOString(),
                'lifetime': 86400
            }
        }
        return await newOrder(account, payload, callback);
    }
    async function newStarOrderForGet(account, callback) {
        let payload = {
            identifiers: [
                {'type': 'dns', 'value': 'localhost'}
            ],
            'auto-renewal': {
                'start-date': new Date().toISOString(),
                'end-date': new Date(Date.now() + 364 * 24 * 60 * 60 * 1000).toISOString(),
                'lifetime': 86400,
                'allow-certificate-get': true
            }
        }
        return await newOrder(account, payload, callback);
    }
    async function newEmailOrder(account, callback) {
        let payload = {
            identifiers: [
                {'type': 'email', 'value': 'ut@mail'}
            ]
        };
        return await newOrder(account, payload, callback);
    }
    async function newIpOrder(account, callback) {
        let ifaces = os.networkInterfaces();
        let ip;
        for (const iname of Object.keys(ifaces)) {
            for (const net of ifaces[`${iname}`]) {
                if (net.family == 'IPv4' && !net.internal)
                    ip = net.address;
            }
        }
        let payload = {
            identifiers: [
                {'type': 'ip', 'value': ip}
            ],
            notAfter: new Date(Date.now() + 364 * 24 * 60 * 60 * 1000).toISOString()
        };
        return await newOrder(account, payload, callback);
    }
    async function newIpv6Order(account, callback) {
        let ifaces = os.networkInterfaces();
        let ip;
        for (const iname of Object.keys(ifaces)) {
            for (const net of ifaces[`${iname}`]) {
                if (net.family == 'IPv6' && !net.internal && !/^f[ef]/.test(net.address))
                    ip = net.address;
            }
        }
        if (ip === undefined) {
            for (const iname of Object.keys(ifaces)) {
                for (const net of ifaces[`${iname}`]) {
                    if (net.family == 'IPv4' && !net.internal)
                        ip = `::ffff:${net.address}`;
                }
            }
        }
        // console.log(`ip: ${ip}`);
        let payload = {
            identifiers: [
                {'type': 'ip', 'value': ip}
            ],
            notAfter: new Date(Date.now() + 364 * 24 * 60 * 60 * 1000).toISOString()
        };
        return await newOrder(account, payload, callback);
    }

    async function authorize(account, order) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop(),
                url: order.data.authorizations[0]
            },
            payload: {}
        };
        let authorization;
        await sendMessage(account, json, function(res) {
            expect(res).to.have.status(200);
            expect(res.body).to.be.an.instanceof(Buffer);
            // console.log(`authorize response: ${res.body.toString()}`);
            authorization = JSON.parse(res.body.toString());
        });
        return authorization;
    }
    async function adminChallenge(account, authorization, callback) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop()
            },
            payload: {}
        };
        for (const challenge of authorization.challenges) {
            if (challenge.type === 'x-admin-01') {
                json.protected.url = challenge.url;
                json.payload.token = challenge.token;
                let jwk = account.jwk.toJSON();
                await jose.JWK.asKey(jwk)
                    .then(async function(key) {
                        let thumbprint = await key.thumbprint('SHA-256');
                        json.payload.thumbprint = encode.b64tob64u(thumbprint.toString('base64'));
                        return json.payload.thumbprint;
                    });
                //console.log(`challenge request: ${JSON.stringify(json, null, 4)}`);
                await sendMessage(account, json, callback ?? function(res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    //console.log(`challenge response: ${JSON.stringify(res.body.toString())}`);
                    let challenge = JSON.parse(res.body.toString());
                    expect(challenge).to.have.property('status');
                    expect(challenge.status).to.be.equal('valid');
                });
            }
        }
    }
    async function triggerHttpChallenge(account, authorization, callback) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop()
            },
            payload: {}
        };
        for (const challenge of authorization.challenges) {
            if (challenge.type === 'http-01') {
                json.protected.url = challenge.url;
                json.payload.token = challenge.token;
                let jwk = account.jwk.toJSON();
                await jose.JWK.asKey(jwk)
                    .then(async function(key) {
                        let thumbprint = await key.thumbprint('SHA-256');
                        json.payload.thumbprint = encode.b64tob64u(thumbprint.toString('base64'));
                        return json.payload.thumbprint;
                    });
                //console.log(`challenge request: ${JSON.stringify(json, null, 4)}`);
                await sendMessage(account, json, callback ?? function(res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    //console.log(`challenge response: ${JSON.stringify(res.body.toString())}`);
                    let challenge = JSON.parse(res.body.toString());
                    expect(challenge).to.have.property('status');
                    expect(challenge.status).to.be.equal('valid');
                });
            }
        }
    }
    async function triggerDnsChallenge(account, authorization, setupDns, callback) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop()
            },
            payload: {}
        };
        // console.log(`authorization: ${JSON.stringify(authorization)}`);
        for (const challenge of authorization.challenges) {
            if (challenge.type === 'dns-01') {
                json.protected.url = challenge.url;
                let jwk = account.jwk.toJSON();
                await jose.JWK.asKey(jwk)
                    .then(async function(key) {
                        let thumbprint = await key.thumbprint('SHA-256');
                        let str = `${challenge.token}.${encode.b64tob64u(thumbprint.toString('base64'))}`;
                        let buf = await subtle.digest('SHA-256', Buffer.from(str, 'binary'));
                        let hash = Buffer.from(buf, 'binary').toString('base64url');
                        setupDns(`_acme-challenge.${authorization.identifier.value}`, hash);
                        return true;
                    });

                // console.log(`challenge request: ${JSON.stringify(json, null, 4)}`);
                await sendMessage(account, json, callback ?? function(res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    //console.log(`challenge response: ${JSON.stringify(res.body.toString())}`);
                    let challenge = JSON.parse(res.body.toString());
                    expect(challenge).to.have.property('status');
                    // should be 'processing' or 'valid'
                    expect(challenge.status).to.be.equal('valid');
                });
            }
        }
    }
    async function triggerTlsAlpnChallenge(account, authorization, setupServer, callback) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop()
            },
            payload: {}
        };
        // console.log(`authorization: ${JSON.stringify(authorization)}`);
        for (const challenge of authorization.challenges) {
            if (challenge.type === 'tls-alpn-01') {
                json.protected.url = challenge.url;
                let jwk = account.jwk.toJSON();
                return jose.JWK.asKey(jwk)
                    .then(async function(key) {
                        return key.thumbprint('SHA-256');
                    })
                    .then(function(thumbprint) {
                        let str = `${challenge.token}.${encode.b64tob64u(thumbprint.toString('base64'))}`;
                        return subtle.digest('SHA-256', Buffer.from(str, 'binary'));
                    })
                    .then(function(challenge) {
                        let hash = Buffer.from(challenge, 'binary').toString('hex');
                        return setupServer(hash);
                    })
                    .then(function() {
                        // console.log(`challenge request: ${JSON.stringify(json, null, 4)}`);
                        return sendMessage(account, json, callback ?? function(res) {
                            expect(res.body).to.be.an.instanceof(Buffer);
                            let body = JSON.parse(res.body.toString());
                            // expect(body.type).to.be.undefined;
                            expect(body.detail).to.be.undefined;

                            expect(res).to.have.status(200);
                            let challenge = body;
                            expect(challenge).to.have.property('status');
                            // should be 'processing' or 'valid'
                            expect(challenge.status).to.be.equal('valid');
                            return challenge;
                        });
                    })
                    .catch((error) => {
                        throw error;
                    });
            }
        }
        throw new Error("Missing tls-alpn-01 challenge");
    }

    async function cancelOrder(account, order, callback) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop(),
                url: order.url
            },
            payload: {
                status: "canceled"
            }
        };
        await sendMessage(account, json, callback ?? function(res) {
            expect(res).to.have.status(200);
            expect(res.body).to.be.an.instanceof(Buffer);
            order.data = JSON.parse(res.body.toString());
        });
    }
    async function retrieveCertificate(account, order, callback) {
        let certificateUrl;
        if (order.data.certificate)
            certificateUrl = order.data.certificate;
        else
            certificateUrl = order.data['star-certificate'];
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop(),
                url: certificateUrl
            },
            payload: {}
        };
        await sendMessage(account, json, callback ?? function(res) {
            //console.log(`certificate request: ${JSON.stringify(json)}`);
            expect(res).to.have.status(200);
            expect(res.body).to.be.an.instanceof(Buffer);
            //console.log(`certificate response: ${res.body.toString()}`);
            order.certificateChain = res.body.toString();
        });
    }
    async function retrieveCertificateViaGet(account, order) {
        let certificateUrl = order.data['star-certificate'];
        const res = await chai.request.execute(server)
            .get(new URL(certificateUrl).pathname)
            .buffer()
            .parse(helpers.binaryParser);
        expect(res).to.have.status(200);
        expect(res.body).to.be.an.instanceof(Buffer);
        order.certificateChain = res.body.toString();
        // console.log(`order.certificateChain: ${order.certificateChain}`);
    }
    async function finalizeOrder(account, order, csr, cb) {
        let _csr = csr ?? ('MIICsjCCAZoCAQAwbTELMAkGA1UEBhMCQ1oxGTAXBgNVBAgMEENlbnRyYWwgQm9o' +
                           'ZW1pYW4xHTAbBgNVBAcMFFN0w4XCmWVkb8OEwo1lc2vDg8K9MRAwDgYDVQQKDAdN' +
                           'eS5Db3JwMRIwEAYDVQQDDAlsb2NhbGhvc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IB' +
                           'DwAwggEKAoIBAQC85Fr6yVDErHflrUdqCxGgQNs6qIpoHznxVEhROSGw22rO3nXs' +
                           'DcWkNP2Vr6DDBoUDWc8YZFXDaLPaDTJPtKLbRoXcrJdmWOwc6698PH3bSgIN-vQH' +
                           'riUM9xASAQDbGkRa3TxDwHg_URQok7OxmRl-o8T2PxOGCVSEHkMZrswsMwDvUMqG' +
                           'ZxEw0en-zhnLt4s2MP9jWmI0SHQhSTl6JddnXpbLZ2M3HjwilJSyCNuE6f9Ojgjk' +
                           'noUQ4HU_9olLEGHOcmvpiPxgyXp0fbpDaPlBSLD0vnFdy_-na8o2HYGzHx_sd98J' +
                           'dIWvsHgHw9gBPnOo0-vOQnuxxUdILV_5L_B1AgMBAAGgADANBgkqhkiG9w0BAQsF' +
                           'AAOCAQEARvdcOk70FM_ehgyDjL3hvqu-Vh9OUM3vyFsg4hcVK2B_XFxqkbKq91o1' +
                           'XkRnCavgJCYcAuLPbgyPNM4SaiazTPRT3uxyf_dCPfHUd7THBdUR95wVqFyV7OHV' +
                           'fqz6owIRIoX1pLtQrqX-5Myw8TnXPa-ok0SMsFi0JlivZuKA0Rh71buCKkMHkKww' +
                           '9BVoNzPLO4n-x53bdD_dT3kazwMycOr7L-f5isrw5QcMzB0PiqZuFieWHSnF6-X7' +
                           'ft6MrFRsSvn2mNPUWWIBntEDQXvqb2Sre9WUdQTNScL0SMURrCdltqxkLQoHdZ22' +
                           'lX0OmZVw-xPKGgO4tAaahVtCpY1DgA');
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop(),
                url: order.data.finalize
            },
            payload: {
                csr: _csr
            }
        };
        await sendMessage(account, json, cb);
    }
    async function finalizeOrderSuccessfully(account, order, csr, callback) {
        return finalizeOrder(account, order, csr, function(res) {
            expect(res).to.have.status(200);
            expect(res.body).to.be.an.instanceof(Buffer);
            //console.log(`finalize response: ${res.body.toString()}`);
            order.data = JSON.parse(res.body.toString());
            if (callback)
                callback(res);
        });
    }
    async function revokeCertificate(account, certificate, reason = 4) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop(),
                url: directory.revokeCert
            },
            payload: {
                reason: reason
            }
        };
        let b64 = certificate.replace(/^-{5}.*$/m, '');
        json.payload.certificate = encode.b64tob64u(b64);

        await sendMessage(account, json, function(res) {
            expect(res).to.have.status(200);
        });
    }
    async function getOrder(account) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop()
            },
            payload: {}
        };
        if (order.data && order.url) {
            //console.log(`order request: ${JSON.stringify(json, null, 4)}`);
            json.protected.url = order.url;
            await sendMessage(account, json, function(res) {
                //console.log(`order response: ${res.body.toString()}`);
                //expect(res).to.have.status(200);
                //expect(res.header['content-type']).should.be.equal('application/json; charset=utf-8');
                //expect(res.body).to.be.json;
                order.data = JSON.parse(res.body.toString());
            });
        }
        else {
            console.log('no current order');
        }
        return order;
    }
    async function getPreAuthorization(account) {
        let json = {
            protected: {
                alg: account.alg,
                kid: account.kid,
                nonce: nonces.pop()
            },
            payload: {}
        };
        if (preAuthz.data && preAuthz.url) {
            //console.log(`preAuthrequest: ${JSON.stringify(json, null, 4)}`);
            json.protected.url = preAuthz.url;
            await sendMessage(account, json, function(res) {
                //console.log(`preAuth response: ${res.body.toString()}`);
                //expect(res).to.have.status(200);
                //expect(res.header['content-type']).should.be.equal('application/json; charset=utf-8');
                //expect(res.body).to.be.json;
                preAuthz.data = JSON.parse(res.body.toString());
            });
        }
        else {
            console.log('no current pre-authorization');
        }
        return preAuthz;
    }

    describe('url protection', function() {
        let base;
        before(function() {
            if (!NodeRED.started)
                this.skip();
        });
        before(getDirectory);
        before(createAccountKey);
        before(createAccountJwk);
        before(function () {
            base = new url.URL(directory.newAccount);
            process.env['ACME_URL_PROTECTION'] = 'strict';
        });
        beforeEach(getNonce);
        after(function() {
            process.env['ACME_URL_PROTECTION'] = 'lax';
        });
        async function triggerUrlProtection(newAccountUrl, callback) {
            let json = {
                protected: {
                    alg: account.alg,
                    jwk: account.jwk.toJSON(),
                    nonce: nonces.pop(),
                    url: newAccountUrl
                },
                payload: {
                    termsOfServiceAgreed: true,
                    contact: []
                }
            };
            await _sendMessage(account, base, json, function(res) {
                expect(res).to.have.status(403);
                expect(res).not.to.have.header('location');
                expect(res.body).to.be.an.instanceof(Buffer);
                expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                let acmeError = JSON.parse(res.body);
                expect(acmeError.type).to.equal('urn:ietf:params:acme:error:unauthorized');
                callback();
            });
        }
        it('rejects invalid url', function(done) {
            triggerUrlProtection(' -- ', done);
        });
        it('rejects different hostname', function(done) {
            let str = `${base.protocol}//nowhere.example.com${base.port && base.port !== 443 ? ':' + base.port : ''}${base.pathname}`
            triggerUrlProtection(str, done);
        });
        it('rejects different pathname', function(done) {
            let str = `${base.protocol}//${base.hostname}${base.port && base.port !== 443 ? ':' + base.port : ''}/acme/Sub/doesnotexist`;
            triggerUrlProtection(str, done);
        });
        it('rejects insecure protocol', function(done) {
            let str = `http://${base.hostname}${base.port && base.port !== 443 ? ':' + base.port : ''}${base.pathname}`;
            triggerUrlProtection(str, done);
        });
        it.skip('rejects different port', function(done) {
            let str = `${base.protocol}//${base.hostname}:8443${base.pathname}`;
            triggerUrlProtection(str, done);
        });
    });

    describe('newAccount resource', function() {
        before(getDirectory);
        const accountMachine = Machine({
            id: 'account',
            initial: 'init',
            states: {
                init: {
                    on: {
                        'NEWACCOUNT': 'valid'
                    },
                    meta: {
                        test: (/* f, event */) => {
                            expect(account).to.have.property('jwk');
                        }
                    }
                },
                valid: {
                    on: {
                        'DEACTIVATE': 'deactivated'
                    },
                    meta: {
                        test: async function(/* f, event */) {
                            let a = await getAccount();
                            expect(a.data.status).to.be.equal('valid');
                        }
                    }
                },
                deactivated: {
                    //type: 'final',
                    on: {},
                    meta: {
                        test: async function(/* f, event */) {
                            let a = await getAccount();
                            expect(a.data.status).to.be.equal('deactivated');
                        }
                    }
                }
            }
        });
        const accountModel = createModel(accountMachine)
            .withEvents({
                NEWACCOUNT: { exec: newAccount },
                DEACTIVATE: { exec: deactivateAccount }
            });

        describe('contacts', function() {
            before(createAccountKey);
            before(createAccountJwk);
            beforeEach(getNonce);

            it('rejects unsupported contacts', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        jwk: account.jwk.toJSON(),
                        nonce: nonces.pop(),
                        url: directory.newAccount
                    },
                    payload: {
                        termsOfServiceAgreed: true,
                        contact: [
                            'http://example.com/contact.html'
                        ]
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:unsupportedContact');
                    done();
                });
            });
            it('rejects invalid contacts', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        jwk: account.jwk.toJSON(),
                        nonce: nonces.pop(),
                        url: directory.newAccount
                    },
                    payload: {
                        termsOfServiceAgreed: true,
                        contact: [
                            'https://user@pass:example.com'
                        ]
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.be.equal('urn:ietf:params:acme:error:invalidContact');
                    done();
                });
            });
        });

        describe('account object', function() {
            before(createAccountKey);
            before(createAccountJwk);
            before(getNonce);
            before(newAccount);

            it('provides orders URL', function() {
                expect(account.data).to.have.property('orders');
                let ordersUrl = new URL(account.data.orders);
                expect(ordersUrl).not.to.be.null;
            });
            it('provides delegations URL', function() {
                if (!account.data.delegations)
                    this.skip();
                expect(account.data).to.have.property('delegations');
            });
            it('provides pre-authorizations', function() {
                if (!account.data['pre-authorizations'])
                    this.skip();
                expect(account.data).to.have.property('pre-authorizations');
            });
            it('provides contact locations', function() {
                expect(account.data).to.have.property('contact');
                expect(account.data.contact).to.be.an('array');
            });
            it('has status property', function() {
                expect(account.data).to.have.property('status');
            });
            it('has status value "valid"', function() {
                expect(account.data.status).to.be.equal('valid');
            });
        });
        describe('state machine', function() {
            let account = {};

            this.slow(1000);

            beforeEach(createAccountKey);
            beforeEach(createAccountJwk);
            beforeEach(getNonce);

            const accountTestPlans = accountModel.getSimplePathPlans();
            for (let plan of accountTestPlans) {
                describe(plan.description, function() {
                    for (let path of plan.paths) {
                        it(path.description, function() {
                            return path.test(account);
                        });
                    }
                });
            }
        });
        describe('state machine test', function() {
            it('covered all states', function() {
                return accountModel.testCoverage({
                    filter: stateNode => !!stateNode.meta
                });
            });
        });
        describe('external account binding', function() {
            before(function() {
                if (!NodeRED.started)
                    this.skip();
            });
            before(getDirectory);
            before(createAccountKey);
            before(createAccountJwk);
            before(function() {
                process.env.ACME_EXTERNAL_ACCOUNT_BINDING = 'required';
            });
            beforeEach(getNonce);
            after(function() {
                process.env.ACME_EXTERNAL_ACCOUNT_BINDING = undefined;
            });
            it('is required', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        jwk: account.jwk.toJSON(),
                        nonce: nonces.pop(),
                        url: directory.newAccount
                    },
                    payload: {
                        termsOfServiceAgreed: true,
                        contact: []
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(403);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:unauthorized');
                    done();
                });
            });
            it('has to be wellformed', async function() {
                let eab = {"key":"cwR8HQqHdI+uyw8gyEGZdA==","extKeyUsage": ["ocspSigning"],"kid":"ocsp-signer-01"};
                fs.writeFileSync(`Sub/acme/external-accounts/${eab.kid}.json`, JSON.stringify(eab), 'utf8');
                let externalAccountBinding = await signWithHMAC(directory.newAccount, account.jwk, eab);
                let json = {
                    protected: {
                        alg: account.alg,
                        jwk: account.jwk.toJSON(),
                        nonce: nonces.pop(),
                        url: directory.newAccount
                    },
                    payload: {
                        termsOfServiceAgreed: true,
                        contact: [],
                        externalAccountBinding
                    }
                };
                json.payload.externalAccountBinding.nosignature = json.payload.externalAccountBinding.signature;
                delete json.payload.externalAccountBinding.signature;
                return new Promise(function(resolve /*, reject */) {
                    sendMessage(account, json, function(res) {
                        expect(res).to.have.status(403);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                        let acmeError = JSON.parse(res.body);
                        expect(acmeError.type).to.equal('urn:ietf:params:acme:error:unauthorized');
                        resolve();
                    });
                });
            });
            it('does not allow an invalid signature', async function() {
                let eab = {"key":"cwR8HQqHdI+uyw8gyEGZdA==","extKeyUsage": ["ocspSigning"],"kid":"ocsp-signer-01"};
                let alt = {"key":"CWr8hoqHdi+UYW8GYegZdA==","extKeyUsage": ["ocspSigning"],"kid":"ocsp-signer-01"};
                fs.writeFileSync(`Sub/acme/external-accounts/${eab.kid}.json`, JSON.stringify(eab), 'utf8');
                let externalAccountBinding = await signWithHMAC(directory.newAccount, account.jwk, alt);
                let json = {
                    protected: {
                        alg: account.alg,
                        jwk: account.jwk.toJSON(),
                        nonce: nonces.pop(),
                        url: directory.newAccount
                    },
                    payload: {
                        termsOfServiceAgreed: true,
                        contact: [],
                        externalAccountBinding
                    }
                };
                json.payload.externalAccountBinding.nosignature = json.payload.externalAccountBinding.signature;
                delete json.payload.externalAccountBinding.signature;
                return new Promise(function(resolve /*, reject */) {
                    sendMessage(account, json, function(res) {
                        expect(res).to.have.status(403);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                        let acmeError = JSON.parse(res.body);
                        expect(acmeError.type).to.equal('urn:ietf:params:acme:error:unauthorized');
                        resolve();
                    });
                });
            });
            it('needs a valid signature', async function() {
                let eab = {"key":"cwR6QHqHdI+uyw8gyEGZdA==","extKeyUsage": ["ocspSigning"],"kid":"ocsp-signer-03"};
                fs.writeFileSync(`Sub/acme/external-accounts/${eab.kid}.json`, JSON.stringify(eab), 'utf8');
                let externalAccountBinding = await signWithHMAC(directory.newAccount, account.jwk, eab);
                let json = {
                    protected: {
                        alg: account.alg,
                        jwk: account.jwk.toJSON(),
                        nonce: nonces.pop(),
                        url: directory.newAccount
                    },
                    payload: {
                        termsOfServiceAgreed: true,
                        contact: [],
                        externalAccountBinding
                    }
                };
                return new Promise(function(resolve, reject) {
                    try {
                        sendMessage(account, json, function(res) {
                            expect(res).to.have.status(201);
                            expect(res.body).to.be.an.instanceof(Buffer);
                            expect(res).to.be.json;
                            resolve();
                        });
                    }
                    catch (error) {
                        reject(error);
                    }
                });
            });
        });
        describe('account key', function() {
            before(function() {
                if (!NodeRED.started)
                    this.skip();
            });
            before(getDirectory);
            beforeEach(getNonce);
            after(function() {
                process.env.ACME_ACCOUNT_RSA_MIN_LENGTH = undefined;
                process.env.ACME_ACCOUNT_RSA_MAX_LENGTH = undefined;
            });
            it('is rejected when algorithm isn\'t known', async function() {
                let account = crypto.generateKeyPairSync('ed448', {});
                let jwk = account.publicKey.export({type: 'spki', format: 'jwk'});

                let json = {
                    protected: {
                        alg: 'EdDSA',
                        crv: 'Ed448',
                        jwk,
                        nonce: nonces.pop(),
                        url: directory.newAccount
                    },
                    payload: {
                        termsOfServiceAgreed: true,
                        contact: []
                    }
                };

                const content = {
                    'protected': encode.strtob64u(JSON.stringify(json.protected)),
                    'payload': encode.strtob64u(JSON.stringify(json.payload)),
                    'signature': '6a73b545c759d38ef4655a423b143e564ac758383645'
                };
                const res = await chai.request.execute(server)
                    .post(new URL(directory.newAccount).pathname)
                    .set('Content-Type', 'application/json')
                    .send(JSON.stringify(content))
                    .buffer()
                    .parse(helpers.binaryParser);
                nonces.push(res.header['replay-nonce']);

                expect(res).to.have.status(400);
                expect(res.body).to.be.an.instanceof(Buffer);
                expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                let acmeError = JSON.parse(res.body);
                expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badSignatureAlgorithm');
                expect(acmeError).to.have.property('algorithms');
                expect(acmeError.algorithms).to.be.an('array').that.does.not.include('EdDSA');
            });
            it('is limited by ACME_ACCOUNT_RSA_MIN_LENGTH', function(done) {
                process.env.ACME_ACCOUNT_RSA_MIN_LENGTH = '2049';
                process.env.ACME_ACCOUNT_RSA_MAX_LENGTH = undefined;

                let params = {modulusLength: 2048, hash: 'SHA-256'};
                let account = crypto.generateKeyPairSync('rsa', params);
                let jwk = account.publicKey.export({format: 'jwk'});

                let json = {
                    protected: {
                        alg: 'PS256', jwk,
                        nonce: nonces.pop(),
                        url: directory.newAccount
                    },
                    payload: {
                        termsOfServiceAgreed: true,
                        contact: []
                    }
                };
                sendMessage({key: account.privateKey}, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badPublicKey');

                    done();
                });
            });
            it('is limited by ACME_ACCOUNT_RSA_MAX_LENGTH', function(done) {
                process.env.ACME_ACCOUNT_RSA_MIN_LENGTH = undefined;
                process.env.ACME_ACCOUNT_RSA_MAX_LENGTH = '2047';

                let params = {modulusLength: 2048, hash: 'SHA-256'};
                let account = crypto.generateKeyPairSync('rsa', params);
                let jwk = account.publicKey.export({format: 'jwk'});

                let json = {
                    protected: {
                        alg: 'PS256', jwk,
                        nonce: nonces.pop(),
                        url: directory.newAccount
                    },
                    payload: {
                        termsOfServiceAgreed: true,
                        contact: []
                    }
                };
                sendMessage({key: account.privateKey}, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badPublicKey');
                    done();
                });
            });
            it('is rejected when is ROCA vulnerable', async function() {
                let pem = fs.readFileSync('test/data/keys/roca.p8', 'utf8');
                let b64 = pem.replaceAll(/-{5}(BEGIN|END) PRIVATE KEY-{5}[\n\r]?/g, '');
                let der = Buffer.from(b64, 'base64');
                let key;
                return subtle.importKey('pkcs8', der, {name: 'RSASSA-PKCS1-v1_5', hash: 'SHA-384'}, true, ['sign'])
                //subtle.importKey('pkcs8', der, {name: 'RSA-PSS', hash: 'SHA-256'}, true, ['sign'])
                      .then((tmp) => {
                          key = tmp;
                          return subtle.exportKey('jwk', key);
                      })
                      .then((jwk) => {
                          let json = {
                              protected: {
                                  alg: jwk.alg, jwk: { kty: jwk.kty, n: jwk.n, e: jwk.e, alg: jwk.alg },
                                  nonce: nonces.pop(),
                                  url: directory.newAccount
                              },
                              payload: {
                                  termsOfServiceAgreed: true,
                                  contact: []
                              }
                          };
                          return sendMessage({key}, json, function(res) {
                              expect(res).to.have.status(400);
                              expect(res.body).to.be.an.instanceof(Buffer);
                              expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                              let acmeError = JSON.parse(res.body);
                              expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badPublicKey');
                              return acmeError;
                          });
                      });
            });
            it('is rejected when weak', async function() {
                let pem = fs.readFileSync('test/data/keys/factor.p8', 'utf8');
                let b64 = pem.replaceAll(/-{5}(BEGIN|END) PRIVATE KEY-{5}[\n\r]?/g, '');
                let der = Buffer.from(b64, 'base64');
                let key;
                return subtle.importKey('pkcs8', der, {name: 'RSA-PSS', hash: 'SHA-256'}, true, ['sign'])
                      .then((tmp) => {
                          key = tmp;
                          return subtle.exportKey('jwk', key);
                      })
                      .then((jwk) => {
                          let json = {
                              protected: {
                                  alg: 'PS256', jwk,
                                  nonce: nonces.pop(),
                                  url: directory.newAccount
                              },
                              payload: {
                                  termsOfServiceAgreed: true,
                                  contact: []
                              }
                          };
                          return sendMessage({key}, json, function(res) {
                              expect(res).to.have.status(400);
                              expect(res.body).to.be.an.instanceof(Buffer);
                              expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                              let acmeError = JSON.parse(res.body);
                              expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badPublicKey');
                              return res;
                          });
                      });
            });

        });
    });
    describe('newOrder resource', function() {
        const requestStar = assign({
            autoRenewal: (/* context, event */) => true
        });
        function revocable(context /*, event */) {
            return ! context.autoRenewal;
        }
        const requestStarForGet = assign({
            autoRenewal: (/* context, event */) => true,
            getCertificate: (/* context, event */) => true
        });
        function allowGet(context /*, event */) {
            return context.getCertificate;
        }
        function needsPost(context /*, event */) {
            return ! context.getCertificate;
        }
        const orderMachine = Machine({
            id: 'order',
            initial: 'init',
            context: {
                autoRenewal: false,
                getCertificate: false
            },
            states: {
                init: {
                    on: {
                        'NEW': 'pending',
                        'NEWSTAR': {
                            target: 'pending-star',
                            actions: requestStar
                        },
                        'NEWSTAR_FOR_GET': {
                            target: 'pending-star',
                            actions: requestStarForGet
                        }
                    },
                    meta: {
                        test: (/* f, event */) => {
                            expect(account).to.have.property('data');
                            expect(account.data).to.have.property('status');
                            expect(account.data.status).to.be.equal('valid');
                        }
                    }
                },
                pending: {
                    on: {
                        'CANCEL': 'canceled',
                        'AUTHORIZE': 'authorized'
                    },
                    meta: {
                        test: async function(/* context, event */) {
                            const order = await getOrder(account);
                            expect(order.data).to.have.property('status');
                            expect(order.data.status).to.be.equal('pending');
                            expect(order.data).not.to.have.property('auto-renewal');
                        }
                    }
                },
                'pending-star': {
                    on: {
                        'CANCEL': 'canceled',
                        'AUTHORIZE': 'authorized'
                    },
                    meta: {
                        test: async function(/* context, event */) {
                            const order = await getOrder(account);
                            expect(order.data).to.have.property('status');
                            expect(order.data.status).to.be.equal('pending');
                            expect(order.data).to.have.property('auto-renewal');
                        }
                    }
                },
                authorized: {
                    on: {
                        'CANCEL': 'canceled',
                        'FULLFILL': 'ready'
                    },
                    meta: {
                        test: async function(/* f, event */) {
                            const order = await getOrder(account);
                            //console.log(`got order: ${JSON.stringify(order)}`);
                            expect(order.data.status).to.be.equal('pending');
                            expect(order).to.have.property('authorization');

                            expect(order.authorization).to.have.property('status');
                            expect(order.authorization.status).to.be.equal('pending');

                            expect(order.authorization).to.have.property('challenges');
                            expect(order.authorization.challenges).to.be.an('array');

                            expect(order.authorization).to.have.property('identifier');
                            expect(order.authorization.identifier).to.have.property('type');
                            expect(order.authorization.identifier).to.have.property('value');

                            expect(order.authorization).to.have.property('expires');
                        }
                    }
                },
                ready: {
                    on: {
                        'CANCEL': 'canceled',
                        'FINALIZE': 'valid'
                    },
                    meta: {
                        test: async function(/* f, event */) {
                            const order = await getOrder(account);
                            expect(order.data).to.have.property('status');
                            expect(order.data.status).to.be.equal('ready');
                            expect(order.data).not.to.have.property('certificate');
                            expect(order.data).not.to.have.property('star-certificate');
                        }
                    }
                },
                valid: {
                    on: {
                        'CANCEL': 'canceled',
                        'RETRIEVE': {
                            target: 'retrieved',
                            cond: needsPost
                        },
                        'RETRIEVE_VIA_GET': {
                            target: 'retrieved',
                            cond: allowGet
                        }
                    },
                    meta: {
                        test: async function(/* f, event */) {
                            const order = await getOrder(account);
                            expect(order.data).to.have.property('status');
                            expect(order.data.status).to.be.equal('valid');
                            let u;
                            if (order.data['auto-renewal']) {
                                expect(order.data).to.have.property('star-certificate');
                                expect(order.data).not.to.have.property('certificate');

                                u = new URL(order.data['star-certificate']);
                            }
                            else {
                                expect(order.data).to.have.property('certificate');
                                expect(order.data).not.to.have.property('star-certificate');

                                u = new URL(order.data['certificate']);
                            }
                            expect(u).not.to.be.null;
                        }
                    }
                },
                retrieved: {
                    on: {
                        'CANCEL': 'canceled',
                        'REVOKE': {
                            target: 'revoked',
                            cond: revocable
                        }
                    },
                    meta: {
                        test: async function(/* f, event */) {
                            const order = await getOrder(account);
                            expect(order.data).to.have.property('status');
                            expect(order.data.status).to.be.equal('valid');
                        }
                   }
                },
                revoked: {
                    on: {},
                    meta: {
                        test: async function(/* f, event */) {
                            const order = await getOrder(account);
                            expect(order.data).to.have.property('status');
                            expect(order.data.status).to.be.equal('valid');
                        }
                    }
                },
                canceled: {
                    on: {},
                    meta: {
                        test: async (/* f, event */) => {
                            //var order = await getOrder(account);
                            expect(order.data).to.have.property('status');
                            expect(order.data.status).to.be.equal('canceled');
                        }
                    }
                }
            }
        });
        const testModel = createModel(orderMachine)
            .withEvents({
                NEW: {
                    exec: async (/* f */) => {
                        await newDnsOrder(account);
                    }
                },
                NEWSTAR: {
                    exec: async (/* f */) => {
                        await newStarOrder(account);
                    }
                },
                NEWSTAR_FOR_GET: {
                    exec: async (/* f */) => {
                        await newStarOrderForGet(account);
                    }
                },
                AUTHORIZE: {
                    exec: async (/* f */) => {
                        let authorization = await authorize(account, order);
                        order.authorization = authorization;
                    }
                },
                FULLFILL: {
                    exec: async (/* f */) => {
                        await adminChallenge(account, order.authorization);
                    }
                },
                FINALIZE: {
                    exec: async (/* f */) => {
                        await finalizeOrderSuccessfully(account, order);
                    }
                },
                RETRIEVE: {
                    exec: async (/* f */) => {
                        await retrieveCertificate(account, order);
                    }
                },
                RETRIEVE_VIA_GET: {
                    exec: async (/* f */) => {
                        await retrieveCertificateViaGet(account, order);
                    }
                },
                REVOKE: {
                    exec: async (/* f */) => {
                        let certificate = order.certificateChain.split(/^-{5}.*$/m)[1]
                        await revokeCertificate(account, certificate);
                    }
                },
                CANCEL: {
                    exec: async (/* f */) => {
                        await cancelOrder(account, order);
                    }
                }
            });
            it('rejects invalid nonces', function() {
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: 'reject-me-please',
                        url: directory.newOrder
                    },
                    payload: {}
                };
                return sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                });
            });

        describe('order object', function() {

            before(getDirectory);
            before(createAccountKey);
            before(createAccountJwk);
            before(getNonce);
            before(getNonce);
            before(newAccount);
            before(function() {
                process.env.CA_BLOCK_SPECIAL_IPS = 'false';
            });
            after(function() {
                process.env.CA_BLOCK_SPECIAL_IPS = undefined;
            });
            describe('limit', function() {
                before(function() {
                    if (!NodeRED.started)
                        this.skip();
                    process.env.ACME_LIMIT_IDENTIFIERS = 1;
                });
                after(function() {
                    process.env.ACME_LIMIT_IDENTIFIERS = undefined;
                });
                it('rejects oversized orders', function(done) {
                    let json = {
                        protected: {
                            alg: account.alg,
                            kid: account.kid,
                            nonce: nonces.pop(),
                            url: directory.newOrder
                        },
                        payload: {
                            identifiers: [
                                {'type': 'email', 'value': 'ut@mail'},
                                {'type': 'email', 'value': 'other@mail'}
                            ]
                        }
                    };
                    sendMessage(account, json, function(res) {
                        expect(res).to.have.status(400);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                        const error = JSON.parse(res.body.toString());
                        expect(error).to.have.property('type');
                        expect(error.type).to.be.equal('urn:ietf:params:acme:error:rejectedIdentifier');
                        done();
                    });
                });
            });
            it('rejects mixed identifiers (email/dns)', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'email', 'value': 'ut@mail'},
                            {'type': 'dns', 'value': 'localhost'}
                        ]
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:rejectedIdentifier');
                    done();
                });
            });
            it('rejects mixed identifiers (email/ip)', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'email', 'value': 'ut@mail'},
                            {'type': 'ip', 'value': '::1'}
                        ]
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:rejectedIdentifier');
                    done();
                });
            });
            it('rejects invalid IPv6 identifier', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'ip', 'value': ':::1:2:3:4:5:6:7'}
                        ]
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:rejectedIdentifier');
                    done();
                });
            });
            it('rejects invalid IPv4 identifier', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'ip', 'value': '421.643.21.2'}
                        ]
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:rejectedIdentifier');
                    done();
                });
            });
            it('rejects invalid DNS identifier', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': 'Noé.Deroux.xn--node'}
                        ]
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:rejectedIdentifier');
                    done();
                });
            });
            it('rejects IPv4 in dns identifier', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': '8.8.8.8'}
                        ]
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:rejectedIdentifier');
                    done();
                });
            });
            it('rejects IPv6 in dns identifier', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': '2001:4860:4860::8844'}
                        ]
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:rejectedIdentifier');
                    done();
                });
            });
            let identifiers = [
                {'type': 'dns', 'value': '-invalid.example'},
                {'type': 'dns', 'value': 'invalid-.example'},
                {'type': 'dns', 'value': 'invalid-sub..example'},
                {'type': 'dns', 'value': 'invalid.example.'},
                {'type': 'dns', 'value': 'in--valid.example.'},
                {'type': 'dns', 'value': 'in_valid.example.'},
                {'type': 'dns', 'value': 'this-is-ways-too-long-for-dns-but-lets-try-it-nevertheless-to-certify.com'},
                {'type': 'dns', 'value': 'xn--iñvalid.com'},
                {'type': 'dns', 'value': 'xn---.com'},
            ];
            for (let identifier of identifiers) {
                it(`rejects domain ${identifier.value}`, function(done) {
                    let json = {
                        protected: {
                            alg: account.alg,
                            kid: account.kid,
                            nonce: nonces.pop(),
                            url: directory.newOrder
                        },
                        payload: {
                            identifiers: [
                                identifier
                            ]
                        }
                    };
                    sendMessage(account, json, function(res) {
                        expect(res).to.have.status(400);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                        const error = JSON.parse(res.body.toString());
                        expect(error).to.have.property('type');
                        expect(error.type).to.be.equal('urn:ietf:params:acme:error:rejectedIdentifier');
                        done();
                    });
                });
            }
            it('allow to configure certificate validity period', function(done) {
                let notBefore = new Date(Date.now() + 5 * 60 * 1000);
                let notAfter = new Date(Date.now() + 24 * 60 * 60 * 1000);
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': 'localhost'}
                        ],
                        notBefore: notBefore.toISOString(),
                        notAfter: notAfter.toISOString()
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(201);
                    expect(res.body).to.be.an.instanceof(Buffer);

                    order.data = JSON.parse(res.body.toString());
                    order.url = res.header['location'];

                    expect(Date.parse(order.data.notBefore)).to.be.equal(notBefore.getTime());
                    expect(Date.parse(order.data.notAfter)).to.be.equal(notAfter.getTime());

                    done();
                });
            });
            it('allow to configure certificate notAfter', function(done) {
                let notAfter = new Date(Date.now() + 24 * 60 * 60 * 1000);
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': 'localhost'}
                        ],
                        notAfter: notAfter.toISOString()
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(201);
                    expect(res.body).to.be.an.instanceof(Buffer);

                    order.data = JSON.parse(res.body.toString());
                    order.url = res.header['location'];

                    expect(Date.parse(order.data.notAfter)).to.be.equal(notAfter.getTime());

                    done();
                });
            });
            it('configuration of certificate notAfter reduces order validity', function(done) {
                let notAfter = new Date(Date.now() + 24 * 60 * 60 * 1000);
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': 'localhost'}
                        ],
                        notAfter: notAfter.toISOString()
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(201);
                    expect(res.body).to.be.an.instanceof(Buffer);

                    order.data = JSON.parse(res.body.toString());
                    order.url = res.header['location'];

                    expect(Date.parse(order.data.notAfter)).to.be.equal(notAfter.getTime());
                    expect(Date.parse(order.data.expires)).to.be.equal(notAfter.getTime());

                    done();
                });
           });
           it('allow to configure certificate notBefore', function(done) {
                let notBefore = new Date(Date.now() + 10 * 60 * 1000);
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': 'localhost'}
                        ],
                        notBefore: notBefore.toISOString()
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(201);
                    expect(res.body).to.be.an.instanceof(Buffer);

                    order.data = JSON.parse(res.body.toString());
                    order.url = res.header['location'];

                    expect(Date.parse(order.data.notBefore)).to.be.equal(notBefore.getTime());

                    done();
                });
            });
            it('requires notBefore after now', function(done) {
                let notBefore = new Date(Date.now() - 10 * 1000);
                let notAfter = new Date(Date.now() + 24 * 60 * 60 * 1000);
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': 'localhost'}
                        ],
                        notBefore: notBefore.toISOString(),
                        notAfter: notAfter.toISOString()
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    done();
                });
            });
            it('requires notBefore before tomorrow', function(done) {
                let notBefore = new Date(Date.now() + 48 * 60 * 60 * 1000);
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': 'localhost'}
                        ],
                        notBefore: notBefore.toISOString()
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    done();
                });
            });
            it('requires notBefore before notAfter', function(done) {
                let notBefore = new Date(Date.now() + 24 * 60 * 60 * 1000);
                let notAfter = new Date(Date.now() + 12 * 60 * 60 * 1000);
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': 'localhost'}
                        ],
                        notBefore: notBefore.toISOString(),
                        notAfter: notAfter.toISOString()
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    done();
                });
            });
            it('requires notAfter after now', function(done) {
                let notAfter = new Date(Date.now() - 100);
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': 'localhost'}
                        ],
                        notAfter: notAfter.toISOString()
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    done();
                });
            });
            it('requires notAfter not after next year', function(done) {
                let notAfter = new Date(Date.now() + 366 * 24 * 60 * 60 * 1000);
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newOrder
                    },
                    payload: {
                        identifiers: [
                            {'type': 'dns', 'value': 'localhost'}
                        ],
                        notAfter: notAfter.toISOString()
                    }
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    done();
                });
            });
            describe('challenges', function() {
                let authorization;
                before(function(done) {
                    let json = {
                        protected: {
                            alg: account.alg,
                            kid: account.kid,
                            nonce: nonces.pop(),
                            url: directory.newOrder
                        },
                        payload: {
                            identifiers: [
                                {'type': 'dns', 'value': 'localhost'}
                            ]
                        }
                    };
                    sendMessage(account, json, async function(res) {
                        expect(res).to.have.status(201);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        let order = {};
                        order.data = JSON.parse(res.body.toString());
                        expect(order.data).to.have.property('authorizations');
                        expect(order.data.authorizations).to.be.an('array');

                        authorization = await authorize(account, order);
                        done();
                    });
                });
                it('include tls-alpn-01 challenge', function() {
                    let challenges = authorization.challenges.map((c) => { return {type: c.type} });
                    expect(challenges).to.deep.include({type: 'tls-alpn-01'});
                });
                it('include http-01 challenge', function() {
                    let challenges = authorization.challenges.map((c) => { return {type: c.type} });
                    expect(challenges).to.deep.include({type: 'http-01'});
                });
                it('include dns-01 challenge', function() {
                    let challenges = authorization.challenges.map((c) => { return {type: c.type} });
                    expect(challenges).to.deep.include({type: 'dns-01'});
                });
                it('include x-admin-01 challenge', function() {
                    let challenges = authorization.challenges.map((c) => { return {type: c.type} });
                    expect(challenges).to.deep.include({type: 'x-admin-01'});
                });
                it('has only dns-01/x-admin-01 challenges for wildcard domains', function(done) {
                    let json = {
                        protected: {
                            alg: account.alg,
                            kid: account.kid,
                            nonce: nonces.pop(),
                            url: directory.newOrder
                        },
                        payload: {
                            identifiers: [
                                {'type': 'dns', 'value': '*.rz-bsd.my.corp'}
                            ]
                        }
                    };
                    sendMessage(account, json, async function(res) {
                        expect(res).to.have.status(201);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        let order = {};
                        order.data = JSON.parse(res.body.toString());
                        expect(order.data).to.have.property('authorizations');
                        expect(order.data.authorizations).to.be.an('array');

                        let authorization = await authorize(account, order);
                        expect(authorization.wildcard).to.be.true;
                        for (let challenge of authorization.challenges) {
                            expect(challenge.type).to.be.oneOf(['dns-01', 'x-admin-01']);
                            expect(challenge.status).to.equal('pending');
                        }
                        done();
                    });
                });
                it('has only http-01/tls-alpn-01/x-admin-01 challenges for ip identifiers', function(done) {
                    let json = {
                        protected: {
                            alg: account.alg,
                            kid: account.kid,
                            nonce: nonces.pop(),
                            url: directory.newOrder
                        },
                        payload: {
                            identifiers: [
                                {'type': 'ip', 'value': '2606:2800:220:1:248:1893:25c8:1946'}
                            ]
                        }
                    };
                    sendMessage(account, json, async function(res) {
                        expect(res).to.have.status(201);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        let order = {};
                        order.data = JSON.parse(res.body.toString());
                        expect(order.data).to.have.property('authorizations');
                        expect(order.data.authorizations).to.be.an('array');

                        let authorization = await authorize(account, order);
                        for (let challenge of authorization.challenges) {
                            expect(challenge.type).to.be.oneOf(['http-01', 'tls-alpn-01', 'x-admin-01']);
                            expect(challenge.status).to.equal('pending');
                        }
                        done();
                    });
                });
                it('has only email-reply-00/x-admin-01 challenges for email identifiers', function(done) {
                    let json = {
                        protected: {
                            alg: account.alg,
                            kid: account.kid,
                            nonce: nonces.pop(),
                            url: directory.newOrder
                        },
                        payload: {
                            identifiers: [
                                {'type': 'email', 'value': 'ut@mail'}
                            ]
                        }
                    };
                    sendMessage(account, json, async function(res) {
                        expect(res).to.have.status(201);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        let order = {};
                        order.data = JSON.parse(res.body.toString());
                        expect(order.data).to.have.property('authorizations');
                        expect(order.data.authorizations).to.be.an('array');

                        let authorization = await authorize(account, order);
                        for (let challenge of authorization.challenges) {
                            expect(challenge.type).to.be.oneOf(['email-reply-00', 'x-admin-01']);
                            expect(challenge.status).to.equal('pending');
                        }
                        done();
                    });
                });
            });

            for (const func of [ newDnsOrder, newStarOrder, newStarOrderForGet, newEmailOrder, newIpOrder ]) {
                describe('via ' + func.name, function() {
                    it('is functional', async function() {
                        order = await func(account);
                        // console.log(`order: ${JSON.stringify(order)}`);
                        expect(order.data).not.to.be.null;
                    });
                    it('provides authorizations', function() {
                        expect(order.data).to.have.property('authorizations');
                        expect(order.data.authorizations).to.be.an('array');
                        for (let authorization of order.data.authorizations) {
                            let u = new URL(authorization);
                            expect(u).not.to.be.null;
                        }
                    });
                    it('contains identifiers', function() {
                        expect(order.data).to.have.property('identifiers');
                        expect(order.data.identifiers).to.be.an('array');
                        for (let identifier of order.data.identifiers) {
                            expect(identifier).to.have.property('type');
                            expect(identifier).to.have.property('value');
                        }
                    });
                    it('has status property', function() {
                        expect(order.data).to.have.property('status');
                    });
                    it('has status value "pending"', function() {
                        expect(order.data.status).to.be.equal('pending');
                    });
                    it('has expires property', function() {
                        if (! order.data.expires)
                            this.skip();
                        expect(order.data).to.have.property('expires');
                        let d = Date.parse(order.data.expires);
                        expect(d).not.to.be.null;
                    });
                    it('has notBefore property', function() {
                        if (! order.data.notBefore)
                            this.skip();
                        expect(order.data).to.have.property('notBefore');
                        let d = Date.parse(order.data.notBefore);
                        expect(d).not.to.be.null;
                    });
                    it('has notAfter property', function() {
                        if (! order.data.notAfter)
                            this.skip();
                        expect(order.data).to.have.property('notAfter');
                        let d = Date.parse(order.data.notAfter);
                        expect(d).not.to.be.null;
                    });
                    it('has finalize property', function() {
                        expect(order.data).to.have.property('finalize');
                        let u = new URL(order.data.finalize);
                        expect(u).not.to.be.null;
                    });
                    it('has no certificate property', function() {
                        expect(order.data).not.to.have.property('certificate');
                    });
                    it('has no star-certificate property', function() {
                        expect(order.data).not.to.have.property('star-certificate');
                    });
                });
            }
            describe('STAR certificate order', function() {
                it('has auto-renewal property', async function() {
                    order = await newStarOrder(account);
                    expect(order.data).not.to.be.null;
                    expect(order.data).to.have.property('auto-renewal');
                });
                it('has auto-renewal.start-date property', function() {
                    expect(order.data['auto-renewal']).to.have.property('start-date');
                    let d = Date.parse(order.data['auto-renewal']['start-date']);
                    expect(d).not.to.be.null;
                });
                it('has auto-renewal.end-date property', function() {
                    expect(order.data['auto-renewal']).to.have.property('end-date');
                    let d = Date.parse(order.data['auto-renewal']['end-date']);
                    expect(d).not.to.be.null;
                });
                it('has auto-renewal.lifetime property', function() {
                    expect(order.data['auto-renewal']).to.have.property('lifetime');
                    expect(order.data['auto-renewal']['lifetime']).to.be.a('number');
                });
                it('has auto-renewal.lifetime-adjust property', function() {
                    expect(order.data['auto-renewal']).to.have.property('lifetime-adjust');
                    expect(order.data['auto-renewal']['lifetime-adjust']).to.be.a('number');
                });
                it('has auto-renewal.allow-certificate-get property', function() {
                    expect(order.data['auto-renewal']).to.have.property('allow-certificate-get');
                    expect(order.data['auto-renewal']['allow-certificate-get']).to.be.a('boolean');
                    expect(order.data['auto-renewal']['allow-certificate-get']).to.equal(false);
                });
                it('can be canceled', function(done) {
                    cancelOrder(account, order, function(res) {
                        expect(res).to.have.status(200);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        order.data = JSON.parse(res.body.toString());
                        expect(order.data.status).to.equal('canceled');
                        done();
                    });
                });
                it('cannot be canceled twice', function(done) {
                    cancelOrder(account, order, function(res) {
                        expect(res).to.have.status(400);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                        let acmeError = JSON.parse(res.body.toString());
                        expect(acmeError.type).to.equal('urn:ietf:params:acme:error:autoRenewalCancellationInvalid');
                        done();
                    });
                });
            });
            describe('STAR certificate order for GET', function() {
                it('has auto-renewal property', async function() {
                    order = await newStarOrderForGet(account);
                    expect(order.data).not.to.be.null;
                    expect(order.data).to.have.property('auto-renewal');
                });
                it('has auto-renewal.start-date property', function() {
                    expect(order.data['auto-renewal']).to.have.property('start-date');
                    let d = Date.parse(order.data['auto-renewal']['start-date']);
                    expect(d).not.to.be.null;
                });
                it('has auto-renewal.end-date property', function() {
                    expect(order.data['auto-renewal']).to.have.property('end-date');
                    let d = Date.parse(order.data['auto-renewal']['end-date']);
                    expect(d).not.to.be.null;
                });
                it('has auto-renewal.lifetime property', function() {
                    expect(order.data['auto-renewal']).to.have.property('lifetime');
                    expect(order.data['auto-renewal']['lifetime']).to.be.a('number');
                });
                it('has auto-renewal.lifetime-adjust property', function() {
                    expect(order.data['auto-renewal']).to.have.property('lifetime-adjust');
                    expect(order.data['auto-renewal']['lifetime-adjust']).to.be.a('number');
                });
                it('has auto-renewal.allow-certificate-get property', function() {
                    expect(order.data['auto-renewal']).to.have.property('allow-certificate-get');
                    expect(order.data['auto-renewal']['allow-certificate-get']).to.be.a('boolean');
                    expect(order.data['auto-renewal']['allow-certificate-get']).to.equal(true);
                });
            });

            describe('EMAIL certificate order', function() {
                it('contains email identifiers', async function() {
                    let order = await newEmailOrder(account);
                    // console.log(`order: ${JSON.stringify(order)}`);
                    expect(order.data).not.to.be.null;
                    expect(order.data).to.have.property('identifiers');
                    expect(order.data.identifiers).to.be.an('array');
                    for (let identifier of order.data.identifiers) {
                        expect(identifier).to.have.property('type');
                        expect(identifier.type).to.equal('email');
                        expect(identifier).to.have.property('value');
                    }
                });
            });
        });
        describe('state machine', function() {

            this.timeout(50000);
            this.slow(1250);

            before(createAccountKey);
            before(createAccountJwk);
            before(getNonce);
            before(newAccount);

            const testPlans = testModel.getSimplePathPlans();
            for (let plan of testPlans) {
                describe(plan.description, function() {
                    for (let path of plan.paths) {
                        it(path.description, function() {
                            return path.test(account);
                        });
                    }
                });
            }
        });
        describe('state machine test', function() {
            it('covered all states', function() {
                return testModel.testCoverage({
                    filter: stateNode => !!stateNode.meta
                });
            });
        });
    });

    describe('account\'s orders resource', function() {
        before(createAccountKey);
        before(createAccountJwk);
        before(getNonce);
        before(newAccount);

        it('can be requested', function(done) {
            expect(account.data).to.have.property('orders');
            const ordersUrl = new URL(account.data.orders);
            expect(ordersUrl).not.to.be.null;
            done();
        });
        it('provides orders', async function() {
            await newDnsOrder(account);
            let ordersUrl = new URL(account.data.orders);
            let json = {
                protected: {
                    alg: account.alg,
                    kid: account.kid,
                    nonce: nonces.pop(),
                    url: ordersUrl
                },
                payload: {}
            };
            return new Promise((resolve, reject) => {
                try {
                    sendMessage(account, json, async function(res) {
                        expect(res).to.have.status(200);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        let body = JSON.parse(res.body.toString());
                        expect(body).to.have.property('orders');
                        expect(body.orders).to.be.an('array');
                        for (let order of body.orders) {
                            expect(order).to.have.protocol('https');
                        }
                        resolve(true);
                    });
                }
                catch (error) {
                    reject(error);
                }
            });
        });
    });
    describe('account\'s delegations resource', function() {
        before(createAccountKey);
        before(createAccountJwk);
        before(getNonce);
        before(newAccount);

        it('can be requested', function(done) {
            if (account.data.delegations === undefined)
                this.skip();
            let delegationsUrl = new URL(account.data.delegations);
            expect(delegationsUrl).not.to.be.null;
            done();
        });
        it('provides orders', async function() {
            if (account.data.delegations === undefined)
                this.skip();
            let delegationsUrl = new URL(account.data.delegations);
            let json = {
                protected: {
                    alg: account.alg,
                    kid: account.kid,
                    nonce: nonces.pop(),
                    url: delegationsUrl
                },
                payload: {}
            };
            return new Promise((resolve, reject) => {
                try {
                    sendMessage(account, json, async function(res) {
                        expect(res).to.have.status(200);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        let body = JSON.parse(res.body.toString());
                        expect(body).to.have.property('delegations');
                        expect(body.orders).to.be.an('array');
                        for (let order of body.orders) {
                            expect(order).to.have.protocol('https');
                        }
                        resolve(true);
                    });
                }
                catch (error) {
                    reject(error);
                }
            });
        });
    });
    describe('authorization object', function() {
        let authorization = {};
        before(createAccountKey);
        before(createAccountJwk);
        before(getDirectory);
        before(getNonce);
        before(getNonce);
        before(newAccount);
        before(function() {
            return newDnsOrder(account);
        });
        it('has status pending', function(done) {
            let json = {
                protected: {
                    alg: account.alg,
                    kid: account.kid,
                    nonce: nonces.pop(),
                    url: order.data.authorizations[0]
                },
                payload: {}
            };
            sendMessage(account, json, function(res) {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an.instanceof(Buffer);
                authorization = JSON.parse(res.body.toString());
                authorization.url = order.data.authorizations[0];
                expect(authorization).to.have.property('status');
                expect(authorization.status).to.be.equal('pending');
                done();
            });
        });
        it('contains an identifier', function() {
            expect(authorization).to.have.property('identifier');
            expect(authorization.identifier).to.have.property('type');
            expect(authorization.identifier).to.have.property('value');
        });
        it('contains challenges', function() {
            expect(authorization).to.have.property('challenges');
            expect(authorization.challenges).to.be.an('array');
            for (let challenge of authorization.challenges) {
               expect(challenge).to.have.property('type');
               expect(challenge).to.have.property('url');
               expect(challenge).to.have.property('status');
               expect(challenge.status).to.be.equal('pending');
            }
        });
        it.skip('is granted with correct http-01 challenge', function() {
            let token;
            for (const challenge of authorization.challenges) {
                if (challenge.type === 'http-01') token = challenge.token;
            }
            let jwk = account.jwk.toJSON();
            return jose.JWK.asKey(jwk)
                .then((key) => {
                    return key.thumbprint('SHA-256');
                })
                .then((thumbprint) => {
                    httpServer.config.challenge = `${token}.${encode.b64tob64u(thumbprint.toString('base64'))}`;
                    return triggerHttpChallenge(account, authorization);
                });
        });
        it('can be deactivated', async function() {
            let response = await deactivateAuthorization(account, authorization);
            expect(response).to.have.property('status');
            expect(response.status).to.be.equal('deactivated');
        });
        it('cannot be deactivated twice', async function() {
            await deactivateAuthorization(account, authorization, (res) => {
                expect(res).to.have.status(403);
                expect(res.body).to.be.an.instanceof(Buffer);
                expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                const error = JSON.parse(res.body.toString());
                expect(error).to.have.property('type');
                expect(error.type).to.be.equal('urn:ietf:params:acme:error:unauthorized');
                return res;
            });
        });
    });

    describe('challenges', function() {

        before(getDirectory);

        describe('http-01', function() {

            let authorization = {};
            before(function() {
                if (!httpServer.started)
                    this.skip();
            });
            before(getDirectory);
            before(createAccountKey);
            before(createAccountJwk);
            before(getNonce);
            before(newAccount);
            before(function() {
                return newDnsOrder(account);
            });
            it('is provided', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: order.data.authorizations[0]
                    },
                    payload: {}
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    // console.log(`authorize response: ${res.body.toString()}`);
                    authorization = JSON.parse(res.body.toString());

                    let found = false;
                    for (const challenge of authorization.challenges) {
                        if (challenge.type === 'http-01')
                            found = true;
                    }
                    expect(found).to.be.true;

                    done();
                });
            });
            it('rejects if authorization is missing', function() {
                httpServer.config.challenge = undefined;
                return triggerHttpChallenge(account, authorization, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:incorrectResponse');
                });
            });
            it('rejects if authorization is protected', function() {
                httpServer.config.challenge = null;
                return triggerHttpChallenge(account, authorization, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:incorrectResponse');
                });
            });
            it('rejects if authorization is invalid', function() {
                httpServer.config.challenge = 'blablabla';
                return triggerHttpChallenge(account, authorization, function(res) {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:incorrectResponse');
                });
            });
            it('accepts valid authorization', function() {
                let token;
                for (const challenge of authorization.challenges) {
                    if (challenge.type === 'http-01') token = challenge.token;
                }
                let jwk = account.jwk.toJSON();
                return jose.JWK.asKey(jwk)
                    .then((key) => {
                        return key.thumbprint('SHA-256');
                    })
                    .then((thumbprint) => {
                        httpServer.config.challenge = `${token}.${encode.b64tob64u(thumbprint.toString('base64'))}`;
                        return triggerHttpChallenge(account, authorization);
                    });
            });
        });
        describe('dns-01', function() {

            let authorization = {};
            before(function() {
                if (!NodeRED.started)
                    this.skip();
            });
            before(createAccountKey);
            before(createAccountJwk);
            before(getNonce);
            before(newAccount);
            before(function() {
                let payload = {
                    identifiers: [
                        {'type': 'dns', 'value': 'www.example.com'}
                    ],
                    notAfter: new Date(Date.now() + 364 * 24 * 60 * 60 * 1000).toISOString()
                };
                return newOrder(account, payload);
            });

            before(dnsserver.start);
            after(dnsserver.stop);

            it('is provided', function(done) {
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: order.data.authorizations[0]
                    },
                    payload: {}
                };
                sendMessage(account, json, function(res) {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    // console.log(`authorize response: ${res.body.toString()}`);
                    authorization = JSON.parse(res.body.toString());

                    let found = false;
                    for (const challenge of authorization.challenges) {
                        if (challenge.type === 'dns-01')
                            found = true;
                    }
                    expect(found).to.be.true;
                    done();
                });
            });
            it('rejects if authorization is invalid', async function() {
                return await triggerDnsChallenge(account, authorization, (domain /*, data */) => {
                    dnsserver.config.records = dnsserver.config.records.filter(
                        element => element.name != domain);
                    dnsserver.config.records.push({name: domain, type: 0x10 /* TXT */, data: 'invalid'});
                }, (res) => {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    const error = JSON.parse(res.body.toString());
                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:dns');
                });
            });
            it('accepts valid authorization', async function() {
                return await triggerDnsChallenge(account, authorization, (domain, data) => {
                    dnsserver.config.records = dnsserver.config.records.filter(
                        element => element.name != domain);
                    dnsserver.config.records.push({name: domain, type: 0x10 /* TXT */, data: data});
                });
            });
        });
        describe('tls-alpn-01 challenge', function() {

            let authorization = {};
            before(createAccountKey);
            before(createAccountJwk);
            before(getNonce);
            before(getNonce);
            before(getNonce);
            before(getNonce);
            before(getNonce);
            before(newAccount);
            describe('for dns identifiers', function() {
                before(function() {
                    return newDnsOrder(account);
                });
                it('is provided', function(done) {
                    let json = {
                        protected: {
                            alg: account.alg,
                            kid: account.kid,
                            nonce: nonces.pop(),
                            url: order.data.authorizations[0]
                        },
                        payload: {}
                    };
                    sendMessage(account, json, function(res) {
                        expect(res).to.have.status(200);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        // console.log(`authorize response: ${res.body.toString()}`);
                        authorization = JSON.parse(res.body.toString());

                        let found = false;
                        for (const challenge of authorization.challenges) {
                            if (challenge.type === 'tls-alpn-01')
                                found = true;
                        }
                        expect(found).to.be.true;
                        done();
                    });
                });
                it('rejects if server not available', function(done) {
                    triggerTlsAlpnChallenge(account, authorization, (/* hash */) => {
                        /* No setup => failure */
                    }, (res) => {
                        expect(res).to.have.status(400);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                        const error = JSON.parse(res.body.toString());
                        expect(error).to.have.property('type');
                        expect(error.type).to.be.equal('urn:ietf:params:acme:error:incorrectResponse');
                        done();
                    });
                });
                describe('challenge response', function() {
                    before(function() {
                        if (!tlsAlpnServer.available)
                            this.skip();
                    });
                    it('is rejected with TLSv1.1', function(done) {
                        triggerTlsAlpnChallenge(account, authorization, async (hash) => {
                            tlsAlpnServer.config.tlsVersion = 'TLSv1.1';
                            await tlsAlpnServer.start(authorization.identifier.value, hash);
                            tlsAlpnServer.config.tlsVersion = undefined;
                        }, (res) => {
                            expect(res).to.have.status(400);
                            expect(res.body).to.be.an.instanceOf(Buffer);
                            expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                            const error = JSON.parse(res.body.toString());
                            expect(error).to.have.property('type');
                            expect(error.type).to.be.equal('urn:ietf:params:acme:error:incorrectResponse');
                            done();
                        });
                    });
                    it('is rejected with a shortened challenge', function(done) {
                        triggerTlsAlpnChallenge(account, authorization, async (hash) => {
                            await tlsAlpnServer.start(authorization.identifier.value, hash.substring(20));
                        }, (res) => {
                            expect(res).to.have.status(400);
                            expect(res.body).to.be.an.instanceOf(Buffer);
                            expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                            const error = JSON.parse(res.body.toString());
                            expect(error).to.have.property('type');
                            expect(error.type).to.be.equal('urn:ietf:params:acme:error:incorrectResponse');
                            done();
                        });
                    });
                    it('is rejected with incorrect challenge', function(done) {
                        triggerTlsAlpnChallenge(account, authorization, async (hash) => {
                            await tlsAlpnServer.start(authorization.identifier.value, [...hash].reverse().join(''));
                        }, (res) => {
                            expect(res).to.have.status(400);
                            expect(res.body).to.be.an.instanceOf(Buffer);
                            expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                            const error = JSON.parse(res.body.toString());
                            expect(error).to.have.property('type');
                            expect(error.type).to.be.equal('urn:ietf:params:acme:error:incorrectResponse');
                            done();
                        });
                    });
                    it('is rejected with incorrect domain', function(done) {
                        triggerTlsAlpnChallenge(account, authorization, async (hash) => {
                            await tlsAlpnServer.start('incorrect.example.com', hash);
                        }, (res) => {
                            expect(res).to.have.status(400);
                            expect(res.body).to.be.an.instanceOf(Buffer);
                            expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                            const error = JSON.parse(res.body.toString());
                            expect(error).to.have.property('type');
                            expect(error.type).to.be.equal('urn:ietf:params:acme:error:incorrectResponse');
                            done();
                        });
                    });
                    it('is accepted when correct', function() {
                        return triggerTlsAlpnChallenge(account, authorization, async (hash) => {
                            await tlsAlpnServer.start(authorization.identifier.value, hash);
                        });
                    });
                });
            });
            describe('for ip identifiers (for IPv4)', function() {
                before(function() {
                    process.env.CA_BLOCK_SPECIAL_IPS = 'false';
                });
                before(function() {
                    return newIpOrder(account);
                });
                after(function() {
                    process.env.CA_BLOCK_SPECIAL_IPS = undefined;
                });
                it('is provided', function(done) {
                    let json = {
                        protected: {
                            alg: account.alg,
                            kid: account.kid,
                            nonce: nonces.pop(),
                            url: order.data.authorizations[0]
                        },
                        payload: {}
                    };
                    sendMessage(account, json, function(res) {
                        expect(res).to.have.status(200);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        // console.log(`authorize response: ${res.body.toString()}`);
                        authorization = JSON.parse(res.body.toString());

                        let found = false;
                        for (const challenge of authorization.challenges) {
                            if (challenge.type === 'tls-alpn-01')
                                found = true;
                        }
                        expect(found).to.be.true;
                        done();
                    });
                });
                describe('challenge response', function() {
                    before(function() {
                        if (!tlsAlpnServer.available)
                            this.skip();
                    });
                    after(async function() {
                        await tlsAlpnServer.stop();
                    });
                    it('is accepted when correct', function(done) {
                        triggerTlsAlpnChallenge(account, authorization, async (hash) => {
                            await tlsAlpnServer.start(authorization.identifier.value, hash);
                        }, (res) => {
                            expect(res.body).to.be.an.instanceof(Buffer);
                            let body = JSON.parse(res.body.toString());
                            // expect(body.type).to.be.undefined;
                            expect(body.detail).to.be.undefined;

                            expect(res).to.have.status(200);
                            let challenge = body;
                            expect(challenge).to.have.property('status');
                            // should be 'processing' or 'valid'
                            expect(challenge.status).to.be.equal('valid');

                            let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                            let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                                subject: {str: `/CN=${authorization.identifier.value}`},
                                          sbjpubkey: key.pubKeyObj,
                                          sigalg: 'SHA256withECDSA',
                                          sbjprvkey: key.prvKeyObj})
                                .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');

                            return finalizeOrderSuccessfully(account, order, csr, (res) => {
                                expect(res).to.have.status(200);
                                expect(res.body).to.be.an.instanceof(Buffer);
                                done();
                            });
                        });
                    });
                });
            });
            describe('for ip identifiers (for IPv6)', function() {
                before(function() {
                    process.env.CA_BLOCK_SPECIAL_IPS = 'false';
                });
                before(function() {
                    return newIpv6Order(account);
                });
                after(async function() {
                    process.env.CA_BLOCK_SPECIAL_IPS = undefined;
                    await tlsAlpnServer.stop();
                });
                it('is provided', function(done) {
                    let json = {
                        protected: {
                            alg: account.alg,
                            kid: account.kid,
                            nonce: nonces.pop(),
                            url: order.data.authorizations[0]
                        },
                        payload: {}
                    };
                    sendMessage(account, json, function(res) {
                        expect(res.body).to.be.an.instanceof(Buffer);
                        // console.log(`authorize response: ${res.body.toString()}`);
                        authorization = JSON.parse(res.body.toString());

                        let found = false;
                        for (const challenge of authorization.challenges) {
                            if (challenge.type === 'tls-alpn-01')
                                found = true;
                        }
                        expect(res).to.have.status(200);
                        expect(found).to.be.true;
                        done();
                    });
                });
                describe('challenge response', function() {
                    before(function() {
                        if (!tlsAlpnServer.available)
                            this.skip();
                    });
                    it('is accepted when correct', function(done) {
                        triggerTlsAlpnChallenge(account, authorization, async (hash) => {
                            await tlsAlpnServer.start(authorization.identifier.value, hash);
                        }, (res) => {
                            expect(res.body).to.be.an.instanceof(Buffer);
                            let body = JSON.parse(res.body.toString());
                            // expect(body.type).to.be.undefined;
                            expect(body.detail).to.be.undefined;

                            expect(res).to.have.status(200);
                            let challenge = body;
                            expect(challenge).to.have.property('status');
                            // should be 'processing' or 'valid'
                            expect(challenge.status).to.be.equal('valid');

                            let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                            let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                                subject: {str: `/CN=${authorization.identifier.value}`},
                                          sbjpubkey: key.pubKeyObj,
                                          sigalg: 'SHA256withECDSA',
                                          sbjprvkey: key.prvKeyObj})
                                .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');

                            finalizeOrderSuccessfully(account, order, csr, (res) => {
                                expect(res).to.have.status(200);
                                expect(res.body).to.be.an.instanceof(Buffer);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    describe('pre-authorization', function() {

        before(createAccountKey);
        before(createAccountJwk);
        before(getDirectory);
        before(getNonce);
        before(getNonce);
        before(newAccount);

        it('is available', function() {
            expect(directory.newAuthz).not.to.be.undefined;
        });
        it('is functional', async function() {
            if (directory.newAuthz === undefined)
                this.skip();
            let authorization = await newPreAuthorization(account, {type: 'dns', value: 'localhost'});
            expect(authorization).not.to.be.null;
            expect(authorization.data).not.to.be.undefined;
            expect(authorization.data.status).to.equal('pending');
        });
        it('can be deactivated', async function() {
            if (directory.newAuthz === undefined)
                this.skip();
            let authorization = await newPreAuthorization(account, {type: 'dns', value: 'localhost'});
            expect(authorization).not.to.be.null;
            expect(authorization.data).not.to.be.undefined;
            authorization = await deactivateAuthorization(account, authorization);
            expect(authorization).not.to.be.null;
            expect(authorization).not.to.be.undefined;
            expect(authorization.status).to.equal('deactivated');
        });
        describe('identifier', function() {
            it('is required', async function() {
                if (directory.newAuthz === undefined)
                    this.skip();
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newAuthz
                    }
                };
                json.payload = {}
                let res = await sendMessageSync(account, json);
                expect(res).not.to.have.status(201);
                expect(res).not.to.have.header('location');
            });
            it('rejects wildcards', async function() {
                if (directory.newAuthz === undefined)
                    this.skip();
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newAuthz
                    }
                };
                json.payload = { identifier: { type: 'dns', value: '*.my.corp' } };
                let res = await sendMessageSync(account, json);
                expect(res).not.to.have.status(201);
                expect(res).not.to.have.header('location');
            });
            it('must have type dns', async function() {
                if (directory.newAuthz === undefined)
                    this.skip();
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newAuthz
                    }
                };
                json.payload = { identifer: { type: 'email', value: 'hacker@my.corp' } };
                let res = await sendMessageSync(account, json);
                expect(res).not.to.have.status(201);
                expect(res).not.to.have.header('location');
            });
            it('must have a value', async function() {
                if (directory.newAuthz === undefined)
                    this.skip();
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newAuthz
                    }
                };
                json.payload = { identifer: { type: 'dns', 'no-value': 'my.corp' } };
                let res = await sendMessageSync(account, json);
                expect(res).not.to.have.status(201);
                expect(res).not.to.have.header('location');
            });
            it('must not be an IPv4 address', async function() {
                if (directory.newAuthz === undefined)
                    this.skip();
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newAuthz
                    }
                };
                json.payload = { identifer: { type: 'dns', 'value': '8.8.4.4' } };
                let res = await sendMessageSync(account, json);
                expect(res).not.to.have.status(201);
                expect(res).not.to.have.header('location');
            });
            it('must not be an IPv6 address', async function() {
                if (directory.newAuthz === undefined)
                    this.skip();
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newAuthz
                    }
                };
                json.payload = { identifer: { type: 'dns', 'value': '2001:4860:4860::8844' } };
                let res = await sendMessageSync(account, json);
                expect(res).not.to.have.status(201);
                expect(res).not.to.have.header('location');
            });
            it('accepts subdomainAuthAllowed attribute', async function() {
                if (directory.newAuthz === undefined)
                    this.skip();
                if (directory.meta.subdomainAuthAllowed === undefined || directory.meta.subdomainAuthAllowed === false)
                    this.skip();
                let json = {
                    protected: {
                        alg: account.alg,
                        kid: account.kid,
                        nonce: nonces.pop(),
                        url: directory.newAuthz
                    }
                };
                json.payload = { identifier: {
                    type: 'dns',
                    value: 'sub.my.corp',
                    subdomainAuthAllowed: true
                } };
                let res = await sendMessageSync(account, json);
                expect(res).to.have.status(201);
                expect(res).to.have.header('location');
            });
        });

        const preAuthMachine = Machine({
            id: 'pre-authorization',
            initial: 'registered',
            states: {
                registered: {
                    on: {
                        'PREAUTH': 'pending',
                        'PREAUTH_SUBDOMAINS': 'pending'
                    }
                },
                pending: {
                    on: {
                        'TRIGGER': 'valid'
                    },
                    meta: {
                        test: async function(/* context, event */) {
                            const preAuth = await getPreAuthorization(account);
                            expect(preAuth.data).to.have.property('status');
                            expect(preAuth.data.status).to.be.equal('pending');
                            expect(preAuth.data).to.have.property('identifier');
                        }
                    }
                },
                valid: {
                    on: {
                        'ORDER': 'ordered',
                        'DEACTIVATE': 'deactivated'
                    },
                    meta: {
                        test: async function(/* context, event */) {
                            const preAuth = await getPreAuthorization(account);
                            expect(preAuth.data).to.have.property('status');
                            expect(preAuth.data.status).to.be.equal('valid');
                            expect(preAuth.data).to.have.property('identifier');
                        }
                    }
                },
                deactivated: {
                    on: {},
                    meta: {
                        test: async function(/* context, event */) {
                            const preAuth = await getPreAuthorization(account);
                            expect(preAuth.data).to.have.property('status');
                            expect(preAuth.data.status).to.be.equal('deactivated');
                        }
                    }
                },
                ordered: {
                    type: 'final',
                    meta: {
                        test: async function(/* context, event */) {
                            const order = await getOrder(account);
                            expect(order.data).to.have.property('status');
                            expect(order.data.status).to.be.equal('ready');
                        }
                    }
                }
            }
        });
        const preAuthModel = createModel(preAuthMachine)
            .withEvents({
                PREAUTH: {
                    exec: async function preAuthorize() {
                        await newPreAuthorization(account, {type: 'dns', value: 'ut.my.corp'});
                    }
                },
                PREAUTH_SUBDOMAINS: {
                    exec: async function preAuthorize() {
                        await newPreAuthorization(account, {
                            type: 'dns',
                            value: 'other.my.corp',
                            subdomainAuthAllowed: true
                        });
                    }
                },
                TRIGGER: {
                    exec: async function dnsChallenge() {
                        let preAuth = await getPreAuthorization(account);
                        await triggerDnsChallenge(account, preAuth.data, (domain, data) => {
                            // console.log(`setup: ${domain} IN TXT '${data}'`);
                            dnsserver.config.records = dnsserver.config.records.filter(
                                element => element.name != domain);
                            dnsserver.config.records.push({name: domain, type: 0x10 /* TXT */, data: data});
                        });
                    }
                },
                DEACTIVATE: {
                    exec: async function deactivate() {
                        let preAuth = await getPreAuthorization(account);
                        return await deactivateAuthorization(account, preAuth);
                    }
                },
                ORDER: {
                    exec: async function newPreAuthorizedOrder() {
                        let payload = {
                            identifiers: [
                                {'type': 'dns', 'value': 'ut.my.corp'}
                            ]
                        }
                        return await newOrder(account, payload);
                    }
                }
            });
        describe('state machine', function() {
            before(function () {
                if (directory.newAuthz === undefined)
                    this.skip();
                //if (!NodeRED.started)
                //    this.skip();
            });
            before(dnsserver.start);
            before(getNonce);
            after(dnsserver.stop);

            const testPlans = preAuthModel.getSimplePathPlans();
            for (let plan of testPlans) {
                describe(plan.description, function() {
                    for (let path of plan.paths) {
                        it(path.description, function() {
                            return path.test(account);
                        });
                    }
                });
            }
        });
        describe('state machine test', function() {
            it('covered all states', function() {
                if (!NodeRED.started)
                    this.skip();
                return preAuthModel.testCoverage({
                    filter: stateNode => !!stateNode.meta
                });
            });
        });
    });
    describe('Certification', function() {
        before(getDirectory);
        before(getNonce);
        before(createAccountKey);
        before(createAccountJwk);
        before(getNonce);
        before(newAccount);
        before(function() {
            return newDnsOrder(account)
                .then((order) => {
                    return authorize(account, order);
                })
                .then((authorization) => {
                    order.authorization = authorization;
                    return adminChallenge(account, order.authorization);
                });
        });
        it('can download certificate twice', function(done) {
            finalizeOrder(account, order, null, (res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an.instanceof(Buffer);
                let order = {data: JSON.parse(res.body) };
                retrieveCertificate(account, order, (res) => {
                    expect(res).to.have.status(200);
                    expect(res).to.have.header('content-type', 'application/pem-certificate-chain');

                    expect(res.body).to.be.an.instanceof(Buffer);
                    let firstDownload = res.body.toString();

                    retrieveCertificate(account, order, (res) => {
                        expect(res).to.have.status(200);
                        expect(res).to.have.header('content-type', 'application/pem-certificate-chain');
                        expect(firstDownload).to.be.equal(res.body.toString());
                        done();
                    });
                });
            });
        });
    });
    describe('Certification process hooks', function() {
        before(getDirectory);
        before(getNonce);
        before(createAccountKey);
        before(createAccountJwk);
        before(getNonce);
        before(newAccount);
        describe('check-order.js', function() {
            before(function() {
                fs.writeFileSync('scripts/order-check.js', `
                    console.error('reject order');
                    throw new Error('order-check failed');`, 'utf8');
            });
            after(function() {
                fs.unlinkSync('scripts/order-check.js');
            });
            it('can reject orders', function(done) {
                newDnsOrder(account, (res) => {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:rejectedIdentifier');
                    done();
                });
            });
        });
        describe('pre-certificate-issuance.js', function() {
            before(function() {
                if (process.env.CA_PRE_CERTIFICATES != 'true' && !NodeRED.started)
                    this.skip();
            });
            before(function() {
                return newDnsOrder(account)
                           .then((order) => {
                               return authorize(account, order);
                           })
                           .then((authorization) => {
                               order.authorization = authorization;
                               return adminChallenge(account, order.authorization);
                           });
            });
            before(function() {
                process.env.CA_PRE_CERTIFICATES = 'true';
            });
            before(function() {
                fs.writeFileSync('scripts/pre-certificate-issuance.js', `
                    console.error('deny issuance');
                    throw new Error('Issuance denied!');`, 'utf8');
            });
            after(function() {
                try {
                    fs.unlinkSync('scripts/pre-certificate-issuance.js');
                }
                catch { /* nothing */ }
            });
            after(function() {
                process.env.CA_PRE_CERTIFICATES = undefined;
            });
            it('script can hinder issuance', function(done) {
                finalizeOrder(account, order, null, (res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    let order = {data: JSON.parse(res.body) };
                    retrieveCertificate(account, order, (res) => {
                        expect(res).to.have.status(500);
                        expect(res).to.have.property('body');
                        expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                        expect(res.body).to.be.an.instanceof(Buffer);

                        let acmeError = JSON.parse(res.body);
                        expect(acmeError).to.have.property('type');
                        expect(acmeError.type).to.equal('urn:ietf:params:acme:error:serverInternal');
                        done();
                    });
                });
            });
        });
        describe('csr-check.js', function() {
            before(function() {
                return newDnsOrder(account)
                           .then((order) => {
                               return authorize(account, order);
                           })
                           .then((authorization) => {
                               order.authorization = authorization;
                               return adminChallenge(account, order.authorization);
                           });
            });
            before(function() {
                fs.writeFileSync('scripts/csr-check.js', `
                    console.error('deny certification');
                    throw new Error("I don't like this request");`, 'utf8');
            });
            after(function() {
                fs.unlinkSync('scripts/csr-check.js');
            });
            it('script stop issuance', function(done) {
                finalizeOrder(account, order, null, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                    done();
                });
            });
        });
        describe('certificate-issuance.js', function() {
            this.timeout(10000);
            before(function() {
                fs.writeFileSync('scripts/certificate-issuance.js', `
                    console.error('deny publishing');
                    throw new Error('Keeping this secret!');`, 'utf8');
            });
            after(function() {
                fs.unlinkSync('scripts/certificate-issuance.js');
            });
            it('script can hinder publishing', function(done) {
                finalizeOrder(account, order, null, (res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    let order = {data: JSON.parse(res.body) };
                    retrieveCertificate(account, order, (res) => {
                        expect(res).to.have.status(500);
                        expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                        expect(res.body).to.be.an.instanceof(Buffer);

                        let acmeError = JSON.parse(res.body);
                        expect(acmeError.type).to.equal('urn:ietf:params:acme:error:serverInternal');
                        done();
                    });
                });
            });
        });
    });
    describe('CSR validation', function() {
        before(function() {
            return newDnsOrder(account)
                       .then((order) => {
                           return authorize(account, order);
                       })
                       .then((authorization) => {
                           order.authorization = authorization;
                           return adminChallenge(account, order.authorization);
                       });
        });
        describe('ASN.1 compliance check', function() {
            const chars = ['@', '&', '\\', '!', '"', '~', '`', '§', '$', '%', '*', '#', ';', '_'];
            describe('PrintableString', function() {
                this.slow(400);
                for (let c of chars) {
                    it(`rejects character '${c}'`, function(done) {
                        try {
                            let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                            let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                                          subject: {str: '/CN=localhost' +
                                                         `/C=X${c}`},
                                          sbjpubkey: key.pubKeyObj,
                                          extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                                          sigalg: 'SHA256withECDSA',
                                          sbjprvkey: key.prvKeyObj})
                                .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                            finalizeOrder(account, order, csr, (res) => {
                                expect(res).to.have.status(400);
                                expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                                expect(res.body).to.be.an.instanceof(Buffer);

                                let acmeError = JSON.parse(res.body);
                                expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                                done();
                            });
                        }
                        catch (error) {
                            expect(error).to.be.null;
                            done();
                        }
                    });
                }
            });
        });
        describe('RFC 5280 compliance check', function() {
            // -- Upper Bounds from RFC5280
            it('rejects overlong commonName (CN) (max=64)', function() {
                // ub-common-name INTEGER ::= 64
                // ub-common-name-length INTEGER ::= 64
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=' + '0123456789' + '0123456789'
                                                    + '0123456789' + '0123456789'
                                                    + '0123456789' + '0123456789'
                                                    + '01234'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);
                    //console.log(`finalize error: ${res.body.toString()}`);
                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects overlong countryNames (C) (max=2)', function() {
                // ub-country-name-alpha-length INTEGER ::= 2
                // ub-country-name-numeric-length INTEGER ::= 3
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' +
                                             '/C=' + '012'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects overlong emailAddress (E) (max=255)', function() {
                // ub-emailaddress-length INTEGER ::= 255
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' +
                                             '/E=' + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '012345'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects overlong givenName (GN) (max=16)', function() {
                // ub-given-name-length INTEGER ::= 16
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' +
                                             '/GN=' + '0123456789' + '01234567'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects overlong localityNames (L) (max=128)', function() {
                // ub-locality-name INTEGER ::= 128
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' +
                                             '/L=' + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '012345678'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects overlong numericUserIds (UID) (32)', function() {
                // ub-numeric-user-id-length INTEGER ::= 32
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' +
                                             '/UID=' + '0123456789' + '0123456789'
                                                     + '0123456789' + '0123'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects overlong surname (SN) (max=40)', function() {
                // ub-surname-length INTEGER ::= 40
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' +
                                             '/SN=' + '0123456789'
                                                    + '0123456789'
                                                    + '0123456789'
                                                    + '0123456789'
                                                    + '0'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects overlong serialNumber (max=64)', function() {
                // ub-serial-number INTEGER ::= 64
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' +
                                             '/serialNumber=' + '0123456789' + '0123456789'
                                                              + '0123456789' + '0123456789'
                                                              + '0123456789' + '0123456789'
                                                              + '01234'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects overlong organizationName (O) (max=64)', function() {
                // ub-organization-name INTEGER ::= 64
                // ub-organization-name-length INTEGER ::= 64
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' +
                                             '/O=' + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '0123456789' + '0123456789'
                                                   + '01234'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects overlong organizationalUnitName (OU) (max=64)', function() {
                // ub-organizational-unit-name INTEGER ::= 64
                // ub-organizational-unit-name-length INTEGER ::= 32
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' +
                                             '/OU=' + '0123456789' + '0123456789'
                                                    + '0123456789' + '0123456789'
                                                    + '0123456789' + '0123456789'
                                                    + '01234'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects overlong stateNames (ST) (max=128)', function() {
                // ub-state-name INTEGER ::= 128
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' +
                                             '/ST=' + '0123456789' + '0123456789'
                                                    + '0123456789' + '0123456789'
                                                    + '0123456789' + '0123456789'
                                                    + '0123456789' + '0123456789'
                                                    + '0123456789' + '0123456789'
                                                    + '0123456789' + '0123456789'
                                                    + '012345678'},
                              sbjpubkey: key.pubKeyObj,
                              extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                              sigalg: 'SHA256withECDSA',
                              sbjprvkey: key.prvKeyObj})
                    .getPEM().replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
        });
        describe('Mozilla Root Store Policy compliance check', function() {
            it('rejects secp521r1', async function() {
                let key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp521r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' },
                                  sbjpubkey: key.pubKeyObj,
                                  extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                                  sigalg: 'SHA256withECDSA',
                                  sbjprvkey: key.prvKeyObj
                          })
                          .getPEM()
                          .replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                return finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                });
            });
            it('rejects RSA key with modulus length less than 2048 bits', function(done) {
                this.timeout(30000);
                let key = jsrsasign.KEYUTIL.generateKeypair('RSA', 2047);
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' },
                                  sbjpubkey: key.pubKeyObj,
                                  extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                                  sigalg: 'SHA256withRSA',
                                  sbjprvkey: key.prvKeyObj
                          })
                          .getPEM()
                          .replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                    done();
                });
            });
            it('rejects RSA key with modulus length not divisible 8', function(done) {
                this.timeout(30000);
                let key = jsrsasign.KEYUTIL.generateKeypair('RSA', 2054);
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' },
                                  sbjpubkey: key.pubKeyObj,
                                  extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                                  sigalg: 'SHA256withRSA',
                                  sbjprvkey: key.prvKeyObj
                          })
                          .getPEM()
                          .replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                    done();
                });
            });
        });
        describe('CA Browser forum Baseline Requirements compliance check', function() {
            it('rejects CSRs signed with a different key', function(done) {
                let csrKey = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let signKey = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' },
                                  sbjpubkey: csrKey.pubKeyObj,
                                  extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                                  sigalg: 'SHA256withRSA',
                                  sbjprvkey: signKey.prvKeyObj
                          })
                          .getPEM()
                          .replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError).to.have.property('type');
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                    done();
                });
            });
            const primes = [{value:   2},{value:   3},{value:   5},{value:   7},{value:  11},{value:  13},{value:  17},
                            {value:  19},{value:  23},{value:  29},{value:  31},{value:  37},{value:  41},{value:  43},
                            {value:  47},{value:  53},{value:  59},{value:  61},{value:  67},{value:  71},{value:  73},
                            {value:  79},{value:  83},{value:  89},{value:  97},{value: 101},{value: 103},{value: 107},
                            {value: 109},{value: 113},{value: 127},{value: 131},{value: 137},{value: 139},{value: 149},
                            {value: 151},{value: 157},{value: 163},{value: 167},{value: 173},{value: 179},{value: 181},
                            {value: 191},{value: 193},{value: 197},{value: 199},{value: 211},{value: 223},{value: 227},
                            {value: 229},{value: 233},{value: 239},{value: 241},{value: 251},{value: 257},{value: 263},
                            {value: 269},{value: 271},{value: 277},{value: 281},{value: 283},{value: 293},{value: 307},
                            {value: 311},{value: 313},{value: 317},{value: 331},{value: 337},{value: 347},{value: 349},
                            {value: 353},{value: 359},{value: 367},{value: 373},{value: 379},{value: 383},{value: 389},
                            {value: 397},{value: 401},{value: 409},{value: 419},{value: 421},{value: 431},{value: 433},
                            {value: 439},{value: 443},{value: 449},{value: 457},{value: 461},{value: 463},{value: 467},
                            {value: 479},{value: 487},{value: 491},{value: 499},{value: 503},{value: 509},{value: 521},
                            {value: 523},{value: 541},{value: 547},{value: 557},{value: 563},{value: 569},{value: 571},
                            {value: 577},{value: 587},{value: 593},{value: 599},{value: 601},{value: 607},{value: 613},
                            {value: 617},{value: 619},{value: 631},{value: 641},{value: 643},{value: 647},{value: 653},
                            {value: 659},{value: 661},{value: 673},{value: 677},{value: 683},{value: 691},{value: 701},
                            {value: 709},{value: 719},{value: 727},{value: 733},{value: 739},{value: 743},{value: 751}];
            let prime = primes[Math.floor(Math.random()*primes.length)];
            it(`rejects RSA key with modulus divisible by ${prime.value}`, function(done) {
                let pem = fs.readFileSync(`test/data/keys/${prime.value}.pem`, 'utf8');
                let key = jsrsasign.KEYUTIL.getKey(pem);
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' },
                                  sbjpubkey: key,
                                  extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                                  sigalg: 'SHA256withRSA',
                                  sbjprvkey: key
                          })
                          .getPEM()
                          .replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError).to.have.property('type');
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                    done();
                });
            });
            it('rejects CSRs signed with a weak key', function(done) {
                let pem = fs.readFileSync(`test/data/keys/factor.pem`, 'utf8');
                let key = jsrsasign.KEYUTIL.getKey(pem);
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=localhost' },
                                  sbjpubkey: key,
                                  extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                                  sigalg: 'SHA256withRSA',
                                  sbjprvkey: key
                          })
                          .getPEM()
                          .replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError).to.have.property('type');
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                    done();
                });
            });
            it('rejects CSRs signed with a different CN', function(done) {
                let pem = fs.readFileSync(`test/data/keys/factor.pem`, 'utf8');
                let key = jsrsasign.KEYUTIL.getKey(pem);
                let csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                              subject: {str: '/CN=example.com' },
                                  sbjpubkey: key,
                                  extreq: [{extname: 'subjectAltName', array: [{'dns': 'www.example.com'}]}],
                                  sigalg: 'SHA256withRSA',
                                  sbjprvkey: key
                          })
                          .getPEM()
                          .replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
                finalizeOrder(account, order, csr, (res) => {
                    expect(res).to.have.status(400);
                    expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    let acmeError = JSON.parse(res.body);
                    expect(acmeError).to.have.property('type');
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badCSR');
                    done();
                });
            });
        });
    });
    describe('CTLOG integration', function() {
        let request;
        let certificates = [];
        let issued;
        before(function() {
            //if (!NodeRED.started)
            //    this.skip();
            process.env.CA_PRE_CERTIFICATES = 'integrate';
            process.env.CA_CTLOG_COUNT = '1';
        });
        before(async function() {
            if (!ctlogServer.started)
                this.skip();
            return ctlogServer.load_key('test/data/ctlog.key.pem');
        });
        before(getDirectory);
        before(getNonce);
        before(createAccountKey);
        before(createAccountJwk);
        before(getNonce);
        before(newAccount);
        before(async function() {
            this.timeout = 60000;
            return newDnsOrder(account)
                       .then((order) => {
                           return authorize(account, order);
                       })
                       .then((authorization) => {
                           order.authorization = authorization;
                           return adminChallenge(account, order.authorization);
                       });
        });
        after(function() {
            process.env.CA_CTLOG_COUNT = undefined;
            process.env.CA_PRE_CERTIFICATES = undefined;
        });
        it('publishes pre-certificates', function(done) {
            this.timeout = 10000;
            finalizeOrder(account, order, null, (res) => {
                expect(res).to.have.status(200);
                expect(res).to.have.property('body');
                expect(res.body).to.be.an.instanceof(Buffer);

                let order = {data: JSON.parse(res.body)};
                retrieveCertificate(account, order, (res) => {
                    expect(res).to.have.status(200);
                    expect(res).to.have.property('body');
                    expect(res.body).to.be.an.instanceof(Buffer);

                    issued = [];
                    let candidates = res.body.toString('binary').split(
                        /(?<=-{5}END CERTIFICATE-{5})[\n\r]+/gm);
                    for (let candidate of candidates) {
                        try {
                            let cert = new crypto.X509Certificate(candidate);
                            issued.push(cert);
                        }
                        catch { /* nothing */ }
                    }
                    expect(issued).to.have.lengthOf.above(1);
                    // console.log(`issued: ${inspect(issued)}`);

                    // expect(ctlogServer.requests).to.be.above(1);
                    request = ctlogServer.requests.pop();
                    expect(request).not.to.be.undefined;
                    done();
                });
            });
        });
        it("uses header Content-Type with value 'application/json'", function() {
            if (request === undefined)
                this.skip();
            expect(request).to.have.header('content-type', 'application/json');
        });
        it('publishes a chain of certificates', function() {
            if (request === undefined)
                this.skip();
            expect(request.body).not.to.be.null;
            expect(request.body.chain).to.be.an('array');
            expect(request.body.chain).to.have.lengthOf(2);
            for (let b64 of request.body.chain) {
                let cert = new jsrsasign.X509();
                cert.readCertHex(Buffer.from(b64, 'base64').toString('hex'));
                certificates.push(cert);
            }
        });
        it('publishes a verifiable chain', function() {
            if (request === undefined)
                this.skip();
            let pubkey = certificates[1].getPublicKey();
            let verification = certificates[0].verifySignature(pubkey);
            expect(verification).to.be.true;
        });
        it('uses a critical poison extension', function() {
            if (request === undefined)
                this.skip();
            let param = certificates[0].getParam();
            let ext = param.ext.find((ext) => ext.extname === '1.3.6.1.4.1.11129.2.4.3');
            expect(ext).not.to.be.null;
            expect(ext.extn).to.have.property('null', '');
            expect(ext.critical).to.be.true;
        });
        it('emits SignedCertificateTimestamps (SCTs)', function() {
            expect(issued[0].checkIssued(issued[1])).to.be.true;
            const pem = issued[0].toString();
            const x509 = new jsrsasign.X509();
            x509.readCertPEM(pem);
            const param = x509.getParam();

            let scts;
            for (let i=0; i<param.ext.length; i++) {
                if (param.ext[`${i}`].extname === '1.3.6.1.4.1.11129.2.4.2')
                    scts = param.ext.splice(i, 1)[0];
            }
            // console.log(`scts: ${inspect(scts)}`);
            expect(scts).not.to.be.undefined;
        });
    });
    describe('CAA validation', function() {
        const domain = 'rejected.my.corp';
        before(dnsserver.start);
        before(getNonce);
        before(createAccountKey);
        before(createAccountJwk);
        before(getNonce);
        before(newAccount);
        after(dnsserver.stop);

        describe('RFC 8659 compliance', function() {
            describe('issue tag', function() {
                before(function() {
                    let payload = {
                        identifiers: [
                            {'type': 'dns', 'value': domain}
                        ]
                    };
                    return newOrder(account, payload)
                        .then((order) => {
                           return authorize(account, order);
                        })
                        .then((authorization) => {
                             return order.authorization = authorization;
                         });
                });
                const testcases = [
                    {name: 'toplevel',  records: [{'domain': 'my.corp', 'flags': 0, 'tag': 'issue',   'value': 'other.ca'}]},
                    {name: 'other.ca',  records: [{'domain': domain,  'flags':   0, 'tag': 'issue',   'value': 'other.ca'}]},
                    {name: 'uppercase', records: [{'domain': domain,  'flags':   0, 'tag': 'Issue',   'value': 'other.ca'}]},
                    {name: 'critical',  records: [{'domain': domain,  'flags': 128, 'tag': 'unknown', 'value': 'other.ca'}]}
                ];
                for (let testcase of testcases) {
                    it(`prohibits authorization with CAA '${testcase.name}'`, function(done) {
                        if (!NodeRED.started)
                            this.skip();

                        for (let entry of testcase.records) {
                            dnsserver.config.records = dnsserver.config.records.filter(
                                element => element.name != entry.domain);
                            dnsserver.config.records.push({
                                name: entry.domain,
                                type: 0x101 /* CAA */,
                                flags: entry.flags,
                                tag: entry.tag,
                                value: entry.value
                            });
                        }
                        adminChallenge(account, order.authorization, (res) => {
                            expect(res).to.have.status(400);
                            expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');
                            const error = JSON.parse(res.body.toString());
                            expect(error.type).to.be.equal('urn:ietf:params:acme:error:caa');
                            done();
                        });
                    });
                }
                it('allows issuance with multiple issue tags present', function(done) {
                    if (!NodeRED.started)
                        this.skip();

                    dnsserver.config.records = dnsserver.config.records.filter(
                        element => element.name != domain);
                    dnsserver.config.records.push({
                        name: domain,
                        type: 0x101 /* CAA */,
                        flags: 1,
                        tag: 'issue',
                        value: 'other.ca; account=123456'
                    },
                    {
                        name: domain,
                        type: 0x101 /* CAA */,
                        flags: 0,
                        tag: 'issue',
                        value: process.env.ACME_IDENTITY || process.env.HOSTNAME || os.hostname()
                    });
                    adminChallenge(account, order.authorization, (res) => {
                        expect(res).to.have.status(200);
                        done();
                    });
                });
            });
            describe('issuewild tag', function() {
                before(function() {
                    let payload = {
                        identifiers: [
                            {'type': 'dns', 'value': `*.${domain}`}
                        ]
                    };
                    return newOrder(account, payload)
                        .then((order) => {
                            return authorize(account, order);
                        })
                        .then((authorization) => {
                            return order.authorization = authorization;
                        });
                });
                const testcases = [
                    {name: 'other.ca',  records: [{'domain': domain,    'flags': 0, 'tag': 'issuewild',    'value': 'other.ca'}]},
                    {name: 'issue',     records: [{'domain': domain,    'flags': 0, 'tag': 'issue',        'value': 'other.ca'}]},
                    {name: 'toplevel',  records: [{'domain': domain,    'flags': 0, 'tag': 'contactemail', 'value': `hostmaster@${domain}`},
                                                  {'domain': 'my.corp', 'flags': 0, 'tag': 'issuewild',    'value': 'other.ca'}]}
                ];
                for (let testcase of testcases) {
                    it(`prohibits authorization with CAA '${testcase.name}'`, function(done) {
                        if (!NodeRED.started)
                            this.skip();

                        for (let entry of testcase.records) {
                            dnsserver.config.records = dnsserver.config.records.filter(
                                element => element.name != entry.domain);
                            dnsserver.config.records.push({
                                name: entry.domain,
                                type: 0x101 /* CAA */,
                                flags: entry.flags,
                                tag: entry.tag,
                                value: entry.value
                            });
                        }
                        adminChallenge(account, order.authorization, (res) => {
                            expect(res).to.have.status(400);
                            expect(res).to.have.header('content-type', 'application/problem+json; charset=utf-8');

                            const error = JSON.parse(res.body.toString());
                            expect(error.type).to.be.equal('urn:ietf:params:acme:error:caa');
                            done();
                        });
                    });
                }
            });
        });
    });
});
