import crypto from 'node:crypto';
const { subtle } = crypto.webcrypto;
import url from 'node:url';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import jsrsasign from 'jsrsasign';

import acme from '../examples/acme-client/acme.mjs';

import * as httpServer from './fixture/http-server.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('Blacklist', function() {
    let key, csr;
    let account;
    before(async function() {
        key = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
        csr = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                      subject: {str: '/CN=localhost'},
                      sbjpubkey: key.pubKeyObj,
                      extreq: [{extname: 'subjectAltName', array: [{'dns': 'localhost'}]}],
                      sigalg: 'SHA256withECDSA',
                      sbjprvkey: key.prvKeyObj})
                  .getPEM()
                  .replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');

        let responder;
        if (httpServer.started)
            responder = new httpServer.ChallengeResponder;
        else
            responder = new acme.WebrootChallengeResponder('/usr/local/www/apache24/data');
        let uri = new url.URL(`${server}/acme/Sub/directory`);
        account = await acme.Account.create(uri, [responder]);
    });
    it('blocks revoked keys in CSRs', function(done) {
        this.timeout(30*1000);
        let identifier = {type: 'dns', value: 'localhost'};

        account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (error, chain) => {
            if (error) throw error;
            account.revokeCertificate(chain[0].toString(), 1 /* keyCompromise */)
                .then(() => {
                    let identifier = {type: 'dns', value: 'localhost'};
                    account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (error, chain) => {
                        expect(chain).to.be.undefined;
                        expect(error).not.to.be.null;
                        expect(error.type).to.be.equal('urn:ietf:params:acme:error:badCSR');
                        done();
                    });
                    return true;
                })
                .catch((error) =>  {
                    throw error;
                });
        });
    });
    it('blocks blacklisted keys used for ACME accounts', function(done) {
        let client = new acme.Client(`${server}/acme/Sub/directory`);
        let jwk = jsrsasign.KEYUTIL.getJWK(key.prvKeyObj);
        subtle.importKey('jwk', jwk, {name: 'ECDSA', namedCurve: 'P-256'}, true, ['sign'])
            .then((privateKey) => {
                return client.newAccount({alg: 'ES256', jwk, privateKey}, undefined, undefined, (error, account) => {
                    expect(account).to.be.undefined;
                    expect(error).not.to.be.null;

                    expect(error).to.have.property('type');
                    expect(error.type).to.be.equal('urn:ietf:params:acme:error:badPublicKey');
                    expect(error).to.have.property('detail');
                    done();
                })
            })
            .catch((error) => {
                expect(error).to.be.undefined;
            });
    });
});
