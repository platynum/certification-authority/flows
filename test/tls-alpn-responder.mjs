import tls from 'node:tls';

import chai from './fixture/chai.mjs';
const assert = chai.assert;

import * as tlsAlpnResponder from './fixture/tls-alpn-server.mjs';

describe('test fixture', function() {
    it('can start TLS ALPN responder', function() {
        tlsAlpnResponder.start('example.com', 'dead');
    });
    it('can stop TLS ALPN responder', function() {
        tlsAlpnResponder.stop();
    });
    describe('TLS ALPN responder', function() {
        before(function start() {
            tlsAlpnResponder.start('example.com', 'beef');
        });
        after(tlsAlpnResponder.stop);

        it('can be connected', function(done) {
            let socket = tls.connect(tlsAlpnResponder.config.port, {
                host: '127.0.0.1',
                ALPNProtocols: ['acme-tls/1'],
                rejectUnauthorized: false,
                checkServerIdentity: () => {
                    assert(true)
                }
            });
            socket.on('secureConnect', () => {
                assert(true);
                socket.destroy();
                done();
            });
        });
    });
});

