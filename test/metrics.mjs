import fs from 'node:fs';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import * as helpers from './lib/helpers.mjs';
import * as NodeRED from './fixture/node-red.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('Metrics', function() {
    it('are provided', function(done) {
        let user = process.env.METRICS_USERNAME || 'prometheus';
        let pass = process.env.METRICS_PASSWORD || 'password';
        chai.request.execute(server).get('/metrics')
            .buffer()
            .parse(helpers.binaryParser)
            .auth(user, pass)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res).to.be.text;
                expect(res).to.have.header('content-type', 'text/plain; charset=utf-8; version=0.0.4');

                fs.writeFileSync('metrics.txt', res.body, 'utf8');
                done();
            });
    });
    describe('authentication', function() {
        it('is required', function(done) {
            if (!NodeRED.started)
                this.skip();
            let user = process.env.METRICS_USERNAME;
            let pass = process.env.METRICS_PASSWORD;
            if (!user || !pass)
                this.skip();
            chai.request.execute(server).get('/metrics')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(401);
                    done();
                });
        });
        describe('username', function() {
            it('is checked', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.METRICS_USERNAME = 'krypton-86';
                let pass = process.env.METRICS_PASSWORD = 'password';
                chai.request.execute(server).get('/metrics')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(`unknown-${user}`, pass)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(401);
                        done();
                    });
            });
            it('can be configured via METRICS_USERNAME', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.METRICS_USERNAME = 'krypton-87';
                let pass = process.env.METRICS_PASSWORD;
                chai.request.execute(server).get('/metrics')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(user, pass)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        done();
                    });

            });
        });
        describe('password', function() {
            it('is checked', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.METRICS_USERNAME = 'krypton-86';
                let pass = process.env.METRICS_PASSWORD = 'Quertziop+';
                chai.request.execute(server).get('/metrics')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(user, `${pass}-incorrect`)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        done();
                    });
            });
            it('can be configured via METRICS_PASSWORD', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.METRICS_USERNAME;
                let pass = process.env.METRICS_PASSWORD = 'Quertziop-';
                chai.request.execute(server).get('/metrics')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(user, pass)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        done();
                    });
            });
        });
    });
});
