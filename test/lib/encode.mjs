
export function b64tob64u(str) {
    return str.replaceAll('+', '-')
              .replaceAll('/', '_')
              .replaceAll('=', '');
}

export function b64utob64(str) {
    let tmp = str.replaceAll('-', '+')
                 .replaceAll('_', '/')
                 .replaceAll('=', '');
    if (tmp.length % 4 != 0)
        tmp += '=';
    if (tmp.length % 4 != 0)
        tmp += '=';
    return tmp;
}

export function b64utostr(b64) {
    return Buffer.from(b64utob64(b64), 'base64').toString();
}

export function strtob64u(str) {
    let tmp = Buffer.from(str, 'binary').toString('base64');
    return b64tob64u(tmp);
}

export function b64toab(b64) {
    const str = Buffer.from(b64, 'base64').toString('binary');
    const buf = new ArrayBuffer(str.length);
    const bufView = new Uint8Array(buf);
    for (let i=0, strLen=str.length; i<strLen; i++) {
        bufView[`${i}`] = str.charCodeAt(i);
    }
    return buf;
}

export default {
    b64tob64u,
    b64utob64,
    b64utostr,
    strtob64u,
    b64toab
}
