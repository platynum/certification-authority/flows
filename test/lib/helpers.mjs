
export function binaryParser(res, cb) {
    res.setEncoding("binary");
    res.data = "";
    res.on("data", function (chunk) {
        res.data += chunk;
    });
    res.on("end", function () {
        cb(null, Buffer.from(res.data, "binary"));
    });
}

export function toArrayBuffer(buf) {
    let ab = new ArrayBuffer(buf.length);
    let view = new Uint8Array(ab);
    for (let i=0; i<buf.length; ++i) {
        view[`${i}`] = buf[`${i}`];
    }
    return ab;
}

export function toHexString(arrayBuffer) {
    if (typeof arrayBuffer !== 'object' || arrayBuffer === null || typeof arrayBuffer.byteLength !== 'number') {
        throw new TypeError('Expected input to be an ArrayBuffer')
    }

    let view = new Uint8Array(arrayBuffer)
    let result = ''
    let value

    for (let i=0; i<view.length; i++) {
        value = view[`${i}`].toString(16)
        result += (value.length === 1 ? '0' + value : value)
    }
    return result
}

export default {
    binaryParser,
    toArrayBuffer,
    toHexString
}
