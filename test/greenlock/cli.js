#!/usr/bin/env

'use strict';

var d = new Date();
var agent = require('crypto').randomBytes(9).toString('base64').replace(/[+\/-]/g, 'X');
var major = Math.floor(d / 1000000000 % 10000);
var minor = Math.floor(d / 1000000 % 1000);
var patch = Math.ceil(d / 1000 % 1000);
var version = `${major}.${minor}.${patch}`
console.log(`agent=${agent}/${version}`);

function downloadRootCertificate() {
	const http = require('http');
	const fs = require('fs');
	const cacert = fs.createWriteStream('Root.crt.pem');
	const request = http.get(`http://${process.env.ACME_SERVER}/download/Root/ca.crt.pem`, (response) => {
		response.pipe(cacert);
	});
	process.env.NODE_EXTRA_CA_CERTS = __dirname + '/Root.crt.pem';
}
// process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

var Greenlock = require('@root/greenlock');
var greenlock = Greenlock.create({
	packageRoot: __dirname,
	configDir: './',
	packageAgent: `agent=${agent}/${version}`,
	maintainerEmail: `${agent}@example.com`,
	notify: function(event, details) {
		if ('error' === event) {
			console.error(details);
		}
		else {
			console.log(details);
		}
	},
	debug: true
	// accountKeyType: 'RSA-2048',
});

greenlock.manager
	.defaults({
		agreeToTerms: true,
		subscriberEmail: `${agent}@example.com`,
		directoryUrl: `https://${process.env.ACME_SERVER}/acme/Sub/`,
		challenges: {
			'http-01': {
				module: 'acme-http-01-webroot',
				webroot: __dirname + '/www/.well-known/acme-challenge'
			}
		}
	})
	.then(function(fullConfig) {
		console.log(fullConfig);
		// ...
	});

const os = require('os');
var altnames = [ os.hostname() ];
greenlock
	.add({
		subject: altnames[0],
		altnames: altnames
	})
	.then(function(fullConfig) {
		// saved config to db or fs
		// console.log(fullConfig);
	});

greenlock
	.get({ servername: altnames[0] })
	.then(function(pems) {
		if (pems && pems.privkey && pems.cert && pems.chain) {
			console.info('Success');
		}
		console.log(pems);
	})
	.catch(function(e) {
		console.error('Big bad error:', e.code);
		console.error(e);
		console.error(e.stack);
		process.exit(1);
	});
