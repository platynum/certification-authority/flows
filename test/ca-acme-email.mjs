import crypto from 'node:crypto';
const { subtle } = crypto.webcrypto;
import { inspect } from 'node:util';
import url from 'node:url';
import fs from 'node:fs';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

//const ocsp = require('ocsp');

import Imap from 'imap';
import dkim from 'dkim';
import nodemailer from 'nodemailer';
import { simpleParser } from 'mailparser';

import jsrsasign from 'jsrsasign';

import forge from 'node-forge';
import { SignPdf } from '@signpdf/signpdf';
import { P12Signer } from '@signpdf/signer-p12';
import { plainAddPlaceholder } from '@signpdf/placeholder-plain';

import encode from './lib/encode.mjs';
import helpers from './lib/helpers.mjs';
import acme from '../examples/acme-client/acme.mjs';
import * as dnsserver from './fixture/dnsserver.mjs';
import * as NodeRED from './fixture/node-red.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

class FaultyEmailChallengeResponder {
    setupImap(imap, options, subjectHandler) {
        this.imap = imap;
        if (imap) {
            imap.on('error', (error) => {
                console.log(`IMAP error: ${error}`);
                if (error) throw error;
            });
            imap.on('ready', () => {
                console.log('Connected, opening INBOX ...');
                imap.openBox('INBOX', false, (error, box) => {
                    if (error) throw error;
                    console.log(`searching INBOX with ${box.messages.new} unseen of ${box.messages.total} messages for ACME challenges ...`);
                    imap.search(['UNSEEN', ['SUBJECT', 'ACME: ']], (error, results) => {
                        console.log(`Found ${results.length} challenge(s)`);
                        let f = imap.fetch(results, {
                            bodies: [''],
                            markSeen: true
                        });
                        f.on('message', function(message, seqno) {
                            let header = '';
                            let body = '';
                            message.on('body', function(stream, info) {
                                if (info.which === 'TEXT')
                                    console.log(`fetching message ${seqno} with ${info.size} bytes ...`);
                                else
                                    console.log(`fetching headers of message ${seqno} (${info.size} bytes) ...`);
                                    stream.on('data', function(chunk) {
                                    if (info.which === 'TEXT')
                                        body += chunk.toString('utf8');
                                    else
                                        header += chunk.toString('utf8');
                                });
                            });
                            message.once('end', async function(/*stream, info*/) {
                                let headers = Imap.parseHeader(header);
                                // console.log(`headers: ${inspect(headers)}`);
                                if (headers.subject && headers.subject.length > 0
                                  && headers.subject[0].startsWith('ACME: ')
                                  && headers['authentication-results'] && headers['authentication-results'].length > 0) {
                                      if (/dkim=pass/.test(headers['authentication-results'][0]))
                                          console.log(`found dkim=pass in "${headers['authentication-results'][0]}`);
                                      else
                                          console.log(`missing dkim=pass in "${headers['authentication-results'][0]}`);
                                      /* Check d= and throw error on permfail */
                                }
                                console.log(`heaers: ${JSON.stringify(headers)}`);
                                /* Extract challenge */
                                let challenge = {
                                    verified: true,
                                    from: headers.from,
                                    to: headers.to,
                                    token: headers.subject[0].substring(6),
                                    body: body
                                };
                                subjectHandler(undefined, challenge);
                            });
                        });
                        f.once('end', function() {
                            imap.end();
                        });
                    });
                });
            });
        }
    }
    async sendReply(error, challenge) {
        console.log(`Challenge (via email): ${inspect(challenge)}`);
        if (error)
            throw new Error(`Challenge received (${error})`);
        let part1 = Buffer.from(challenge.token, 'base64');
        let index = this.problems.findIndex(problem => problem.authorization.identifier.value == challenge.to);
        if (index == -1)
            throw new Error('Cannot find problem for challenge received');
        let problem = this.problems[`${index}`];
        this.problems.splice(index, 1);

        let from;
        if (typeof challenge.from === "string")
            from = challenge.from;
        else if (Array.isArray(challenge.from)) {
            if (challenge.from.length != 1)
                 throw new Error(`Got more than one sender address (${JSON.stringify(challenge.from)})`);
            from = challenge.from[0];
        }
        if (from !== problem.challenge.from) {
            console.log(`from: "${from}" is not problem.challenge.from: "${problem.challenge.from}"`);
            throw new Error('The from header of the email does not match the from sent by challenge');
        }
        let part2 = Buffer.from(problem.challenge.token, 'base64url');

        let token = Buffer.concat([part1, part2]);
        let thumbprint = await acme.createJwkThumbprint(this.key.jwk);
        let keyAuthorization = `${token.toString('base64url')}.${thumbprint}.fault`;

        let hash = crypto.createHash('sha256');
        hash.update(keyAuthorization);
        let response = '-----BEGIN ACME RESPONSE-----\r\n'
                     + hash.digest('base64url') + '\r\n'
                     + '-----END ACME RESPONSE-----\r\n'

        console.log(`sending to ${challenge.to} from ${problem.challenge.from} with\n${response}`);
        this.smtp.sendMail({
            from: challenge.to, // problem.authorization.identifier.value?
            to: problem.challenge.from,
            subject: `Re: ACME: ${part1.toString('base64url')}`,
            text: response,
        });
        problem.solved = true;
        return true;
    }

    constructor(imap, smtp, options) {
        this.type = 'email-reply-00';
        this.problems = [];
        this.options = options;
        this.smtp = smtp;
        this.imap = imap;
        this.setupImap(imap, options, (error, challenge) => { this.sendReply(error, challenge); });
    }

    solve(key, authorization, challenge, timeout=12 * 1000) {
        this.key = key;
        this.problems.push({authorization: authorization, challenge: challenge});
        setTimeout(() => {
            console.log('Connecting to IMAP server ...');
            this.imap.connect();
        }, timeout);
    }
}

// TODO: should be 'ACME Client <client@mail>'
const email = process.env.ACME_SMTP_TEST_FROM || 'client@mail';
function setupImap() {
    return new Imap({ user: process.env.ACME_IMAP_TEST_USER || 'client@mail',
                      password: process.env.ACME_IMAP_TEST_PASSWORD || 'client@mail',
                      host: process.env.ACME_IMAP_TEST_SERVER || 'mail',
                      port: process.env.ACME_IMAP_TEST_PORT || 3143,
                      tls: (process.env.ACME_IMAP_TEST_TLS ?? "false") == "true"
    });
}
const smtp = nodemailer.createTransport({
        host: process.env.ACME_SMTP_TEST_SERVER || 'mail',
        port: process.env.ACME_SMTP_TEST_PORT || 3025,
        secure: (process.env.ACME_SMTP_TEST_SECURE ?? "false") == "true",
        auth: {
            user: process.env.ACME_SMTP_TEST_USER || 'client@mail',
            pass: process.env.ACME_SMTP_TEST_PASSWORD || 'client@mail'
        },
        logger: (process.env.ACME_SMTP_TEST_DEBUG ?? "false") == "true",
        console: (process.env.ACME_SMTP_TEST_DEBUG ?? "false") == "true"
});

describe('ACME for End-User S/MIME Certificates', function() {
    describe('test fixture', function() {
        let message = {};

        it('can send email', function(done) {
            let token = 'aff7c2a0f49ff52c2f3642c5f586e805';
            smtp.sendMail({
                from: email,
                to: email,
                subject: `ACME-TEST: ${token}`,
                headers: { 'Auto-Submitted': 'auto-generated' },
                html: '<h1>Hello</h1><p>World</p>',
                alternatives: [
                    { raw: 'Content-Type: application/json; charset=utf-8\r\n' +
                           'Content-Encoding: utf-8\r\n' +
                           '\r\n' +
                           `{ token: ${token} }` }
                ]
            });
            setTimeout(done, 1000);
        });
        it('can receive email', function(done) {
            this.timeout(25*1000);
            let debug = (process.env.ACME_IMAP_TEST_DEBUG ?? 'false') == 'true';
            let imap = setupImap();
            imap.once('ready', function() {
                if (debug)
                    console.log('Connection established');
                imap.openBox('INBOX', false, function(err /* box */) {
                    if (err) throw err;
                    if (debug)
                        console.log('INBOX opened');
                    imap.search(['UNSEEN', ['SUBJECT', 'ACME-TEST: ']], (err, results) => {
                        if (debug)
                            console.log('found e-mails');
                        let f = imap.fetch(results, {
                            bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE DKIM-SIGNATURE AUTHENTICATION-STATUS)', 'TEXT'],
                            markSeen: true
                        });
                        f.on('message', function(msg, seqno) {
                            msg.on('body', function(stream, info) {
                                if (debug && info.which === 'TEXT')
                                    console.log('Body of message %i [%s] found, %d total bytes', seqno, inspect(info.which), info.size);
                                let buffer = '', count = 0;
                                stream.on('data', function(chunk) {
                                    count += chunk.length;
                                    buffer += chunk.toString('utf8');
                                    if (debug && info.which === 'TEXT')
                                        console.log('Body [%s] (%d/%d)', inspect(info.which), count, info.size);
                                });
                                stream.once('end', function() {
                                    if (info.which === 'TEXT') {
                                        message.body = Buffer.from(buffer);
                                    }
                                    else {
                                        message.headers = Imap.parseHeader(buffer);
                                    }

                                    if (debug) {
                                        if (info.which === 'TEXT') {
                                            console.log('Body [%s] Finished', inspect(info.which));
                                            console.log(`${buffer}`);
                                        }
                                        else {
                                            console.log('Parsed header: %s', inspect(Imap.parseHeader(buffer)));
                                        }
                                    }
                                });
                            });
                            msg.once('attributes', function(attrs) {
                                if (debug)
                                    console.log('Attributes: %s', inspect(attrs, false, 8));
                                message.attributes = attrs;
                                imap.end();
                            });
                        });
                        f.once('error', function(err) {
                            throw new Error(`Fetch error: ${err}`);
                        });
                        f.once('end', function() {
                            done();
                        });
                    });
                });
            });
            imap.once('end', function() {
                console.log('Connection ended');
            });
            if (debug)
                console.log(`Connecting ${(process.env.ACME_IMAP_TEST_TLS ?? "false") == "true" ? "imaps": "imap"}://${process.env.ACME_IMAP_TEST_SERVER || "mail"}:${process.env.ACME_IMAP_TEST_PORT || 3143}`);
            imap.connect();
        });
        it('DKIM signature can be verified', function(done) {
            this.skip();
            dkim.verify(message, (err, result) => {
                console.log(`result: ${JSON.stringify(result)}`);
                expect(err).to.be.null;
                expect(result[0].verified).to.be.true;
                done();
            });
        });
    });
    describe('process', function() {
        let account;
        let key;
        let csr;
        let certificates = [];

        before(async function() {
           try {
              let imap = setupImap();
              let responder = new acme.EmailChallengeResponder(imap, smtp, {"verification": false});
              account = await acme.Account.create(`${server}/acme/Sub/directory`, [responder]);
              return account;
           }
           catch (error) {
              console.error(`setup account: ${error} ${error.stack}`);
              throw error;
           }
        });
        beforeEach(function() {
            key = jsrsasign.KEYUTIL.generateKeypair("EC", "secp256r1");

            let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                subject: {str: '/E=' + email},
                extreq: [{
                    "extname": "subjectAltName",
                    array: [{ rfc822: email }]
                }],
                sbjpubkey: key.pubKeyObj,
                sigalg: "SHA256withECDSA",
                sbjprvkey: key.prvKeyObj
            });
            req.sign();
            csr = req.getPEM();
        });
        it('emits certificates', function(done) {
            this.timeout(120*1000);
            let identifier = {type: 'email', value: email};
            account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (err, chain) => {
                expect(err).to.be.null;
                if (err) throw err;
                // Store for revocation
                certificates.push(chain);

                expect(chain).not.to.be.undefined;
                let cert = chain[0];
                // console.log(`${cert}`);
                expect(cert.ca).to.be.false;
                expect(cert.checkEmail(email)).equal(email);
                expect(cert.keyUsage).to.include('1.3.6.1.5.5.7.3.4');

                let x509 = new jsrsasign.X509();
                x509.readCertPEM(cert.toString());
                let keyUsage = x509.getExtKeyUsage();
                expect(keyUsage.names).deep.to.equal(['digitalSignature', 'keyAgreement']);
                let extKeyUsage = x509.getExtExtKeyUsage();
                // console.log(inspect(extKeyUsage.array));
                expect(extKeyUsage.array).deep.to.equal(['emailProtection']);

                done();
            });
        });
        it.skip('fails with faulty challenge', function(done) {
            let imap = setupImap();
            account.responders[0] = new FaultyEmailChallengeResponder(imap, smtp, {"verification": false});

            this.timeout(120*1000);
            let identifier = {type: 'email', value: email};
            account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (err, chain) => {
                expect(err).to.be.null;
                expect(chain).to.be.undefined;
                done();
            });
       });
       it('can request certificate with digitalSignature, only', function(done) {
            this.timeout(120*1000);

            let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                subject: {str: '/E=' + email},
                extreq: [{
                    "extname": "subjectAltName",
                    array: [{ rfc822: email }]
                },
                {
                    "extname": "keyUsage",
                    names: ['digitalSignature']
                }],
                sbjpubkey: key.pubKeyObj,
                sigalg: "SHA256withECDSA",
                sbjprvkey: key.prvKeyObj
            });
            req.sign();
            csr = req.getPEM();

            let identifier = {type: 'email', value: email};
            account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (err, chain) => {
                if (err) throw err;
                certificates.push(chain);

                let cert = chain[0];
                // console.log(`${cert}`);
                expect(cert.ca).to.be.false;
                expect(cert.checkEmail(email)).equal(email);
                expect(cert.keyUsage).to.include('1.3.6.1.5.5.7.3.4');

                let x509 = new jsrsasign.X509();
                x509.readCertPEM(cert.toString());
                let keyUsage = x509.getExtKeyUsage();
                expect(keyUsage.names).deep.to.equal(['digitalSignature']);
                let extKeyUsage = x509.getExtExtKeyUsage();
                expect(extKeyUsage.array).deep.to.equal(['emailProtection']);

                done();
             });
        });
        it('can request certificate with digitalSignature and id-kp-documentSigning', function(done) {
            this.timeout(120*1000);

            let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                subject: {str: '/E=' + email},
                extreq: [{
                    "extname": "subjectAltName",
                    array: [{ rfc822: email }]
                },
                {
                    "extname": "keyUsage",
                    names: ['digitalSignature']
                },
                {
                    "extname": "extKeyUsage",
                    array: [
                        'emailProtection',
                        '1.3.6.1.5.5.7.3.36' /* id-kp-documentSigning */
                    ]
                }],
                sbjpubkey: key.pubKeyObj,
                sigalg: "SHA256withECDSA",
                sbjprvkey: key.prvKeyObj
            });
            req.sign();
            csr = req.getPEM();

            let identifier = {type: 'email', value: email};
            account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (err, chain) => {
                if (err) throw err;
                certificates.push(chain);

                let cert = chain[0];
                // console.log(`${cert}`);
                expect(cert.ca).to.be.false;
                expect(cert.checkEmail(email)).equal(email);
                expect(cert.keyUsage).to.include('1.3.6.1.5.5.7.3.4');

                let x509 = new jsrsasign.X509();
                x509.readCertPEM(cert.toString());
                let keyUsage = x509.getExtKeyUsage();
                expect(keyUsage.names).deep.to.equal(['digitalSignature']);
                let extKeyUsage = x509.getExtExtKeyUsage();
                expect(extKeyUsage.array).deep.to.equal(['emailProtection', '1.3.6.1.5.5.7.3.36']);

                done();
             });
        });

        it('can request certificate with keyAgreement, only', function(done) {
            this.timeout(120*1000);
            let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                subject: {str: '/E=' + email},
                extreq: [{
                    "extname": "subjectAltName",
                    array: [{ rfc822: email }]
                },
                {
                    "extname": "keyUsage",
                    names: ['keyAgreement', 'keyCertSign']
                }],
                sbjpubkey: key.pubKeyObj,
                sigalg: "SHA256withECDSA",
                sbjprvkey: key.prvKeyObj
            });
            req.sign();
            csr = req.getPEM();

            let identifier = {type: 'email', value: email};
            account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (err, chain) => {
                if (err) throw err;
                certificates.push(chain);

                let cert = chain[0];
                // console.log(`${cert}`);
                expect(cert.ca).to.be.false;
                expect(cert.checkEmail(email)).equal(email);
                expect(cert.keyUsage).to.include('1.3.6.1.5.5.7.3.4');

                let x509 = new jsrsasign.X509();
                x509.readCertPEM(cert.toString());
                let keyUsage = x509.getExtKeyUsage();
                expect(keyUsage.names).deep.to.equal(['keyAgreement']);
                let extKeyUsage = x509.getExtExtKeyUsage();
                expect(extKeyUsage.array).deep.to.equal(['emailProtection']);

                done();
            });
        });
        it('certificate can be revoked', function(done) {
            const chain = certificates.pop();
            const certPem = chain[0].toString();
            // console.log(`certPem: ${certPem}`);

            account.revokeCertificate(certPem, 4, (err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);

                /* Does not support EC key and SHA256
                ocsp.check({
                    cert: chain[0],
                    issuer: chain[1]}, (err, res) => {
                        expect(err).not.to.be.null;
                        expect(err.toString()).to.equal('Error: OCSP Status: revoked');

                        expect(res).not.to.be.null;
                        expect(res.type).to.equal('revoked');

                        expect(res.value).not.to.be.null;
                        expect(res.value.revocationTime).not.to.be.null;
                        expect(res.value.revocationReason).to.equal('superseded');
                        done();
                    }
                ); */

                let cert = new jsrsasign.X509();
                cert.readCertPEM(certPem);

                const issuerPem = chain[1].toString();
                // console.log(`issuerPem: ${issuerPem}`);
                let issuer = new jsrsasign.X509();
                issuer.readCertPEM(issuerPem);

                let request = new jsrsasign.KJUR.asn1.ocsp.OCSPRequest({
                    reqList: [{
                        issuerCert: issuerPem,
                        subjectCert: certPem,
                        alg: 'sha256'
                    }]
                });
                let hex = request.getEncodedHex();
                // console.log(`hex: ${hex}`);

                const aia = cert.getExtAIAInfo();
                const ocspUri = new url.URL(aia.ocsp[0]);
                chai.request.execute(server).post(ocspUri.pathname)
                    .set('Content-Type', 'application/ocsp-request')
                    .send(Buffer.from(hex, 'hex'))
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        res.header['content-type'].should.be.equal('application/ocsp-response');
                        expect(res.body).to.be.an.instanceof(Buffer);
                        // console.log(`OCSPResponse: ${res.body.toString('hex')}`);

                        const parser = new jsrsasign.KJUR.asn1.ocsp.OCSPParser();
                        let resp = parser.getOCSPResponse(res.body.toString('hex'));
                        // console.log(`resp: ${JSON.stringify(resp)}`);
                        expect(resp.array[0].status.status).to.equal('revoked');
                        done();
                });
            });
        });
        it('certificate cannot be revoked twice', function(done) {
            const chain = certificates.pop();
            const certPem = chain[0].toString();
            account.revokeCertificate(certPem, 4, (err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);

                account.revokeCertificate(certPem, 4, (err, res) => {
                    expect(res).to.have.status(400);

                    let acmeError = JSON.parse(res.text);
                    expect(acmeError.type).to.equal('urn:ietf:params:acme:error:alreadyRevoked');

                    done();
                });
            });
        });
        it('certificate cannot be revoked with cACompromise', function(done) {
            const chain = certificates.pop();
            const certPem = chain[0].toString();
            account.revokeCertificate(certPem, 2, (err, res) => {
                expect(res).to.have.status(400);

                let acmeError = JSON.parse(res.text);
                expect(acmeError.type).to.equal('urn:ietf:params:acme:error:badRevocationReason');

                done();
            });
        });
        it('certificate can be revoked without account', function(done) {
            this.timeout(120*1000);
            let identifier = {type: 'email', value: email};
            account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (err, chain) => {
                expect(err).to.be.null;
                expect(chain).not.to.be.null;

                let acmeServer = new acme.Client(new url.URL(`${server}/acme/Sub/directory`));
                acmeServer.getDirectory((err, dir) => {
                    expect(err).to.be.null;
                    expect(dir).not.to.be.null;

                    /* Revoke certificate without account */
                    let pkcs8pem = jsrsasign.KEYUTIL.getPEM(key.prvKeyObj, "PKCS8PRV");
                    let pkcs8b64 = pkcs8pem.replaceAll(/-{5}(BEGIN|END) PRIVATE KEY-{5}[\n\r]?/g, '');
                    let certKey = {};
                    subtle.importKey('pkcs8', encode.b64toab(pkcs8b64), {name: 'ECDSA', namedCurve: 'P-256'}, true, ["sign"])
                          .then((privateKey) => {
                              certKey.privateKey = privateKey;
                              certKey.alg = 'ES256';
                              return subtle.exportKey('jwk', privateKey);
                          })
                          .then((jwk) => {
                              jwk.d = jwk.key_ops = jwk.ext = undefined;
                              certKey.jwk = jwk;
                              return acmeServer.revokeCert(certKey, chain[0], 1, (err) => {
                                  expect(err).to.be.null;

                                  const certPem = chain[0].toString();
                                  // console.log(`certPem: ${certPem}`);
                                  let cert = new jsrsasign.X509();
                                  cert.readCertPEM(certPem);

                                  const issuerPem = chain[1].toString();
                                  // console.log(`issuerPem: ${issuerPem}`);
                                  let issuer = new jsrsasign.X509();
                                  issuer.readCertPEM(issuerPem);

                                  let request = new jsrsasign.KJUR.asn1.ocsp.OCSPRequest({
                                      reqList: [{
                                          issuerCert: issuerPem,
                                          subjectCert: certPem,
                                          alg: 'sha256'
                                      }]
                                  });
                                  let hex = request.getEncodedHex();
                                  // console.log(`hex: ${hex}`);

                                  const aia = cert.getExtAIAInfo();
                                  const ocspUri = new url.URL(aia.ocsp[0]);
                                  chai.request.execute(server).post(ocspUri.pathname)
                                      .set('Content-Type', 'application/ocsp-request')
                                      .send(Buffer.from(hex, 'hex'))
                                      .buffer()
                                      .parse(helpers.binaryParser)
                                      .end(function(err, res) {
                                          expect(err).to.be.null;
                                          expect(res).to.have.status(200);
                                          res.header['content-type'].should.be.equal('application/ocsp-response');
                                          expect(res.body).to.be.an.instanceof(Buffer);
                                          // console.log(`OCSPResponse: ${res.body.toString('hex')}`);

                                          const parser = new jsrsasign.KJUR.asn1.ocsp.OCSPParser();
                                          let resp = parser.getOCSPResponse(res.body.toString('hex'));
                                          // console.log(`resp: ${JSON.stringify(resp)}`);
                                          expect(resp.array[0].status.status).to.equal('revoked');
                                          done();
                                      });
                              });
                          })
                          .catch((error) => {
                              done();
                              throw error;
                          });
                });
            });
        });
    });
    describe('challenge message', function() {
        let eml, parsed;
        let account;
        function keep(message) {
            eml = message;
        }
        before(async function() {
            this.timeout(300*1000);
            let imap = setupImap();
            let responder = new acme.EmailChallengeResponder(imap, smtp, {"verification": false});
            responder.messageHandler = keep;
            account = await acme.Account.create(`${server}/acme/Sub/directory`, [responder]);

            let key = jsrsasign.KEYUTIL.generateKeypair("EC", "secp256r1");

            let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                subject: {str: '/E=' + email},
                extreq: [{
                    "extname": "subjectAltName",
                    array: [{ rfc822: email }]
                }],
                sbjpubkey: key.pubKeyObj,
                sigalg: "SHA256withECDSA",
                sbjprvkey: key.prvKeyObj
            });
            req.sign();
            let csr = req.getPEM();

            eml = undefined;
            parsed = undefined;

            let identifier = {type: 'email', value: email};
            await new Promise((resolve, reject) => {
                account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (err, chain) => {
                    if (err)
                        reject(err);
                    else
                        resolve(chain);
                });
            })
            .then((/* chain */) => {
                console.log(`got certificate chain`);
                return true;
            });
        });

        function get_signed_headers(signedAttrs) {
            let signedHeaders = signedAttrs?.set?.find(attr => (attr.seq[0].oid === '1.2.840.113549.1.9.16.2.55'));
            return signedHeaders.seq[1].set[0].set[1].seq;
        }
        it('can be parsed', function() {
            return simpleParser(eml, {})
                .then((msg) => {
                    parsed = msg;
                    return true;
                });
        });
        it('has an S/MIME signature', function() {
            let signature = parsed.attachments?.find(attachment => attachment.contentType == 'application/pkcs7-signature');
            expect(signature).not.to.be.undefined;
        });
        describe('S/MIME signature', function() {
            let signature;
            let signedHeaders;
            before(function () {
                signature = parsed.attachments?.find(attachment => attachment.contentType == 'application/pkcs7-signature');
            });
            it('can be parsed', function() {
                let hex = signature.content.toString('hex');
                let cms = jsrsasign.KJUR.asn1.cms.CMSUtil.verifySignedData({ cms: hex });
                expect(cms).not.to.be.undefined;
            });
            it('is valid', function() {
                let hex = signature.content.toString('hex');
                let cms = jsrsasign.KJUR.asn1.cms.CMSUtil.verifySignedData({ cms: hex });
                expect(cms).not.to.be.undefined;
                expect(cms.isValid).to.be.true;
            });
            it('has signed attributes', function() {
                let hex = signature.content.toString('hex');
                let cms = jsrsasign.KJUR.asn1.cms.CMSUtil.verifySignedData({ cms: hex });
                expect(cms).not.to.be.undefined;
                expect(cms.isValid).to.be.true;
            });
            it('has valid signerInfos', function() {
                let hex = signature.content.toString('hex');
                let cms = jsrsasign.KJUR.asn1.cms.CMSUtil.verifySignedData({ cms: hex });
                expect(cms).not.to.be.undefined;
                expect(cms.isValid).to.be.true;

                expect(cms).to.have.property('parse');
                expect(cms.parse).to.have.property('signerInfos');
                for (let signerInfo of cms.parse.signerInfos) {
                    expect(signerInfo).to.have.property('verifyDetail');

                    expect(signerInfo.verifyDetail).to.have.property('validMessageDigest');
                    expect(signerInfo.verifyDetail.validMessageDigest).to.be.true;

                    expect(signerInfo.verifyDetail).to.have.property('validSignatureValue');
                    expect(signerInfo.verifyDetail.validSignatureValue).to.be.true;

                    expect(signerInfo.verifyDetail).to.have.property('validSignatureValue_isValid');
                    expect(signerInfo.verifyDetail.validSignatureValue_isValid).to.be.true;
                }
            });
            it('has signed headers', function() {
                let hex = signature.content.toString('hex');
                let cms = jsrsasign.KJUR.asn1.cms.CMSUtil.verifySignedData({ cms: hex });
                expect(cms).not.to.be.undefined;

                expect(cms).to.have.property('parse');
                expect(cms.parse).to.have.property('signerInfos');
                for (let signerInfo of cms.parse.signerInfos) {
                    //console.log(`Signer: ${cms.certs[signerInfo.certkey_idx].getSubjectString()}`);
                    let signedAttrs = jsrsasign.ASN1HEX.parse(signerInfo.signedattrshex);
                    signedHeaders = get_signed_headers(signedAttrs);
                    expect(signedHeaders).not.to.be.undefined;
                }
            });
            describe('signed headers', function() {
                it('have matching from', function() {
                    let sFrom = signedHeaders.find(header => header.seq[0].visstr.str.toLowerCase() === 'from');
                    expect(sFrom.seq[1].utf8str.str).to.be.equal(parsed.from.text);
                });
                it('have matching to', function() {
                    let sTo = signedHeaders.find(header => header.seq[0].visstr.str.toLowerCase() === 'to');
                    expect(sTo.seq[1].utf8str.str).to.be.equal(parsed.to.text);
                });
                it('have subject', function() {
                    let sSubject = signedHeaders.find(header => header.seq[0].visstr.str.toLowerCase() === 'subject');
                    expect(sSubject).not.to.be.undefined;
                });
            });
        });
    });
    describe('CAA validation', function() {
        let csr;
        let account;
        let responder;
        before(dnsserver.start);
        before(function() {
            let key = jsrsasign.KEYUTIL.generateKeypair("EC", "secp256r1");
            let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                subject: {str: '/E=' + email},
                extreq: [{
                    "extname": "subjectAltName",
                    array: [{ rfc822: email }]
                }],
                sbjpubkey: key.pubKeyObj,
                sigalg: "SHA256withECDSA",
                sbjprvkey: key.prvKeyObj
            });
            req.sign();
            csr = req.getPEM();
        });
        beforeEach(async function() {
           try {
              let imap = setupImap();
              responder = new acme.EmailChallengeResponder(imap, smtp, {"verification": false});
              account = await acme.Account.create(`${server}/acme/Sub/directory`, [responder]);
              return account;
           }
           catch (error) {
              console.error(`setup account: ${error} ${error.stack}`);
              throw error;
           }
        });
        afterEach(function() {
            responder.stop();
        });
        after(dnsserver.stop);
        after(function() {
            let imap = setupImap();
            imap.once('ready', function() {
                imap.openBox('INBOX', false, function(err /* box */) {
                    if (err) throw err;
                    imap.search(['UNSEEN', ['SUBJECT', 'ACME: ']], (err, results) => {
                        if (results.length > 0) {
                            let f = imap.fetch(results, {
                                bodies: ['HEADER.FIELDS (FROM TO)', 'TEXT'],
                                markSeen: true
                            });
                            f.on('end', function() { imap.end(); });
                        }
                        else {
                            imap.end();
                        }
                    });
                });
            });
            imap.connect();
        });
        describe('RFC 9495 compliance (issuemail)', function() {
            const domain = new url.URL(`http://${email}`).hostname;
            const identifier = {'type': 'email', 'value': email};
            const testcases = [
                {name: 'other.ca',  records: [{'domain': domain, 'flags':   0, 'tag': 'issuemail', 'value': 'other.ca'}]},
                {name: 'uppercase', records: [{'domain': domain, 'flags':   0, 'tag': 'Issuemail', 'value': 'other.ca'}]},
                {name: 'critical',  records: [{'domain': domain, 'flags': 128, 'tag': 'unknown',   'value': 'other.ca'}]},
                {name: 'emtpy',     records: [{'domain': domain, 'flags':   0, 'tag': 'issuemail', 'value': ';'}]},
                {name: 'invalid',   records: [{'domain': domain, 'flags':   0, 'tag': 'issuemail', 'value': '%%%%%%'}]}
            ];
            for (let testcase of testcases) {
                it(`prohibits authorization with CAA '${testcase.name}'`, function(done) {
                    this.timeout(120*1000);
                    if (!NodeRED.started)
                        this.skip();

                    for (let entry of testcase.records) {
                        dnsserver.config.records = dnsserver.config.records.filter(
                            element => element.name != entry.domain);
                        dnsserver.config.records.push({
                            name: entry.domain,
                            type: 0x101 /* CAA */,
                            flags: entry.flags,
                            tag: entry.tag,
                            value: entry.value
                        });
                    }
                    account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (err, chain) => {
                        expect(err).not.to.be.null;
                        expect(err).to.have.property('type');
                        expect(err.type).to.be.equal('urn:ietf:params:acme:error:caa');
                        done();
                    });
               });
            }
        });
    });
    describe('account', function() {
        describe('recovery', function() {
            let existing;
            before(async function() {
                existing = await acme.Account.create(`${server}/acme/Sub/directory`, []);
            });
            it('is functional', function(done) {
                let acmeServer = new acme.Client(new url.URL(`${server}/acme/Sub/directory`));
                acmeServer.getDirectory((err, dir) => {
                    expect(err).to.be.null;
                    expect(dir).not.to.be.null;

                    acmeServer.recoverAccount(existing.key, (err, account) => {
                        expect(err).to.be.null;
                        expect(account).not.to.be.null;
                        expect(account.kid.toString()).to.equal(existing.kid.toString());
                        done();
                    });
                });
            });
            it('fails with unknown account key', function(done) {
                let acmeServer = new acme.Client(`${server}/acme/Sub/directory`);
                let key = crypto.generateKeyPairSync('ec', {namedCurve: 'prime256v1'});
                key.alg = 'ES256';
                key.pkcs8 = key.privateKey.export({type: 'pkcs8', format: 'pem'});
                key.jwk = key.publicKey.export({type: 'pkcs1', format: 'jwk'});

                acmeServer.getDirectory((err, dir) => {
                    expect(err).to.be.null;
                    expect(dir).not.to.be.null;

                    acmeServer.recoverAccount(key, (err, account) => {
                        expect(err).not.to.be.null;
                        expect(account).to.be.undefined;
                        done();
                    });
                });
            });
        });
    });
    describe('emitted certificates', function() {
        let key;
        let certificates = [];

        let cert;
        let param;
        let caCert;

        before(function(done) {
            this.timeout(120*1000);
            let imap = setupImap();
            let responder = new acme.EmailChallengeResponder(imap, smtp, {"verification": false});
            acme.Account.create(`${server}/acme/Sub/directory`, [responder])
                .then((account) => {
                    key = jsrsasign.KEYUTIL.generateKeypair("RSA", 2048);

                    let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                        subject: {str: '/E=' + email},
                        extreq: [{
                            "extname": "subjectAltName",
                            array: [{ rfc822: email }]
                        }],
                        sbjpubkey: key.pubKeyObj,
                        sigalg: "SHA256withRSA",
                        sbjprvkey: key.prvKeyObj
                    });
                    req.sign();
                    let csr = req.getPEM();

                    let identifier = {type: 'email', value: email};

                    return account.getCertificate([identifier], undefined, undefined, csr, undefined, undefined, (err, chain) => {
                        if (err) throw err;
                        // console.log(`chain: ${inspect(chain)}`);
                        certificates = chain;

                        cert = new jsrsasign.X509();
                        cert.readCertPEM(chain[0].toString());

                        param = cert.getParam();
                        // console.log(`param: ${inspect(param)}`);

                        caCert = new jsrsasign.X509();
                        caCert.readCertPEM(chain[1].toString());

                        done();
                        return chain;
                    });
                })
                .catch((error) => {
                    done();
                    throw error;
                });
        });
        describe('Apple Root Certificate Program (1st Oct. 2022)', function() {
            describe('Policy Requirements', function() {
                /* Note: For effective dates related to certificate issuance,
                the requirement is enforced for certificates issued on or after
                the specified date at 00:00:00 UTC.

                Effective April 1, 2022, CA providers must disclose in the
                CCADB all CA certificates which chain up to their CA
                Certificate(s) included in the Apple Root Program.

                Effective April 1, 2022, S/MIME certificates must:
                 - include the emailProtection EKU
                 - include at least one subjectAlternativeName rFC822Name
                   value containing an email address
                 - not have a validity period greater than 1185 days
                 - use a signature hash algorithm of greater than or equal
                   strength to SHA-256 (see section 7.1.3.1 and 7.1.3.2 of the
                   CA/B Forum’s Baseline Requirements).
                 - meet the following key size requirements:
                   - For RSA key pairs, the modulus size must be at least 2048
                     bits when encoded and its size in bits must be evenly
                     divisible by 8.
                   - For ECDSA key pairs, the key must represent a valid point
                     on the NIST P‐256, NIST P‐384 or NIST P‐521 named elliptic
                     curve. */
                it('include the emailProtection EKU', function() {
                    let ekus = cert.getExtExtKeyUsage();
                    expect(ekus).not.to.be.undefined;
                    expect(ekus.array).to.be.an('array');
                    expect(ekus.array).to.have.members(['emailProtection']);
                });
                it('include at least one SAN rfc822Name value containing an email address', function() {
                    let sans = cert.getExtSubjectAltName();
                    expect(sans).not.to.be.undefined;
                    expect(sans.array).to.be.an('array');
                    let found = false;
                    for (let san of sans.array) {
                        if (san.rfc822) {
                            found = true;
                        }
                    }
                    expect(found).to.be.true;
                });
                it('does not have a validity period greater than 1185 days', function() {
                    let notBefore = cert.getNotBefore();
                    notBefore = jsrsasign.zulutodate(notBefore);
                    let notAfter = cert.getNotAfter();
                    notAfter = jsrsasign.zulutodate(notAfter);

                    let duration = 1185 * 24 * 60 * 60 * 1000;
                    expect(notAfter.getTime() - notBefore.getTime()).to.be.below(duration);
                });
                it('use a signature hash algorithm of greater than or equal strength to SHA-256', function() {
                    expect(param.sigalg).not.to.match(/^MD5/);
                    expect(param.sigalg).not.to.match(/^SHA1/);
                });
                describe('key requirements', function() {
                    it('RSA modulus must be at least 2048 bits', function() {
                        let key = cert.getPublicKey();
                        if (key.type === 'EC')
                            this.skip();
                        expect(key.n.bitLength()).to.be.above(2047);
                    });
                    it('RSA modulus size is evenly divisible by 8', function() {
                        let key = cert.getPublicKey();
                        if (key.type === 'EC')
                            this.skip();
                        expect(key.n.bitLength() % 8).to.be.equal(0);
                    });
                    it('ECDSA curve is one of P-256, P-384, P-521', function() {
                        let key = cert.getPublicKey();
                        if (key.type !== 'EC')
                            this.skip();
                        expect(key.curveName).oneOf(['secp256r1', 'secp384r1', 'secp521r1']);
                    });
                });
                /* Effective October 1, 2022, CA providers must populate the
                CCADB fields under "Pertaining to Certificates Issued by This
                CA" with either the CRL Distribution Point for the "Full CRL
                Issued By This CA" or a "JSON Array of Partitioned CRLs" on
                Root and Intermediate Certificate records, within 7 days of
                the corresponding CA issuing its first certificate. This
                requirement applies to each included CA Certificate and each
                CA Certificate chaining up to an included CA Certificate in
                the Apple Root Program.

                Under normal operating conditions, the CRL URLs provided by
                CAs in this section must be available such that Apple systems
                are able to successfully retrieve the current CRL every 4
                hours. */
            });
        });
        describe('Adobe Approved Trust List (Version 2.0)', function() {
            describe('Requirements for End-entity certificates', function() {
                it('EE1: is an X.509v3 certificate', function() {
                    let param = cert.getParam();
                    expect(param.version).to.be.equal(3);
                });
                it('EE2: has known keyUsage and extendedKeyUsage extensions', function() {
                    let ekus = cert.getExtExtKeyUsage();
                    expect(ekus).not.to.be.undefined;
                    expect(ekus.array).to.be.an('array');
                    expect(ekus.array).to.be.containingAnyOf([
                        'emailProtection',
                        'codeSigning',
                        'anyExtendedKeyUsage',
                        '1.2.840.113583.1.1.5' // Adobe Authentic Documents Trust
                    ]);
                    let kus = cert.getExtKeyUsage();
                    expect(kus).not.to.be.undefined;
                    expect(kus.names).to.be.an('array');
                    expect(kus.names).to.have.containingAnyOf([
                        'nonRepudiation',
                        'signTransaction',
                        'digitalSignature'
                    ]);
                });
                it('EE3: custom OIDs', function() {
                    this.skip();
                });
                it('EE4: end-entity key pair', function() {
                    let key = cert.getPublicKey();
                    if (key.type === 'EC') {
                        expect(key.curveName).oneOf(['secp256r1', 'secp384r1', 'secp521r1']);
                    }
                    else /* RSA */ {
                        expect(key.n.bitLength()).to.be.above(2047);
                    }
                    let param = cert.getParam();
                    expect(param.sigalg).not.to.be.undefined;
                    expect([
                        'SHA256withECDSA', 'SHA256withRSA', 'SHA256withRSAandMGF1',
                        'SHA384withECDSA', 'SHA384withRSA', 'SHA384withRSAandMGF1',
                        'SHA512withECDSA', 'SHA512withRSA', 'SHA512withRSAandMGF1'
                    ]).to.include(param.sigalg);
                });
                it('EE5: can sign pdf files', async function() {
                    const p8 = jsrsasign.KEYUTIL.getPEM(key.prvKeyObj, 'PKCS8PRV');
                    // console.log(`p8: ${p8}`);
                    let forgeKey = forge.pki.privateKeyFromPem(p8);
                    let forgeCert = forge.pki.certificateFromPem(certificates[0]);
                    // node-forge does not support ECDSA
                    // let forgeCaCert = forge.pki.certificateFromPem(certificates[1]);
                    let p12 = forge.pkcs12.toPkcs12Asn1(forgeKey, [forgeCert /*, forgeCaCert */], 'password',
                        {generateLocalKeyId: true, friendlyName: 'signer', algorithm: '3des'}
                    );
                    let der = forge.asn1.toDer(p12).getBytes();
                    fs.writeFileSync('cps.signer.pfx', der, {encoding: 'binary'});
                    let p12buf = fs.readFileSync('cps.signer.pfx');
                    const signer = new P12Signer(p12buf, {passphrase: 'password'});
                    let pdf = fs.readFileSync('test/data/cps.pdf');
                    let inputBuffer = plainAddPlaceholder({
                        pdf: pdf,
                        pdfBuffer: pdf,
                        reason: 'Signed for EE5',
                        name: 'Sign PDF',
                        contactInfo: email,
                        location: 'Oakland',
                    });
                    return new SignPdf()
                        .sign(inputBuffer, signer)
                        .then(function(signedPdf) {
                            fs.writeFileSync('cps.signed.pdf', signedPdf, 'binary');
                            return signedPdf;
                        });
                });
            });
        });
        describe('Mozilla Root Store Policy (2.8)', function() {
            describe('5.2 Forbidden and Required Practices', function() {
                /* CA operations MUST at all times be in accordance with the
                applicable CP and CPS (or combined CP/CPS).

                CA operators MUST maintain a certificate hierarchy such that an
                included root certificate does not directly issue end entity
                certificates to customers (i.e. a root certificate signs
                intermediate issuing certificates), as described in section
                6.1.7 of the Baseline Requirements. */

                /* CA operators MUST maintain current best practices to prevent
                algorithm attacks against certificates. As such, all new
                certificates MUST have a serial number greater than zero,
                containing at least 64 bits of output from a CSPRNG. */

                it('serial number is greater than zero', function() {
                    let sn = BigInt(`0x${cert.getSerialNumberHex()}`);
                    expect(sn > 0n).to.be.true;
                });
                it('serial number contains at least 64 bits of output from CSPRNG', function() {
                    let sn = cert.getSerialNumberHex();
                    expect(sn).to.have.length.greaterThan(16);
                });

                /* CA operators MUST NOT issue certificates, CRLs, or OCSP
                responses, that have:
                 - ASN.1 DER encoding errors;
                 - invalid public keys (e.g., RSA certificates with public
                   exponent equal to 1); or
                 - missing or incorrect extensions (e.g., TLS certificates with
                   no subjectAltName extension, delegated OCSP responders
                   without the id-pkix-ocsp-nocheck extension, partial/scoped
                   CRLs that lack a distributionPoint in a critical
                   issuingDistributionPoint extension). */

                it('certificate does not have invalid public key', function() {
                    let key = cert.getPublicKey();
                    if (key.type === 'EC')
                        this.skip();
                    expect(key.e).not.to.be.equal(1);
                });

                /* CA operators MUST NOT issue certificates that have:
                 - duplicate issuer names and serial numbers (except that a
                   Certificate Transparency pre-certificate is allowed to match
                   the corresponding certificate); or
                 - cRLDistributionPoints or OCSP authorityInfoAccess extensions
                   for which no operational CRL or OCSP service exists.

                CA operators MUST NOT generate the key pairs for end entity
                certificates that have an EKU extension containing the
                KeyPurposeIds id-kp-serverAuth or anyExtendedKeyUsage, unless
                the certificate is being issued to the CA itself.

                Effective for certificates with a notBefore date of July 1,
                2020 or later, end entity certificates MUST include an EKU
                extension containing KeyPurposeId(s) describing the intended
                usage(s) of the certificate, and the EKU extension MUST NOT
                contain the KeyPurposeId anyExtendedKeyUsage. */

                it('certificate includes an EKU extension', function() {
                    let ekus = cert.getExtExtKeyUsage();
                    expect(ekus).not.to.be.undefined;
                    expect(ekus.array).to.be.an('array');
                });
                it('EKU does not contain anyExtendedKeyUsage', function() {
                    let ekus = cert.getExtExtKeyUsage();
                    expect(ekus.array).not.to.have.members(['anyExtendedKeyUsage']);
                });
            });
        });
    });
});
