import java.net.URI;
import java.net.URL;
import java.security.KeyPair;
import java.security.Security;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.io.InputStream;
import java.io.FileInputStream;

import java.io.FileWriter;
import java.io.File;
import java.security.cert.X509Certificate;
import java.security.cert.CertificateFactory;

import jakarta.mail.Store;
import jakarta.mail.Folder;
import jakarta.mail.Message;
import jakarta.mail.Transport;

import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.shredzone.acme4j.challenge.Challenge;
import org.shredzone.acme4j.exception.AcmeException;
import org.shredzone.acme4j.util.KeyPairUtils;
import org.shredzone.acme4j.Session;
import org.shredzone.acme4j.Metadata;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.AccountBuilder;
import org.shredzone.acme4j.Order;
import org.shredzone.acme4j.Status;
import org.shredzone.acme4j.Authorization;
import org.shredzone.acme4j.Certificate;
import org.shredzone.acme4j.RevocationReason;
import org.shredzone.acme4j.smime.challenge.EmailReply00Challenge;
import org.shredzone.acme4j.smime.EmailIdentifier;
import org.shredzone.acme4j.smime.email.EmailProcessor;
import org.shredzone.acme4j.smime.csr.SMIMECSRBuilder;
// import org.shredzone.acme4j.smime.Challenge;

public class SmimeTest {
    Session session = null;
    Metadata meta = null;
    KeyPair accountKeyPair = null;
    Account account = null;
    Order order = null;
    Certificate cert = null;

    @BeforeClass
    public void setup() {

        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    public void createSession() throws Exception {
        String serverUrl = System.getProperty("serverUrl");
        assertNotNull(serverUrl);
        session = new Session(serverUrl);
        assertNotNull(session);
    }

    @Test(dependsOnMethods="createSession")
    public void providesMetadata() throws Exception {
        meta = session.getMetadata();
        assertNotNull(meta);
    }

    @Test(dependsOnMethods="createSession")
    public void registerAccount() throws Exception {
        accountKeyPair = KeyPairUtils.createECKeyPair("secp384r1");
        assertNotNull(accountKeyPair);

        account = new AccountBuilder()
                          .addContact("mailto:acme@my.corp")
                          .agreeToTermsOfService()
                          .useKeyPair(accountKeyPair)
                          .create(session);
        assertNotNull(account);
    }

    @Test(dependsOnMethods="registerAccount")
    public void newSmimeOrder() throws Exception {
        String email = System.getProperty("subjectAltNameEmail");
        order = account.newOrder()
                       .identifier(EmailIdentifier.email(email))
.autoRenewal()
                       .create();
        assertNotNull(order);
    }

    @Test(dependsOnMethods="newSmimeOrder")
    public void processAuthorizations() throws Exception {
        for (Authorization auth : order.getAuthorizations()) {
            Thread.sleep(10 * 1000);
            if (auth.getStatus() != Status.VALID) {

                Properties props = System.getProperties();
                jakarta.mail.Session mailSession = jakarta.mail.Session.getDefaultInstance(props, null);
                assertNotNull(mailSession);

                Store store = mailSession.getStore("imap");
                store.connect(System.getProperty("mail.user"),
                              System.getProperty("mailPassword"));
                Folder inbox = store.getFolder("INBOX");
                inbox.open(Folder.READ_WRITE);

                Message[] messages = inbox.getMessages();
                assertTrue(messages.length > 0);

                Message challengeMessage = messages[0];
                assertNotNull(challengeMessage);

                EmailReply00Challenge challenge = auth
                    .findChallenge(EmailReply00Challenge.class)
                    .orElseThrow(() -> new AcmeException("Found no " + EmailReply00Challenge.TYPE + " challenge, don't know what to do ..."));

                String email = System.getProperty("subjectAltNameEmail");
                EmailIdentifier identifier = EmailIdentifier.email(email);
                assertNotNull(identifier);

                /* @todo: Verify that message is signed (S/MIME or DKIM) here */
                Message response = null;
                try {
                    response = EmailProcessor
                                   .signedMessage(challengeMessage)
                                   .expectedIdentifier(identifier)
                                   .withChallenge(challenge)
                                   .respond()
                                   .generateResponse(mailSession);
                }
                catch (Exception e) {
                    System.out.println("Response verification failed: " + e.toString());

                    response = EmailProcessor.plainMessage(challengeMessage)
                                   .expectedIdentifier(identifier)
                                   .withChallenge(challenge)
                                   .respond()
                                   .generateResponse(mailSession);
                }
                Transport.send(response);

                Thread.sleep(10 * 1000);
                challenge.trigger();

                for (int tries=0; auth.getStatus() != Status.VALID; tries++) {
                    assertTrue(tries < 200);
                    auth.update();
                    Thread.sleep(10 * 1000);
                }
            }
        }
    }

    @Test(dependsOnMethods="processAuthorizations")
    public void finalizeOrder() throws Exception {
        String email = System.getProperty("subjectAltNameEmail");
        assertNotNull(email);

        KeyPair emailKeyPair = KeyPairUtils.createECKeyPair("secp521r1");

        SMIMECSRBuilder scsrb = new SMIMECSRBuilder();
        scsrb.addEmail(new InternetAddress(email));
        scsrb.sign(emailKeyPair);
        try (FileWriter fw = new FileWriter("SmimeTest.csr")) {
            scsrb.write(fw);
        }

        byte csr [] = scsrb.getEncoded();
        order.execute(csr);

cert = order.getAutoRenewalCertificate();
// cert = order.getCertificate();
        assertNotNull(cert);
        X509Certificate x509 = cert.getCertificate();
        try (FileWriter fw = new FileWriter("SmimeTest.crt")) {
            cert.writeCertificate(fw);
        }
    }
}
