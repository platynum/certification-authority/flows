import java.net.URI;
import java.net.URL;
import java.security.KeyPair;
import java.security.Security;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.io.InputStream;
import java.io.FileInputStream;

import java.io.FileWriter;
import java.io.File;
import java.security.cert.X509Certificate;
import java.security.cert.CertificateFactory;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.shredzone.acme4j.challenge.Challenge;
import org.shredzone.acme4j.exception.AcmeException;
import org.shredzone.acme4j.util.KeyPairUtils;
import org.shredzone.acme4j.Session;
import org.shredzone.acme4j.Metadata;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.AccountBuilder;
import org.shredzone.acme4j.Authorization;
import org.shredzone.acme4j.Identifier;
// import org.shredzone.acme4j.smime.Challenge;

public class PreAuthorizationTest {
    Session session = null;
    Metadata meta = null;
    KeyPair accountKeyPair = null;
    Account account = null;

    @BeforeClass
    public void setup() {

        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    public void createSession() throws Exception {
        String serverUrl = System.getProperty("serverUrl");
        assertNotNull(serverUrl);
        session = new Session(serverUrl);
        assertNotNull(session);
    }

    @Test(dependsOnMethods="createSession")
    public void providesMetadata() throws Exception {
        meta = session.getMetadata();
        assertNotNull(meta);
    }

    @Test(dependsOnMethods="providesMetadata")
    public void registerAccount() throws Exception {
        accountKeyPair = KeyPairUtils.createECKeyPair("secp384r1");
        assertNotNull(accountKeyPair);

        account = new AccountBuilder()
                          .addContact("mailto:acme@my.corp")
                          .agreeToTermsOfService()
                          .useKeyPair(accountKeyPair)
                          .create(session);
        assertNotNull(account);
    }

    @Test(dependsOnMethods="registerAccount")
    public void newPreAuthorization() throws Exception {
        String domain = System.getProperty("preAuthorization");
        Authorization auth = account.preAuthorizeDomain(domain);
        assertNotNull(auth);
        assertFalse(auth.isSubdomainAuthAllowed());
    }

    @Test(dependsOnMethods="registerAccount")
    public void newPreAuthorizationForSubDomains() throws Exception {
        String domain = System.getProperty("preAuthorization");
        Identifier identifier = new Identifier("dns", domain);
        identifier = identifier.allowSubdomainAuth();
        Authorization auth = account.preAuthorize(identifier);
        assertNotNull(auth);
        assertTrue(auth.isSubdomainAuthAllowed());
    }
}
