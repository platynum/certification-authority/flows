import java.net.URI;
import java.net.URL;
import java.security.KeyPair;
import java.security.Security;
import java.time.Duration;
import java.util.Collection;
import java.util.List;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.shredzone.acme4j.challenge.Challenge;
import org.shredzone.acme4j.challenge.Dns01Challenge;
import org.shredzone.acme4j.challenge.Http01Challenge;
import org.shredzone.acme4j.exception.AcmeException;
import org.shredzone.acme4j.util.CSRBuilder;
import org.shredzone.acme4j.util.KeyPairUtils;
import org.shredzone.acme4j.Session;
import org.shredzone.acme4j.Metadata;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.AccountBuilder;

public class AccountTest {
    Session session = null;
    Metadata meta = null;
    KeyPair accountKeyPair = null;
    Account account = null;
    Account accountEC = null;

    @BeforeClass
    public void setup() {

        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    public void createSession() throws Exception {
        String serverUrl = System.getProperty("serverUrl");
        assertNotNull(serverUrl);
        session = new Session(serverUrl);
        assertNotNull(session);

        accountKeyPair = KeyPairUtils.createKeyPair(2048);
        assertNotNull(accountKeyPair);
    }

    @Test(dependsOnMethods="createSession")
    public void providesMetadata() throws Exception {
        meta = session.getMetadata();
        assertNotNull(meta);
    }
    @Test(dependsOnMethods="providesMetadata")
    public void providesTermsOfService() throws Exception {
        URI tos = meta
            .getTermsOfService()
            .orElseThrow(() -> new AcmeException("Missing terms of service"));
        assertNotNull(tos);
    }
    @Test(dependsOnMethods="providesMetadata")
    public void providesWebsite() throws Exception {
        URL website = meta
            .getWebsite()
            .orElseThrow(() -> new AcmeException("Missing website"));
        assertNotNull(website);
    }
    @Test(dependsOnMethods="providesMetadata")
    public void providesAcmeStar() throws Exception {
        assertTrue(meta.isAutoRenewalEnabled());
    }
    @Test(dependsOnMethods="providesMetadata")
    public void requiresNoExternalAccount() throws Exception {
        assertFalse(meta.isExternalAccountRequired());
    }
    @Test(dependsOnMethods="providesMetadata")
    public void announcesCAAIdentity() throws Exception {
        Collection caaIdentities = meta.getCaaIdentities();
        assertNotNull(caaIdentities);
        assertTrue(caaIdentities.size() > 0);
    }

    @Test(dependsOnMethods="createSession")
    public void registerAccountRSA() throws Exception {
        account = new AccountBuilder()
                          .addContact("mailto:acme@my.corp")
                          .agreeToTermsOfService()
                          .useKeyPair(accountKeyPair)
                          .create(session);
        assertNotNull(account);
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    @Test
    public void registerAccountEC() throws Exception {
        String serverUrl = System.getProperty("serverUrl");
        assertNotNull(serverUrl);

        //KeyPair accountKeyPair = KeyPairUtils.createECKeyPair("secp256r1");
        KeyPair accountKeyPair = KeyPairUtils.createECKeyPair("secp384r1");
        //KeyPair accountKeyPair = KeyPairUtils.createECKeyPair("secp521r1");
        assertNotNull(accountKeyPair);
        System.out.println(bytesToHex(accountKeyPair.getPrivate().getEncoded()));

        Session session = new Session(serverUrl);
        assertNotNull(session);

        accountEC = new AccountBuilder()
                          .addContact("mailto:p384@my.corp")
                          .agreeToTermsOfService()
                          .useKeyPair(accountKeyPair)
                          .create(session);
        assertNotNull(accountEC);
    }
    @Test
    public void registerAccountECwithP521() throws Exception {
        String serverUrl = System.getProperty("serverUrl");
        assertNotNull(serverUrl);

        KeyPair accountKeyPair = KeyPairUtils.createECKeyPair("secp521r1");
        assertNotNull(accountKeyPair);
        System.out.println(bytesToHex(accountKeyPair.getPrivate().getEncoded()));

        Session session = new Session(serverUrl);
        assertNotNull(session);

        Account accountP521 = new AccountBuilder()
                                   .addContact("mailto:p521@my.corp")
                                   .agreeToTermsOfService()
                                   .useKeyPair(accountKeyPair)
                                   .create(session);
        assertNotNull(accountP521);
    }
    @Test(dependsOnMethods="registerAccountEC")
    public void modifyAccount() throws Exception {
        accountEC.modify()
                 .addContact("mailto:other@my.corp")
                 .commit();
    }
    @Test(dependsOnMethods="modifyAccount")
    public void getContacts() throws Exception {
        List<URI> contacts = accountEC.getContacts();
        assertNotNull(contacts);
        assertEquals(contacts.size(), 2);
    }

    @Test(dependsOnMethods="registerAccountRSA")
    public void providesAccountURL() throws Exception {
        URL location = account.getLocation();
        assertNotNull(location);
    }
    @Test(dependsOnMethods="registerAccountRSA")
    public void findAccountURL() throws Exception {
        URL location = account.getLocation();
        assertNotNull(location);
    }
    @Test(dependsOnMethods="registerAccountRSA", expectedExceptions=AcmeException.class)
    public void rejectsKeyChange() throws Exception {
        KeyPair newKeyPair = KeyPairUtils.createKeyPair(2048);
        assertNotNull(newKeyPair);

        account.changeKey(newKeyPair);
    }
    @Test(dependsOnMethods="registerAccountRSA",priority=2)
    public void deactivate() throws Exception {
        account.deactivate();
    }
}
