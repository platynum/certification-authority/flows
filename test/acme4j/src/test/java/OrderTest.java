import java.net.URI;
import java.net.URL;
import java.security.KeyPair;
import java.security.Security;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.io.FileWriter;
import java.io.File;
import java.security.cert.X509Certificate;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.shredzone.acme4j.challenge.Challenge;
import org.shredzone.acme4j.challenge.Dns01Challenge;
import org.shredzone.acme4j.challenge.Http01Challenge;
import org.shredzone.acme4j.exception.AcmeException;
import org.shredzone.acme4j.util.CSRBuilder;
import org.shredzone.acme4j.util.KeyPairUtils;
import org.shredzone.acme4j.Session;
import org.shredzone.acme4j.Metadata;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.AccountBuilder;
import org.shredzone.acme4j.Order;
import org.shredzone.acme4j.Identifier;
import org.shredzone.acme4j.Status;
import org.shredzone.acme4j.Authorization;
import org.shredzone.acme4j.Certificate;
import org.shredzone.acme4j.RevocationReason;

public class OrderTest {
    Session session = null;
    Metadata meta = null;
    KeyPair accountKeyPair = null;
    Account account = null;
    Order order = null;
    Certificate cert = null;

    @BeforeClass
    public void setup() throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        String serverUrl = System.getProperty("serverUrl");
        assertNotNull(serverUrl);
        session = new Session(serverUrl);
        accountKeyPair = KeyPairUtils.createKeyPair(2048);
        account = new AccountBuilder()
                          .addContact("mailto:acme@my.corp")
                          .agreeToTermsOfService()
                          .useKeyPair(accountKeyPair)
                          .create(session);
    }

    @Test
    public void orderCertificate() throws Exception {
        String domain = System.getProperty("subjectAltName");
        assertNotNull(domain);
        order = account.newOrder()
            .domain(domain)
            .create();
        assertNotNull(order);
    }

    @Test
    public void orderIPCertificate() throws Exception {
        Order ipOrder = account.newOrder()
            .identifier(new Identifier(Identifier.TYPE_IP, "127.0.0.1"))
            .create();
        assertNotNull(ipOrder);
    }

    // @Test(dependsOnMethods="orderCertificate")
    public void requireAuthorizationsNotValid() throws Exception {
        for (Authorization auth : order.getAuthorizations()) {
            assertEquals(auth.getStatus(), Status.INVALID);
        }
    }

    @Test(dependsOnMethods="orderCertificate")
    public void processAuthorizations() throws Exception {
        for (Authorization auth : order.getAuthorizations()) {
            if (auth.getStatus() != Status.VALID) {
                Http01Challenge challenge = auth
                    .findChallenge(Http01Challenge.class)
                    .orElseThrow(() -> new AcmeException("Found no " + Http01Challenge.TYPE + " challenge, don't know what to do ..."));

                String token = challenge.getToken();
                String content = challenge.getAuthorization();
                String webRoot = System.getProperty("webRoot");
                assertNotNull(webRoot);
                String fileName = webRoot + "/.well-known/acme-challenge/" + token;
                FileWriter fw = new FileWriter(fileName);
                fw.write(content);
                fw.flush();
                fw.close();

                challenge.trigger();
                for (int tries=0; auth.getStatus() != Status.VALID; tries++) {
                    assertTrue(tries < 10);
                    Thread.sleep(1000);
                    auth.update();
                }

                File f = new File(fileName);
                f.delete();
            }
        }
    }

    @Test(dependsOnMethods="processAuthorizations")
    public void finalizeOrder() throws Exception {
        String domain = System.getProperty("subjectAltName");
        assertNotNull(domain);

        //KeyPair domainKeyPair = KeyPairUtils.createECKeyPair("secp256k1");
        KeyPair domainKeyPair = KeyPairUtils.createKeyPair(2048);

        CSRBuilder csrb = new CSRBuilder();
        csrb.addDomain(domain);
        csrb.sign(domainKeyPair);
        try (FileWriter fw = new FileWriter("OrderTest.csr")) {
            csrb.write(fw);
        }

        byte csr [] = csrb.getEncoded();
        order.execute(csr);

        cert = order.getCertificate();
        assertNotNull(cert);
        X509Certificate x509 = cert.getCertificate();
        try (FileWriter fw = new FileWriter("OrderTest.crt")) {
            cert.writeCertificate(fw);
        }
    }

    @Test(dependsOnMethods="finalizeOrder")
    public void revokeCertificate() throws Exception {
        cert.revoke(RevocationReason.KEY_COMPROMISE);
    }

    @Test(dependsOnMethods="finalizeOrder")
    public void deactivateAuthorization() throws Exception {
        for (Authorization auth : order.getAuthorizations()) {
            auth.deactivate();
        }
    }
}
