import java.net.URI;
import java.net.URL;
import java.security.KeyPair;
import java.security.Security;
import java.time.Duration;
import java.util.Collection;
import java.util.List;

import java.io.FileWriter;
import java.io.File;
import java.security.cert.X509Certificate;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.shredzone.acme4j.challenge.Challenge;
import org.shredzone.acme4j.challenge.Dns01Challenge;
import org.shredzone.acme4j.challenge.Http01Challenge;
import org.shredzone.acme4j.exception.AcmeException;
import org.shredzone.acme4j.util.CSRBuilder;
import org.shredzone.acme4j.util.KeyPairUtils;
import org.shredzone.acme4j.Session;
import org.shredzone.acme4j.Metadata;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.AccountBuilder;
import org.shredzone.acme4j.Order;
import org.shredzone.acme4j.Status;
import org.shredzone.acme4j.Authorization;
import org.shredzone.acme4j.Certificate;
import org.shredzone.acme4j.RevocationReason;


public class StarWithGetTest {
    Session session = null;
    Metadata meta = null;
    KeyPair accountKeyPair = null;
    Account account = null;
    Order order = null;
    Certificate cert = null;

    @BeforeClass
    public void setup() {

        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    public void createSession() throws Exception {
        String serverUrl = System.getProperty("serverUrl");
        assertNotNull(serverUrl);
        session = new Session(serverUrl);
        assertNotNull(session);
    }

    @Test(dependsOnMethods="createSession")
    public void providesMetadata() throws Exception {
        meta = session.getMetadata();
        assertNotNull(meta);
    }

    @Test(dependsOnMethods="providesMetadata")
    public void allowsAcmeStarGetAllowed() throws Exception {
        assertTrue(meta.isAutoRenewalGetAllowed());
    }

    @Test(dependsOnMethods="createSession")
    public void registerAccount() throws Exception {
        accountKeyPair = KeyPairUtils.createECKeyPair("secp256r1");
        assertNotNull(accountKeyPair);

        account = new AccountBuilder()
                          .addContact("mailto:acme@my.corp")
                          .agreeToTermsOfService()
                          .useKeyPair(accountKeyPair)
                          .create(session);
        assertNotNull(account);
    }

    @Test(dependsOnMethods={"registerAccount","allowsAcmeStarGetAllowed"})
    public void newStarOrderForGet() throws Exception {
        String domain = System.getProperty("subjectAltName");
        order = account.newOrder()
                .domain(domain)
                .autoRenewal()
                .autoRenewalEnableGet()
                .create();
        assertNotNull(order);
    }

    @Test(dependsOnMethods="newStarOrderForGet")
    public void processAuthorizationsForGet() throws Exception {
        for (Authorization auth : order.getAuthorizations()) {
            if (auth.getStatus() != Status.VALID) {
                Http01Challenge challenge = auth
                    .findChallenge(Http01Challenge.class)
                    .orElseThrow(() -> new AcmeException("Found no " + Http01Challenge.TYPE + " challenge, don't know what to do ..."));

                String token = challenge.getToken();
                String content = challenge.getAuthorization();
                String webRoot = System.getProperty("webRoot");
                assertNotNull(webRoot);
                String fileName = webRoot + "/.well-known/acme-challenge/" + token;
                FileWriter fw = new FileWriter(fileName);
                fw.write(content);
                fw.flush();
                fw.close();

                challenge.trigger();
                for (int tries=0; auth.getStatus() != Status.VALID; tries++) {
                    assertTrue(tries < 10);
                    Thread.sleep(1000);
                    auth.update();
                }

                File f = new File(fileName);
                f.delete();
            }
        }
    }

    @Test(dependsOnMethods="processAuthorizationsForGet")
    public void finalizeOrderForGet() throws Exception {
        String domain = System.getProperty("subjectAltName");
        assertNotNull(domain);

        // KeyPair domainKeyPair = KeyPairUtils.createKeyPair(2048);
        KeyPair domainKeyPair = KeyPairUtils.createECKeyPair("secp256r1");

        CSRBuilder csrb = new CSRBuilder();
        csrb.addDomain(domain);
        csrb.sign(domainKeyPair);
        try (FileWriter fw = new FileWriter("StarTest.csr")) {
            csrb.write(fw);
        }

        byte csr [] = csrb.getEncoded();
        order.execute(csr);
    }

    @Test(dependsOnMethods="finalizeOrderForGet")
    public void getCertificate() throws Exception {
        cert = order.getAutoRenewalCertificate();
        assertNotNull(cert);
        X509Certificate x509 = cert.getCertificate();
        try (FileWriter fw = new FileWriter("StarTest.crt")) {
            cert.writeCertificate(fw);
        }
    }

    @Test(dependsOnMethods="getCertificate")
    public void getCertificateAgain() throws Exception {
        cert = order.getAutoRenewalCertificate();
        assertNotNull(cert);

        X509Certificate x509 = cert.getCertificate();
        try (FileWriter fw = new FileWriter("StarTest-2.crt")) {
            cert.writeCertificate(fw);
        }
    }
}
