import url from 'node:url';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import acme from '../examples/acme-client/acme.mjs';

import * as dnsserver from './fixture/dnsserver.mjs';
import * as NodeRED from './fixture/node-red.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('ACME for Subdomains', function() {
    let acmeClient;
    let acmeAccount;
    before(async function() {
       process.env.ACME_PRE_AUTHORIZATION_ENABLED = 'true';
       acmeClient = new acme.Client(new url.URL(`${server}/acme/Sub/directory`));
       dnsserver.start();
       let responder = new acme.DnsProxyChallengeResponder(dnsserver.config.records);
       acmeAccount = await acme.Account.create(`${server}/acme/Sub/directory`, [responder]);
    });
    after(function() {
        dnsserver.stop();
        delete process.env.ACME_PRE_AUTHORIZATION_ENABLED;
    });
    describe('4.4 Directory Object Metadata', function() {
        it('contains subdomainAuthAllowed', function(done) {
            acmeClient.getDirectory((err, dir) => {
                expect(err).to.be.null;
                expect(dir).to.be.an('object');
                expect(dir).to.have.property('meta');
                expect(dir.meta).to.be.an('object');
                expect(dir.meta).to.have.property('subdomainAuthAllowed');
                expect(dir.meta.subdomainAuthAllowed).to.be.equal(true);
                done();
            });
        });
    }); 
    describe('4.2 Pre-authorization', function() {
        before(function() {
            if (!NodeRED.started)
                this.skip();
        });
        it('can be requested with subdomainAuthAllowed', function(done) {
            let identifier = { type: "dns", value: "sda.example.com", subdomainAuthAllowed: true };
            acmeAccount.preAuthorize(identifier, function(error, authorization) {
                expect(error).to.be.null;
                expect(authorization).to.be.an('object');
                expect(authorization).to.have.property('status');
                expect(authorization.status).to.be.equal('valid');
                expect(authorization).to.have.property('subdomainAuthAllowed');
                expect(authorization.subdomainAuthAllowed).to.be.equal(true);
                expect(authorization).to.have.property('identifier');
                expect(authorization.identifier).to.be.an('object');
                expect(authorization.identifier).to.have.property('type');
                expect(authorization.identifier.type).to.be.equal('dns');
                expect(authorization.identifier).to.have.property('value');
                expect(authorization.identifier.value).to.be.equal('sda.example.com');
                done();
            });
        });
        it('can be requested with subdomainAuthAllowed multiple times', function(done) {
            let identifier = { type: "dns", value: "second.example.com", subdomainAuthAllowed: true };
            acmeAccount.preAuthorize(identifier, function(error, authorization) {
                expect(error).to.be.null;
                expect(authorization).to.be.an('object');
                expect(authorization).to.have.property('status');
                expect(authorization.status).to.be.equal('valid');
                expect(authorization).to.have.property('subdomainAuthAllowed');
                expect(authorization.subdomainAuthAllowed).to.be.equal(true);
                expect(authorization).to.have.property('identifier');
                expect(authorization.identifier).to.be.an('object');
                expect(authorization.identifier).to.have.property('type');
                expect(authorization.identifier.type).to.be.equal('dns');
                expect(authorization.identifier).to.have.property('value');
                expect(authorization.identifier.value).to.be.equal('second.example.com');

                acmeAccount.preAuthorize(identifier, function(error, authorization) {
                    expect(error).to.be.null;
                    expect(authorization).to.be.an('object');
                    expect(authorization).to.have.property('status');
                    expect(authorization.status).to.be.equal('valid');
                    expect(authorization).to.have.property('subdomainAuthAllowed');
                    expect(authorization.subdomainAuthAllowed).to.be.equal(true);
                    expect(authorization).to.have.property('identifier');
                    expect(authorization.identifier).to.be.an('object');
                    expect(authorization.identifier).to.have.property('type');
                    expect(authorization.identifier.type).to.be.equal('dns');
                    expect(authorization.identifier).to.have.property('value');
                    expect(authorization.identifier.value).to.be.equal('second.example.com');
                    done();
                });
            });
        });

        it('can be requested without subdomainAuthAllowed', function(done) {
            let identifier = { type: "dns", value: "no-sda.example.com", subdomainAuthAllowed: false };
            acmeAccount.preAuthorize(identifier, function(error, authorization) {
                expect(error).to.be.null;
                expect(authorization).to.be.an('object');
                expect(authorization).to.have.property('status');
                expect(authorization.status).to.be.equal('valid');
                expect(authorization).not.to.have.property('subdomainAuthAllowed');
                expect(authorization).to.have.property('identifier');
                expect(authorization.identifier).to.be.an('object');
                expect(authorization.identifier).to.have.property('type');
                expect(authorization.identifier.type).to.be.equal('dns');
                expect(authorization.identifier).to.have.property('value');
                expect(authorization.identifier.value).to.be.equal('no-sda.example.com');
                done();
            });
        });
    });
    describe('4.3 New Orders', function() {
        it('can be placed with ancestorDomain', function(done) {
            let identifiers = [{ type: "dns", value: "sub.sda.example.com", ancestorDomain: "sda.example.com" }];
            acmeAccount.newOrder(identifiers, undefined, function(error, order) {
                expect(error).to.be.null;
                expect(order).to.be.an('object');
                expect(order).to.have.property('status');
                expect(order.status).to.be.equal('ready');
                expect(order).to.have.property('identifiers');
                expect(order.identifiers).to.be.an('array');
                expect(order.identifiers).to.have.lengthOf(1);
                expect(order.identifiers[0]).to.be.an('object');
                expect(order.identifiers[0]).to.have.property('type');
                expect(order.identifiers[0].type).to.be.equal('dns');
                expect(order.identifiers[0]).to.have.property('value');
                expect(order.identifiers[0].value).to.be.equal('sub.sda.example.com');
                done();
            });
        });
        it('can be placed without ancestorDomain', function(done) {
            let identifiers = [{ type: "dns", value: "sub.sda.example.com" }];
            acmeAccount.newOrder(identifiers, undefined, function(error, order) {
                expect(error).to.be.null;
                expect(order).to.be.an('object');
                expect(order).to.have.property('status');
                expect(order.status).to.be.equal('pending');
                expect(order).to.have.property('identifiers');
                expect(order.identifiers).to.be.an('array');
                expect(order.identifiers).to.have.lengthOf(1);
                expect(order.identifiers[0]).to.be.an('object');
                expect(order.identifiers[0]).to.have.property('type');
                expect(order.identifiers[0].type).to.be.equal('dns');
                expect(order.identifiers[0]).to.have.property('value');
                expect(order.identifiers[0].value).to.be.equal('sub.sda.example.com');
                done();
            });
        });
        it("isn't ready with incorrect ancestorDomain", function(done) {
            let identifiers = [{ type: "dns", value: "sub.no-sda.example.com", ancestorDomain: "no-sda.example.com" }];
            acmeAccount.newOrder(identifiers, undefined, function(error, order) {
                expect(error).to.be.null;
                expect(order).to.be.an('object');
                expect(order).to.have.property('status');
                expect(order.status).to.be.equal('pending');
                expect(order).to.have.property('identifiers');
                expect(order.identifiers).to.be.an('array');
                expect(order.identifiers).to.have.lengthOf(1);
                expect(order.identifiers[0]).to.be.an('object');
                expect(order.identifiers[0]).to.have.property('type');
                expect(order.identifiers[0].type).to.be.equal('dns');
                expect(order.identifiers[0]).to.have.property('value');
                expect(order.identifiers[0].value).to.be.equal('sub.no-sda.example.com');
                done();
            });
        });
        it("isn't ready with invalid ancestorDomain", function(done) {
            let identifiers = [{ type: "dns", value: "sub.dne.example.com", ancestorDomain: "dne.example.com" }];
            acmeAccount.newOrder(identifiers, undefined, function(error, order) {
                expect(error).to.be.null;
                expect(order).to.be.an('object');
                expect(order).to.have.property('status');
                expect(order.status).to.be.equal('pending');
                expect(order).to.have.property('identifiers');
                expect(order.identifiers).to.be.an('array');
                expect(order.identifiers).to.have.lengthOf(1);
                expect(order.identifiers[0]).to.be.an('object');
                expect(order.identifiers[0]).to.have.property('type');
                expect(order.identifiers[0].type).to.be.equal('dns');
                expect(order.identifiers[0]).to.have.property('value');
                expect(order.identifiers[0].value).to.be.equal('sub.dne.example.com');
                done();
            });
        });
        it('cannot be placed with invalid ancestorDomain', function(done) {
            let identifiers = [{ type: "dns", value: "sub.dne.example.com", ancestorDomain: "other.example.com" }];
            acmeAccount.newOrder(identifiers, undefined, function(error, response) {
                expect(error).not.to.be.null;
                expect(response.type).to.be.equal('urn:ietf:params:acme:error:malformed');
                done();
            });
        });

    });
});
