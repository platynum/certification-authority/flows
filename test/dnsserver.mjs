import dns from 'node:dns/promises';
import { inspect } from 'node:util';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import * as dnsserver from './fixture/dnsserver.mjs';

describe('test fixture', function() {
    it('can start DNS server', function() {
        dnsserver.start();
    });
    it('can stop DNS server', function() {
        dnsserver.stop();
    });
    describe('DNS server', function() {
        before(dnsserver.start);
        after(dnsserver.stop);

        it('can resolve www.my.corp', function() {
            return dns.resolve('www.my.corp')
                .then((addresses) => {
                    expect(addresses).to.be.an.array();
                    expect(addresses).to.have.lengthOf(1);
                    expect(addresses[0]).to.equal('1.2.3.4');
                    return addresses;
                });
        });
        it('can configure A entries', function() {
            dnsserver.config.records.push({name: 'new.my.corp', address: '4.3.2.1'});
            return dns.resolve('new.my.corp')
                .then((addresses) => {
                    expect(addresses).to.be.an.array();
                    expect(addresses).to.have.lengthOf(1);
                    expect(addresses[0]).to.equal('4.3.2.1');
                    return addresses;
                });
        });
        it('can configure TXT entries', function() {
            dnsserver.config.records.push({name: '_acme-challenge.new.my.corp', type: 0x10, data: 'valid'});
            return dns.resolveTxt('_acme-challenge.new.my.corp')
                .then((addresses) => {
                    expect(addresses).to.be.an.array();
                    expect(addresses).to.have.lengthOf(1);
                    expect(addresses[0]).to.be.an.array();
                    expect(addresses[0]).to.have.lengthOf(1);
                    expect(addresses[0][0]).to.equal('valid');
                    return addresses;
                });
        });
         it('can configure CAA entries', function() {
            dnsserver.config.records.push(
                {name: 'new.my.corp', type: 0x101, tag: 'issuemail', value: 'nodejs.dc-git.my.corp'});
            return dns.resolveCaa('new.my.corp')
                .then((addresses) => {
                    expect(addresses).to.be.an.array();
                    expect(addresses).to.have.lengthOf(1);
                    expect(addresses[0]).to.have.property('critical');
                    expect(addresses[0]['critical']).to.equal(0);
                    expect(addresses[0]).to.have.property('issuemail');
                    expect(addresses[0]['issuemail']).to.equal('nodejs.dc-git.my.corp');
                    return addresses;
                });
        });

    });
});

