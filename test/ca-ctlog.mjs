import fs from 'node:fs';
import crypto from 'node:crypto';
const { subtle } = crypto.webcrypto;
import https from 'node:https';
import tls from 'node:tls';
import net from 'node:net';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import {Parser} from 'binary-parser';
import jsrsasign from 'jsrsasign';

import * as helpers from './lib/helpers.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

/* MIT License: https://github.com/bma73/hexdump-nodejs */
let _fillUp = function (value, count, fillWith) {
        let l = count - value.length;
        let ret = "";
        while (--l > -1)
            ret += fillWith;
        return ret + value;
    },
    hexdump = function (buffer, offset, length) {

        offset = offset || 0;
        length = length || buffer.length;

        let out = ""; // _fillUp("Offset", 8, " ") + "  00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F\n";
        let row = "";
        for (let i=0; i<length; i+=16) {
            row += _fillUp(offset.toString(16).toUpperCase(), 8, "0") + "  ";
            let n = Math.min(16, length - offset);
            let string = "";
            for (let j=0; j<16; ++j) {
                if (j < n) {
                    let value = buffer.readUInt8(offset);
                    string += value >= 32 && value < 127 ? String.fromCharCode(value) : ".";
                    row += _fillUp(value.toString(16).toUpperCase(), 2, "0") + " ";
                    offset++;
                }
                else {
                    row += "   ";
                    string += " ";
                }
            }
            row += " " + string + "\n";
        }
        out += row;
        return out;
    };


function stoab(str) {
  let len = str.length;
  let bytes = new Uint8Array(len);
  for (let i=0; i<len; i++)
    bytes[`${i}`] = str.charCodeAt(i);
  return bytes.buffer;
}

function b64toab(b64str) {
  let str = Buffer.from(b64str, 'base64');
  return btoab(str);
}

function btoab(buf) {
  let str = buf.toString('binary');
  return stoab(str);
}

function get_sth(log, cb) {
  https.get(log.url, (/* resp */) => {
    let url = `${log.url}ct/v1/get-sth`;
    console.log(`${url}`);
    get(url, (sth) => {
      // console.log(`signed_tree_head: ${JSON.stringify(sth, null, 4)}`);
      // console.log(`verifying tree head of ${log.url} with #${sth.tree_size} entries at ${new Date(sth.timestamp)}`);
      let key = Buffer.from(log.key, 'base64');
      let hash = Buffer.from(sth.sha256_root_hash, 'base64');
      let sig = Buffer.from(sth.tree_head_signature, 'base64');
      subtle.importKey('spki', key, {name: 'ECDSA', namedCurve: 'P-256'}, true, ['verify'])
        .then((keyObj) => {
          let buf = new Uint8Array(1 + 1 + 8 + 8 + 32);
          buf[0] = 0;     // Version: v1
          buf[1] = 1;     // SignatureType: tree_hash
                          // uint64 timestamp
          let hexString = sth.timestamp.toString(16);
          while (hexString.length < 16)
            hexString = '0' + hexString;
          let tsArray = new Uint8Array(hexString.match(/.{1,2}/g).map(byte => Number.parseInt(byte, 16)));
          for (let i=0; i<tsArray.length; i++)
                        buf[2+i] = tsArray[`${i}`];
                          // uint64 tree_size
          hexString = sth.tree_size.toString(16);
          while (hexString.length < 16)
            hexString = '0' + hexString;
          tsArray = new Uint8Array(hexString.match(/.{1,2}/g).map(byte => Number.parseInt(byte, 16)));
          for (let i=0; i<tsArray.length; i++)
                        buf[10+i] = tsArray[`${i}`];
                          // opaque sha256_root_hash[32]
          for (let i=0; i<hash.length; i++)
            buf[18+i] = hash[`${i}`];

          // Remove OCTET STRING tag + length from signature with .slice(4)
          sth.verified = crypto.verify('SHA-256', buf, keyObj, sig.slice(4));

          cb(sth);
        })
        .catch((error) => {
          console.log(`error while verifying signature (${error})`);
        });
    });
  });
}

function get_sth_consistency(log, first, second, cb) {
  let url = `${log.url}ct/v1/get-sth-consistency?first=${first}&second=${second}`;
  console.log(url);
  get(url, cb);
//  get(url, (consistency) => {
//    cb(consistency);
//  });
}

function get_proof_by_hash(log, hash, tree_size, cb) {
  let lhash = hash.replaceAll('+', '%2B').replaceAll('/', '%2F').replaceAll('=', '%3D');
  let url = `${log.url}ct/v1/get-proof-by-hash?hash=${lhash}&tree_size=${tree_size}`;
  console.log(url);
  get(url, (proof) => {
    cb(proof);
  });
}

function get_entry_and_proof(log, leaf_index, tree_size, cb) {
  let url = `${log.url}ct/v1/get-entry-and-proof?leaf_index=${leaf_index}&tree_size=${tree_size}`;
  console.log(url);
  get(url, (entry, proof) => {
    console.log(`entry: ${JSON.stringify(entry)}`);
    console.log(`proof: ${JSON.stringify(proof)}`);
    cb(entry, proof);
  });
}

function get_entries(log, start, end, cb) {
  let url = `${log.url}ct/v1/get-entries?start=${start}&end=${end}`;
  console.log(url);
  get(url, (reply) => {
    for (const [leaf_hash, entry] of reply.entries.entries()) {
      cb(entry, leaf_hash);
    }
  });
}

function parse_leaf(buf) {
    let x509_entry = new Parser()
             .namely('x509_entry')
             .endianess('big')
             .uint8('type')
             .uint16('length')
             .array('ASN1Cert', {
               type: 'uint8',
               length: 'length'
             });
    let precert_entry = new Parser()
             .namely('precert_entry')
             .endianess('big')
             .array('issuer_key_hash', {
               type: 'uint8',
               length: 32
             })
             .uint8('type')
             .uint16('length')
             .array('TBSCertificate', {
               type: 'uint8',
               length: 'length'
             });
    let TimestampedEntry = new Parser()
             .namely('TimestampedEntry')
             .endianess('big')
             .uint64('timestamp', {
               formatter: function(ts) {
                 return new Date(Number.parseInt(ts) / 256)
               }
             })
             .uint16('LogEntryType' /*,{
               formatter: function(type) {
                 switch (type) {
                   case 0:  return 'x509_entry';    break;
                   case 1:  return 'precert_entry'; break;
                   default: return type;            break;
                 }
               }
             }*/)
             .choice('LogEntry', {
               tag: 'LogEntryType',
               choices: {
                 0: x509_entry,
                 1: precert_entry
               },
             })
             .uint16('CTExtensionsLength')
             .array('CTExtensions', {
               type: 'uint8',
               length: 'CTExtensionsLength'
             });
    let MerkleTreeLeaf = new Parser()
             .endianess('big')
             .uint8('Version')
             .uint8('MerkleLeafType')
             .choice('TimestampedEntry', {
               tag: 'MerkleLeafType',
               choices: {
                 0: TimestampedEntry
               }
             });

    let leaf_input = { MerkleTreeLeaf: MerkleTreeLeaf.parse(buf) };
    //console.log(`leaf_input: ${JSON.stringify(leaf_input)}`);

    return leaf_input;
}

function get_entry(log, num, cb) {
  get_entries(log, num, num, (entry) => {
    let buf = Buffer.from(entry.leaf_input, 'base64');
    let leaf_input = parse_leaf(buf);
    let md = new jsrsasign.KJUR.crypto.MessageDigest({alg: 'sha256', prov: 'cryptojs'});
    md.updateHex('00');
    md.updateHex(jsrsasign.b64nltohex(entry.leaf_input));
    let leaf_hash = jsrsasign.hextob64(md.digest());
    //console.log(`leaf_hash: ${leaf_hash}`);

    if (leaf_input.MerkleTreeLeaf.TimestampedEntry.LogEntry.ASN1Cert) {
      let nBuf = Buffer.from(leaf_input.MerkleTreeLeaf.TimestampedEntry.LogEntry.ASN1Cert);
      let hex = nBuf.toString('hex');
      try {
        let x509 = new jsrsasign.X509();
        x509.readCertHex(hex);
        cb(x509, leaf_hash);
      }
      catch (error) {
        console.log(`error: ${error}\n${error.stack}`);
        let str = hexdump(nBuf);
        console.log(`str: ${str}`);
        console.log(`TimestampedEntry: ${JSON.stringify(leaf_input.MerkleTreeLeaf.TimestampedEntry)}`);
      }
    }
    else {
      let nBuf = Buffer.from(leaf_input.MerkleTreeLeaf.TimestampedEntry.LogEntry.TBSCertificate);
      let hex = nBuf.toString('hex');

      try {
        //let str = jsrsasign.ASN1HEX.dump(hex);
        //console.log(`str: ${str}`);

        //var x509 = new jsrsasign.X509();
        //x509.readCertHex(hex);
        cb(undefined, leaf_hash);
      }
      catch (error) {
        console.log(`error: ${error}\n${error.stack}`);
        let str = hexdump(nBuf);
        console.log(`str: ${str}`);
        console.log(`TimestampedEntry: ${JSON.stringify(leaf_input.MerkleTreeLeaf.TimestampedEntry)}`);
      }
    }
  });
}

function get_roots(log, cb) {
  let url = log.url + `ct/v1/get-roots`;
  console.log(url);
  get(url, (root) => {
    for (let base64 of root.certificates) {
      let pem = '-----BEGIN CERTIFICATE-----\n'
              + base64 + '\n'
              + '-----END CERTIFICATE-----';
      // console.log(pem);
      let hex = jsrsasign.b64nltohex(base64);
      let cert = new jsrsasign.X509();
      try {
        cert.readCertHex(hex);
        cb(cert, pem, hex);
      }
      catch (error) {
        console.log(`error: ${error}\n${error.stack}`);
        console.log(`pem: ${pem}`);
      }
    }
  });
}

function verify_proof(sth, hash, proof) {
  function combine(left, right) {
    let md = new jsrsasign.KJUR.crypto.MessageDigest({alg: 'sha256', prov: 'cryptojs'});
    //console.log(`left: ${Buffer.from(left, 'hex').toString('base64')}; right: ${Buffer.from(right, 'hex').toString('base64')}`);
    md.updateHex('01' + left + right);
    return md.digest();
  }
  let leaf_index = proof.leaf_index;
  let curr = Buffer.from(hash, 'base64').toString('hex');
  for (let last_node = sth.tree_size - 1; last_node > 0; last_node = Math.floor(last_node / 2)) {
    //console.log(`leaf_index: ${leaf_index}, last_node: ${last_node}`);
    if (leaf_index % 2 != 0) {
      let left = Buffer.from(proof.audit_path.shift(), 'base64').toString('hex');
      curr = combine(left, curr);
    }
    else if (leaf_index < last_node) {
      let right = Buffer.from(proof.audit_path.shift(), 'base64').toString('hex');
      curr = combine(curr, right);
    }
    //console.log(`curr: ${Buffer.from(curr, 'hex').toString('base64')}`);
    leaf_index = Math.floor(leaf_index / 2);
  }
  //console.log(`sth.sha256_root_hash: ${sth.sha256_root_hash}`);
  //console.log(`sth.sha256_root_hash (hex): ${Buffer.from(sth.sha256_root_hash, 'base64').toString('hex')}`);
  return curr == Buffer.from(sth.sha256_root_hash, 'base64').toString('hex');
}

describe('Certificate transparency log (CTLOG)', function() {

    let signer;
    let sth;
    let entries;

    describe('pre-signer certificate', function() {
        it('was generated', function() {
            // Extract key from CTLOG signer certificate
            const crt = fs.readFileSync('Root/ctlog/keys/ctlog.crt.pem', 'utf8');
            signer = new jsrsasign.X509();
            signer.readCertPEM(crt);
            // console.log(signer.getSubjectString());
            expect(signer).not.to.be.null;
        });
        it('has Certficate transparency signer extension', function() {
            let ext = signer.getExtParamArray();
            expect(signer.findExt(ext, '1.3.6.1.4.1.11129.2.4.4')).not.to.be.null;
        });
    });
    describe('get-roots method', function() {
        it('provides root certificates', function(done) {
            chai.request.execute(server).get('/ct/v1/get-roots')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res).to.be.json;
                    res.header['content-type'].should.be.equal('application/json; charset=utf-8');

                    let body = JSON.parse(res.body);
                    for (let base64 of body.certificates) {
                        /* const pem = '-----BEGIN CERTIFICATE-----\n'
                                + base64 + '\n'
                                + '-----END CERTIFICATE-----\n';
                        console.log(pem); */
                        const hex = jsrsasign.b64nltohex(base64);
                        let cert = new jsrsasign.X509();
                        cert.readCertHex(hex);
                        // console.log(`issuer: ${cert.getIssuerString()}`);
                    }
                    done();
                });
        });
    });
    describe('get-sth method', function() {
        it('is provided', function(done) {
            chai.request.execute(server).get('/ct/v1/get-sth')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    res.header['content-type'].should.be.equal('application/json; charset=utf-8');

                    sth = JSON.parse(res.body);
                    //console.log(`sth: ${JSON.stringify(sth, null, 2)}`);
                    done();
                });
        });
        it('provides sha256_root_hash', function() {
            expect(sth).to.have.property('sha256_root_hash');
            let hash = Buffer.from(sth.sha256_root_hash, 'base64');
            expect(hash).not.to.be.undefined;
            expect(hash).to.have.lengthOf(32);
        });
        it('provides tree_head_signature', function() {
            expect(sth).to.have.property('tree_head_signature');
            let sig = Buffer.from(sth.tree_head_signature, 'base64');
            expect(sig).not.to.be.undefined;
        });
        it('is signed correctly', function(done) {
            let hash = Buffer.from(sth.sha256_root_hash, 'base64');
            let sig = Buffer.from(sth.tree_head_signature, 'base64');

            const hex = signer.getPublicKeyHex();
            let bin = new Uint8Array(hex.match(/.{1,2}/g).map(byte => Number.parseInt(byte, 16)));
            // console.log(`bin: ${bin}`);
            subtle.importKey('spki', bin, {name: 'ECDSA', namedCurve: 'P-256'}, true, ['verify'])
                  .then((keyObj) => {
                      let buf = new Uint8Array(1 + 1 + 8 + 8 + 32);
                      buf[0] = 0;     // Version: v1
                      buf[1] = 1;     // SignatureType: tree_hash
                                      // uint64 timestamp
                      let hexString = sth.timestamp.toString(16);
                      while (hexString.length < 16)
                          hexString = '0' + hexString;
                      let tsArray = new Uint8Array(hexString.match(/.{1,2}/g).map(byte => Number.parseInt(byte, 16)));
                      for (let i=0; i<tsArray.length; i++)
                          buf[2+i] = tsArray[`${i}`];
                                      // uint64 tree_size
                      hexString = sth.tree_size.toString(16);
                      while (hexString.length < 16)
                          hexString = '0' + hexString;
                      tsArray = new Uint8Array(hexString.match(/.{1,2}/g).map(byte => Number.parseInt(byte, 16)));
                      for (let i=0; i<tsArray.length; i++)
                          buf[10+i] = tsArray[`${i}`];
                                      // opaque sha256_root_hash[32]
                      for (let i=0; i<hash.length; i++)
                          buf[18+i] = hash[`${i}`];

                      //console.log(`verifying (${buf.length} bytes): ${Buffer.from(buf).toString('hex')}`);
                      // Remove OCTET STRING tag + length from signature with .slice(4)
                      let rawSig = sig.slice(4);
                      try {
                          sth.verified = crypto.verify('sha256', buf, keyObj, rawSig);
                      } catch (error) { console.log(error); }

                      //expect(crypto.verify('sha256', buf, keyObj, rawSig)).to.eventually.be.true.notify(done);
                      expect(sth.verified).to.be.true;
                      done();
                  });
              });
        });
        describe('get-entries method', function() {
            it('provides entries from log', function(done) {
                let leaf_index = Math.random() * sth.tree_size;
                chai.request.execute(server).get(`/ct/v1/get-entries?start=${leaf_index}&end=${leaf_index}`)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        res.header['content-type'].should.be.equal('application/json; charset=utf-8');

                        let doc = JSON.parse(res.body);
                        entries = doc.entries;

                        // console.log(`res.body: ${res.body}`);
                        done();
                    });
            });
            it('does not return unknown entries (1)', function(done) {
                chai.request.execute(server).get(`/ct/v1/get-entries?start=${sth.tree_size}&end=${sth.tree_size + 1}`)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(404);
                        done();
                    });
            });
            it('does not return unknown entries (2)', function(done) {
                chai.request.execute(server).get('/ct/v1/get-entries?start=1&end=0')
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(404);
                        done();
                    });
            });
            it('does not return unknown entries (3)', function(done) {
                chai.request.execute(server).get(`/ct/v1/get-entries?start=-1&end=${sth.tree_size + 1}`)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(404);
                        done();
                    });
            });
            describe('provided entries', function() {
                it('are parseable', function(done) {
                    for (let entry of entries) {
                        let buf = Buffer.from(entry.leaf_input, 'base64');
                        let parsed = parse_leaf(buf);
                        entry.MerkleTreeLeaf = parsed.MerkleTreeLeaf;
                        // console.log(JSON.stringify(entry));
                        let b64 = Buffer.from(parsed.MerkleTreeLeaf.TimestampedEntry.LogEntry.ASN1Cert).toString('base64');
                        let pem = '-----BEGIN CERTIFICATE-----\n'
                        for (let i=0; i<b64.length; i+=64)
                            pem += b64.substring(i, i+64) + '\n';
                        pem += '-----END CERTIFICATE-----';
                        // console.log(pem);
                        let secureContext = tls.createSecureContext({
                             cert: pem
                        });
                        let secureSocket = new tls.TLSSocket(new net.Socket(), { secureContext });
                        entry.certificate = secureSocket.getCertificate();
                        secureSocket.destroy();

                        subtle.digest('SHA-256', b64toab(entry.leaf_input))
                              .then((hash) => {
                                  entry.sha256hash = hash;
                                  done();
                              });
                        // console.log(cert);
                    }
                });
            });
        });
        describe('get-proof-by-hash method', function() {
            let proof;
            it('requires leaf hash', function(done) {
                let tree_size = sth.tree_size;
                chai.request.execute(server).get(`/ct/v1/get-proof-by-hash?tree_size=${tree_size}`)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(406);
                        done();
                    });
            });
            it('requires tree_size', function(done) {
                let hash = Buffer.from(entries[0].sha256hash).toString('base64');
                chai.request.execute(server).get(`/ct/v1/get-proof-by-hash?hash=${hash}`)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(406);
                        done();
                    });
            });
            it('returns a proof', function(done) {
                let hash = Buffer.from(entries[0].sha256hash).toString('base64');
                let enc = encodeURIComponent(hash);
                let tree_size = sth.tree_size;
                chai.request.execute(server).get(`/ct/v1/get-proof-by-hash?hash=${enc}&tree_size=${tree_size}`)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        res.header['content-type'].should.be.equal('application/json; charset=utf-8');
                        expect(res.body).not.to.be.null;
                        proof = res.body;
                        done();
                    });
            });
            describe('returned proof', function() {
                it('provides leaf_index', function() {
                    expect(proof).to.have.property('leaf_index');
                    expect(proof.leaf_index).to.be.a('number');
                });
                it('provides audit_path', function() {
                    expect(proof).to.have.property('audit_path');
                    expect(proof.audit_path).to.be.an('array');
                });
                it('verifies against signed tree head', function(done) {
                    let hash = Buffer.from(entries[0].sha256hash).toString('base64');
                    //console.log(proof);
                    expect(verify_proof(sth, hash, proof)).to.be.true;
                    done();
                });
            });
        });
        describe('get-sth-consistency method', function() {
            it('requires first', function(done) {
                chai.request.execute(server).get(`/ct/v1/get-sth-consistency?second=2`)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(406);
                        done();
                    });
            });
            it('requires second', function(done) {
                chai.request.execute(server).get(`/ct/v1/get-sth-consistency?first=1`)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(406);
                        done();
                    });
            });
            it('requires first to be before second', function(done) {
                chai.request.execute(server).get(`/ct/v1/get-sth-consistency?first=1&second=0`)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(406);
                        done();
                    });
            });
            it('provides consistency between two signed tree heads', function(done) {
                let second = Math.floor(Math.random() * (sth.tree_size - 1)) + 1;
                chai.request.execute(server).get(`/ct/v1/get-sth-consistency?first=1&second=${second}`)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        res.header['content-type'].should.be.equal('application/json; charset=utf-8');
                        expect(res.body).not.to.be.null;
                        expect(res.body).to.have.property('consistency');
                        expect(res.body.consistency).to.be.an('array');
                        done();
                    });
            });
        });
        describe('get-entry-and-proof debug method', function() {
            it('is not provided', function(done) {
                chai.request.execute(server).get('/ct/v1/get-entry-and-proof')
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(404);
                        done();
                    });
            });
        });
});

