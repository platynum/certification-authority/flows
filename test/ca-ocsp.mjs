import fs from 'node:fs';
import crypto from 'node:crypto';
import { format } from 'node:util';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import pkijs from 'pkijs';
pkijs.setEngine('Node.JS', crypto, new pkijs.CryptoEngine({
    name: 'Node.JS',
    crypto: crypto,
    subtle: crypto.subtle
}));
import asn1js from 'asn1js';
import pvutils from 'pvutils';
import jsrsasign from 'jsrsasign';

import * as NodeRED from './fixture/node-red.mjs';

import encode from './lib/encode.mjs';
import helpers from './lib/helpers.mjs';

let server = process.env.ACME_SERVER || 'http://localhost:1880';

function getSerial(certificate) {
    let serial = pvutils.bufferToHexCodes(certificate.serialNumber.valueBlock.valueHex);
    if (serial.startsWith('00'))
        serial = serial.substring(2);
    return serial.toLowerCase();
}

function createOcspQuery(request) {

    const hashes = {
        'sha1':   '1.3.14.3.2.26',
        'sha256': '2.16.840.1.101.3.4.2.1',
        'sha384': '2.16.840.1.101.3.4.2.2',
        'sha512': '2.16.840.1.101.3.4.2.3',
        'sha224': '2.16.840.1.101.3.4.2.4'
    };

    let hash = crypto.createHash(request.hash);
    hash.update(Buffer.from(request.issuer.subject.valueBeforeDecode));
    let issuerNameHash = hash.digest('binary');

    hash = crypto.createHash(request.hash);
    hash.update(Buffer.from(request.issuer.subjectPublicKeyInfo.subjectPublicKey.valueBeforeDecode));
    let issuerKeyHash = hash.digest('binary');
    //console.log(`issuerKeyHash: ${Buffer.from(issuerKeyHash).toString('hex')}`);

    const req = new pkijs.OCSPRequest();

    if (request.addRequestorName) {
        req.tbsRequest.requestorName = new pkijs.GeneralName({
            type: 4,
            value: request.certificate.subject
        });
    }

    req.tbsRequest.requestList = [
        new pkijs.Request({
            reqCert: new pkijs.CertID({
                hashAlgorithm: new pkijs.AlgorithmIdentifier({
                    algorithmId: hashes[request.hash]
                }),
                issuerNameHash: new asn1js.OctetString({
                    valueHex: helpers.toArrayBuffer(Buffer.from(issuerNameHash, 'binary'))
                }),
                issuerKeyHash: new asn1js.OctetString({
                    valueHex: helpers.toArrayBuffer(Buffer.from(issuerKeyHash, 'binary'))
                }),
                serialNumber: request.serialNumber ?? request.certificate.serialNumber
            })
        })
    ];

    if (request.addNonce >= 0) {
        const fictionBuffer = new ArrayBuffer(request.addNonce);
        const fictionView = new Uint8Array(fictionBuffer);
        for (let j=0; j<request.addNonce; j++)
            fictionView[`${j}`] = (j + 1) % 256;

        req.tbsRequest.requestExtensions = [
            new pkijs.Extension({
                extnID: "1.3.6.1.5.5.7.48.1.2", // ocspNonce
                extnValue: new asn1js.OctetString({
                    valueHex: fictionBuffer
                }).toBER(false)
            })
        ];
    }

    if (request.addCritical) {
        req.tbsRequest.requestExtensions = [
            new pkijs.Extension({
                extnID: "1.3.6.1.5.5.7.48.1.666", // does not exist
                critical: true,
                extnValue: new asn1js.Null().toBER(false)
            })
        ];
    }

    let buf = req.toSchema(true).toBER(false);
    return pvutils.arrayBufferToString(buf);
}

let rfc5019 = {};
let rfc6960 = {
    'ocsp-nocheck': false
};
function conformsTo(specification) {
    let found = Object.keys(specification).find((requirement) => !specification[`${requirement}`]);
    return found === undefined || found;
}

describe('Online certificate status protocol (OCSP)', function() {

    let ocspUrl;
    let certificate;
    let issuer;

    describe('Certificate to be checked (https certificate)', function() {
        it('is available via filesystem', function(done) {
            const pem = fs.readFileSync('Sub/https/https-RSA.crt.pem', 'utf8');
            const b64 = pem.replaceAll(/(-{5}(BEGIN|END) CERTIFICATE-{5})[\n\r]/g, '');
            const der = Buffer.from(b64, 'base64');
            const asn1 = asn1js.fromBER(helpers.toArrayBuffer(der));
            certificate = new pkijs.Certificate({ schema: asn1.result });
            expect(certificate).not.to.be.null;
            done();
        });
        it('provides authority information access (AIA)', function(done) {
            for (let extn of certificate.extensions) {
                if (extn.extnID === '1.3.6.1.5.5.7.1.1') { // Authority Information Access
                    for (let ad of extn.parsedValue.accessDescriptions) {
                        if (ad.accessMethod === '1.3.6.1.5.5.7.48.1') { // OCSP
                            ocspUrl = new URL(ad.accessLocation.value);
                        }
                        else if (ad.accessMethod === '1.3.6.1.5.5.7.48.2') { // CA Issuer
                            issuer = new URL(ad.accessLocation.value);
                        }
                    }
                }
            }
            expect(ocspUrl).not.to.be.null;
            expect(issuer).not.to.be.null;
            done();
        });
        describe('It\'s issuer certificate', function() {
            let asn1;
            it('can be downloaded at AIA', function(done) {
                //console.log(`${server}/${issuer.pathname}`);
                chai.request.execute(server).get(issuer.pathname)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        res.header['content-type'].should.be.equal('application/x-x509-ca-certificate');
                        expect(res.body).to.be.an.instanceof(Buffer);
                        asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                        expect(asn1).not.to.be.null;
                        done();
                });
            });
            it('can be parsed (x509)', function(done) {
                issuer = new pkijs.Certificate({ schema: asn1.result });
                expect(issuer).not.to.be.null;
                done();
            });
        });
    });

    describe('OCSP responder', function() {
        it('answers POST requests', function(done) {
            let request = { certificate, issuer, hash: 'sha1' };
            let der = createOcspQuery(request);
            chai.request.execute(server).post(ocspUrl.pathname)
                .set('Content-Type', 'application/ocsp-request')
                .set('X-Forwarded-For', '50.116.58.136')
                .send(Buffer.from(der, 'binary'))
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                     expect(err).to.be.null;
                     expect(res).to.have.status(200);
                     res.header['content-type'].should.be.equal('application/ocsp-response');
                     expect(res.body).to.be.an.instanceof(Buffer);
                     const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                     const resp = new pkijs.OCSPResponse({ schema: asn1.result });
                     expect(res.body).to.have.lengthOf.above(5);
                     let dv = new DataView(resp.responseStatus.valueBlock.valueHex);
                     expect(dv.getUint8(0), 'response status success').to.equal(0x00);
                     done();
                });
        });

        it('reports revoked certificates', function(done) {
            let request = { certificate, issuer, hash: 'sha1' };
            let isn = getSerial(issuer);
            let csn = getSerial(certificate);
            let certificateRevoked = `Sub/certificates/${isn}/${csn}.revoked`;
            //console.log(`Writing superseded into ${certificateRevoked}`);
            fs.writeFileSync(certificateRevoked, 'superseded', 'utf8');

            let der = createOcspQuery(request);
            chai.request.execute(server).post(ocspUrl.pathname)
                .set('Content-Type', 'application/ocsp-request')
                .send(Buffer.from(der, 'binary'))
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    res.header['content-type'].should.be.equal('application/ocsp-response');
                    expect(res.body).to.be.an.instanceof(Buffer);
                    //fs.writeFileSync(`ocsp.response.revoked`, res.body.toString('binary'), 'binary');
                    const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                    const resp = new pkijs.OCSPResponse({ schema: asn1.result });
                    expect(res.body).to.have.lengthOf.above(5);

                    let dv = new DataView(resp.responseStatus.valueBlock.valueHex);
                    expect(dv.getUint8(0), 'response status success').to.equal(0x00);

                    const asn1Basic = asn1js.fromBER(resp.responseBytes.response.valueBlock.valueHex);
                    const basicResponse = new pkijs.BasicOCSPResponse({ schema: asn1Basic.result });
                    expect(basicResponse).not.to.be.null;
                    // CertStatus.good => IMPLICIT NULL
                    for (let singleResponse of basicResponse.tbsResponseData.responses) {
                        expect(singleResponse.certStatus).not.to.be.null;
                        // revokedInfo
                        expect(singleResponse.certStatus.valueBlock.value).to.have.length.above(1);
                        // revocationReason
                        expect(singleResponse.certStatus.valueBlock.value[1].valueBlock.value).to.have.length.above(0);
                        expect(singleResponse.certStatus.valueBlock.value[1].valueBlock.value[0].valueBlock._valueDec).to.be.equal(4);
                    }
                    fs.unlinkSync(certificateRevoked);
                    done();
                });
        });

        it('reports pre-certificates', function(done) {
            let request = { certificate, issuer, hash: 'sha256' };
            let isn = getSerial(issuer);
            let csn = getSerial(certificate);
            fs.renameSync(`Sub/certificates/${isn}/${csn}.crt.pem`,
                          `Sub/certificates/${isn}/${csn}.pre.pem`);

            let der = createOcspQuery(request);
            chai.request.execute(server).post(ocspUrl.pathname)
                .set('Content-Type', 'application/ocsp-request')
                .send(Buffer.from(der, 'binary'))
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    res.header['content-type'].should.be.equal('application/ocsp-response');
                    expect(res.body).to.be.an.instanceof(Buffer);
                    //fs.writeFileSync(`ocsp.response.revoked`, res.body.toString('binary'), 'binary');
                    const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                    const resp = new pkijs.OCSPResponse({ schema: asn1.result });
                    expect(res.body).to.have.lengthOf.above(5);

                    let dv = new DataView(resp.responseStatus.valueBlock.valueHex);
                    expect(dv.getUint8(0), 'response status success').to.equal(0x00);

                    const asn1Basic = asn1js.fromBER(resp.responseBytes.response.valueBlock.valueHex);
                    const basicResponse = new pkijs.BasicOCSPResponse({ schema: asn1Basic.result });
                    expect(basicResponse).not.to.be.null;
                    // CertStatus.good => IMPLICIT NULL
                    for (let singleResponse of basicResponse.tbsResponseData.responses) {
                        expect(singleResponse.certStatus).not.to.be.null;
                        // revokedInfo
                        expect(singleResponse.certStatus.valueBlock.value).to.have.length.above(1);
                        // revocationReason
                        expect(singleResponse.certStatus.valueBlock.value[1].valueBlock.value).to.have.length.above(0);
                        expect(singleResponse.certStatus.valueBlock.value[1].valueBlock.value[0].valueBlock._valueDec).to.be.equal(6);
                    }
                    fs.renameSync(`Sub/certificates/${isn}/${csn}.pre.pem`,
                                  `Sub/certificates/${isn}/${csn}.crt.pem`)
                    done();
                });
        });


        it('reports revoked pre-certificates', function(done) {
            let request = { certificate, issuer, hash: 'sha384' };
            let isn = getSerial(issuer);
            let csn = getSerial(certificate);
            let certificateRevoked = `Sub/certificates/${isn}/${csn}.revoked`;
            //console.log(`Writing superseded into ${certificateRevoked}`);
            fs.writeFileSync(certificateRevoked, 'superseded', 'utf8');
            fs.renameSync(`Sub/certificates/${isn}/${csn}.crt.pem`,
                          `Sub/certificates/${isn}/${csn}.pre.pem`);

            let der = createOcspQuery(request);
            chai.request.execute(server).post(ocspUrl.pathname)
                .set('Content-Type', 'application/ocsp-request')
                .send(Buffer.from(der, 'binary'))
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    res.header['content-type'].should.be.equal('application/ocsp-response');
                    expect(res.body).to.be.an.instanceof(Buffer);
                    //fs.writeFileSync(`ocsp.response.revoked`, res.body.toString('binary'), 'binary');
                    const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                    const resp = new pkijs.OCSPResponse({ schema: asn1.result });
                    expect(res.body).to.have.lengthOf.above(5);

                    let dv = new DataView(resp.responseStatus.valueBlock.valueHex);
                    expect(dv.getUint8(0), 'response status success').to.equal(0x00);

                    const asn1Basic = asn1js.fromBER(resp.responseBytes.response.valueBlock.valueHex);
                    const basicResponse = new pkijs.BasicOCSPResponse({ schema: asn1Basic.result });
                    expect(basicResponse).not.to.be.null;
                    // CertStatus.good => IMPLICIT NULL
                    for (let singleResponse of basicResponse.tbsResponseData.responses) {
                        expect(singleResponse.certStatus).not.to.be.null;
                        // revokedInfo
                        expect(singleResponse.certStatus.valueBlock.value).to.have.length.above(1);
                        // revocationReason
                        expect(singleResponse.certStatus.valueBlock.value[1].valueBlock.value).to.have.length.above(0);
                        expect(singleResponse.certStatus.valueBlock.value[1].valueBlock.value[0].valueBlock._valueDec).to.be.equal(4);
                    }
                    fs.unlinkSync(certificateRevoked);
                    fs.renameSync(`Sub/certificates/${isn}/${csn}.pre.pem`,
                                  `Sub/certificates/${isn}/${csn}.crt.pem`)
                    done();
                });
        });

        it('replies with nonce', function(done) {
            let request = { certificate, issuer, hash: 'sha1', addNonce: 16};
            let der = createOcspQuery(request);
            //fs.writeFileSync('ocsp.request.der', der, 'binary');
            chai.request.execute(server).post(ocspUrl.pathname)
                .set('Content-Type', 'application/ocsp-request')
                .send(Buffer.from(der, 'binary'))
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                     expect(err).to.be.null;
                     expect(res).to.have.status(200);
                     res.header['content-type'].should.be.equal('application/ocsp-response');
                     expect(res.body).to.be.an.instanceof(Buffer);
                     //fs.writeFileSync('ocsp.response.der', Buffer.from(res.body, 'binary'));
                     const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                     const resp = new pkijs.OCSPResponse({ schema: asn1.result });
                     let dv = new DataView(resp.responseStatus.valueBlock.valueHex);
                     expect(dv.getUint8(0), 'response status success').to.equal(0x00);
                     //console.log(resp);
                     expect(res.body).to.have.lengthOf.above(5);
                     done();
                });
        });
        it('does not accept an invalid OCSP request', function(done) {
            crypto.randomBytes(1024, (err, buf) => {
                chai.request.execute(server).post(ocspUrl.pathname)
                    .set('Content-Type', 'application/ocsp-request')
                    .send(buf)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                     expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        expect(res.body).to.be.an.instanceof(Buffer);
                        expect(res.body.length).to.be.equal(5);
                        res.header['content-type'].should.be.equal('application/ocsp-response');
                        done();
                    })
            });
        });
        it('does not accept nonces larger than 32 bytes', function(done) {
            let request = { certificate, issuer, hash: 'sha1', addNonce: 33};
            let der = createOcspQuery(request);
            //fs.writeFileSync('ocsp.request.der', der, 'binary');
            chai.request.execute(server).post(ocspUrl.pathname)
                .set('Content-Type', 'application/ocsp-request')
                .send(Buffer.from(der, 'binary'))
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                     expect(err).to.be.null;
                     expect(res).to.have.status(200);
                     res.header['content-type'].should.be.equal('application/ocsp-response');
                     expect(res.body).to.be.an.instanceof(Buffer);
                     expect(res.body.length).to.be.equal(5);

                     const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                     const resp = new pkijs.OCSPResponse({ schema: asn1.result });
                     let dv = new DataView(resp.responseStatus.valueBlock.valueHex);
                     expect(dv.getUint8(0), 'response status malformedRequest').to.equal(0x01);
                     done();
                });
        });
        it('does not accept empty nonces', function(done) {
            let request = { certificate, issuer, hash: 'sha1', addNonce: 0};
            let der = createOcspQuery(request);
            //fs.writeFileSync('ocsp.request.der', der, 'binary');
            chai.request.execute(server).post(ocspUrl.pathname)
                .set('Content-Type', 'application/ocsp-request')
                .send(Buffer.from(der, 'binary'))
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                     expect(err).to.be.null;
                     expect(res).to.have.status(200);
                     res.header['content-type'].should.be.equal('application/ocsp-response');
                     expect(res.body).to.be.an.instanceof(Buffer);
                     expect(res.body.length).to.be.equal(5);

                     const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                     const resp = new pkijs.OCSPResponse({ schema: asn1.result });
                     let dv = new DataView(resp.responseStatus.valueBlock.valueHex);
                     expect(dv.getUint8(0), 'response status malformedRequest').to.equal(0x01);
                     done();
                });
        });

        it('rejects unknown critical extensions', function(done) {
            let request = { certificate, issuer, hash: 'sha1', addCritical: true };
            let der = createOcspQuery(request);
            chai.request.execute(server).post(ocspUrl.pathname)
                .set('Content-Type', 'application/ocsp-request')
                .send(Buffer.from(der, 'binary'))
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                     expect(err).to.be.null;
                     expect(res).to.have.status(200);
                     res.header['content-type'].should.be.equal('application/ocsp-response');
                     expect(res.body).to.be.an.instanceof(Buffer);
                     expect(res.body.length).to.be.equal(5);
                     const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                     const resp = new pkijs.OCSPResponse({ schema: asn1.result });
                     let dv = new DataView(resp.responseStatus.valueBlock.valueHex);
                     expect(dv.getUint8(0), 'response status malformedRequest').to.equal(0x01);
                     done();
                });
        });
        it('accepts requestor name', function(done) {
            let request = { certificate, issuer, hash: 'sha1', addRequestorName: true };
            let der = createOcspQuery(request);
            // console.log(ocspUrl.protocol + '//' + ocspUrl.host + ocspUrl.pathname);
            // fs.writeFileSync('ocsp.request.der', Buffer.from(der, 'binary'));
            chai.request.execute(server).post(ocspUrl.pathname)
                .set('Content-Type', 'application/ocsp-request')
                .send(Buffer.from(der, 'binary'))
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                     expect(err).to.be.null;
                     expect(res).to.have.status(200);
                     res.header['content-type'].should.be.equal('application/ocsp-response');
                     expect(res.body).to.be.an.instanceof(Buffer);
                     const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                     const resp = new pkijs.OCSPResponse({ schema: asn1.result });
                     expect(res.body).to.have.lengthOf.above(5);
                     let dv = new DataView(resp.responseStatus.valueBlock.valueHex);
                     expect(dv.getUint8(0), 'response status success').to.equal(0x00);
                     done();
                });
        });
        it('answers GET requests', function(done) {
            let request = { certificate, issuer, hash: 'sha1' };
            let der = createOcspQuery(request);
            let path = ocspUrl.pathname + encode.strtob64u(der);
            chai.request.execute(server).get(path)
                .set('X-Forwarded-For', ['103.224.182.249', '50.116.58.136'])
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                     expect(err).to.be.null;
                     expect(res).to.have.status(200);
                     res.header['content-type'].should.be.equal('application/ocsp-response');
                     expect(res.body).to.be.an.instanceof(Buffer);
                     const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                     const resp = new pkijs.OCSPResponse({ schema: asn1.result });
                     expect(res.body).to.have.lengthOf.above(5);
                     let dv = new DataView(resp.responseStatus.valueBlock.valueHex);
                     expect(dv.getUint8(0), 'response status success').to.equal(0x00);
                     done();
                });
        });
    });
    describe('RFC5019', function() {
        describe('HTTP response', function() {
            let httpResponse;
            before(function (done) {
                let request = { certificate, issuer, hash: 'sha1' };
                let der = createOcspQuery(request);
                let path = ocspUrl.pathname + encode.strtob64u(der);
                chai.request.execute(server).get(path)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                         expect(err).to.be.null;
                         expect(res).to.have.status(200);
                         res.header['content-type'].should.be.equal('application/ocsp-response');
                         expect(res.body).to.be.an.instanceof(Buffer);

                         const parser = new jsrsasign.KJUR.asn1.ocsp.OCSPParser();
                         let ocspResponse = parser.getOCSPResponse(res.body.toString('hex'));
                         expect(ocspResponse).not.to.be.null;
                         // console.log(`ocspResponse: ${JSON.stringify(ocspResponse, null, 4)}`);

                         httpResponse = res;

                         done();
                    });
            });
            /* RFC 5019 6.2 HTTP Proxies */
            it('does not include "Pragma: no-cache"', function() {
                let pragma = httpResponse.header['pragma'];
                if (pragma === undefined)
                    this.skip();
                rfc5019.http_proxies_no_cache = false;
                expect(pragma).not.to.match(/no-cache/i);
                rfc5019.http_proxies_no_cache = true;
            });
            it('contains a "Cache-Control" header', function() {
                let cacheControl = httpResponse.header['cache-control'];
                if (cacheControl === undefined)
                    this.skip();
                rfc5019.http_proxies_cache_control = false;
                let values = cacheControl.split(/, */);
                expect(values).to.include('public');
                expect(values).to.include('no-transform');
                expect(values).to.include('must-revalidate');
                // expect(values).to.include(/max-age=[1-9][0-9]*/);
                const max_age = values.find((value) => value.startsWith('max-age='));
                const n = Number.parseInt(max_age.substr(8))
                expect(n).to.be.a('number');
                expect(n).to.satisfy(Number.isInteger);
                rfc5019.http_proxies_cache_control = true;
            });
            it('does not include "Cache-Control: no-cache"', function() {
                let cacheControl = httpResponse.header['cache-control'];
                if (cacheControl === undefined)
                    this.skip();
                rfc5019.http_proxies_cache_control_no_cache = false;
                expect(cacheControl).not.to.match(/no-cache/i);
                rfc5019.http_proxies_cache_control_no_cache = true;
            });
            it('does not include "Cache-Control: no-store"', function() {
                let cacheControl = httpResponse.header['cache-control'];
                if (cacheControl === undefined)
                    this.skip();
                rfc5019.http_proxies_no_store = false;
                expect(cacheControl).not.to.match(/no-store/i);
                rfc5019.http_proxies_no_store = true;
            });
        });
        describe('OCSP response', function() {
            let httpResponse;
            let ocspResponse;
            before(function (done) {
                let request = { certificate, issuer, hash: 'sha1' };
                let der = createOcspQuery(request);
                let path = ocspUrl.pathname + encode.strtob64u(der);
                chai.request.execute(server).get(path)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                         expect(err).to.be.null;
                         expect(res).to.have.status(200);
                         res.header['content-type'].should.be.equal('application/ocsp-response');
                         expect(res.body).to.be.an.instanceof(Buffer);

                         const parser = new jsrsasign.KJUR.asn1.ocsp.OCSPParser();
                         ocspResponse = parser.getOCSPResponse(res.body.toString('hex'));
                         // console.log(`ocspResponse: ${JSON.stringify(ocspResponse, null, 4)}`);

                         httpResponse = res;

                         done();
                    });
            });
            /* RFC 5019 2.2.4 thisUpdate, nextUpdate, and producedAt */
            it('thisUpdate does not have fractional seconds', function() {
                let thisUpdate = ocspResponse.array[0].thisupdate;
                if (thisUpdate === undefined)
                    this.skip();
                rfc5019.this_update = false;
                expect(thisUpdate).to.match(/\d{14}Z/);
                rfc5019.this_update = true;
            });
            it('nextUpdate does not have fractional seconds', function() {
                rfc5019.next_update = false;
                expect(ocspResponse.array[0].nextupdate).to.match(/\d{14}Z/);
                rfc5019.next_update = true;
            });
            it('producedAt does not have fractional seconds', function() {
                let producedAt = ocspResponse.prodat;
                if (producedAt === undefined)
                    this.skip();
                rfc5019.produced_at = false;
                expect(producedAt).to.match(/\d{14}Z/);
                rfc5019.produced_at = true;
            });
            /* RFC 5019 6.2 HTTP Proxies */
            it('thisUpdate aligns with HTTP header "Last-Modified"', function() {
                let lastModified = httpResponse.header['last-modified'];
                if (lastModified === undefined)
                    this.skip();
                rfc5019.last_modified = false;
                lastModified = new Date(lastModified);

                let thisUpdate = jsrsasign.zulutodate(ocspResponse.array[0].thisupdate);
                expect(thisUpdate.toUTCString()).to.be.equal(lastModified.toUTCString());
                rfc5019.last_modified = true;
            });
            it('nextUpdate aligns with HTTP header "Expires"', function() {
                let expires = httpResponse.header['expires'];
                if (expires === undefined)
                    this.skip();
                expires = new Date(expires);
                let nextUpdate = ocspResponse.array[0].nextupdate;
                if (nextUpdate === undefined)
                    this.skip();
                rfc5019.expires = false;
                nextUpdate = jsrsasign.zulutodate(nextUpdate);
                expect(nextUpdate.getTime()).to.be.equal(expires.getTime());
                rfc5019.expires = true;
            });
            it('nextUpdate aligns with HTTP header "Cache-Control"', function() {
                let cacheControl = httpResponse.header['cache-control'];
                if (cacheControl === undefined)
                    this.skip();
                rfc5019.alignment = false;
                let values = cacheControl.split(/, */);
                const max_age = values.find((value) => value.startsWith('max-age='));
                const n = Number.parseInt(max_age.substr(8))
                expect(n).to.be.a('number');
                expect(n).to.satisfy(Number.isInteger);
                let nextUpdate = ocspResponse.array[0].nextupdate;
                if (nextUpdate === undefined)
                    this.skip();
                nextUpdate = jsrsasign.zulutodate(nextUpdate);
                let diff = Date.now() - nextUpdate.getTime();
                expect(n).to.be.greaterThan(diff);
                rfc5019.alignment = true;
            });
        });
    });
    describe('OCSP certificate', function() {
        let root;
        before(function(done) {
            chai.request.execute(server).get('/download/Root/ca.crt.cer')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    res.header['content-type'].should.be.equal('application/x-x509-ca-certificate');
                    expect(res.body).to.be.an.instanceof(Buffer);
                    let asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                    expect(asn1).not.to.be.null;
                    root = new pkijs.Certificate({ schema: asn1.result });
                    expect(root).not.to.be.null;
                    done();
            });
        });
        let asn1;
        let certificate;
        it('can be downloaded', function(done) {
            chai.request.execute(server).get('/download/Sub/ocsp.crt.pem')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res.header['content-type']).to.contain('application/x-x509-user-certificate');
                    expect(res.body).to.be.an.instanceof(Buffer);
                    const pem = res.body.toString('binary');
                    const b64 = pem.replaceAll(/(-{5}(BEGIN|END) CERTIFICATE-{5})[\n\r]/g, '');
                    const der = Buffer.from(b64, 'base64');
                    asn1 = asn1js.fromBER(helpers.toArrayBuffer(der));
                    expect(asn1).not.to.be.null;
                    done();
            });
        });
        it('can be parsed (x509)', function(done) {
            certificate = new pkijs.Certificate({ schema: asn1.result });
            expect(certificate).not.to.be.null;
            done();
        });
        it('has id-kp-OCSPSigning extended key usage', function(done) {
            for (let extn of certificate.extensions) {
                if (extn.extnID === '2.5.29.37') { // id-ce-extKeyUsage
                    expect(extn.critical).to.be.true;
                    for (let purpose of extn.parsedValue.keyPurposes) {
                        if (purpose == '1.3.6.1.5.5.7.3.9') // id-kp-OCSPSigning
                            done();
                    }
                }
            }
        });
        it('has id-pkix-ocsp-nocheck extension', function(done) {
            for (let extn of certificate.extensions) {
                if (extn.extnID === '1.3.6.1.5.5.7.48.1.5') { // id-pkix-ocsp-nocheck
                    expect(extn.extnValue.valueBlock.blockLength).to.equal(2);
                    let dv = new DataView(extn.extnValue.valueBlock.valueHex);
                    expect(dv.getUint8(0), 'asn.1 null').to.equal(0x05);
                    expect(dv.getUint8(1), 'asn.1 null').to.equal(0x00);

                    rfc6960['ocsp-nocheck'] = true;
                    done();
                }
            }
        });
        it('does not provide OCSP authority information access (AIA)', function() {
            for (let extn of certificate.extensions) {
                if (extn.extnID === '1.3.6.1.5.5.7.1.1') { // Authority Information Access
                    for (let ad of extn.parsedValue.accessDescriptions) {
                        expect(ad.accessMethod).not.to.equal('1.3.6.1.5.5.7.48.1'); // OCSP
                    }
                }
            }
        });
        it('does not provide CRL distribution points', function() {
            let cdp = certificate.extensions.find((extn) => extn.extnID === '2.5.29.31'); // CDP
            expect(cdp).to.be.undefined;
        });
        it('can be verified', function(done) {
            let trustedCerts = [ root, issuer ];
            for (let cert of [/* root, issuer, certificate */]) {
                console.log('-----BEGIN CERTIFICATE-----\r\n'
                           + Buffer.from(cert.toSchema().toBER(), 'binary')
                                   .toString('base64')
                                   .replaceAll(/(.{64}(?=[^$]))/g, "$1\r\n") + '\r\n'
                           + '-----END CERTIFICATE-----');
            }
            let certs = [ root, issuer, certificate ];
            let ccve = new pkijs.CertificateChainValidationEngine({
                trustedCerts: trustedCerts,
                certs: certs
            });
            ccve.verify()
                .then((result) => {
                    expect(result).to.have.property('resultCode');
                    expect(result.resultCode).to.equal(0);
                    expect(result).to.have.property('result');
                    expect(result.result).to.be.true;
                    expect(result).to.have.property('resultMessage');
                    expect(result.resultMessage).to.equal('');
                    return true;
                })
                .catch((error) => {
                    console.error(error);
                    expect(error).to.be.null;
                    return false;
                });
            done();
            //expect(ccve.verify()).to.eventually.include({result: true}).notify(done);
            //expect(ccve.verify()).to.eventually.include({resultMessage: 'Ok'}).notify(done);
        });
    });
    describe('CAB Baseline Requirements Compliance', function() {
        let ocspResponse;

        describe('4.9.9 On-line revocation/status checking availability', function() {
            /* OCSP responses MUST conform to RFC6960 and/or RFC5019. */
            it('conforms to RFC6960 and/or RFC5019', function() {
                expect(conformsTo(rfc6960) || conformsTo(rfc5019)).to.be.true;
            });
            /* OCSP responses MUST either:
              1. Be signed by the CA that issued the Certificates whose
                 revocation status is being checked, or
              2. Be signed by an OCSP Responder whose Certificate is signed by
                 the CA that issued the Certificate whose revocation status is
                 being checked.

            In the latter case, the OCSP signing Certificate MUST contain an
            extension of type id-pkix-ocsp-nocheck, as defined by RFC6960. */
            it('OCSP signing certificate has id-pkix-ocsp-nocheck extension', function() {
                expect(rfc6960['ocsp-nocheck']).to.be.true;
            });
        });
        describe('4.9.10 On-line revocation checking requirements', function() {
            /* OCSP responders operated by the CA SHALL support the HTTP GET
               method, as described in RFC 6960 and/or RFC 5019. */
            it('supports HTTP GET', function(done) {
                let request = { certificate, issuer, hash: 'sha1' };
                let der = createOcspQuery(request);
                let path = ocspUrl.pathname + encode.strtob64u(der);
                chai.request.execute(server).get(path)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                         expect(err).to.be.null;
                         expect(res).to.have.status(200);
                         res.header['content-type'].should.be.equal('application/ocsp-response');
                         expect(res.body).to.be.an.instanceof(Buffer);
                         expect(res.body).to.have.lengthOf.above(5);

                         const parser = new jsrsasign.KJUR.asn1.ocsp.OCSPParser();
                         ocspResponse = parser.getOCSPResponse(res.body.toString('hex'));
                         expect(ocspResponse.array[0].status.status).to.be.equal('good');

                         done();
                    });
            });
            /* The validity interval of an OCSP response is the difference in
            time between the thisUpdate and nextUpdate field, inclusive. For
            purposes of computing differences, a difference of 3,600 seconds
            shall be equal to one hour, and a difference of 86,400 seconds
            shall be equal to one day, ignoring leap-seconds.

            For the status of Subscriber Certificates:

            Prior to 2020-09-30: The CA SHALL update information provided
            via an Online Certificate Status Protocol at least every four
            days. OCSP responses from this service MUST have a maximum
            expiration time of ten days.

            Effective 2020-09-30:
               1. OCSP responses MUST have a validity interval greater than
                  or equal to eight hours;
               2. OCSP responses MUST have a validity interval less than or
                  equal to ten days;
               3. For OCSP responses with validity intervals less than
                  sixteen hours, then the CA SHALL update the information
                  provided via an Online Certificate Status Protocol prior to
                  one- half of the validity period before the nextUpdate.
               4. For OCSP responses with validity intervals greater than or
                  equal to sixteen hours, then the CA SHALL update the
                  information provided via an Online Certificate Status
                  Protocol at least eight hours prior to the nextUpdate, and
                  no later than four days after the thisUpdate. */
             it('validity period is greater than or equal to eight hours', function() {
                 let thisUpdate = jsrsasign.zulutodate(ocspResponse.array[0].thisupdate);
                 let nextUpdate = jsrsasign.zulutodate(ocspResponse.array[0].nextupdate);
                 const eightHours = 8 * 60 * 60 * 1000;

                 expect(nextUpdate.getTime() - thisUpdate.getTime()).to.be.above(eightHours - 1);
             });
             it('validity period is less than or equal to ten days', function() {
                 let thisUpdate = jsrsasign.zulutodate(ocspResponse.array[0].thisupdate);
                 let nextUpdate = jsrsasign.zulutodate(ocspResponse.array[0].nextupdate);
                 const tenDays = 10 * 24 * 60 * 60 * 1000;

                 expect(nextUpdate.getTime() - thisUpdate.getTime()).to.be.below(tenDays + 1);
             });

             /* For the status of Subordinate CA Certificates:
               • The CA SHALL update information provided via an Online
                 Certificate Status Protocol
                   i. at least every twelve months; and
                  ii. within 24 hours after revoking a Subordinate CA
                      Certificate.

             If the OCSP responder receives a request for the status of a
             certificate serial number that is “unused”, then the responder
             SHOULD NOT respond with a “good” status. If the OCSP responder
             is for a CA that is not Technically Constrained in line with
             Section 7.1.5, the responder MUST NOT respond with a “good”
             status for such requests. */

             it('does not answers "unused" with good', function(done) {
                let request = { serialNumber: new asn1js.Integer({value: '12345678'}), issuer, hash: 'sha1' };
                let der = createOcspQuery(request);
                let path = ocspUrl.pathname + encode.strtob64u(der);
                chai.request.execute(server).get(path)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                         expect(err).to.be.null;
                         expect(res).to.have.status(200);
                         res.header['content-type'].should.be.equal('application/ocsp-response');
                         expect(res).to.have.property('body');
                         expect(res.body).to.be.an.instanceof(Buffer);

                         const parser = new jsrsasign.KJUR.asn1.ocsp.OCSPParser();
                         let ocspResponse = parser.getOCSPResponse(res.body.toString('hex'));
                         if (ocspResponse.resstatus === 0) // ok
                             expect(ocspResponse.array[0].status.status).to.be.equal('unknown');
                         else // unauthorized
                             expect(ocspResponse.resstatus).to.be.equal(6);
                         done();
                    });
             });
             /* The CA SHOULD monitor the OCSP responder for requests for
             “unused” serial numbers as part of its security response
             procedures. */
             it('provides a metric for "unused" serial numbers', function(done) {
                let user = process.env.METRICS_USERNAME || 'prometheus';
                let pass = process.env.METRICS_PASSWORD || 'password';
                chai.request.execute(server).get('/metrics')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(user, pass)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);

                        let metrics = res.body.toString('utf8');
                        // console.log(metrics);
                        expect(metrics).to.match(/ocsp_results_total{result="unknown"} ([1-9]\d*)/);
                                       // .and.capture(0).is.above(0);
                        done();
                    });
             });
             /* The OCSP responder MAY provide definitive responses about
             “reserved” certificate serial numbers, as if there was a
             corresponding Certificate that matches the Precertificate
             [RFC6962].

             A certificate serial number within an OCSP request is one of the
             following three options:
               1. “assigned” if a Certificate with that serial number has
                  been issued by the Issuing CA, using any current or previous
                  key associated with that CA subject; or
               2. “reserved” if a Precertificate [RFC6962] with that serial
                  number has been issued by
                    a. the Issuing CA; or
                    b. a Precertificate Signing Certificate [RFC6962]
                       associated with the Issuing CA; or
               3. “unused” if neither of the previous conditions are met.  */
        });
        describe('5.4 Audit logging procedures', function() {
            before(function() {
                if (!NodeRED.started)
                    this.skip();
            });
            describe('5.4.1 Types of events recorded', function() {
                /* The CA and each Delegated Third Party SHALL record events
                related to the security of their Certificate Systems,
                Certificate Management Systems, Root CA Systems, and Delegated
                Third Party Systems. The CA and each Delegated Third Party SHALL
                record events related to their actions taken to process a
                certificate request and to issue a Certificate, including all
                information generated and documentation received in connection
                with the certificate request; the time and date; and the
                personnel involved.

                The CA SHALL make these records available to its Qualified
                Auditor as proof of the CA’s compliance with these Requirements.

                The CA SHALL record at least the following events:
                1. CA certificate and key lifecycle events, including:
                  1. Key generation, backup, storage, recovery, archival, and
                     destruction;
                  2. Certificate requests, renewal, and re-key requests, and
                     revocation;
                  3. Approval and rejection of certificate requests;
                  4. Cryptographic device lifecycle management events;
                  5. Generation of Certificate Revocation Lists;
                  6. Signing of OCSP Responses (as described in Section 4.9 and
                     Section 4.10); and
                  7. Introduction of new Certificate Profiles and retirement of
                     existing Certificate Profiles.
                2. Subscriber Certificate lifecycle management events,
                   including:
                  1. Certificate requests, renewal, and re-key requests, and
                     revocation;
                  2. All verification activities stipulated in these
                     Requirements and the CA’s Certification Practice Statement;
                  3. Approval and rejection of certificate requests;
                  4. Issuance of Certificates;
                  5. Generation of Certificate Revocation Lists; and
                  6. Signing of OCSP Responses (as described in Section 4.9 and
                     Section 4.10).
                3. Security events, including:
                  1. Successful and unsuccessful PKI system access attempts;
                  2. PKI and security system actions performed;
                  3. Security profile changes;
                  4. Installation, update and removal of software on a
                     Certificate System;
                  5. System crashes, hardware failures, and other anomalies;
                  6. Firewall and router activities; and
                  7. Entries to and exits from the CA facility. */
                let logged_lines;
                let default_log;
                function save_log() {
                    logged_lines.push(format(...arguments).split('\n'));
                }
                before(function() {
                    logged_lines = [];
                    default_log = console.log;
                    console.log = save_log;
                    process.env.OCSP_LOG_RESPONSES = 'full';
                    process.env.OCSP_LOG_X_FORWARDED_FOR = 'true';
                });
                after(function() {
                    process.env.OCSP_LOG_RESPONSES = 'true';
                    console.log = default_log;
                    logged_lines = [];
                    delete process.env.OCSP_LOG_X_FORWARDED_FOR;
                    delete process.env.OCSP_LOG_RESPONSES;
                });

                let log_record;
                it('logs signing of OCSP responses', function(done) {
                    let request = { certificate, issuer, hash: 'sha1' };
                    let der = createOcspQuery(request);
                    chai.request.execute(server).post(ocspUrl.pathname)
                        .set('Content-Type', 'application/ocsp-request')
                        .set('X-Forwarded-For', '50.116.58.136')
                        .send(Buffer.from(der, 'binary'))
                        .buffer()
                        .parse(helpers.binaryParser)
                        .end(function(err, res) {
                             expect(err).to.be.null;
                             expect(res).to.have.status(200);
                             let infos = logged_lines.filter((line) => /\[info]/.test(line));
                             expect(infos).to.have.lengthOf.above(0);
                             log_record = infos.find((line) => /OCSP response] signed/.test(line));
                             expect(log_record).not.to.be.undefined;
                             expect(log_record).to.be.an('array');
                             expect(log_record).to.have.lengthOf.above(0);
                             done();
                        });
                });
                /* Log records MUST include the following elements:
                1. Date and time of event;
                2. Identity of the person making the journal record; and
                3. Description of the event. */
// '19 Feb 21:27:29 - [info] [function:OCSP response] signed with 1f7725881813116d8dce21eb9a1869a99b00f744 of 5a63f09db817563bd69fb28dd4446efa178437d0 state good for 50.116.58.136'
                describe('Log records', function() {
                    it('contain date and time', function() {
                        expect(log_record).not.to.be.undefined;
                        expect(log_record).to.be.an('array');
                        expect(log_record).to.have.lengthOf.above(0);
                        expect(log_record[0]).to.be.an('string');
                        expect(log_record[0]).to.have.lengthOf.above(14);
                        let date_and_time = log_record[0].substring(0, 15);
                        expect(date_and_time).to.match(/[1-3]?\d (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) ([01]?\d|2[0-4])(?::[0-5]\d){2}/);
                    });
                    it('contain identity', function() {
                        expect(log_record).to.match(/signed with [\da-f]{32}/);
                    });
                    it('contain description', function() {
                        expect(log_record).to.match(/OCSP response] signed/);
                    });
                });
            });
        });

        describe('7.3 OCSP profile', function() {
            /* Effective 2020-09-30, if an OCSP response is for a Root CA or
            Subordinate CA Certificate, including Cross Certificates, and that
            certificate has been revoked, then the revocationReason field
            within the RevokedInfo of the CertStatus MUST be present.

            Effective 2020-09-30, the CRLReason indicated MUST contain a value
            permitted for CRLs, as specified in Section 7.2.2. */
            describe('7.3.1 Version number(s)', function() {
                it('has valid CRLReason', function() {
                    for (let singleResponse of ocspResponse.array) {
                        if (singleResponse.status.status === 'revoked')
                            expect(singleResponse.status.reason).not.to.be.oneOf(['unspecified', 'onHold']);
                    }
                });
            });
            describe('7.3.2 OCSP extensions', function() {
            /* The singleExtensions of an OCSP response MUST NOT contain the
            reasonCode (OID 2.5.29.21) CRL entry extension. */
                it('does not have singleExtensions reasonCode (OID 2.5.29.21)', function() {
                    for (let singleResponse of ocspResponse.array) {
                        let found = singleResponse.ext?.find((ext) => ext.extname === 'cRLReason');
                        expect(found).to.be.undefined;
                    }
                });
            });
        });
    });
});

