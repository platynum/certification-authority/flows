import tls from 'node:tls';
import net from 'node:net';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import helpers from './lib/helpers.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('node-red', function() {
    describe('provides', function() {
        describe('http download', function() {
           let downloads = [
               {"url": "/download/Root/ca.crt.pem"},
               {"url": "/download/Root/ca.crt.cer"},
               {"url": "/download/Root/tsa.crt.pem"},
               {"url": "/download/Sub/ca.crt.pem"},
               {"url": "/download/Sub/ca.crt.cer"},
               {"url": "/download/Sub/ocsp.crt.pem"}
           ];
           for (let {url} of downloads) {
               it(`should provide ${url}`, function(done) {
                   chai.request.execute(server).get(url)
                       .end(function(err, res) {
                           expect(err).to.be.null;
                           expect(res).to.have.status(200);
                           done();
                   });
               });
           }
       });
    });
});

describe('nodejs', function() {
    const downloads = [
        {"url": "/download/Root/ca.crt.pem"},
        {"url": "/download/Root/tsa.crt.pem"},
        {"url": "/download/Sub/ca.crt.pem"},
        {"url": "/download/Sub/ocsp.crt.pem"}
    ];
    for (let { url } of downloads) {
        it(`can parse ${url}`, function(done) {
            chai.request.execute(server).get(url)
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res.header['content-type']).to.match(/application\/x-x509-(ca|user)-certificate/);
                    let pem = res.body.toString();
                    expect(pem).to.match(/^-{5}BEGIN CERTIFICATE-{5}\r\n/);
                    expect(pem).to.match(/-{5}END CERTIFICATE-{5}\r\n$/);
                    let secureContext = tls.createSecureContext({cert: pem});
                    let secureSocket = new tls.TLSSocket(new net.Socket(), { secureContext });
                    let cert = secureSocket.getCertificate();
                    secureSocket.destroy();

                    expect(cert.serialNumber).to.have.lengthOf(40);

                    done();
            });
        });
    }
});
