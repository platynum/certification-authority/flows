= platynum Certification Practice Statement (CPS)

## 1. Introduction

### 1.1. Overview

This is a combined Certificate Policy (CP) and Certfication Practice Statement
(CPS) outlined according to Request for Comments (RFC) 3647 [RFC3647].

### 1.2. Document Name and Identification

This document is named “platynum Certification Practice Statement (CPS)” and
can be referenced via Object Identifier (OID) 1.3.6.1.4.1.61013.1.3. The
revision equals the effective date and is denoted as YYYY-MM-DD, i.e.
2023-02-20 for the 20th February 2023.

| Revision / Effective date | Changes                                         |
| :-----------------------: | :---------------------------------------------- |
|        2023-02-20         | Changed document OID
|        2024-02-20         | Updated contact email
|        2024-12-22         | Format changes

This document can be freely re-distributed under CC BY-SA 4.0 [CC BY-SA].

### 1.3. PKI Participants

### 1.4. Certificate Usage

### 1.5. Policy Administration

The responsible for the drafting, registering, maintaining, and updating of
this CP/CPS can be reached under:

    platynum
    pki-policy@platynum.ch
    https://platynum.ch/

### 1.6. Definitions and Acronyms

 - CPS: Certfication Practice Statement
 - CA: Certificate Authority
 - CP: Certificate Policy
 - CRL: Certificate Revocation List
 - I&A: Identification and Authentication
 - OID: Object Identifier
 - OCSP: Online Certificate Status Protocol
 - RA: Registration Authority
 - RFC: Request for Comments

## 2. Publication and Repository Responsibilities

## 3. Identification and Authentication (I&A)

### 3.1. Naming

### 3.2. Initial Identity Validation

### 3.3. I&A for Re-key Requests

### 3.4. I&A for Revocation Requests

## 4. Certificate Life-Cycle Operational Requirements

### 4.1. Certificate Application

### 4.2. Certificate Application Processing

### 4.3. Certificate Issuance

### 4.4. Certificate Acceptance

### 4.5. Key Pair and Certificate Usage

### 4.6. Certificate Renewal

### 4.7. Certificate Re-key

### 4.8. Certificate Modification

## 5. Certificate Revocation and Suspension

### 5.1. Certificate Status Services

### 5.2. End of Subscription

### 5.3. Key Escrow and Recovery

### 5.4. Facility, Management, and Operational Controls

### 5.5. Physical Security Controls

### 5.6. Procedural Controls

### 5.7. Personnel Controls

### 5.8. Audit Logging Procedures

### 5.9. Records Archival

### 5.10. Key Changeover

### 5.11. Compromise and Disaster Recovery

### 5.12. CA or RA Termination

## 6. Technical Security Controls

### 6.1. Key Pair Generation and Installation

### 6.2. Private Key Protection and Cryptographic Module Engineering Controls

### 6.3. Other Aspects of Key Pair Management

### 6.4. Activation Data

### 6.5. Computer Security Controls

### 6.6. Life Cycle Security Controls

### 6.7. Network Security Controls

### 6.8. Timestamping

## 7. Certificate, CRL, and OCSP Profiles

### 7.1. Certificate Profile

### 7.2. CRL Profile

Certificate Revocation List (CRL)

### 7.3. OCSP Profile

Online Certificate Status Protocol (OCSP)

## 8. Compliance Audit and Other Assessment

## 9. Other Business and Legal Matters

### 9.1. Fees

### 9.2. Financial Responsibility

### 9.3. Confidentiality of Business Information

### 9.4. Privacy of Personal Information

### 9.5. Intellectual Property Rights

### 9.6. Representations and Warranties

### 9.7. Disclaimers of Warranties

### 9.8. Limitations of Liability

### 9.9. Indemnities

### 9.10. Term and Termination

### 9.11. Individual notices and communications with participants

### 9.12. Amendments

### 9.13. Dispute Resolution Procedures

### 9.14. Governing Law

### 9.15. Compliance with Applicable Law

### 9.16. Miscellaneous Provisions

### 9.17. Other Provisions

No Stipulation

# Bibliography

 - RFC3647: S. Chokhani, et al., Internet X.509 Public Key Infrastructure Certificate Policy and Certification Practices Framework, 2003,
 - CC BY-SA: Attribution-ShareAlike 4.0 International, 2015,

