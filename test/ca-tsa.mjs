import crypto from 'node:crypto';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import pkijs from 'pkijs';
pkijs.setEngine('Node.JS', crypto, new pkijs.CryptoEngine({
    name: 'Node.JS',
    crypto: crypto,
    subtle: crypto.subtle
}));
import asn1js from 'asn1js';
import pvutils from 'pvutils';
import jsrsasign from 'jsrsasign';

import * as helpers from './lib/helpers.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

function createTimestampQuery(request) {

    const hashes = {
        'sha1':   '1.3.14.3.2.26',
        'sha256': '2.16.840.1.101.3.4.2.1',
        'sha384': '2.16.840.1.101.3.4.2.2',
        'sha512': '2.16.840.1.101.3.4.2.3',
        'sha224': '2.16.840.1.101.3.4.2.4'
    };
    const hashLen = {
        'sha1': 20,
        'sha256': 32,
        'sha384': 48,
        'sha512': 64,
        'sha224': 28
    };

    const fictionBuffer = new ArrayBuffer(hashLen[request.imprintHash]);
    const fictionView = new Uint8Array(fictionBuffer);
    for (let j=0; j<hashLen[request.imprintHash]; j++)
        fictionView[`${j}`] = (j + 1) % 256;

    const tsq = new pkijs.TimeStampReq();
    tsq.version = 1;
    tsq.messageImprint = new pkijs.MessageImprint({
        hashAlgorithm: new pkijs.AlgorithmIdentifier({
            algorithmId: hashes[request.imprintHash]
        }),
        hashedMessage: new asn1js.OctetString({ valueHex: fictionBuffer })
    });

    if (request.policy !== undefined)
        tsq.reqPolicy = request.policy;
    if (request.reqCertificates)
        tsq.certReq = request.reqCertificates;
    if (request.withNonce) {
        const nonceBuffer = new ArrayBuffer(20);
        const nonceView = new Uint8Array(nonceBuffer);
        for (let j=0; j<20; j++)
            nonceView[`${j}`] = (j + 1) % 256;
        tsq.nonce = new asn1js.Integer({ valueHex: nonceBuffer });
    }

    // console.log(tsq.toJSON());
    const buf = tsq.toSchema().toBER();
    return Buffer.from(pvutils.arrayBufferToString(buf), 'binary');
}

describe('Time stamping authority (TSA)', function() {
    before(function () {
        process.env.TSA_POLICIES = "1.1.0";
        process.env.TSA_HASH = "sha512";
    });
    it('answers requests with timeStampTokens', function(done) {
        let request = { reqCertificates: false, withNonce: false, imprintHash: 'sha1' };
        let der = createTimestampQuery(request);
        chai.request.execute(server)
            .post('/tsa/Root/')
            .set('Content-Type', 'application/timestamp-query')
            .send(der)
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                res.header['content-type'].should.be.equal('application/timestamp-reply');
                expect(res.body).to.be.an.instanceof(Buffer);
                expect(res.body).to.have.lengthOf.above(5);
                const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                const tsr = new pkijs.TimeStampResp({ schema: asn1.result });
                expect(tsr.status.status).to.equal(0);
                expect(tsr.timeStampToken.contentType).to.equal('1.2.840.113549.1.7.2');
                done();
            });
    });
    it('emits certificates', function(done) {
        let request = { reqCertificates: true, withNonce: true, imprintHash: 'sha1' };
        let der = createTimestampQuery(request);
        chai.request.execute(server)
            .post('/tsa/Root/')
            .set('Content-Type', 'application/timestamp-query')
            .send(der)
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                res.header['content-type'].should.be.equal('application/timestamp-reply');
                expect(res.body).to.be.an.instanceof(Buffer);
                expect(res.body).to.have.lengthOf.above(5);
                const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                const tsr = new pkijs.TimeStampResp({ schema: asn1.result });
                expect(tsr.status.status).to.equal(0);
                done();
          });
    });
    it('supports nonces', function(done) {
        let request = { reqCertificates: false, withNonce: true, imprintHash: 'sha1' };
        let der = createTimestampQuery(request);
        chai.request.execute(server)
            .post('/tsa/Root/')
            .set('Content-Type', 'application/timestamp-query')
            .send(der)
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                res.header['content-type'].should.be.equal('application/timestamp-reply');
                expect(res.body).to.be.an.instanceof(Buffer);
                expect(res.body).to.have.lengthOf.above(5);
                const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                const tsr = new pkijs.TimeStampResp({ schema: asn1.result });
                expect(tsr.status.status).to.equal(0);
                done();
          });
    });
    it('serves correct timeStampTokens', function(done) {
        let request = { reqCertificates: true, withNonce: true, imprintHash: 'sha1' };
        let der = createTimestampQuery(request);
        chai.request.execute(server)
            .post('/tsa/Root/')
            .set('Content-Type', 'application/timestamp-query')
            .send(der)
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                res.header['content-type'].should.be.equal('application/timestamp-reply');
                expect(res.body).to.be.an.instanceof(Buffer);
                expect(res.body).to.have.lengthOf.above(5);
                let hex = res.body.toString('hex');
                let parser = new jsrsasign.KJUR.asn1.tsp.TSPParser();
                let tsr = parser.getResponse(hex);
                // console.log(`tsr: ${JSON.stringify(tsr, null, 4)}`);

                expect(tsr.version).to.equal(3);
                expect(tsr.hashalgs[0]).to.equal('sha512');

                expect(tsr.econtent.type).to.equal('tstinfo');
                expect(tsr.econtent.content.serial.hex).to.have.lengthOf.above(39);
                expect(tsr.econtent.content.nonce.hex).to.equal('0102030405060708090a0b0c0d0e0f1011121314');
                expect(tsr.econtent.content.messageImprint.alg).to.equal('sha1');
                expect(tsr.econtent.content.messageImprint.hash).to.equal('0102030405060708090a0b0c0d0e0f1011121314');

                expect(tsr.certs.array).to.have.lengthOf.above(0);

                expect(tsr.sinfos.length).to.equal(1);
                expect(tsr.sinfos[0].hashalg).to.equal('sha512');

                expect(tsr.statusinfo.status).to.equal('granted');
                expect(tsr.statusinfo.statusstr[0]).to.equal('OK');
                done();
          });
    });

    it('supports policy 1.1.0', function(done) {
        let request = { reqCertificates: false, withNonce: false, imprintHash: 'sha1', policy: '1.1.0' };
        // PKI.js breaks default OID here: 1.3.6.1.4.32473.31337.256
        let der = createTimestampQuery(request);
        chai.request.execute(server)
            .post('/tsa/Root/')
            .set('Content-Type', 'application/timestamp-query')
            .send(der)
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                res.header['content-type'].should.be.equal('application/timestamp-reply');
                expect(res.body).to.be.an.instanceof(Buffer);
                expect(res.body).to.have.lengthOf.above(5);
                const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                const tsr = new pkijs.TimeStampResp({ schema: asn1.result });
                expect(tsr.status.status).to.equal(0);
                done();
          });
    });
    it('rejects policy 1.2.3', function(done) {
        let request = { reqCertificates: false, withNonce: false, imprintHash: 'sha1', policy: '1.2.3' };
        let der = createTimestampQuery(request);
        chai.request.execute(server)
            .post('/tsa/Root/')
            .set('Content-Type', 'application/timestamp-query')
            .send(der)
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                res.header['content-type'].should.be.equal('application/timestamp-reply');
                expect(res.body).to.be.an.instanceof(Buffer);
                expect(res.body).to.have.length(5);
                done();
          });
    });
    it('rejects GET request with 404', function(done) {
        chai.request.execute(server)
            .get('/tsa/Root/')
            .end(function(err, res) {
                 expect(err).to.be.null;
                 expect(res).to.have.status(404);
                 done();
            });
    });
    describe('supports', function() {
        const hashes = [
            'sha224', 'sha256', 'sha384', 'sha512', 'sha1'
        ];
        for (let name of hashes) {
            it(`messageImprint hash ${name}`, function(done) {
                let request = { withNonce: true, imprintHash: name };
                let der = createTimestampQuery(request);
                // console.log(`der: ${der.toString('hex')}`);
                chai.request.execute(server)
                    .post('/tsa/Root/')
                    .set('Content-Type', 'application/timestamp-query')
                    .send(der)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        res.header['content-type'].should.be.equal('application/timestamp-reply');
                        expect(res.body).to.be.an.instanceof(Buffer);
                        expect(res.body).to.have.lengthOf.above(5);
                        // console.log(`reply: ${res.body.toString('hex')}`);
                        const asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                        const tsr = new pkijs.TimeStampResp({ schema: asn1.result });
                        expect(tsr.status.status).to.equal(0);
                        // console.log(`tsr: ${JSON.stringify(tsr, null, 1)}`);
                        done();
                    });
            });
        }
    });
    it('does not accept an invalid TSA request', function(done) {
        crypto.randomBytes(1024, (err, buf) => {
            chai.request.execute(server)
                .post('/tsa/Root/')
                .set('Content-Type', 'application/timestamp-query')
                .send(buf)
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                 expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an.instanceof(Buffer);
                    expect(res.body.length).to.be.equal(5);
                    res.header['content-type'].should.be.equal('application/timestamp-reply');
                    done();
                })
        });
    });
});
