import fs from 'node:fs';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import jsrsasign from 'jsrsasign';

import acme from '../examples/acme-client/acme.mjs';
import * as httpServer from './fixture/http-server.mjs';
import * as NodeRED from './fixture/node-red.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('Attestation via ACME', function() {

    let account;
    let key;
    let csr;
    let attestation = {
        root: {},
        issuing: {}
    };
    let cert;
    before(function() {
        attestation.root.key = jsrsasign.KEYUTIL.generateKeypair("EC", "secp256r1");
        attestation.root.cert = new jsrsasign.KJUR.asn1.x509.Certificate({
            serial: {hex:"01020304050607"},
            issuer: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Root'},
            subject: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Root'},
            sigalg: "SHA256withECDSA",
            notbefore: "000101235959Z",
            notafter: "381231235959Z",
            ext: [
              {extname:"keyUsage", "critical": true, "names": ["keyCertSign","cRLSign"]},
              {extname:"basicConstraints", "cA": true, "pathLen": 1, "critical": true}
            ],
            sbjpubkey: attestation.root.key.pubKeyObj,
            cakey: attestation.root.key.prvKeyObj
        });
        attestation.root.cert.pem = attestation.root.cert.getPEM();
        try {
            fs.mkdirSync('Sub/attestation', {'mode': 0o755});
        }
        catch (error) {
            if (error.code !== "EEXIST")
                throw error;
        }
        fs.writeFileSync('Sub/attestation/mocha-attestation.crt.pem', attestation.root.cert.pem, 'utf8');
        // let x509 = new jsrsasign.X509();
        // x509.readCertPEM(attestation.root.cert.pem);
        // let result = x509.verifySignature(x509.getPublicKey());

        attestation.issuing.key = jsrsasign.KEYUTIL.generateKeypair("EC", "secp256r1");
        attestation.issuing.cert = new jsrsasign.KJUR.asn1.x509.Certificate({
            serial: {hex:"01020304050607"},
            issuer: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Root'},
            subject: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Issuing'},
            sigalg: "SHA256withECDSA",
            notbefore:"000101235959Z",
            notafter:"381231235959Z",
            sbjpubkey: attestation.issuing.key.pubKeyObj,
            cakey: attestation.root.key.prvKeyObj
        });
        attestation.issuing.cert.pem = attestation.issuing.cert.getPEM();
        // x509 = new jsrsasign.X509();
        // x509.readCertPEM(attestation.issuing.cert.pem);
        // result = x509.verifySignature(attestation.root.key.pubKeyObj);
    });
    before(function() {
        key = jsrsasign.KEYUTIL.generateKeypair("EC", "secp256r1");

        let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
            subject: {str: '/CN=localhost'},
            sbjpubkey: key.pubKeyObj,
            sigalg: "SHA256withECDSA",
            sbjprvkey: key.prvKeyObj
        });
        req.sign();
        csr = req.getPEM();
    });
    before(function() {
        cert = new jsrsasign.KJUR.asn1.x509.Certificate({
            serial: {hex:"01020304050607"},
            issuer: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Issuing'},
            subject: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Token-1234'},
            sigalg: "SHA256withECDSA",
            notbefore: "000101235959Z",
            notafter: "381231235959Z",
            sbjpubkey: key.pubKeyObj,
            cakey: attestation.issuing.key.prvKeyObj
        });
        attestation.chain = [
            attestation.issuing.cert.pem,
            cert.getPEM()
        ];
        // console.log(`attestation.chain: ${inspect(attestation.chain)}`);
    });
    before(async function() {
        let responder;
        if (httpServer.started)
            responder = new httpServer.ChallengeResponder;
        else
            responder = new acme.WebrootChallengeResponder('/usr/local/www/apache24/data');
        account = await acme.Account.create(`${server}/acme/Sub/directory`, [responder]);
    });

    it('allows nonRepudiation', function(done) {
        this.timeout(30*1000);
        let identifier = {type: 'dns', value: 'localhost'};
        account.getCertificate([identifier], undefined, undefined, csr, undefined, attestation.chain, (err, chain) => {
            if (err) throw err;

            let cert = chain[0];
            // console.log(`${cert}`);
            expect(cert.ca).to.be.false;

            let x509 = new jsrsasign.X509();
            x509.readCertPEM(cert.toString());
            let keyUsage = x509.getExtKeyUsage();
            expect(keyUsage.names).deep.to.equal(['nonRepudiation', 'keyAgreement']);
            let extKeyUsage = x509.getExtExtKeyUsage();
            // console.log(inspect(extKeyUsage.array));
            expect(extKeyUsage.array.sort()).deep.to.equal(['clientAuth', 'serverAuth']);
            done();
        });
    });

    describe('enforcing attestation', function() {
        before(function() {
            if (NodeRED.started)
                process.env.CA_ATTESTATION_MODE="required";
        });
        after(function() {
            if (NodeRED.started)
                process.env.CA_ATTESTATION_MODE=undefined;
        });
        it('rejects issuer mismatch', function(done) {
            this.timeout(30*1000);
            let tailCert = new jsrsasign.KJUR.asn1.x509.Certificate({
                serial: {hex:"01020304050607"},
                issuer: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Non-Issuer'},
                subject: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Token-1234'},
                sigalg: "SHA256withECDSA",
                notbefore: "000101235959Z",
                notafter: "381231235959Z",
                sbjpubkey: key.pubKeyObj,
                cakey: attestation.issuing.key.prvKeyObj
            });
            attestation.chain = [
                attestation.issuing.cert.pem,
                tailCert.getPEM()
            ];
            let identifier = {type: 'dns', value: 'localhost'};
            account.getCertificate([identifier], undefined, undefined, csr, undefined, attestation.chain, (err, chain) => {
                expect(chain).to.be.undefined;
                expect(err).not.to.be.null;
                expect(err.type).to.be.equal('urn:platynum:params:acme:error:attestation');
                done();
            });
        });

        it('rejects overlong chain', function(done) {
            this.timeout(30*1000);

            let tailKey = jsrsasign.KEYUTIL.generateKeypair("EC", "secp256r1");
            let tailCert = new jsrsasign.KJUR.asn1.x509.Certificate({
                serial: {hex:"01020304050607"},
                issuer: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Token-1234'},
                subject: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Fake-1234'},
                sigalg: "SHA256withECDSA",
                notbefore: "000101235959Z",
                notafter: "381231235959Z",
                sbjpubkey: tailKey.pubKeyObj,
                cakey: key.prvKeyObj
            });
            let chain = [
                attestation.issuing.cert.pem,
                cert.getPEM(),
                tailCert.getPEM(),
            ];
            let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                subject: {str: '/CN=localhost'},
                sbjpubkey: tailKey.pubKeyObj,
                sigalg: "SHA256withECDSA",
                sbjprvkey: tailKey.prvKeyObj
            });
            req.sign();
            let tailCsr = req.getPEM();
            // console.log(`chain: ${inspect(chain)}`);

            let identifier = {type: 'dns', value: 'localhost'};
            account.getCertificate([identifier], undefined, undefined, tailCsr, undefined, chain, (err, chain) => {
                expect(chain).to.be.undefined;
                expect(err).not.to.be.null;
                expect(err.type).to.be.equal('urn:platynum:params:acme:error:attestation');
                done();
            });
        });
        it('rejects CSR key mismatch', function(done) {
            this.timeout(30*1000);

            let tailKey = jsrsasign.KEYUTIL.generateKeypair("EC", "secp256r1");
            let chain = [
                attestation.issuing.cert.pem,
                cert.getPEM(),
            ];
            let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                subject: {str: '/CN=localhost'},
                sbjpubkey: tailKey.pubKeyObj,
                sigalg: "SHA256withECDSA",
                sbjprvkey: tailKey.prvKeyObj
            });
            req.sign();
            let tailCsr = req.getPEM();
            // console.log(`chain: ${inspect(chain)}`);

            let identifier = {type: 'dns', value: 'localhost'};
            account.getCertificate([identifier], undefined, undefined, tailCsr, undefined, chain, (err, chain) => {
                expect(chain).to.be.undefined;
                expect(err).not.to.be.null;
                expect(err.type).to.be.equal('urn:platynum:params:acme:error:attestation');
                done();
            });
        });
        it('rejects outdated certificate', function(done) {
            this.timeout(30*1000);

            let tailKey = jsrsasign.KEYUTIL.generateKeypair("EC", "secp256r1");
            let tailCert = new jsrsasign.KJUR.asn1.x509.Certificate({
                serial: {hex:"01020304050607"},
                issuer: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Issuing'},
                subject: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Token-1234'},
                sigalg: "SHA256withECDSA",
                notbefore: "000101235959Z",
                notafter: "201231235959Z",
                sbjpubkey: tailKey.pubKeyObj,
                cakey: attestation.issuing.key.prvKeyObj
            });
            let chain = [
                attestation.issuing.cert.pem,
                tailCert.getPEM(),
            ];
            let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                subject: {str: '/CN=localhost'},
                sbjpubkey: tailKey.pubKeyObj,
                sigalg: "SHA256withECDSA",
                sbjprvkey: tailKey.prvKeyObj
            });
            req.sign();
            let tailCsr = req.getPEM();
            // console.log(`chain: ${inspect(chain)}`);

            let identifier = {type: 'dns', value: 'localhost'};
            account.getCertificate([identifier], undefined, undefined, tailCsr, undefined, chain, (err, chain) => {
                expect(chain).to.be.undefined;
                expect(err).not.to.be.null;
                expect(err.type).to.be.equal('urn:platynum:params:acme:error:attestation');
                done();
            });
        });
        it('rejects future certificate', function(done) {
            this.timeout(30*1000);

            let tailKey = jsrsasign.KEYUTIL.generateKeypair("EC", "secp256r1");
            let tailCert = new jsrsasign.KJUR.asn1.x509.Certificate({
                serial: {hex:"01020304050607"},
                issuer: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Issuing'},
                subject: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Token-1234'},
                sigalg: "SHA256withECDSA",
                notbefore: "380101235959Z",
                notafter: "991231235959Z",
                sbjpubkey: tailKey.pubKeyObj,
                cakey: attestation.issuing.key.prvKeyObj
            });
            let chain = [
                attestation.issuing.cert.pem,
                tailCert.getPEM(),
            ];
            let req = new jsrsasign.KJUR.asn1.csr.CertificationRequest({
                subject: {str: '/CN=localhost'},
                sbjpubkey: tailKey.pubKeyObj,
                sigalg: "SHA256withECDSA",
                sbjprvkey: tailKey.prvKeyObj
            });
            req.sign();
            let tailCsr = req.getPEM();
            // console.log(`chain: ${inspect(chain)}`);

            let identifier = {type: 'dns', value: 'localhost'};
            account.getCertificate([identifier], undefined, undefined, tailCsr, undefined, chain, (err, chain) => {
                expect(chain).to.be.undefined;
                expect(err).not.to.be.null;
                expect(err.type).to.be.equal('urn:platynum:params:acme:error:attestation');
                done();
            });
        });
        it('rejects outdated CA certificate', function(done) {
            this.timeout(30*1000);

            let caCert = new jsrsasign.KJUR.asn1.x509.Certificate({
                serial: {hex:"01020304050607"},
                issuer: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Root'},
                subject: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Issuing'},
                sigalg: "SHA256withECDSA",
                notbefore:"000101235959Z",
                notafter:"201231235959Z",
                sbjpubkey: attestation.issuing.key.pubKeyObj,
                cakey: attestation.root.key.prvKeyObj
            });
            let chain = [
                caCert.getPEM(),
                cert.getPEM()
            ];
            // console.log(`chain: ${inspect(chain)}`);

            let identifier = {type: 'dns', value: 'localhost'};
            account.getCertificate([identifier], undefined, undefined, csr, undefined, chain, (err, chain) => {
                expect(chain).to.be.undefined;
                expect(err).not.to.be.null;
                expect(err.type).to.be.equal('urn:platynum:params:acme:error:attestation');
                done();
            });
        });
        it('rejects future CA certificate', function(done) {
            this.timeout(30*1000);

            let caCert = new jsrsasign.KJUR.asn1.x509.Certificate({
                serial: {hex:"01020304050607"},
                issuer: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Root'},
                subject: {str: '/C=ES/L=Barcelona/O=My.Corp/CN=Issuing'},
                sigalg: "SHA256withECDSA",
                notbefore: "380101235959Z",
                notafter: "991231235959Z",
                sbjpubkey: attestation.issuing.key.pubKeyObj,
                cakey: attestation.root.key.prvKeyObj
            });
            let chain = [
                caCert.getPEM(),
                cert.getPEM()
            ];
            // console.log(`chain: ${inspect(chain)}`);

            let identifier = {type: 'dns', value: 'localhost'};
            account.getCertificate([identifier], undefined, undefined, csr, undefined, chain, (err, chain) => {
                expect(chain).to.be.undefined;
                expect(err).not.to.be.null;
                expect(err.type).to.be.equal('urn:platynum:params:acme:error:attestation');
                done();
            });
        });
    });
});
