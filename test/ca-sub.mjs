import fs from 'node:fs';
import url from 'node:url';
// import { inspect } from 'node:util';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import pkijs from 'pkijs';
import asn1js from 'asn1js';
import jsrsasign from 'jsrsasign';

import * as helpers from './lib/helpers.mjs';

import { Crypto } from "@peculiar/webcrypto";
const webcrypto = new Crypto();
pkijs.setEngine("newEngine", webcrypto, new pkijs.CryptoEngine({ name: "", crypto: webcrypto, subtle: webcrypto.subtle }));
//console.log(pkijs.getEngine());

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('Certification Authority (CA)', function() {

    let certificate;
    let issuer;
    let cdps = [];
    let crls = [];
    let der;
    let asn1;

    describe('Sub CA certificate', function() {
        it('is available via http', function(done) {
            chai.request.execute(server).get("/download/Sub/ca.crt.cer")
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    res.header['content-type'].should.be.equal('application/x-x509-ca-certificate');
                    expect(res.body).to.be.an.instanceof(Buffer);
                    der = res.body;
                    asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                    expect(asn1).not.to.be.null;
                    done();
                });
        });
        it('can be parsed (x509)', function(done) {
            certificate = new pkijs.Certificate({ schema: asn1.result });
            expect(certificate).not.to.be.null;
            done();
        });
        it('is a CA certificate', function(done) {
            for (let extn of certificate.extensions) {
                if (extn.extnID === '2.5.29.19') { // Basic Constraints
                    //expect(extn.parsedValue).to.be.a('basicconstraints');
                    expect(extn.parsedValue.cA).to.be.true;
                    done();
                }
            }
        });
        it('has a pathLen constraint greater zero', function(done) {
            for (let extn of certificate.extensions) {
                if (extn.extnID === '2.5.29.19') { // Basic Constrains
                    expect(extn.parsedValue).to.have.property('pathLenConstraint');
                    expect(extn.parsedValue.pathLenConstraint).to.be.greaterThan(-1);
                    done();
                }
            }
        });
        it('has keyUsage extension', function() {
            let extensions = certificate.extensions.map(extn => extn.extnID);
            expect(extensions).to.include('2.5.29.15');
        });
        /* TODO: check for keyCertSign crlSign */
        it('does not have a CT poison extension', function() {
            let poisoned = certificate.extensions.some((element) => element.extnID === '1.3.6.1.4.1.11129.2.4.3');
            expect(poisoned).to.be.false;
        });
        it('provides authority information access (AIA)', function(done) {
            for (let extn of certificate.extensions) {
                if (extn.extnID === '1.3.6.1.5.5.7.1.1') { // Authority Information Access
                    for (let ad of extn.parsedValue.accessDescriptions) {
                        if (ad.accessMethod === '1.3.6.1.5.5.7.48.2') { // CA Issuer
                            issuer = new URL(ad.accessLocation.value);
                        }
                    }
                }
            }
            expect(issuer).not.to.be.null;
            done();
        });
        it('provides CRL distribution points (CDP)', function(done) {
            for (let extn of certificate.extensions) {
                if (extn.extnID === '2.5.29.31') { // CRL Distribution Points
                    expect(extn.parsedValue.distributionPoints).to.be.an('array');
                    for (let cdp of extn.parsedValue.distributionPoints) {
                        //console.log(cdp.distributionPoint[0].value);
                        expect(cdp.distributionPoint[0].type).to.be.equal(6); // http
                        cdps.push({url: new URL(cdp.distributionPoint[0].value)});
                    }
                }
            }
            expect(cdps).to.have.lengthOf.above(0);
            done();
        });
        describe('Sub CA\'s issuer certificate', function() {
            let asn1;
            it('is accessible via http', function(done) {
                expect(issuer.protocol).to.equal('http:');
                done();
            });
            it('can be downloaded at AIA', function(done) {
                chai.request.execute(server).get(issuer.pathname)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        res.header['content-type'].should.be.equal('application/x-x509-ca-certificate');
                        expect(res.body).to.be.an.instanceof(Buffer);
                        asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                        expect(asn1).not.to.be.null;
                        done();
                    });
            });
            it('can be parsed (x509)', function(done) {
                issuer = new pkijs.Certificate({ schema: asn1.result });
                expect(issuer).not.to.be.null;
                done();
            });
        });
    });
    describe('CRL distribution points', function() {
        it('are accessible via old http CDP URI (DER)', function(done) {
            for (let cdp of cdps) {
                expect(cdp.url.protocol).to.be.equal('http:');
                chai.request.execute(server).get(cdp.url.pathname.replace(/\.crl$/, '/crl.der'))
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);

                        res.header['content-type'].should.be.equal('application/pkix-crl');
                        expect(res.body).to.be.an.instanceof(Buffer);
                        let asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                        expect(asn1).not.to.be.null;
                        crls.push({asn1: asn1});
                        if (cdps.length-1 === cdps.indexOf(cdp))
                            done();
                    });
            }
        });
        it('are accessible via http URI (PEM)', function(done) {
            for (const [i, cdp] of cdps.entries()) {
                expect(cdp.url.protocol).to.be.equal('http:');
                chai.request.execute(server).get(cdp.url.pathname.replace(/(\.crl|\/crl.der)$/, '/crl.pem'))
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);

                        res.header['content-type'].should.be.equal('application/pkix-crl');
                        expect(res.body).to.be.an.instanceof(Buffer);
                        const b64 = res.body.toString().replaceAll(/(-{5}(BEGIN|END) X509 CRL-{5})|[\n\r]/g, '');
                        const der = Buffer.from(b64, 'base64');
                        const asn1 = asn1js.fromBER(helpers.toArrayBuffer(der));
                        expect(asn1).not.to.be.null;
                        crls.push({asn1: asn1});
                        if (i === cdps.length-1)
                            done();
                    });
            }
        });
        it('are accessible via http CDP URI (DER)', function(done) {
            cdps.forEach((cdp, key, cdps) => {
                expect(cdp.url.protocol).to.be.equal('http:');
                chai.request.execute(server).get(cdp.url.pathname)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);

                        res.header['content-type'].should.be.equal('application/pkix-crl');
                        expect(res.body).to.be.an.instanceof(Buffer);
                        let asn1 = asn1js.fromBER(helpers.toArrayBuffer(res.body));
                        expect(asn1).not.to.be.null;
                        crls.push({asn1: asn1});
                        if (Object.is(cdps.length-1, key))
                            done();
                    });
            });
        });
    });

    describe('Certificate revocation lists (CRLs)', function() {
        let parsed = [];
        it('can be parsed', function(done) {
            for (let crl of crls) {
                let obj = new pkijs.CertificateRevocationList({ schema: crl.asn1.result });
                expect(obj).not.to.be.null;
                parsed.push(obj);
            }
            done();
        });
        it('are up to date', function(done) {
            for (let crl of parsed) {
                let d = new Date();
                expect(crl.thisUpdate.value, "thisUpdate lies in future").to.be.below(d);
                expect(crl.nextUpdate.value, "nextUpdate has passed").to.be.above(d);
            }
            done();
        });
        it('can be verified', async function() {
            let verifyObject = {
                issuerCertificate: issuer,
                publicKeyInfo: issuer.subjectPublicKeyInfo
            };
            for (let crl of parsed)
                expect(crl.verify(verifyObject)).to.eventually.be.true;
        });
    });
    describe('Adobe Approved Trust List (Verion 2.0)', function() {
        let cert;
        let httpsPEM;
        let httpsCert;

        before(function() {
            cert = new jsrsasign.X509();
            cert.readCertHex(der.toString('hex'));

            httpsPEM = fs.readFileSync(`Sub/https/https-EC.crt.pem`, 'utf8');
            httpsCert = new jsrsasign.X509();
            httpsCert.readCertPEM(httpsPEM);
        });

        describe('Requirements for Issuing CA certificates', function() {
            it('ICA1: differenciate certificates', function() {
                this.skip();
            })
            it('ICA2: is an X.509v3 certificate', function() {
                let param = cert.getParam();
                expect(param.version).to.be.equal(3);
            });
            it('ICA3: contains an organizationName', function() {
                let issuer = cert.getSubject();
                expect(issuer).not.to.be.undefined;
                expect(issuer.array).to.be.an('array');

                let organization = issuer.array.find(element => element[0].type === 'O');
                expect(organization).to.be.an('array');
                expect(organization).to.have.lengthOf.above(0);
                expect(organization[0]).to.have.property('value');
                expect(organization[0].value).to.have.lengthOf.above(0);
            });
            it('ICA4: uses a hardware security module that meet FIPS 140-2 level 3', function() {
                this.skip();
            });
            it('ICA5: identify the subscriber', function() {
                this.skip();
            });
            describe('ICA6', function() {
                it('(a): can revoke certificates', function(done) {
                    let isn = cert.getSerialNumberHex();
                    let csn = httpsCert.getSerialNumberHex();
                    let certificateRevoked = `Sub/certificates/${isn}/${csn}.revoked`;
                    // console.log(`Writing superseded into ${certificateRevoked}`);
                    fs.writeFileSync(certificateRevoked, 'superseded', 'utf8');

                    const aia = httpsCert.getExtAIAInfo();
                    let request = jsrsasign.KJUR.asn1.ocsp.OCSPUtil.getRequestHex(
                                      jsrsasign.hextopem(der.toString('hex'), 'CERTIFICATE'),
                                      httpsPEM,
                                      "sha384");
                    chai.request.execute(server).post(new url.URL(aia.ocsp[0]).pathname)
                        .set('Content-Type', 'application/ocsp-request')
                        .send(Buffer.from(request, 'hex'))
                        .buffer()
                        .parse(helpers.binaryParser)
                        .end(function(err, res) {
                            expect(err).to.be.null;
                            expect(res).to.have.status(200);
                            res.header['content-type'].should.be.equal('application/ocsp-response');
                            expect(res.body).to.be.an.instanceof(Buffer);

                            fs.unlinkSync(certificateRevoked);

                            let state = new jsrsasign.KJUR.asn1.ocsp.OCSPParser().getOCSPResponse(res.body.toString('hex'));
                            // console.log(`${inspect(state, {depth: 4})}`);
                            expect(state).not.to.be.undefined;
                            expect(state).to.have.property('array');
                            expect(state.array).to.be.an('array');
                            expect(state.array).to.have.lengthOf(1);
                            expect(state.array[0]).to.have.property('status');
                            expect(state.array[0].status).to.have.property('status');
                            expect(state.array[0].status.status).to.equal('revoked');
                            done();
                        });
                });
                it('(b): does not need a revocation service', function() {
                    this.skip();
                });
            });
            describe('ICA7', function() {
                it('(a): provides CDP and OCSP AIA', function() {
                    const cdp = httpsCert.getExtCRLDistributionPointsURI();
                    expect(cdp).not.to.be.undefined;
                    expect(cdp).to.be.an('array');
                    expect(cdp).to.have.lengthOf.above(0);

                    const aia = httpsCert.getExtAIAInfo();
                    expect(aia).not.to.be.undefined;
                    expect(aia).to.have.property('ocsp');
                    expect(aia.ocsp).to.be.an('array');
                    expect(aia.ocsp).to.have.lengthOf.above(0);
                });
                it('(b): has a specific dedicated policy OID', function() {
                    this.skip();
                });
            });
            it('ICA8: can revoke CA certificates', function() {
                const cdp = cert.getExtCRLDistributionPointsURI();
                expect(cdp).not.to.be.undefined;
                expect(cdp).to.be.an('array');
                expect(cdp).to.have.lengthOf.above(0);
            });
            it('ICA9: issuer key pair', function() {
                let key = cert.getPublicKey();
                if (key.type === 'EC') {
                    expect(key.curveName).oneOf(['secp256r1', 'secp384r1', 'secp521r1']);
                }
                else /* RSA */ {
                    expect(key.n.bitLength()).to.be.above(2048);
                }
                let param = cert.getParam();
                expect(param.sigalg).not.to.be.undefined;
                expect([
                    'SHA256withECDSA', 'SHA256withRSA', 'SHA256withRSAandMGF1',
                    'SHA384withECDSA', 'SHA384withRSA', 'SHA384withRSAandMGF1',
                    'SHA512withECDSA', 'SHA512withRSA', 'SHA512withRSAandMGF1'
                ]).to.include(param.sigalg);
            });
            it('ICA10: Certificate Authority Security Controls', function() {
                this.skip();
            });
        });
    });
    describe('Mozilla Root Store Policy', function() {
        describe('CA certificate', function() {
            let subCert;
            before(function() {
                subCert = new jsrsasign.X509();
                subCert.readCertHex(der.toString('hex'));
            });
            describe('RSA key', function() {
                let key;
                before(function() {
                    key = subCert.getPublicKey();
                    if (key.type === 'EC')
                        this.skip();
                });
                it('RSA modulus is at least 2048 bits', function() {
                    expect(key.n.bitLength()).to.be.above(2047);
                });
                it('RSA modulus is divisible by 8', function() {
                    expect(key.n.bitLength() % 8).to.be.equal(0);
                });
                describe('SubjectPublicKeyInfo', function() {
                    let spki;
                    before(function() {
                        spki = asn1.result['tbsCertificate.subjectPublicKeyInfo'];
                    });
                    it('has OID 1.2.840.113549.1.1.1', function() {
                        let oid = spki.algorithm.algorithm;
                        let hex = helpers.toHexString(oid.valueBeforeDecode);
                        expect(hex).to.be.equal('06092a864886f70d010101');
                    });
                    it('has a NULL parameter', function() {
                        let params = spki.algorithm.params;
                        let hex = helpers.toHexString(params.valueBeforeDecode);
                        expect(hex).to.be.equal('0500');
                    });
                    it('encoded AlgorithmIdentifier equals 300d06092a864886f70d0101010500', function() {
                        // let str = spki.algorithm.valueBeforeDecode.toString('hex');
                        let hex = helpers.toHexString(spki.algorithm.valueBeforeDecode);
                        expect(hex).to.be.equal('300d06092a864886f70d0101010500');
                    });
                    it('has not id-RSASSA-PSS OID (1.2.840.113549.1.1.10)', function() {
                        let oid = spki.algorithm.algorithm;
                        let hex = helpers.toHexString(oid.valueBeforeDecode);
                        expect(hex).not.to.be.equal('06092a864886f70d01010a');
                    });
                });
            });
            describe('ECDSA key', function() {
                let key;
                before(function() {
                    key = subCert.getPublicKey();
                    if (key.type !== 'EC')
                        this.skip();
                });
                describe('SubjectPublicKeyInfo', function() {
                    let spki;
                    before(function() {
                        spki = asn1.result['tbsCertificate.subjectPublicKeyInfo'];
                    });
                    it('has OID 1.2.840.10045.2.1', function() {
                        let oid = spki.algorithm.algorithm;
                        let hex = helpers.toHexString(oid.valueBeforeDecode);
                        expect(hex).to.be.equal('06072a8648ce3d0201');
                    });
                });
            });
        });
    });
    describe('360 Browser CA Policy', function() {
        describe('CA certificate', function() {
            let subCert;
            before(function () {
                subCert = new jsrsasign.X509();
                subCert.readCertHex(der.toString('hex'));
            });
            it('RSA modulus is greater than 1024 bits', function() {
                let key = subCert.getPublicKey();
                if (key.type === 'EC')
                    this.skip();
                expect(key.n.bitLength()).to.be.above(1024);
            });
            it('does not use SHA1', function() {
                let param = subCert.getParam();
                expect(param.sigalg).not.to.match(/^SHA1/);
            });
        });
        describe('end user certificate (https)', function() {
            let httpsCert;
            before(function() {
                const httpsPem = fs.readFileSync(`Sub/https/https-EC.crt.pem`, 'utf8');
                httpsCert = new jsrsasign.X509();
                httpsCert.readCertPEM(httpsPem);
            });
            it('contains valid OCSP URL', function() {
                const aia = httpsCert.getExtAIAInfo();
                expect(aia).not.to.be.undefined;
                expect(aia.ocsp).to.be.an('array');

                const ocspUri = new url.URL(aia.ocsp[0]);
                expect(ocspUri).not.to.be.undefined;
            });
            it('contains valid AIA URL', function() {
                const aia = httpsCert.getExtAIAInfo();
                expect(aia).not.to.be.undefined;
                expect(aia.caissuer).to.be.an('array');

                const issuerUri = new url.URL(aia.caissuer[0]);
                expect(issuerUri).not.to.be.undefined;
            });
            it('contains valid CRL URL', function() {
                const cdps = httpsCert.getExtCRLDistributionPoints();
                expect(cdps).not.to.be.undefined;
                expect(cdps.array).to.be.an('array');

                for (let cdp of cdps.array) {
                    expect(cdp).to.have.property('dpname');
                    expect(cdp.dpname).to.have.property('full');
                    expect(cdp.dpname.full).to.be.an('array');
                    const cdpUri = new url.URL(cdp.dpname.full[0].uri);
                    expect(cdpUri).not.to.be.undefined;
                }
            });
            it('has a validity shorter than 825 days', function() {
               let notBefore = httpsCert.getNotBefore();
               notBefore = jsrsasign.zulutodate(notBefore);
               let notAfter = httpsCert.getNotAfter();
               notAfter = jsrsasign.zulutodate(notAfter);

               const eighthundredTwentyfiveDays = 825 * 24 * 60 * 60 * 1000;
               let validity = notAfter.getTime() - notBefore.getTime();
               expect(validity).to.be.below(eighthundredTwentyfiveDays);
            });
        });
    });
});

