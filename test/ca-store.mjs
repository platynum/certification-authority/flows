import fs from 'node:fs';
import crypto from 'node:crypto';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import jsrsasign from 'jsrsasign';

import * as helpers from './lib/helpers.mjs';
import * as NodeRED from './fixture/node-red.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('Certificate Store', function() {
    let caCert;

    before(function(done) {
        chai.request.execute(server)
            .get("/download/Sub/ca.crt.cer")
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                res.header['content-type'].should.be.equal('application/x-x509-ca-certificate');
                expect(res.body).to.be.an.instanceof(Buffer);
                caCert = new jsrsasign.X509(res.body.toString('hex'));
                done();
            });
    });
    describe('certificate upload', function() {
        before(function() {
            if (!NodeRED.started)
                this.skip();
            process.env.CA_CERTIFICATE_UPLOAD_ALLOWED = 'true';
        });
        it('does not accept duplicate certificates', function(done) {
            let caSerial = caCert.getSerialNumberHex();
            let pem = fs.readFileSync('Sub/https/https-EC.crt.pem', 'utf8');
            let cert = new jsrsasign.X509();
            cert.readCertPEM(pem);
            let certSerial = cert.getSerialNumberHex();
            chai.request.execute(server)
                .post(`/upload/Sub/${caSerial}/${certSerial}.crt.cer`)
                .set('Content-Type', 'application/pkix-cert')
                .send(Buffer.from(cert.hex, 'hex'))
                .buffer()
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(204);
                    done();
                })
        });
        it('does not accept an invalid certificate', function(done) {
            let serial = caCert.getSerialNumberHex();
            crypto.randomBytes(1024, (err, buf) => {
                chai.request.execute(server)
                    .post(`/upload/Sub/${serial}/${serial}.crt.cer`)
                    .set('Content-Type', 'application/pkix-cert')
                    .send(buf)
                    .buffer()
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(403);
                        done();
                    })
            });
        });
        it('accepts a new certificate', function(done) {
            let caSerial = caCert.getSerialNumberHex();
            let pem = fs.readFileSync('Sub/https/https-RSA.crt.pem', 'utf8');
            let cert = new jsrsasign.X509();
            cert.readCertPEM(pem);
            let certSerial = cert.getSerialNumberHex();

            fs.unlink(`Sub/certificates/${caSerial}/${certSerial}.crt.pem`, (error) => {
                if (error) throw error;
                chai.request.execute(server)
                    .post(`/upload/Sub/${caSerial}/${certSerial}.crt.cer`)
                    .set('Content-Type', 'application/pkix-cert')
                    .send(Buffer.from(cert.hex, 'hex'))
                    .buffer()
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(201);
                        done();
                    });
            });
        });
    });
});
