import fs from 'node:fs';
import crypto from 'node:crypto';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import jsrsasign from 'jsrsasign';

import * as helpers from './lib/helpers.mjs';
import * as NodeRED from './fixture/node-red.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('Certificate Revocation List', function() {
    let caCert;
    let der;
    let crl;

    before(function(done) {
        chai.request.execute(server)
            .get("/download/Sub/ca.crt.cer")
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                res.header['content-type'].should.be.equal('application/x-x509-ca-certificate');
                expect(res.body).to.be.an.instanceof(Buffer);
                caCert = new jsrsasign.X509(res.body.toString('hex'));
                done();
            });
    });
    it('is available via http', function(done) {
        let serial = caCert.getSerialNumberHex();
        chai.request.execute(server)
            .get(`/download/Sub/${serial}/crl.der`)
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                res.header['content-type'].should.be.equal('application/pkix-crl');
                expect(res.body).to.be.an.instanceof(Buffer);
                der = res.body;
                done();
            });
    });
    it('can be parsed (X509CRL)', function() {
        let hex = der.toString('hex');
        crl = new jsrsasign.X509CRL(hex);
        expect(crl).not.to.be.null;
    });
    it('can be verified', function() {
        let key = caCert.getPublicKey();
        expect(crl.verifySignature(key)).to.be.true;
    });
    it('is up to date', function() {
        let now = new Date();
        let thisUpdate = crl.getThisUpdate();
        expect(jsrsasign.zulutodate(thisUpdate), "thisUpdate lies in future").to.be.below(now);
        let nextUpdate = crl.getNextUpdate();
        expect(jsrsasign.zulutodate(nextUpdate), "nextUpdate has passed").to.be.above(now);
    });
    describe('endpoint', function() {
        it('rejects invalid queries', function(done) {
            chai.request.execute(server)
                .get("/download/Sub/000102030405060708090a0b0c0d0e0f000102030405060708090a0b0c0d0e0f0001/crl.der")
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(404);
                    res.header['content-length'].should.be.equal('0');
                    done();
                });
        });
        describe('CRL upload', function() {
            let caKey;
            before(function() {
                if (!NodeRED.started)
                    this.skip();
                process.env.CRL_UPLOAD_ALLOWED = 'true';

                let serial = caCert.getSerialNumberHex();
                caKey = fs.readFileSync(`Sub/keys/${serial}/ca.priv.key.pem`, 'utf8');
                //let caKey = jsrsasign.KEYUTIL.getKey(pem);
            });
            it('does not accept current CRL', function(done) {
                let serial = caCert.getSerialNumberHex();
                chai.request.execute(server)
                    .post(`/upload/Sub/${serial}.crl`)
                    .set('Content-Type', 'application/pkix-crl')
                    .send(Buffer.from(crl.hex, 'hex'))
                    .buffer()
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(403);
                        done();
                    })
            });
            it('does not accept an invalid CRL', function(done) {
                let serial = caCert.getSerialNumberHex();
                crypto.randomBytes(1024, (err, buf) => {
                    chai.request.execute(server)
                        .post(`/upload/Sub/${serial}.crl`)
                        .set('Content-Type', 'application/pkix-crl')
                        .send(buf)
                        .buffer()
                        .end(function(err, res) {
                            expect(err).to.be.null;
                            expect(res).to.have.status(403);
                            done();
                        })
                });
            });
            it('does not accept CRL with smaller thisUpdate', function(done) {
                let serial = caCert.getSerialNumberHex();
                let param = crl.getParam();
                let thisUpdate = crl.getThisUpdate();
                let newThisUpdate = new Date(jsrsasign.zulutodate(thisUpdate).getTime() - 1000);
                let newCRL = new jsrsasign.KJUR.asn1.x509.CRL({
                    sigalg: param.sigalg,
                    issuer: {str: param.issuer.str},
                    thisupdate: jsrsasign.datetozulu(newThisUpdate, true),
                    nextupdate: crl.getNextUpdate(),
                    revcert: param.revcert,
                    ext: [
                    ],
                    cakey: caKey
                });
                chai.request.execute(server)
                    .post(`/upload/Sub/${serial}.crl`)
                    .set('Content-Type', 'application/pkix-crl')
                    .send(Buffer.from(newCRL.getEncodedHex(), 'hex'))
                    .buffer()
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(403);
                        done();
                    });
            });
            it('does not accept CRL with old cRLNumber', function(done) {
                let serial = caCert.getSerialNumberHex();
                let param = crl.getParam();
                let thisUpdate = crl.getThisUpdate();
                let newThisUpdate = new Date(jsrsasign.zulutodate(thisUpdate).getTime() + 1000);
                let newCRL = new jsrsasign.KJUR.asn1.x509.CRL({
                    sigalg: param.sigalg,
                    issuer: {str: param.issuer.str},
                    thisupdate: jsrsasign.datetozulu(newThisUpdate, true),
                    nextupdate: crl.getNextUpdate(),
                    revcert: param.revcert,
                    ext: [
                        {extname: 'cRLNumber', num: {'int': 0}}
                    ],
                    cakey: caKey
                });
                chai.request.execute(server)
                    .post(`/upload/Sub/${serial}.crl`)
                    .set('Content-Type', 'application/pkix-crl')
                    .send(Buffer.from(newCRL.getEncodedHex(), 'hex'))
                    .buffer()
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(403);
                        done();
                    });
            });
            it('accepts correct CRL', function(done) {
                let serial = caCert.getSerialNumberHex();
                let param = crl.getParam();

                let cRLNumber = param.ext.find((ext) => ext.extname === 'cRLNumber');
                if (cRLNumber) {
                    if (cRLNumber.num.hex)
                        cRLNumber.num.hex = (BigInt(`0x${cRLNumber.num.hex}`) + 1n).toString(16);
                    else if (cRLNumber.num.int)
                        cRLNumber.num = { hex: BigInt(Number.parseInt(cRLNumber.num.int) + 1n).toString(16) };
                    if (cRLNumber.num.hex.length % 2 === 1)
                        cRLNumber.num.hex = '0' + cRLNumber.num.hex;
                }

                let thisUpdate = crl.getThisUpdate();
                let newThisUpdate = new Date(jsrsasign.zulutodate(thisUpdate).getTime() + 1000);
                let newParam = {
                    sigalg: param.sigalg,
                    issuer: {str: param.issuer.str},
                    thisupdate: jsrsasign.datetozulu(newThisUpdate, true),
                    nextupdate: crl.getNextUpdate(),
                    revcert: param.revcert,
                    ext: param.ext,
                    cakey: caKey
                };
                let newCRL = new jsrsasign.KJUR.asn1.x509.CRL(newParam);
                chai.request.execute(server)
                    .post(`/upload/Sub/${serial}.crl`)
                    .set('Content-Type', 'application/pkix-crl')
                    .send(Buffer.from(newCRL.getEncodedHex(), 'hex'))
                    .buffer()
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(201);
                        done();
                    });
            });
        });
    });
    describe('360 Browser CA Policy', function() {
        it('does not use SHA-1', function() {
            let param = crl.getParam();
            expect(param.sigalg).not.to.match(/^sha1/i);
        });
    });
    describe('Mozilla Root Store Policy', function() {
        describe('5.1.3 SHA-1', function() {
            /* CAs MAY sign SHA-1 hashes over CRLs for root and intermediates
            only if the have issued SHA-1 certificates. */
            it('does not use SHA-1', function() {
                let param = crl.getParam();
                expect(param.sigalg).not.to.match(/^sha1/i);
            });
        });
        describe('6. Revocation', function() {
            /* For end-entity certificates, CRLs MUST be updated and reissued
            at least every seven days, and the value of the nextUpdate field
            MUST NOT be more than ten days beyond the value of the thisUpdate
            field. */
            it('are up to date', function() {
                let thisUpdate = jsrsasign.zulutodate(crl.getThisUpdate());
                let nextUpdate = jsrsasign.zulutodate(crl.getNextUpdate());
                let now = new Date();
                expect(now.getTime()).to.be.above(thisUpdate.getTime());
                expect(now.getTime()).to.be.below(nextUpdate.getTime());
            });
            it('is not older than 7 days', function() {
                let thisUpdate = jsrsasign.zulutodate(crl.getThisUpdate());
                let now = new Date();
                const issuedBefore = now.getTime() - thisUpdate.getTime();
                const sevenDays = 7 * 24 * 60 * 60 * 1000;
                expect(issuedBefore).to.be.below(sevenDays);
            });
            it('do not span more than 10 days', function() {
                let thisUpdate = jsrsasign.zulutodate(crl.getThisUpdate());
                let nextUpdate = jsrsasign.zulutodate(crl.getNextUpdate());
                const validity = nextUpdate.getTime() - thisUpdate.getTime();
                const tenDays = 10 * 24 * 60 * 60 * 1000;
                expect(validity).to.be.below(tenDays);
            });
        });
    });
});
