import fs from 'node:fs';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import nodeForge from 'node-forge';
const asn1 = nodeForge.asn1;
const pki = nodeForge.pki;
import jsrsasign from 'jsrsasign';

import * as helpers from './lib/helpers.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('Root certificate authority (CA)', function() {
    describe('Root certificate', function() {
        let rootDer;
        let rootAsn1;
        it('can be downloaded', function(done) {
            chai.request.execute(server).get('/download/Root/ca.crt.cer')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    res.header['content-type'].should.be.equal('application/x-x509-ca-certificate');
                    expect(res.body).to.be.an.instanceof(Buffer);
                    rootDer = res.body;
                    done();
                });
        });
        it('can be parsed (DER)', function(done) {
            rootAsn1 = asn1.fromDer(rootDer.toString('binary'));
            // console.log(caAsn1);
            expect(rootAsn1).not.to.be.null;
            done();
        });
        it('has issuer equal to subject', function() {
            let rootCert = new jsrsasign.X509();
            rootCert.readCertHex(rootDer.toString('hex'));
            let param = rootCert.getParam();
            expect(param.issuer.str).to.be.equal(param.subject.str);
        });
        describe('Policy compliance', function() {
            let rootCert;
            before(function() {
                rootCert = new jsrsasign.X509();
                rootCert.readCertHex(rootDer.toString('hex'));
            });
            describe('360 Browser CA Policy (Version 1.2)', function() {
                it('validity is more than 8 years from now', function() {
                    let notAfter = rootCert.getNotAfter();
                    notAfter = jsrsasign.zulutodate(notAfter);

                    let inEightYears = new Date(Date.now() + 8 * 365 * 24 * 60 * 60 * 1000);
                    expect(notAfter.getTime()).to.be.above(inEightYears.getTime());
                });
                it('validity is less than 25 years', function() {
                    let notBefore = rootCert.getNotBefore();
                    notBefore = jsrsasign.zulutodate(notBefore);
                    let notAfter = rootCert.getNotAfter();
                    notAfter = jsrsasign.zulutodate(notAfter);

                    let twentyFiveYears = 25 * 365 * 24 * 60 * 60 * 1000;
                    let validity = notAfter.getTime() - notBefore.getTime();
                    expect(validity).to.be.below(twentyFiveYears);
                });
                it('RSA modulus is greater than 1024 bits', function() {
                    let key = rootCert.getPublicKey();
                    if (key.type === 'EC')
                        this.skip();
                    expect(key.n.bitLength()).to.be.above(1024);
                });
                it('does not use SHA1', function() {
                    let param = rootCert.getParam();
                    expect(param.sigalg).not.to.match(/^SHA1/);
                });
            });
            describe('Mozilla Root Store Policy', function() {
                let key;
                before(function() {
                    key = rootCert.getPublicKey();
                });
                describe('Root certificate', function() {
                    it('has a serial number of at least 64 bits', function() {
                        let param = rootCert.getParam();
                        expect(param).to.have.property('serial');
                        expect(param.serial).to.have.property('hex');
                        let min = BigInt('0x8000000000000000');
                        let serial = BigInt(`0x${param.serial.hex}`);
                        expect(serial - min > 0n).to.be.true;
                        //expect(serial).to.be.above(min);
                    });
                });
                describe('RSA key', function() {
                    before(function() {
                        if (key.type === 'EC')
                            this.skip();
                    });
                    it('has modulus size is at least 2048 bits', function() {
                        expect(key.n.bitLength()).to.be.above(2047);
                    });
                    it('has modulus size is divisible by 8', function() {
                        expect(key.n.bitLength() % 8).to.be.equal(0);
                    });
                    describe('SubjectPublicKeyInfo', function() {
                        let spki;
                        before(function() {
                            spki = rootAsn1.value[0].value[6].value[0];
                        });
                        it('has OID 1.2.840.113549.1.1.1', function() {
                            expect(spki.value).to.be.an('array');
                            // OID
                            expect(spki.value[0].type).to.be.equal(asn1.Type.OID);
                            // value
                            let oid = asn1.derToOid(spki.value[0].value);
                            expect(oid).to.be.equal('1.2.840.113549.1.1.1');
                        });
                        it('has a NULL parameter', function() {
                            expect(spki.value).to.have.lengthOf(2);
                            expect(spki.value[1].type).to.be.equal(asn1.Type.NULL);
                            expect(spki.value[1].value).to.be.equal('');
                        });
                        it('encoded AlgorithmIdentifier equals 300d06092a864886f70d0101010500', function() {
                            // sequence -> 0x30
                            expect(spki.value).to.be.an('array');
                            // tag 0 -> 0x0a
                            expect(spki.value[0]).to.have.property('tagClass');
                            expect(spki.value[0].tagClass).to.be.equal(0);
                            // OID -> 0x06
                            expect(spki.value[0]).to.have.property('type');
                            expect(spki.value[0].type).to.be.equal(asn1.Type.OID);

                            // length -> 0x0d
                            let oid = Buffer.from(spki.value[0].value, 'binary').toString('hex');
                            expect(oid).to.be.equal('2a864886f70d010101');

                            // NULL -> 0x0500
                            expect(spki.value[1]).to.have.property('tagClass');
                            expect(spki.value[1].tagClass).to.be.equal(0);
                            expect(spki.value[1]).to.have.property('type');
                            expect(spki.value[1].type).to.be.equal(asn1.Type.NULL);
                            expect(spki.value[1].value).to.be.equal('');
                        });
                        it('has not id-RSASSA-PSS OID (1.2.840.113549.1.1.10)', function() {
                            expect(spki.value).to.be.an('array');

                            // OID
                            expect(spki.value[0].type).to.be.equal(asn1.Type.OID);
                            // value
                            let oid = asn1.derToOid(spki.value[0].value);
                            expect(oid).not.to.be.equal('1.2.840.113549.1.1.10');
                        });
                    });
                });
                describe('ECDSA key', function() {
                    before(function() {
                        if (key.type !== 'EC')
                            this.skip();
                    });
                    it('is using one of P-256/P-384', function() {
                        expect(key.curveName).to.be.oneOf(['secp256r1', 'secp384r1']);
                    });
                    describe('SubjectPublicKeyInfo', function() {
                        let spki;
                        before(function() {
                            spki = rootAsn1.value[0].value[6].value[0];
                        });
                        it('uses a named curve', function() {
                            expect(spki.value).to.be.an('array');
                            // OID
                            expect(spki.value[0].type).to.be.equal(asn1.Type.OID);
                        });
                        it('has OID 1.2.840.10045.2.1', function() {
                            // value
                            let oid = asn1.derToOid(spki.value[0].value);
                            expect(oid).to.be.equal('1.2.840.10045.2.1');
                        });
                        describe('AlgorithmIdentifier', function() {
                            it('matches 301306072a8648ce3d020106082a8648ce3d030107', function() {
                                if (key.curveName !== 'secp256r1')
                                    this.skip();
                                expect(spki.value).to.be.an('array');
                                expect(spki.value).to.have.lengthOf(2);
                                let algorithm = Buffer.from(spki.value[0].value, 'binary').toString('hex');
                                expect(algorithm).to.be.equal('2a8648ce3d0201');

                                let parameter = Buffer.from(spki.value[1].value, 'binary').toString('hex');
                                expect(parameter).to.be.equal('2a8648ce3d030107');
                            });
                            it('matches 301006072a8648ce3d020106052b81040022', function() {
                                if (key.curveName !== 'secp384r1')
                                    this.skip();
                                expect(spki.value).to.be.an('array');
                                expect(spki.value).to.have.lengthOf(2);
                                let algorithm = Buffer.from(spki.value[0].value, 'binary').toString('hex');
                                expect(algorithm).to.be.equal('2a8648ce3d0201');

                                let parameter = Buffer.from(spki.value[1].value, 'binary').toString('hex');
                                expect(parameter).to.be.equal('2b81040022');
                            });
                        });
                    });
                    describe('signature', function() {
                        let param;
                        before(function() {
                            param = rootCert.getParam();
                        });
                        it('uses P-256 with SHA-256', function() {
                            if (key.curveName !== 'secp256r1')
                                this.skip();
                            expect(param.sigalg).to.be.equal('SHA256withECDSA');
                        });
                        it('uses P-384 with SHA-384', function() {
                            if (key.curveName !== 'secp384r1')
                                this.skip();
                            expect(param.sigalg).to.be.equal('SHA384withECDSA');
                        });
                        describe('AlgorithmIdentifier', function() {
                            it('matches 300a06082a8648ce3d040302', function() {
                                if (param.sigalg !== 'SHA256withECDSA')
                                    this.skip();

                                expect(rootAsn1.value).to.be.an('array');
                                // sequence -> 0x30
                                expect(rootAsn1.value[1].value).to.be.an('array');
                                // tag 0 -> 0x0a
                                expect(rootAsn1.value[1].value[0]).to.have.property('tagClass');
                                expect(rootAsn1.value[1].value[0].tagClass).to.be.equal(0);
                                // OID -> 0x06
                                expect(rootAsn1.value[1].value[0]).to.have.property('type');
                                expect(rootAsn1.value[1].value[0].type).to.be.equal(asn1.Type.OID);
                                // length -> 0x08
                                // OID 1.2.840.10045.4.3.2 -> 0x2a8648ce3d040302
                                let oid = Buffer.from(rootAsn1.value[1].value[0].value, 'binary').toString('hex');
                                expect(oid).to.be.equal('2a8648ce3d040302');
                            });
                            it('matches 300a06082a8648ce3d040303', function() {
                                if (param.sigalg !== 'SHA384withECDSA')
                                    this.skip();
                                expect(rootAsn1.value).to.be.an('array');
                                // sequence -> 0x30
                                expect(rootAsn1.value[1].value).to.be.an('array');
                                // tag 0 -> 0x0a
                                expect(rootAsn1.value[1].value[0]).to.have.property('tagClass');
                                expect(rootAsn1.value[1].value[0].tagClass).to.be.equal(0);
                                // OID -> 0x06
                                expect(rootAsn1.value[1].value[0]).to.have.property('type');
                                expect(rootAsn1.value[1].value[0].type).to.be.equal(asn1.Type.OID);
                                // length -> 0x08
                                // OID 1.2.840.10045.4.3.3 -> 0x2a8648ce3d040303
                                let oid = Buffer.from(rootAsn1.value[1].value[0].value, 'binary').toString('hex');
                                expect(oid).to.be.equal('2a8648ce3d040303');
                            });
                            it('does not include a NULL parameter', function() {
                                expect(rootAsn1.value[1].value).to.be.an('array');
                                expect(rootAsn1.value[1].value).to.have.lengthOf(1);
                            });
                        });
                    });
                });
            });
            describe('Microsoft Trusted Root Program (09/02/2022)', function() {
                let key;
                before(function() {
                    key = rootCert.getPublicKey();
                });
                describe('3. Technical Requirements', function() {
                    describe('A. Root Requirements', function() {
                        it('is a x.509 v3 certificate', function() {
                            let param = rootCert.getParam();
                            expect(param.version).to.be.equal(3);
                        });
                        it('subject has a CN field', function() {
                            let param = rootCert.getParam();
                            let cn = param.subject.array.find(x => x[0].type == 'CN');
                            expect(cn).not.to.be.undefined;
                            expect(cn).to.have.length.above(0);
                            expect(cn[0].value).to.have.length.above(0);
                        });
                        it('basicConstraints extension has cA=true', function() {
                            let param = rootCert.getParam();
                            let bc = param.ext.find(ext => ext.extname == 'basicConstraints');
                            expect(bc).not.to.be.undefined;
                            expect(bc.cA).to.be.true;
                        });
                        it('has a keyUsage extension', function() {
                             let param = rootCert.getParam();
                             let ku = param.ext.find(ext => ext.extname == 'keyUsage');
                             expect(ku).not.to.be.undefined;
                        });
                        it('keyUsage extension is marked critical', function() {
                             let param = rootCert.getParam();
                             let ku = param.ext.find(ext => ext.extname == 'keyUsage');
                             expect(ku).to.have.property('critical');
                             expect(ku.critical).to.be.true;
                        });
                        it('keyUsage has keyCertSign and cRLSign bits set', function() {
                             let param = rootCert.getParam();
                             let ku = param.ext.find(ext => ext.extname == 'keyUsage');
                             expect(ku.names).to.include('keyCertSign');
                             expect(ku.names).to.include('cRLSign');
                        });
                        it('keyUsage has digitalSignature bit set', function() {
                             let param = rootCert.getParam();
                             let ku = param.ext.find(ext => ext.extname == 'keyUsage');
                             expect(ku.names).to.include('digitalSignature');
                        });
                        it('is self signed', function() {
                            let param = rootCert.getParam();
                            expect(param.issuer.str).to.be.equal(param.subject.str);
                            expect(rootCert.verifySignature(param.sbjpubkey)).to.be.true;
                        });
                        it('is valid for a minimum of 8 years', function() {
                            let notBefore = rootCert.getNotBefore();
                            notBefore = jsrsasign.zulutodate(notBefore);
                            let notAfter = rootCert.getNotAfter();
                            notAfter = jsrsasign.zulutodate(notAfter);

                            const eightYears = 8 * 365 * 24 * 60 * 60 * 1000;
                            let validity = notAfter.getTime() - notBefore.getTime();
                            expect(validity).to.be.above(eightYears);
                        });
                        it('is valid for a maximum of 25 years', function() {
                            let notBefore = rootCert.getNotBefore();
                            notBefore = jsrsasign.zulutodate(notBefore);
                            let notAfter = rootCert.getNotAfter();
                            notAfter = jsrsasign.zulutodate(notAfter);

                            const twentyfiveYears = 25 * 365 * 24 * 60 * 60 * 1000;
                            let validity = notAfter.getTime() - notBefore.getTime();
                            expect(validity).to.be.below(twentyfiveYears);
                        });
                        it('is not a 1024-bit RSA certificate', function() {
                            if (key.type === 'EC')
                                this.skip();
                            expect(key.n.bitLength()).to.be.above(1024);
                        });
                    });
                    describe('B. Signature Requirements', function() {});
                    describe('C. Revocation Requirements', function() {});
                    describe('D. Code Signing Root Certificate Requirements', function() {});
                    describe('E. Signature Requirements', function() {
                        it('has known ekus', function() {
                            let param = rootCert.getParam();
                            let eku = param.ext.find(ext => ext.extname == 'extKeyUsage');
                            expect(eku).to.be.undefined;
                        });
                    });
                });
            });
            describe('Adobe Approved Trust List (Verion 2.0)', function() {
                describe('Requirements for Upper level CA or Root CA certificate', function() {
                    it('RCA1: hierarchy', function() {
                        this.skip();
                    });
                    it('RCA2: certificates chain up', function() {
                        this.skip();
                    });
                    it('RCA3: is an X.509 V3 certificate', function() {
                        let param = rootCert.getParam();
                        expect(param.version).to.be.equal(3);
                    });
                    it('RCA4: contains an organizationName', function() {
                        let issuer = rootCert.getSubject();
                        expect(issuer).not.to.be.undefined;
                        expect(issuer.array).to.be.an('array');

                        let organization = issuer.array.find(element => element[0].type === 'O');
                        expect(organization).to.be.an('array');
                        expect(organization).to.have.lengthOf.above(0);
                        expect(organization[0]).to.have.property('value');
                        expect(organization[0].value).to.have.lengthOf.above(0);
                    });
                    it('RCA5: uses a hardware security module that meet FIPS 140-2 Level 3', function() {
                        this.skip();
                    });
                    it('RCA6: can revoke certificates', function() {
                        this.skip();
                    });
                    it('RCA7: can revoke own certificates', function() {
                        this.skip();
                    });
                    it('RCA8: key length and algorithm', function() {
                        let key = rootCert.getPublicKey();
                        if (key.type === 'EC') {
                            expect(key.curveName).oneOf(['secp384r1', 'secp521r1']);
                        }
                        else { // RSA
                            expect(key.n.bitLength()).to.be.above(3072);
                        }
                        let param = rootCert.getParam();
                        expect(param.sigalg).not.to.be.undefined;
                        expect([
                            'SHA256withECDSA', 'SHA256withRSA', 'SHA256withRSAandMGF1',
                            'SHA384withECDSA', 'SHA384withRSA', 'SHA384withRSAandMGF1',
                            'SHA512withECDSA', 'SHA512withRSA', 'SHA512withRSAandMGF1'
                        ]).to.include(param.sigalg);
                    });
                    it('RCA9: Certificate Authority Security Controls', function() {
                        this.skip();
                    });
                });
            });
        });
    });
    describe('Time stamping authority (TSA)', function() {
        let tsaPem;
        let tsaCertificate;
        let issuer;

        it('is available via filesystem', function(done) {
            tsaPem = fs.readFileSync('Root/tsa/tsa.crt.pem', 'utf8');
            expect(tsaPem).not.to.be.null;
            done();
        });
        it('can be parsed (x509)', function(done) {
            tsaCertificate = pki.certificateFromPem(tsaPem);
            // console.log(tsaCertificate);
            expect(tsaCertificate).not.to.be.null;
            done();
        });
        it('does not have a CT poison extension', function() {
            let poisoned = tsaCertificate.extensions.some((element) => element.extnID === '1.3.6.1.4.1.11129.2.4.3');
            expect(poisoned).to.be.false;
        });
        it('provides subject information access', function(done) {
            for (let extn of tsaCertificate.extensions) {
                if (extn.id === '1.3.6.1.5.5.7.1.11') { // subject information access
                    let obj = asn1.fromDer(extn.value);
                    let loc = obj.value[0].value[1].value;
                    let sia = new URL(loc);
                    expect(sia).not.to.be.null;
                    done();
                }
            }
        });
        it('provides authority information access', function(done) {
            for (let extn of tsaCertificate.extensions) {
                if (extn.name === 'authorityInfoAccess') {
                    //console.log(extn);
                    let obj = asn1.fromDer(extn.value);
                    //console.log(obj);
                    for (let entry of obj.value) {
                        for (let loc of entry.value) {
                            // check OID === '1.3.6.1.5.5.7.48.2') { // CA Issuer
                            if (loc.tagClass === 128) {
                                // console.log(loc.value);
                                issuer = new URL(loc.value);
                                expect(issuer.protocol).to.be.equal('http:');
                            }
                        }
                    }
                }
            }
            expect(issuer).not.to.be.null;
            done();
        });
        describe('It\'s issuer certificate', function() {
            let caDer;
            let caAsn1;
            let caCertificate;
            let caStore;

            it('is accessible via http', function(done) {
                expect(issuer.protocol).to.equal('http:');
                done();
            });
            it('can be downloaded at AIA', function(done) {
                chai.request.execute(server).get(issuer.pathname)
                        .buffer()
                        .parse(helpers.binaryParser)
                        .end(function(err, res) {
                            expect(err).to.be.null;
                            expect(res).to.have.status(200);
                            res.header['content-type'].should.be.equal('application/x-x509-ca-certificate');
                            expect(res.body).to.be.an.instanceof(Buffer);
                            caDer = res.body;
                            done();
                    });
            });
            it('can be parsed (DER)', function(done) {
                caAsn1 = asn1.fromDer(caDer.toString('binary'));
                // console.log(caAsn1);
                expect(caAsn1).not.to.be.null;
                done();
            });
            it('is an x509 certificate', function(done) {
                this.skip(); // <- Parse error in node-forge (not an RSA certificate)
                caCertificate = pki.certificateFromAsn1(caAsn1);
                expect(caCertificate).not.to.be.null;
                caStore = pki.createCaStore([ caCertificate ]);
                expect(caStore).not.to.be.null;
                done();
            });
        });
    });
    /* describe.only('Root certificate renewal', function() {
        it('can be triggered', function() {
            chai.request.execute(server).post('/admin/inject/8007.ca')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.have.status(200);
                        fs.watch('Root/certificates/', {recursive: true}, (eventType, filename) => {
                            done();
                        });
                    });
        });
    }); */
});
