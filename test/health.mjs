import chai from './fixture/chai.mjs';
const expect = chai.expect;

import * as helpers from './lib/helpers.mjs';
import * as NodeRED from './fixture/node-red.mjs';

const server = process.env.ACME_SERVER || 'http://localhost:1880';

describe('Liveness checks', function() {
    it('are provided', function(done) {
        process.env.HEALTH_USERNAME = undefined;
        process.env.HEALTH_PASSWORD = undefined;
        chai.request.execute(server).get('/ping')
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.be.json;
                expect(res).to.have.header('content-type', 'application/json; charset=utf-8');
                done();
            });
    });
    it('authentication is not required', function(done) {
        if (!NodeRED.started)
            this.skip();
        process.env.HEALTH_USERNAME = 'hippocrate';
        process.env.HEALTH_PASSWORD = 'password';
        chai.request.execute(server).get('/ping')
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.be.json;
                expect(res).to.have.status(200);
                let json = JSON.parse(res.body.toString('utf8'));
                expect(json).to.have.property('status');
                expect(json.status).to.equal('ok');
                done();
            });
    });
});

describe('Health checks', function() {
    it('are provided', function(done) {
        process.env.HEALTH_USERNAME = undefined;
        process.env.HEALTH_PASSWORD = undefined;
        chai.request.execute(server).get('/health')
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.be.json;
                expect(res).to.have.header('content-type', 'application/json; charset=utf-8');
                done();
            });
    });
    describe('authentication', function() {
        it('is not required', function(done) {
            if (!NodeRED.started)
                this.skip();
            chai.request.execute(server).get('/health')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.be.json;
                    expect(res).to.have.status(401);
                    let json = JSON.parse(res.body.toString('utf8'));
                    expect(json).to.have.property('status');
                    expect(json.status).to.equal('ok');
                    expect(json).not.to.have.property('ocsp');
                    done();
                });
        });
        describe('username', function() {
            it('is checked', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.HEALTH_USERNAME = 'hippocrate';
                let pass = process.env.HEALTH_PASSWORD = 'password';
                chai.request.execute(server).get('/health')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(`unknown-${user}`, pass)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.be.json;
                        expect(res).to.have.status(401);
                        let json = JSON.parse(res.body.toString('utf8'));
                        expect(json).to.have.property('status');
                        expect(json.status).to.equal('ok');
                        expect(json).not.to.have.property('ocsp');
                        done();
                    });
            });
            it('can be configured via HEALTH_USERNAME', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.HEALTH_USERNAME = 'hypocrétin';
                let pass = process.env.HEALTH_PASSWORD;
                chai.request.execute(server).get('/health')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(user, pass)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.be.json;
                        expect(res).to.have.status(200);
                        let json = JSON.parse(res.body.toString('utf8'));
                        expect(json).not.to.have.property('status');
                        expect(json).to.have.property('ocsp');
                        expect(json.ocsp).to.equal('ok');
                        done();
                    });
            });
        });
        describe('password', function() {
            it('is checked', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.HEALTH_USERNAME = 'hippocrate';
                let pass = process.env.HEALTH_PASSWORD = 'Quertziop';
                chai.request.execute(server).get('/health')
                    .auth(user, `${pass}-incorrect`)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.be.json;
                        expect(res).to.have.status(401);
                        let json = JSON.parse(res.body.toString('utf8'));
                        expect(json).to.have.property('status');
                        expect(json.status).to.equal('ok');
                        expect(json).not.to.have.property('ocsp');
                        done();
                    });
            });
            it('can be configured via HEALTH_PASSWORD', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.HEALTH_USERNAME;
                let pass = process.env.HEALTH_PASSWORD = 'Quertziop-';
                chai.request.execute(server).get('/health')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(user, pass)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.be.json;
                        expect(res).to.have.status(200);
                        let json = JSON.parse(res.body.toString('utf8'));
                        expect(json).not.to.have.property('status');
                        expect(json).to.have.property('ocsp');
                        expect(json.ocsp).to.equal('ok');
                        done();
                    });
            });
        });
    });
});

describe('Startup checks', function() {
    it('are provided', function(done) {
        process.env.HEALTH_USERNAME = undefined;
        process.env.HEALTH_PASSWORD = undefined;
        chai.request.execute(server).get('/startup')
            .buffer()
            .parse(helpers.binaryParser)
            .end(function(err, res) {
                expect(err).to.be.null;
                expect(res).to.be.json;
                expect(res).to.have.header('content-type', 'application/json; charset=utf-8');
                done();
            });
    });
    describe('authentication', function() {
        it('is not required', function(done) {
            if (!NodeRED.started)
                this.skip();
            chai.request.execute(server).get('/startup')
                .buffer()
                .parse(helpers.binaryParser)
                .end(function(err, res) {
                    expect(err).to.be.null;
                    expect(res).to.be.json;
                    expect(res).to.have.status(401);
                    let json = JSON.parse(res.body.toString('utf8'));
                    expect(json).to.have.property('status');
                    expect(json.status).to.equal('ok');
                    expect(json).not.to.have.property('https');
                    done();
                });
        });
        describe('username', function() {
            it('is checked', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.HEALTH_USERNAME = 'hippocrate';
                let pass = process.env.HEALTH_PASSWORD = 'password';
                chai.request.execute(server).get('/startup')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(`unknown-${user}`, pass)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.be.json;
                        expect(res).to.have.status(401);
                        let json = JSON.parse(res.body.toString('utf8'));
                        expect(json).to.have.property('status');
                        expect(json.status).to.equal('ok');
                        expect(json).not.to.have.property('https');
                        done();
                    });
            });
            it('can be configured via HEALTH_USERNAME', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.HEALTH_USERNAME = 'hypocrétin';
                let pass = process.env.HEALTH_PASSWORD;
                chai.request.execute(server).get('/startup')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(user, pass)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.be.json;
                        expect(res).to.have.status(200);
                        let json = JSON.parse(res.body.toString('utf8'));
                        expect(json).not.to.have.property('status');
                        expect(json).to.have.property('https');
                        expect(json.https).to.equal('ok');
                        done();
                    });
            });
        });
        describe('password', function() {
            it('is checked', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.HEALTH_USERNAME = 'hippocrate';
                let pass = process.env.HEALTH_PASSWORD = 'Quertziop';
                chai.request.execute(server).get('/startup')
                    .auth(user, `${pass}-incorrect`)
                    .buffer()
                    .parse(helpers.binaryParser)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.be.json;
                        expect(res).to.have.status(401);
                        let json = JSON.parse(res.body.toString('utf8'));
                        expect(json).to.have.property('status');
                        expect(json.status).to.equal('ok');
                        expect(json).not.to.have.property('https');
                        done();
                    });
            });
            it('can be configured via HEALTH_PASSWORD', function(done) {
                if (!NodeRED.started)
                    this.skip();
                let user = process.env.HEALTH_USERNAME;
                let pass = process.env.HEALTH_PASSWORD = 'Quertziop-';
                chai.request.execute(server).get('/startup')
                    .buffer()
                    .parse(helpers.binaryParser)
                    .auth(user, pass)
                    .end(function(err, res) {
                        expect(err).to.be.null;
                        expect(res).to.be.json;
                        expect(res).to.have.status(200);
                        let json = JSON.parse(res.body.toString('utf8'));
                        expect(json).not.to.have.property('status');
                        expect(json).to.have.property('https');
                        expect(json.https).to.equal('ok');
                        done();
                    });
            });
        });
    });
});
