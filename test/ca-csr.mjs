import fs from 'node:fs';

import chai from './fixture/chai.mjs';
const expect = chai.expect;

import nodeForge from 'node-forge';
const pki = nodeForge.pki;

describe('Generated certificate signing requests (CSR)', function() {
    describe('for Timestamping Authority (TSA)', function() {
        let csrPem;
        let csr;
        it('has been generated', function() {
            csrPem = fs.readFileSync(`Root/tsa/tsa.csr.pem`, 'utf8');
            expect(csrPem).not.to.be.null;
        });
        it('can be parsed (PEM)', function() {
            csr = pki.certificationRequestFromPem(csrPem);
            expect(csr).not.to.be.null;
        });
        it('can be verified', function() {
            expect(csr.verify()).to.be.true;
        });
    });
    describe('for http server EC certificate', function() {
        it('has been generated', function() {
            let csrPem = fs.readFileSync(`Sub/https/https-EC.csr.pem`, 'utf8');
            expect(csrPem).not.to.be.null;
        });
    });
    describe('for http server RSA certificate', function() {
        let csrPem;
        let csr;
        it('has been generated', function() {
            csrPem = fs.readFileSync(`Sub/https/https-RSA.csr.pem`, 'utf8');
            expect(csrPem).not.to.be.null;
        });
        it('can be parsed (PEM)', function() {
            csr = pki.certificationRequestFromPem(csrPem);
            expect(csr).not.to.be.null;
        });
        it('can be verified', function() {
            expect(csr.verify()).to.be.true;
        });
    });
    describe('for Admin Certificate Authority (CA)', function() {
        it('has been generated', function() {
            let cas = fs.readdirSync('Admin/keys');
            for (let ca of cas) {
                let csrPem = fs.readFileSync(`Admin/keys/${ca}/ca.csr.pem`, 'utf8');
                expect(csrPem).not.to.be.null;
            }
        });
    });
    describe('for Sub Certificate Authority (CA)', function() {
        it('has been generated', function() {
            let cas = fs.readdirSync('Sub/keys');
            for (let ca of cas) {
                let csrPem = fs.readFileSync(`Sub/keys/${ca}/ca.csr.pem`, 'utf8');
                expect(csrPem).not.to.be.null;
            }
        });
    });
    describe('for Root Certificate Authority (CA)', function() {
        it('has been generated', function() {
            let cas = fs.readdirSync('Root/keys');
            for (let ca of cas) {
                let csrPem = fs.readFileSync(`Root/keys/${ca}/ca.csr.pem`, 'utf8');
                expect(csrPem).not.to.be.null;
            }
        });
    });
});

