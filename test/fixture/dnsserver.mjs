import dns from 'node:dns';

import dns2 from 'dns2';
const { Packet } = dns2;

export var config = {};
config.records = [];

export const TYPE = dns2.TYPE;
export const CLASS = dns2.CLASS;

let server;
let resolvers;

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

export async function start() {
    config.records.push(
        {name: '_acme-challenge.my.corp', type: Packet.TYPE.TXT, data: 'xxx'},
        {name: 'www.my.corp', type: Packet.TYPE.A, address: '1.2.3.4'}
    );
    resolvers = dns.getServers();
    server = dns2.createServer({udp : true});
    server.on('request', (request, send /*, client */) => {
        const response = Packet.createResponseFromRequest(request);
        const [ question ] = request.questions;
        // console.log(`question: ${JSON.stringify(question)}`);
        const { name, type } = question;
        const records = config.records.filter(element => element.name == name
                                          && (element.type ?? Packet.TYPE.A) == type);
        for (let record of records) {
            // console.log(`record: ${JSON.stringify(record)}`);
            response.answers.push({
                name,
                type    : record.type ?? Packet.TYPE.A,
                class   : record.class ?? Packet.CLASS.IN,
                ttl     : record.ttl ?? 100,
                data    : record?.data,
                flags   : record?.flags,
                tag     : record?.tag,
                value   : record?.value,
                address : record?.address
            });
        }
        send(response);
    });
    dns.setServers(['127.0.0.1:5333', '[::1]:5333']);
    (async() => {
        const closed = new Promise(resolve => process.on('SIGUSR2', resolve));
        await server.listen({ udp : 5333 });
        // console.log(`dnsserver: ${JSON.stringify(server.addresses())}`);;
        await closed;
        await server.close();
        // console.log('Closed.');
    })();
    await delay(200);
}

export async function stop() {
    config.records = [];
    if (resolvers !== undefined) {
        dns.setServers(resolvers);
        resolvers = undefined;
        process.kill(process.pid, 'SIGUSR2');
        await delay(100);
    }
}
