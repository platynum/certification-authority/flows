import fs from 'node:fs';
import http from 'node:http';

import express from 'express';
import RED from 'node-red';

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
let server;

import {settings} from './node-red-settings.js';

export var started = false;

export async function mochaGlobalSetup() {

    // Create an Express app
    let app = express();

    // Add a simple route for static content served from 'public'
    app.use('/', express.static('public'));

    // Create a server
    server = http.createServer(app);

    // Initialise the runtime with a server and settings
    RED.init(server, settings);

    // Serve the editor UI
    app.use(settings.httpAdminRoot, RED.httpAdmin);

    // Serve the http nodes
    app.use(settings.httpNodeRoot, RED.httpNode);

    server.listen(1880);

    // Start the runtime
    RED.start();

    /* var runtimeStateEvents = 0;
    var promise = new Promise(function(resolve, reject) {
        RED.events.eventNames().forEach((e) => {
            RED.events.on(e, function(eventName) {
                if (eventName.id == 'runtime-state') {
                    runtimeStateEvents++;
                    console.log(`got ${runtimeStateEvents} event: ` + JSON.stringify(eventName));
                    if (runtimeStateEvents == 2) {
                        console.log('resolving promise');
                        resolve(app);
                    }
                }
            });
        });
    }); */

    do {
        await delay(5000);
        try {
            fs.accessSync('Sub/https/https-RSA.crt.pem', fs.constants.R_OK);

            fs.accessSync('Sub/https/https-EC.crt.pem', fs.constants.R_OK);

            let cas = fs.readdirSync('Sub/ocsp/');
            if (cas.length === 0)
                throw new Error(`Cannot find CA`);
            fs.accessSync(`Sub/ocsp/${cas[0]}/ocsp.crt.pem`, fs.constants.R_OK);

            started = true;
        }
        catch { started = false; }
    }
    while (!started);
}

export async function mochaGlobalTeardown() {
    RED.stop();
    await server.close();
    started = false;
}
