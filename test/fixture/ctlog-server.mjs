import http from 'node:http';
import crypto from 'node:crypto';
const { subtle } = crypto.webcrypto;
import fs from 'node:fs';
// import { inspect } from 'node:util';

import jsrsasign from 'jsrsasign';
import express from 'express';
import bodyParser from 'body-parser';

export var key = crypto.generateKeyPairSync('ec', {namedCurve: 'prime256v1'});
key.pkcs8 = key.privateKey.export({type: 'pkcs8', format: 'pem'});
key.der = key.publicKey.export({type: 'spki', format: 'der'});
key.jwk = key.publicKey.export({type: 'pkcs1', format: 'jwk'});
key.hash = crypto.createHash('sha256').update(key.der).digest('hex');
export var started = false;
export var sct_version = '00';
export var signature_type = '00';
export var extensions = {
    length: '0000',
    value: ''
};
export var requests = [];
export var id = `${Buffer.from(key.hash, 'hex').toString('base64')}`;

let server;
let app = express();
app.use(bodyParser.json());

export async function load_key(path) {
    key = {};
    key.pem = fs.readFileSync(path, 'utf8');
    const tmp = jsrsasign.KEYUTIL.getKey(key.pem);
    key.pkcs8 = jsrsasign.KEYUTIL.getPEM(tmp, 'PKCS8PRV');
    const b64 = key.pkcs8.replaceAll(/-{5}(BEGIN|END) PRIVATE KEY-{5}[\n\r]+/gm, '');
    const buf = Buffer.from(b64, 'base64');
    key.privateKey = await subtle.importKey('pkcs8', buf, {name: 'ECDSA', namedCurve: 'P-256'}, true, ['sign']);
    let jwk = await subtle.exportKey('jwk', key.privateKey);
    delete jwk.d, jwk.key_ops = ['verify'];
    key.publicKey = await subtle.importKey('jwk', jwk, {name: 'ECDSA', namedCurve: 'P-256'}, true, ['verify']);
    key.der = await subtle.exportKey('spki', key.publicKey);
    let spki = Buffer.from(key.der);
    key.hash = crypto.createHash('sha256').update(spki).digest('hex');
    key.jwk = await subtle.exportKey('jwk', key.publicKey);

    // console.log(`${inspect(key)}`);
    id = `${Buffer.from(key.hash, 'hex').toString('base64')}`;
}

function sign_log_entry(type, der) {

    const timestamp = Date.now();
    let timestampStr = timestamp.toString(16);
    while (timestampStr.length < 16)
        timestampStr = '0' + timestampStr;

    let data = Buffer.from(
        sct_version +       // SCT Version
        signature_type +    // SignatureType signature_type
        timestampStr +      // uint64 timestamp
        type +              // entry_type (0x0000 = certificate, 0x0001 = pre-certificate)
        der +               // (pre-)certificate length + value
        extensions.length + // CtExtensions extensions (zero-length array)
        extensions.value,
        'hex');
    // console.log(`data: ${data.toString('hex')}`);

    // Signature
    // key.privateKey.dsaEncoding = 'ieee-p1363';
    let signature = crypto.createSign('SHA256')
                          .update(data)
                          .end()
                          .sign(key.privateKey);
    // console.log(`signature: ${signature.toString('hex')}`);

    let sigHex = signature.toString('hex');
    if (sigHex.length % 2 == 1)
        sigHex = '0' + sigHex;
    let sigLen = `${signature.length.toString(16)}`;
    while (sigLen.length < 4)
        sigLen = '0' + sigLen;
    sigHex = '0403' + sigLen + sigHex;

    return {
        sct_version: Number.parseInt(sct_version, 16),
        timestamp,
        id: id,
        extensions: Buffer.from(extensions.value, 'hex').toString('base64'),
        signature: Buffer.from(sigHex, 'hex').toString('base64')
    }
}

function extract_tbs(x509) {
    let param = x509.getParam()
    // Delete poison extension
    for (let i=0; i<param.ext.length; i++) {
        if (param.ext[`${i}`].extname && param.ext[`${i}`].extname === '1.3.6.1.4.1.11129.2.4.3')
            param.ext.splice(i, 1)[0];
    }
    // console.log(`param: ${inspect(param, {depth: 4})}`);

    let tbs = new jsrsasign.asn1.x509.TBSCertificate();
    tbs.setByParam(param);

    return tbs.tohex();
}

function generate_precert_entry(issuer, certificate) {
    const hex = issuer.getPublicKeyHex();
    const issuer_key_hash = jsrsasign.KJUR.crypto.Util.hashHex(hex, 'sha256');
    // console.log(`issuer_key_hash: ${issuer_key_hash}`);

    const tbs = extract_tbs(certificate);
    // console.log(`tbs: ${tbs.toString('hex')}`);

    const tbsLen = tbs.length / 2;
    let tbsLenStr = tbsLen.toString(16);
    while (tbsLenStr.length < 6)
        tbsLenStr = '0' + tbsLenStr;

    return issuer_key_hash + tbsLenStr + tbs;
}

function generate_x509_entry(certificate) {
    const hex = certificate.tohex();

    const hexLen = hex.length / 2;
    let hexLenStr = hexLen.toString(16);
    while (hexLenStr.length < 6)
        hexLenStr = '0' + hexLenStr;

    return hexLenStr + hex;
}

app.post('/ct/v1/add-pre-chain', (request, response) => {
    // publish request for examination
    requests.push(request);

    // @todo: check chain length
    let cert = new jsrsasign.X509();
    cert.readCertHex(Buffer.from(request.body.chain[0], 'base64').toString('hex'));

    let issuer = new jsrsasign.X509();
    issuer.readCertHex(Buffer.from(request.body.chain[1], 'base64').toString('hex'));
    let issuer_key = issuer.getPublicKey();
    if (!cert.verifySignature(issuer_key))
        console.error(`Cannot verify signature of certificate`);
    // @todo: validate full chain
    // @todo: validate last cert via roots

    let signedTimestamp;
    let ext = cert.getExtInfo('1.3.6.1.4.1.11129.2.4.3');
    if (ext === undefined) {
        let x509_cert = generate_x509_entry(cert);
        signedTimestamp = sign_log_entry('0000', x509_cert);
    }
    else {
        let preCert = generate_precert_entry(issuer, cert);
        signedTimestamp = sign_log_entry('0001', preCert);
    }
    response.send(signedTimestamp);
});

export async function mochaGlobalSetup() {
    let port = 8081;
    server = http.createServer(app);
    server.unref();
    await server.listen(port, () => {
        console.log(`ctlog server listening on http://localhost:${port}`);
        started = true;
    });
}

export async function mochaGlobalTeardown() {
    requests = [];
    await server.close();
    console.log(`ctlog server stopped`);
    started = false;
}
