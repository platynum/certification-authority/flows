import http from 'node:http';
import process from 'node:process';
import { inspect } from 'node:util';

import express from 'express';

import * as acme from '../../examples/acme-client/acme.mjs';

export var started = false;
export var config = {};
config.challenge = "not-set";

let server;
let app = express();

app.get('/.well-known/acme-challenge/:token', (request, response) => {
    if (config.challenge === undefined) {
        response.status(404).send('undefined');
    } else if (config.challenge === null) {
        response.status(403).send('null');
    }
    else {
        response.send(config.challenge);
    }
});

export async function mochaGlobalSetup() {
    let port = (process.geteuid() === 0) ? 80 : 8080;
    server = http.createServer(app);
    server.listen(port, () => {
        // console.log(`http server listening on http://localhost:${port}`);
        started = true;
    });
    server.unref();
}

export class ChallengeResponder {
    constructor() {
        this.type = [ 'http-01' ];
        this.problems = [];
    }
    async solve(key, authorization, challenge) {
        console.log(`challenge: ${inspect(challenge)}`);
        let thumbprint = await acme.createJwkThumbprint(key.jwk);
        config.challenge = `${challenge.token}.${thumbprint}`;
        console.log(`prepared challenge: ${config.challenge}`);
    }
}

export async function mochaGlobalTeardown() {
    await server.close();
    console.log(`http server stopped`);
    started = false;
}
