import * as chaiModule from 'chai';
import chaiHttp from 'chai-http';
import chaiUrl from 'chai-url';
import chaiArrays from 'chai-arrays';
import chaiInteger from 'chai-integer';
import chaiAsPromised from 'chai-as-promised';
import chaiMatch from 'chai-match';
chaiModule.use(chaiAsPromised);
chaiModule.use(chaiMatch);
chaiModule.use(chaiInteger);
chaiModule.use(chaiArrays);
chaiModule.use(chaiUrl);
chaiModule.should();
const chai = chaiModule.use(chaiHttp);

export default chai;

