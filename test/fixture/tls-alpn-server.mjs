import tls from 'node:tls';
import net from 'node:net';

import ip6addr from 'ip6addr';
import jsrsasign from 'jsrsasign';

export var available = false;
export var config = {};
config.tlsVersion = 'TLSv1.2';
config.port = (process.geteuid() === 0) ? 443 : 8443;

let server;

export async function start(domain, challenge) {
    if (server) {
				// console.log("Stopping old server ...");
        await stop();
    }

    let keypair = jsrsasign.KEYUTIL.generateKeypair('EC', 'secp256r1');
    let params = {
        serial: 1,
        sigalg: {name: 'SHA256withECDSA'},
        issuer: {str: `/CN=${domain}`},
        notbefore: jsrsasign.datetozulu(new Date(Date.now())),
        notafter: jsrsasign.datetozulu(new Date(Date.now() + 1000 * 60 * 15)),
        subject: {str: `/CN=${domain}`},
        sbjpubkey: keypair.pubKeyObj,
        ext: [
            { extname: '1.3.6.1.5.5.7.1.31', critical: true, extn: { octstr: { hex: challenge } } }
        ],
        cakey: keypair.prvKeyObj
    };
    if (net.isIPv6(domain)) {
        let ip = ip6addr.parse(domain);
        let ipStr = ip.toString({format: 'v6'});
        params.ext.push({extname: 'subjectAltName', array: [{'ip': `${ipStr}`}] });
    }
    else if (net.isIPv4(domain)) {
        params.ext.push({extname: 'subjectAltName', array: [{'ip': `${domain}`}] });
    }
    else
        params.ext.push({extname: 'subjectAltName', array: [{'dns': `${domain}`}] });
    let cert = new jsrsasign.asn1.x509.Certificate(params);
    let pem = cert.getPEM();
    // console.log(`pem: ${pem}`);
    pem = pem.replaceAll('\r\n\r\n', '\r\n');

    let servername;
    if (net.isIPv4(domain)) {
        servername = domain.split('.').reverse().join('.') + '.in-addr.arpa';
    }
    else if (net.isIPv6(domain)) {
        let ip = ip6addr.parse(domain).toString({format: 'v6', zeroElide: false});
        let digits = [];
        ip.split(/[.:]/).map((word) => {
            let n = Number.parseInt(word, 16);
            digits.push((n - (n%256)) / 256, n%256);
        });
        servername = digits.reverse().join('.') + '.ip6.arpa';
    }
    else {
        servername = domain;
    }
    //console.log(`servername: ${servername}`);

    try {
        let options = {
            host: domain,
            cert: pem,
            key: jsrsasign.KEYUTIL.getPEM(keypair.prvKeyObj, 'PKCS1PRV'),
            port: config.port,
            servername: servername,
            minVersion: config.tlsVersion ?? 'TLSv1.2',
            maxVersion: config.tlsVersion ?? 'TLSv1.2',
            ALPNProtocols: ['acme-tls/1']
        };

        server = tls.createServer(options, (/* socket */) => {
            // console.log(`${options.minVersion} server created for ${options.ALPNProtocols[0]} with domain ${domain} and servername ${servername}`);
        });
        server.unref();
        server.listen(config.port, () => {
            // console.log(`https server listening on https://localhost:${config.port}`);
        });
    }
    catch (error) {
        console.error(`error: ${error}`);
        throw error;
    }
}

export async function stop() {
    if (server) {
        let tmp = server;
        server = undefined;
        await tmp.close();
        // console.log('tls-alpn-server closed');
    }
}

export async function mochaGlobalSetup() {
    available = true;
}

export async function mochaGlobalTeardown() {
    stop();
    available = false;
}

