
const process = require('node:process');

let settings = require('../../settings.js');
settings.flowFile = 'flows.json';
settings.editorTheme = { projects: { enable: false } };
settings.userDir = `${process.env.HOME}/.node-red`;

settings.httpAdminRoot = '/admin';
settings.disableEditor = false;
settings.httpNodeRoot = '/';

module.exports.settings = settings;

