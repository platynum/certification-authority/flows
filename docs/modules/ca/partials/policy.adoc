== Policies

There are a number of resources regarding policies for certification
authorities. The following are important, if your CA has been added into
official trust stores:

* https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/policy/[Mozilla Root Store Policy]
  footnote:[https://www.mozilla.org/en-US/about/governance/policies/security-group/certs/policy/]
* https://aka.ms/RootCert[Microsoft Trusted Root Program Requirements]
  footnote:[https://aka.ms/RootCert]
* https://www.apple.com/certificateauthority/ca_program.html[Apple Root Certificate Program]
  footnote:[https://www.apple.com/certificateauthority/ca_program.html]
  ** https://support.apple.com/en-us/HT205280[Apple's Certificate Transparency policy]
     footnote:[https://support.apple.com/en-us/HT205280]
* https://g.co/chrome/root-policy[Chrome Root Program]
  footnote:[https://g.co/chrome/root-policy]
  ** https://googlechrome.github.io/CertificateTransparency/[Certificate Transparency in Chrome]
     footnote:[https://googlechrome.github.io/CertificateTransparency/]
* https://caprogram.360.cn[360 Browser CA Policy]
  footnote:[https://caprogram.360.cn]
* https://www.oracle.com/java/technologies/javase/carootcertsprogram.html[Oracle Java Root Certificate program]
  footnote:[https://www.oracle.com/java/technologies/javase/carootcertsprogram.html]
* https://helpx.adobe.com/en/acrobat/kb/approved-trust-list2.html[Adobe Approved Trust List]
  footnote:[https://helpx.adobe.com/en/acrobat/kb/approved-trust-list2.html]

=== CAB (CA/Browser) Forum

- https://cabforum.org/baseline-requirements-documents/[Baseline Requirements Documents]
  footnote:[https://cabforum.org/baseline-requirements-documents/]

== Relevant RFCs

- https://tools.ietf.org/html/rfc2527[RFC 2527] (obsolete) - Internet X.509 Public Key Infrastructure / Certificate Policy and Certification Practices Framework footnote:[https://tools.ietf.org/html/rfc2527]
- https://tools.ietf.org/html/rfc3647[RFC 3647] - Internet X.509 Public Key Infrastructure / Certificate Policy and Certification Practices Framework footnote:[https://tools.ietf.org/html/rfc3647]

