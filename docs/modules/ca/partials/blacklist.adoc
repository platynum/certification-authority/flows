== Blacklist

The Certification Authority (CA) fosters a blacklist of keys, that cannot be
used anymore. For each blacklisted key exist an entry in the `blacklist/`
sub-directory.

Whenever an ACME account is tried to be registered or a Certification Signing
Request (CSR) is submitted, the blacklist is consulted. If the blacklist
contains the key, the request is denied.

When a certificate is revoked with reason `keyCompromise` the key is added
to the blacklist.

=== Manual entries

Additional entries to the blacklist, can be created using a command line tool.
This tool can read various PEM encoded file formats (CSR, X509v3, public and
private keys).  The commandline tool calculates a hash (SHA256) over the key
contained in the input files and copies it to the blacklist using the hash
as the filename.

[source,sh]
----
$ node bin/blacklist.mjs cert.pem
----
