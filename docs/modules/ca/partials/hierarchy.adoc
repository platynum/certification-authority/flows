== Certificate hierarchy

When the certification authority is started, the following certification
authorities and end entity certificates are generated automatically:

image::hierarchy.svg[]

CAUTION: All keys are stored unencrypted in your file system!

== Validity periods

=== Root Certification Authority

The following diagram illustrates the default certificate validity periods and
roll-overs. Beside the active CAs (Certification Authorities) and TSA (Time
Stamping Authority) signer certificates marked red, the respectively last
certificate is display in dark gray. The respectively next certificates from the
upcoming certification authority (CAs) are shown in blue.

[mermaid]
----
gantt
    dateFormat  YYYY-MM-DD
    axisFormat  %Y-%m
    title       Certificate validity periods and roll-overs

    section Root CA
    Root 2011 G1        :done,          root1, 2011-06-01,2026-05-30
    Root 2018 G2        :active, crit,  root2, 2018-06-01,2033-05-30
    Root 2025 G3        :               root3, 2025-06-01,2040-05-30

    section Admin CA
    Admin 2017 G3       :done,          admin3, 2017-06-01,2020-05-30
    Admin 2019 G4       :active, crit,  admin4, 2019-06-01,2022-05-30
    Admin 2021 G5       :active,        admin5, 2021-06-01,2024-05-30
    Admin 2023 G6       :active,        admin6, 2023-06-01,2026-05-30
    Admin 2025 G7       :               admin7, 2025-06-01,2028-05-30

    section Sub CA
    Sub 2017 G3         :done,          sub3, 2017-06-01,2020-05-30
    Sub 2019 G4         :active, crit,  sub4, 2019-06-01,2022-05-30
    Sub 2021 G5         :active,        sub5, 2021-06-01,2024-05-30
    Sub 2023 G6         :active,        sub6, 2023-06-01,2026-05-30
    Sub 2025 G7         :               sub7, 2025-06-01,2028-05-30

    section TSA Certificates
    TSA 2017 G7         :done,         tsa7, 2017-06-01,2024-05-30
    TSA 2018 G8         :active,       tsa8, 2018-06-01,2025-05-30
    TSA 2019 G9         :active,       tsa9, 2019-06-01,2026-05-30
    TSA 2020 G10        :active, crit, tsa10, 2020-06-01,2027-05-30
    TSA 2021 G11        :active,       tsa11, 2021-06-01,2028-05-30
    TSA 2022 G12        :active,       tsa12, 2022-06-01,2029-05-30
    TSA 2023 G13        :active,       tsa13, 2023-06-01,2030-05-30
    TSA 2024 G14        :active,       tsa14, 2024-06-01,2031-05-30
    TSA 2025 G15        :              tsa15, 2025-06-01,2032-05-30
----

=== Sub Certification Authority

[mermaid]
----
gantt
    dateFormat  YYYY-MM-DD
    axisFormat  %Y-%m-%d
    title       Certificate validity periods and roll-overs

    section Sub CA
    % notBefore of sub3 is 2017-06-01
    Sub 2017 G3         :done,          sub3, 2019-02-01,2020-05-30
    Sub 2019 G4         :active, crit,  sub4, 2019-06-01,2022-05-30
    % notAfter of sub5 is 2023-05-03
    Sub 2021 G5         :               sub5, 2021-06-01,2022-11-30

    section Subscriber Certificates
    CN=a         :done,         cert1, 2019-05-30,2020-05-29
    CN=b         :active,       cert2, 2019-06-01,2020-05-30
    CN=c         :active,       cert3, 2021-05-30,2022-05-29
    CN=d         :              cert4, 2021-06-01,2022-05-30
----

