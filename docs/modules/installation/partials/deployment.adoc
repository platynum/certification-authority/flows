== Deploy flows

Deploy flows to your `~/.node-red/` directory:

[source,sh]
----
$ cp flows_nodejs.json ~/.node-red/flows_$(hostname).json
----

