
`ACME_EMAIL_DKIM_VALIDATION`::
  If this is `true`, the server validates DKIM signatures of incoming email
  responses. All headers required by RFC8823 have to be signed then. When set
  to `strict` DKIM signatures have to include all headers that should be
  signed, too. By default the server relies on the mail server software to
  verify DKIM signatures and reject/discard unsigned messages, the validation
  is disabled.
`ACME_EMAIL_CHALLENGE`::
  If this is `true`, the `email-reply-00` challenge from RFC8823 will be
  available. This allows to issue certificates for `emailProtection`. By
  default the `email-reply-00` challenge is disabled.
`ACME_EMAIL_SMIME_RENEWAL`::
  Sets the period, after which the S/MIME signer certificate can be renewed in
  milliseconds.  Certificate renewal is disabled, when this set to `0`.
  This values should be less than the `ACME_EMAIL_SMIME_CERTIFICATE_VALIDITY`
  to ensure, that a new certificate is generated before the current certificate
  expires. The default renewal time is 14 days.
`ACME_EMAIL_SMIME_CERTIFICATE_VALIDITY`::
  Specifies the validity period of the S/MIME signer certificate in days, the
  default validity period is 90 days.

`ACME_IMAP_PASSWORD`::
  Password to login to `ACME_IMAP_SERVER` as `ACME_IMAP_USER`.
`ACME_IMAP_PORT`::
  The port number to use, when connecting to `ACME_IMAP_SERVER`. The default
  port is 993 for `imaps`.
`ACME_IMAP_TLS`::
  If Transport Layer Security (TLS) should be enabled IMAP connections. By
  default, this will be `false` if `ACME_IMAP_PORT` is 143 and `true`
  otherwise.
`ACME_IMAP_SERVER`::
  IMAP server to contact for incoming E-Mails (replies to `email-reply-00`
  challenges). There is no default value.
`ACME_IMAP_USER`::
  User to be used when loggin into `ACME_IMAP_SERVER`. There is no default
  value.
`ACME_IMAP_CONNECTION_TIMEOUT`::
  Number of milliseconds to wait for a connection to be established. By default
  the value from `node-imap` is used.
`ACME_IMAP_AUTHENTICATION_TIMEOUT`::
  Number of milliseconds to wait to be authenticated after a connection has
  been established. By default the value from `node-imap` is used.
`ACME_IMAP_CHECK_INTERVAL`::
  How often a new connection to the IMAP server is attempted in seconds. By
  default a new connection is made every minute.

`ACME_SMTP_FROM`::
  The `From:` used in outgoing emails when sending `email-reply-00` challenges.
  The default is `ACME S/MIME challenge <acme@${HOSTNAME}>`.
`ACME_SMTP_PASSWORD`::
  Password to login to `ACME_SMTP_SERVER` as `ACME_SMTP_USER`.
`ACME_SMTP_PORT`::
  The port number to use, when connecting to `ACME_SMTP_SERVER`. The default
  port is 587 for `submission`.
`ACME_SMTP_SERVER`::
  SMTP server to contact for sending emails (challenges for `email-reply-00`).
  There is no default value.
`ACME_SMTP_USER`::
  User to be used when logging into `ACME_SMTP_SERVER`. There is no default
  value.

