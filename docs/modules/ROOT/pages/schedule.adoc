== Upcoming releases

The https://gitlab.com/platynum/certification-authority/flows/-/releases[current
release] is `v2.0.x`. Patch versions changing the minor version number are
released occasionally. All packages
are available for https://gitlab.com/platynum/certification-authority/flows/-/packages[
download on gitlab.com].

The following figure visualizes the upcoming releases of the certification
authority.

[mermaid]
----
gantt
    dateFormat   YYYY-MM-DD
    axisFormat   %b
    title        Certification authority schedule
    % tickInterval 3month

    section Dev 1.0.0
    Development         :active, dev10, 2022-02-15,2022-01-04
    0.9.0               :milestone, rel09, after dev10, 1d
    Testing             :test, after rel09, 60d
    1.0.0               :milestone, rel10, after test, 1d
    Maintainence        :sup10, after rel10, 365d

    section Dev v2.x
    Development         :dev20, after rel10, 365d
    2.0.0               :milestone, rel20, after dev20, 1d
    Maintainence        :sup20, after rel20, 365d

    Development         :dev21, after rel20, 365d
    2.1.0               :milestone, rel21, after dev21, 1d
    Maintainence        :sup21, after rel21, 365d

    click rel09 href "https://gitlab.com/platynum/certification-authority/flows/-/releases"
    click rel10 href "https://gitlab.com/platynum/certification-authority/flows/-/milestones/1#tab-issues"
    click rel20 href "https://gitlab.com/platynum/certification-authority/flows/-/releases"
    click rel21 href "https://gitlab.com/platynum/certification-authority/flows/-/milestones/3#tab-issues"
----

== Past releases

* **Version v1.0.0**: First official release.
* **Version v0.9.0**: First feature complete version for testing.
