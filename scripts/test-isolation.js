#!/usr/bin/env node

const fs = require('node:fs');
const { inspect } = require('node:util');
const ivm = require('isolated-vm');

if (process.argv.length < 4) {
    console.log(`usage: ${process.argv[1]} <script> [<order.json>] <pem>`);
    process.exit(1);
}
let env = {};
for (let ev in process.env) {
    if (process.env.hasOwnProperty(ev) && /(^CA_|HOSTNAME$|^BASE_DN$)/.test(ev))
        env[`${ev}`] = `${process.env[ev]}`;
}
// console.log(`${inspect(env)}`);
let argv = [
    `${process.argv[2]}`,
    `${process.argv[2].replace(/.js$/i, '')}`
];
if (process.argv.length > 4) {
    const order = fs.readFileSync(process.argv[3], 'utf8');
    argv.push(order);
}
const pem = fs.readFileSync(process.argv[process.argv.length-1], 'utf8');
argv.push(pem);
// console.log(`${inspect(argv)}`);

// Create a new isolate limited to 8MB
const isolate = new ivm.Isolate({memoryLimit: 8});

const script = fs.readFileSync(process.argv[2], 'utf8');
const hostile = isolate.compileScriptSync(`
    let process = prcess.copySync();
    let console = { trace, debug, info, log, warn, error };
    let certificate = process.argv[2];
    ${script}
`);

// Create a new context within this isolate. Each context has its own copy of all the builtin
// Objects. So for instance if one context does Object.prototype.foo = 1 this would not affect any
// other contexts.
const context = isolate.createContextSync();

// Get a Reference{} to the global object within the context.
const jail = context.global;

// This makes the global object available in the context as `global`. We use `derefInto()` here
// because otherwise `global` would actually be a Reference{} object in the new isolate.
jail.setSync('global', jail.derefInto());

// We will create a basic `log` function for the new isolate to use.
jail.setSync('trace', function(...args) { console.trace(...args); });
jail.setSync('debug', function(...args) { console.debug(...args); });
jail.setSync('log', function(...args) { console.log(...args); });
jail.setSync('info', function(...args) { console.log(...args); });
jail.setSync('warn', function(...args) { console.warn(...args); });
jail.setSync('error', function(...args) { console.error(...args); });
jail.setSync('prcess', new ivm.Reference({argv, env}));

try {
    hostile.runSync(context);
    console.info(`${process.argv[2]} accepted request`);

    console.debug(`heapStatistics: ${JSON.stringify(isolate.getHeapStatisticsSync())}`);
    console.debug(`wallTime: ${isolate.wallTime}; cpuTime: ${isolate.cpuTime}`);
}
catch (err) {
    console.error(`${process.argv[2]} rejected request`);
    console.error(err);
}

//hostile.run(context)
//    .then((/* result */) => { console.log(`accepted`); })
//    .catch(err => { console.error(`rejected`); });
