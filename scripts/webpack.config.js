const path = require("node:path")

module.exports = {
  mode: "production",
  entry : {
    "csr-check": "./csr-check.js.example",
    "order-check": "./order-check.js.example",
    "pre-certificate-issuance": "./pre-certificate-issuance.js.example",
    "certificate-issuance": "./certificate-issuance.js.example"
  },
  output : {
    path: path.resolve("."),
    filename: "[name].js"
  }
}
